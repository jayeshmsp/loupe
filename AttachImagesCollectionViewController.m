//
//  AttachImagesCollectionViewController.m
//  OHBSYS Storyboards iOS8
//
//  Created by David Forsberg on 2015-09-27.
//  Copyright (c) 2015 David Forsberg. All rights reserved.
//

#import "AttachImagesCollectionViewController.h"
#import "AttachImagesCollectionViewCell.h"
#import "AppDelegate.h"
#import "signBezierPath.h"

@interface AttachImagesCollectionViewController ()

@end

@implementation AttachImagesCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    [self.collectionView registerClass:[AttachImagesCollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];

    // Do any additional setup after loading the view.
    NSLog(@"AttachImages laddades, fieldvalue: %@", _fieldValue);

    if(_editMode == [NSNumber numberWithInt:1]){
        
    }else{
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationItem.titleView.hidden = TRUE;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.imageToDrawOn = nil;
    appDelegate.imageToDrawOnFieldName = nil;
    appDelegate.imageToDrawOnFieldValue = nil;
    appDelegate.imageToDrawOnFieldSelectedIndex = nil;
    
    if (appDelegate.attachImagesDict != nil) {
        if ([appDelegate.attachImagesDict count] > 0) {
            if ([appDelegate.attachImagesDict valueForKey:_fieldName] != nil){
                _fieldValue = [appDelegate.attachImagesDict valueForKey:_fieldName];
            }
        }
    }
    
    [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation


//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//
//    //Fråga om användaren vill maila kraschrapporten som hittades
//    if([title isEqualToString:@"Rita"])
//    {
//        [self editImage:_lastIndex];
//    }
//    if([title isEqualToString:@"Radera"])
//    {
//        [self deleteImage:_lastIndex];
//    }
//    if([title isEqualToString:@"Ja, radera bilden"])
//    {
//        [self deleteImageForReal:_lastIndex];
//    }
//}

- (void)alertButtonClick:(NSString *)title
{
    
    //Fråga om användaren vill maila kraschrapporten som hittades
    if([title isEqualToString:@"Rita"])
    {
        [self editImage:_lastIndex];
    }
    if([title isEqualToString:@"Radera"])
    {
        [self deleteImage:_lastIndex];
    }
    if([title isEqualToString:@"Ja, radera bilden"])
    {
        [self deleteImageForReal:_lastIndex];
    }
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Tryckte på item %ld", indexPath.item);
    
    if (_editMode == [NSNumber numberWithInt:1]){
        _lastIndex = indexPath;
        
        //Change
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bild" message:(@"Vill du rita på eller ta bort bilden?") delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Rita",@"Radera",nil];
//        [alert show];
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Bild"
                                                                                            message:(@"Vill du rita på eller ta bort bilden?")
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
           //We add buttons to the alert controller by creating UIAlertActions:
           UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Rita"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                [self alertButtonClick: @"Rita"];
                                                            }]; //You can use a block here to handle a press on this button
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                }];

            [alertController addAction:actionCancel];
           [alertController addAction:actionOk];

           [self presentViewController:alertController animated:YES completion:nil];
        
    }
}

- (void)editImage:(NSIndexPath*)indexPath{
    //Hämta bild
    NSArray *imageAndTextList = [_fieldValue componentsSeparatedByString:@"###;###"];
    
    if([imageAndTextList count] > 0){
        // Get file name
        NSString *imageVal = [imageAndTextList objectAtIndex:indexPath.item];
        NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
        NSString *imageFilename = [imageObject objectAtIndex:0];
        //NSString *imageText = [imageObject objectAtIndex:1];
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",imageFilename]];
        
        // Create file manager
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        
        UIImage *image = [[UIImage alloc] init];
        
        //Finns filen?
        if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
            image = [UIImage imageWithContentsOfFile:jpgPath];
        }else{
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_images/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], imageFilename]]]];
        }
        
        //Lagra i delegate
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.imageToDrawOn = image;
        appDelegate.imageToDrawOnFieldName = _fieldName;
        appDelegate.imageToDrawOnFieldValue = _fieldValue;
        appDelegate.imageToDrawOnFieldSelectedIndex = (NSInteger*)indexPath.item;
        
        [self performSegueWithIdentifier:@"DrawOnPhoto" sender:[self.collectionView cellForItemAtIndexPath:indexPath]];
    }
}

- (void)deleteImage:(NSIndexPath*)indexPath{
    // Fråga "Är du säker?"
    //change
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Radera bild" message:(@"Vill du verkligen radera bilden?") delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Ja, radera bilden",nil];
//    [alert show];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Radera bild"
                                                                                        message:(@"Vill du verkligen radera bilden?")
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
           //We add buttons to the alert controller by creating UIAlertActions:
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ja, radera bilden"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                [self alertButtonClick: @"Ja, radera bilden"];
                                                            }]; //You can use a block here to handle a press on this button
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            
        [alertController addAction:actionCancel];
        [alertController addAction:actionOk];
            
        [self presentViewController:alertController animated:YES completion:nil];
    
}


-(void)deleteImageForReal:(NSIndexPath*)indexPath{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Hämta fältets värde
    NSLog(@"Fieldvalue: %@", _fieldValue);

    // Splitta till array (mutable) på "###;###"
    NSArray * imageListNonMutable = [_fieldValue componentsSeparatedByString:@"###;###"];
    NSMutableArray * imageList = [[NSMutableArray alloc] init];
    for(NSString*image in imageListNonMutable){
        [imageList addObject:image];
    }

    // Om arrayens längd är 0/1, sätt fältvärdet till ""
    if ([imageList count] < 2){
        [appDelegate.reportValuesDelegate setValue:@"" forKey:_fieldName];
        
        //Ta även bort bilderna från attachImagesDict, där de hamnar om de just blivit tillagda
        appDelegate.attachImagesDict = nil;
    }else{
            NSLog(@"attachImagesDict: %@", [appDelegate.attachImagesDict valueForKey:_fieldName]);
        //Ta bort bilderna från attachImagesDict, där de hamnar om de just blivit tillagda
        if(appDelegate.attachImagesDict != nil){            
            NSString* attachImagesToAdd = [appDelegate.attachImagesDict valueForKey:_fieldName];
            NSArray* attachImagesToAddArr = [attachImagesToAdd componentsSeparatedByString:@"###;###"];
            NSMutableArray* attachImagesToAddArr2 = [[NSMutableArray alloc] init];
            
            for(NSString* imgName in attachImagesToAddArr){
                //Change
//                if(![imgName isEqualToString:(@"%@###&###", [imageList objectAtIndex:indexPath.item]) ]){
                if(![imgName isEqualToString:((void)(@"%@###&###"), [imageList objectAtIndex:indexPath.item]) ]){
                    [attachImagesToAddArr2 addObject:imgName];
                }
            }
            NSString * newAttachImagesDictValue = [attachImagesToAddArr2 componentsJoinedByString:@"###;###"];
            [appDelegate.attachImagesDict setObject:newAttachImagesDictValue forKey:_fieldName];
            NSLog(@"attachImagesDict: %@", [appDelegate.attachImagesDict valueForKey:_fieldName]);
        }
        
        
        //  Ta sen bort bilden (namn och bildtext) på position indexPath.item från rapportens model
        [imageList removeObjectAtIndex:indexPath.item];
        
        NSString* newFieldValue = [NSString stringWithFormat:@"%@", [imageList componentsJoinedByString:@"###;###"]];
        [appDelegate.reportValuesDelegate setValue:newFieldValue forKey:_fieldName];
    }
    
    
    NSLog(@"Uppdaterat fältvärde efter borttagen bild: %@", [appDelegate.reportValuesDelegate valueForKey:_fieldName]);
    _fieldValue = [appDelegate.reportValuesDelegate valueForKey:_fieldName];

    // Ladda om vyn
//    [self.collectionView reloadData]; //funkar inte... poppa vyn
    
//    [self.navigationController dismissModalViewControllerAnimated:YES];
    [[self navigationController] popViewControllerAnimated:YES];
    
    
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender cellIndexPath:(NSIndexPath *)indexPath {

    NSLog(@"Segue: %@", segue.identifier);
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"DrawOnPhoto"]) {
        
        signBezierPath *destViewController = segue.destinationViewController;
        
        NSIndexPath *selectedIndexPath = [self.collectionView indexPathForCell:sender];
        
        NSArray *imageAndTextList = [_fieldValue componentsSeparatedByString:@"###;###"];
        
        if([imageAndTextList count] > 0){
            // Get file name
            NSString *imageVal = [imageAndTextList objectAtIndex:selectedIndexPath.item];
            NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
            NSString *imageFilename = [imageObject objectAtIndex:0];
            NSString *imageText = [imageObject objectAtIndex:1];
            NSLog(@"Image Text = %@", imageText);
            NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",imageFilename]];
            
            // Create file manager
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            
            UIImage *image = [[UIImage alloc] init];
            NSLog(@"5");
            
            //Finns filen?
            if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                image = [UIImage imageWithContentsOfFile:jpgPath];
            }else{
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_images/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], imageFilename]]]];
            }
            
            CGFloat imgWidth = image.size.width;
            CGFloat imgHeight = image.size.height;
            NSLog(@"Image width: %f; Image height: %f", imgWidth, imgHeight);
            
            destViewController.BackgroundImage = image;
        }
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout Protocol methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;
{
    
    NSArray *imageAndTextList = [_fieldValue componentsSeparatedByString:@"###;###"];
    
    if([imageAndTextList count] > 0){
        // Get file name
        NSString *imageVal = [imageAndTextList objectAtIndex:indexPath.item];
        NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
        NSString *imageFilename = [imageObject objectAtIndex:0];
        NSString *imageText = [imageObject objectAtIndex:1];
        NSLog(@"Image Text = %@", imageText);
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",imageFilename]];
        
        // Create file manager
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        
        UIImage *image = [[UIImage alloc] init];
        
        //Finns filen?
        if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
            image = [UIImage imageWithContentsOfFile:jpgPath];
        }else{
            image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_images/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], imageFilename]]]];
        }
        
        CGFloat imgHeight = image.size.height;
        CGFloat imgWidth = image.size.width;
        
        NSLog(@"img height, width = %f ,%f", imgHeight, imgWidth);
        
        return CGSizeMake(768, 768); //imgHeight/imgWidth*768);
        
    }
    
    return CGSizeMake(768, 300);

}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}











- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if( [_fieldValue length] < 1){
        return 0;
    }
    NSArray *imageAndTextList = [_fieldValue componentsSeparatedByString:@"###;###"];
  
    //koden nedan ger krasch, index over limit
//    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//    if ([[appDelegate.attachImagesDict valueForKey:_fieldName] length] > 0 && ![_fieldValue isEqualToString:[appDelegate.attachImagesDict valueForKey:_fieldName]]) {
//        imageAndTextList = [[appDelegate.attachImagesDict valueForKey:_fieldName] componentsSeparatedByString:@"###;###"];
//    }
    
    return [imageAndTextList count];
}

/*
//call this when connection start
UIActivityIndicator *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
activityIndicator.frame = CGRectMake(140, 236, 37, 37);
[activityIndicator startAnimating];
[self.view addSubview:activityIndicator];
self.view.userInteractionEnabled = NO;

//remove activity indicator while connection did finish loadin
-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [activityIndicator stopAnimating];
    [activityIndicator removeFromSuperview];
    self.view.userInteractionEnabled = YES;
}
*/

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AttachImagesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    // Configure the cell
    NSArray *imageAndTextList = [_fieldValue componentsSeparatedByString:@"###;###"];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([[appDelegate.attachImagesDict valueForKey:_fieldName] length] > 0 && ![_fieldValue isEqualToString:[appDelegate.attachImagesDict valueForKey:_fieldName]]) {
        imageAndTextList = [[appDelegate.attachImagesDict valueForKey:_fieldName] componentsSeparatedByString:@"###;###"];
    }
    
    if([imageAndTextList count] > 0){
        // Get file name
        NSString *imageVal = [imageAndTextList objectAtIndex:indexPath.item];
        NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
        NSString *imageFilename = [imageObject objectAtIndex:0];
        //NSString *imageText = [imageObject objectAtIndex:1];
        
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",imageFilename]];
        NSString  *jpgCachePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//Cache//cached_%@",imageFilename]];
        
        // Create file manager
        NSFileManager *fileMgr = [NSFileManager defaultManager];
        
        UIImage *image = [[UIImage alloc] init];
        
        //Finns filen?
        if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
            image = [UIImage imageWithContentsOfFile:jpgPath];
            
            NSLog(@"Bild finns");
        }else{
            //Finns filen i cache-mapp?
            if ([fileMgr fileExistsAtPath: jpgCachePath ] == YES){
                image = [UIImage imageWithContentsOfFile:jpgCachePath];
                
                NSLog(@"Bild finns i cache");
            }else{
                NSString *imgUrl = [NSString stringWithFormat:@"%@/_images/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], imageFilename];
                NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgUrl]];
               
                image = [UIImage imageWithData: imgData];
                
                [imgData writeToFile:jpgCachePath atomically:YES];
                
                NSLog(@"Bild laddades ned ");
            }
            
            
            // Sätt timeout för hämtningen:
            // http://stackoverflow.com/questions/10198012/how-to-set-timeout-for-datawithcontentsofurlurl
            
            // spara filen i en cache-mapp
            // hämta upp filen därifrån
            // töm cache-mappen på bilder äldre än 1v (titta på timestamp när filen skapades) när nya rapporter hämtas
        }
        
        
        CGFloat imgHeight = image.size.height;
        CGFloat imgWidth = image.size.width;
        
        NSLog(@"Image height, width = %f, %f", imgHeight, imgWidth);
        
        cell.imageView.frame = CGRectMake(0, 0, 768, 768); // imgHeight/imgWidth*768);
        cell.imageView.center = cell.imageView.superview.center;
        [cell.imageView setImage:image];
        
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/


// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
//Change
//- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
//	return YES;
//}
//
//- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
//	return YES;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
//    NSLog(@"Action performed");
//}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return YES;
}



- (void) addImage {
    [self chooseFromLibrary];
}
- (void) addImageByCamera {
    [self takePhoto];
}


-(void)takePhoto
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    
    // image picker needs a delegate,
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion:nil];
}



-(void)chooseFromLibrary
{
    
    UIImagePickerController *imagePickerController= [[UIImagePickerController alloc]init];
    [imagePickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    // image picker needs a delegate so we can respond to its messages
    [imagePickerController setDelegate:self];
    
    // Place image picker on the screen
    [self presentViewController:imagePickerController animated:YES completion:nil];
    
}


- (UIImage *) scaleAndRotateImage: (UIImage *) imageIn
//...thx: http://blog.logichigh.com/2008/06/05/uiimage-fix/
{
    //int kMaxResolution = 3264; // Or whatever
    int kMaxResolution = 1024; // Or whatever
    
    CGImageRef        imgRef    = imageIn.CGImage;
    CGFloat           width     = CGImageGetWidth(imgRef);
    CGFloat           height    = CGImageGetHeight(imgRef);
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect            bounds    = CGRectMake( 0, 0, width, height );
    
    if ( width > kMaxResolution || height > kMaxResolution )
    {
        CGFloat ratio = width/height;
        
        if (ratio > 1)
        {
            bounds.size.width  = kMaxResolution;
            bounds.size.height = bounds.size.width / ratio;
        }
        else
        {
            bounds.size.height = kMaxResolution;
            bounds.size.width  = bounds.size.height * ratio;
        }
    }
    
    CGFloat            scaleRatio   = bounds.size.width / width;
    CGSize             imageSize    = CGSizeMake( CGImageGetWidth(imgRef),         CGImageGetHeight(imgRef) );
    UIImageOrientation orient       = imageIn.imageOrientation;
    CGFloat            boundHeight;
    
    switch(orient)
    {
        case UIImageOrientationUp:                                        //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored:                                //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown:                                      //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored:                              //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored:                              //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft:                                      //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored:                             //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight:                                     //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise: NSInternalInconsistencyException
                        format: @"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext( bounds.size );
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if ( orient == UIImageOrientationRight || orient == UIImageOrientationLeft )
    {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else
    {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM( context, transform );
    
    CGContextDrawImage( UIGraphicsGetCurrentContext(), CGRectMake( 0, 0, width, height ), imgRef );
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return( imageCopy );
}


//delegate methode will be called after picking photo either from camera or library
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    if ([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
        // Image came from the camera, save it
        UIImageWriteToSavedPhotosAlbum(image, self, nil, nil);
    }
    
    UIImage *smallerImage = [self scaleAndRotateImage: image];
    image = smallerImage;
    
    //Save image to documents
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
	CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
	CFRelease(uuidRef);
	//NSLog(@"UUID: %@",uuidStringRef);
	NSString * fileName = [NSString stringWithFormat:@"%@.jpg",(__bridge NSString *)(uuidStringRef)];
	
	NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
    
    //[self saveImage:image withFileName:fileName ofType:@"jpg" inDirectory:@"Documents"];
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:jpgPath atomically:YES];
    
				// Check if image was saved
				NSError *error;
				NSFileManager *fileMgr = [NSFileManager defaultManager];
				
				// Point to Document directory
				NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
				
				// Write out the contents of home directory to console
				NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
				
    
    
    //Add image to field value string
    NSLog(@"%@", _fieldValue);
    NSLog(@"->");
    NSLog(@"%@###;###%@###&###", _fieldValue, fileName);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (appDelegate.attachImagesDict == nil) {
        appDelegate.attachImagesDict = [NSMutableDictionary new];
    }
    if ([appDelegate.attachImagesDict valueForKey:_fieldName] == nil) {
        [appDelegate.attachImagesDict setValue: _fieldValue forKey:_fieldName];
    }
    if ([_fieldValue length] > 0 ) {
        [appDelegate.attachImagesDict setValue:[NSString stringWithFormat:@"%@###;###%@###&###", [appDelegate.attachImagesDict valueForKey:_fieldName], fileName] forKey:_fieldName];
    }else{
        [appDelegate.attachImagesDict setValue:[NSString stringWithFormat:@"%@###&###", fileName] forKey:_fieldName];
    }
    _fieldValue = [appDelegate.attachImagesDict valueForKey:_fieldName];
    
    [self.collectionView reloadData];
    

    
}

//Save Image
-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    if ([[extension lowercaseString] isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}

//Load Image
-(UIImage *) loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, extension]];
    
    return result;
}



//  ==============================================================
//  resizedImage
//  ==============================================================
// Return a scaled down copy of the image.

UIImage* resizedImage(UIImage *inImage, CGRect thumbRect)
{
    CGImageRef          imageRef = [inImage CGImage];
    CGImageAlphaInfo    alphaInfo = CGImageGetAlphaInfo(imageRef);
    
    // There's a wierdness with kCGImageAlphaNone and CGBitmapContextCreate
    // see Supported Pixel Formats in the Quartz 2D Programming Guide
    // Creating a Bitmap Graphics Context section
    // only RGB 8 bit images with alpha of kCGImageAlphaNoneSkipFirst, kCGImageAlphaNoneSkipLast, kCGImageAlphaPremultipliedFirst,
    // and kCGImageAlphaPremultipliedLast, with a few other oddball image kinds are supported
    // The images on input here are likely to be png or jpeg files
    if (alphaInfo == kCGImageAlphaNone)
        alphaInfo = kCGImageAlphaNoneSkipLast;
    
    // Build a bitmap context that's the size of the thumbRect
    CGContextRef bitmap = CGBitmapContextCreate(
                                                NULL,
                                                thumbRect.size.width,       // width
                                                thumbRect.size.height,      // height
                                                CGImageGetBitsPerComponent(imageRef),   // really needs to always be 8
                                                4 * thumbRect.size.width,   // rowbytes
                                                CGImageGetColorSpace(imageRef),
                                                alphaInfo
                                                );
    
    // Draw into the context, this scales the image
    CGContextDrawImage(bitmap, thumbRect, imageRef);
    
    // Get an image from the context and a UIImage
    CGImageRef  ref = CGBitmapContextCreateImage(bitmap);
    UIImage*    result = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);   // ok if NULL
    CGImageRelease(ref);
    
    return result;
}

@end

//
//  LoginViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-26.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "AppDelegate.h"

@interface LoginViewController : UIViewController{
    sqlite3 *dbConnection;
    
}


@property (strong, nonatomic) IBOutlet UITextField *server;
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UIButton *loginbtn;

@property (strong, nonatomic) IBOutlet NSString *loggedInUser;

@property (strong, nonatomic) AppDelegate *appDelegate;

@property (weak, nonatomic) IBOutlet UILabel *feedbackLabel;

@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

- (IBAction) loginUser;
- (IBAction) emptyDB;

- (IBAction)serverFieldChanged:(UITextField *)sender;

 
@end

# docs: https://docs.fastlane.tools/actions/sigh/
export PATH="$HOME/.fastlane/bin:$PATH"

echo "cert repo: https://davidanton@bitbucket.org/davidanton1d/ohbsys-ipad-certificates.git"

match adhoc -u david.a.forsberg@gmail.com -a se.loupe.app.ipad

cert

sigh —-adhoc -u david.a.forsberg@gmail.com -a se.loupe.app.ipad --force
sigh resign
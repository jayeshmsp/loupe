//
//  AutotextAddTextViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-21.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "AutotextSortCell.h"
#import "AutotextAddTextSelectorViewController.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"

@interface AutotextAddTextViewController : UITableViewController{
    bool isFiltered;
}

@property (nonatomic, strong) NSString *editMode;
//addZone, addRoom, addInfo, addNote,
//sortZone, sortRoom, sortInfo, sortNote
//simpleAutotext

@property (retain, nonatomic) NSString * autotextRoomName;

@property (retain, nonatomic) NSMutableArray * categoriesArr;
@property (retain, nonatomic) NSMutableArray * autotextObject;
@property (strong, nonatomic) IBOutlet UITableView *categoryTable;


@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) NSMutableArray *allFilteredTexts;
@property (nonatomic, retain) NSMutableArray *allTexts;

@property (retain, nonatomic) NSString * simpleAutotextColname;
@property (retain, nonatomic) NSString * simpleAutotextText;

@property (retain, nonatomic) NSString * tagForRiskInputField;
@property (retain, nonatomic) NSString * currentRiskText;

//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

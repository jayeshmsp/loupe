//
//  AutotextAddTextViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-21.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "AutotextAddTextViewController.h"

@interface AutotextAddTextViewController ()

@end

@implementation AutotextAddTextViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
		
	//Anslut till appDelegate
	_appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];


    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
   //NSLog(@"addTextVC viewDidLoad");
    
    //Hämta kategorier från db
    
    
    //Ladda array från databasen

	NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray * tempArray2 = [[NSMutableArray alloc] initWithCapacity:0];
        sqlite3_stmt *statement;
    	
		//Lägg till alternativet fritext
		NSMutableDictionary * tempDict2 = [[NSMutableDictionary alloc]init];
		[tempDict2 setObject:@"FritextCell" forKey:@"celltype"];
		[tempArray addObject:tempDict2];

        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad - viewDidLoad - sys_formtables, för att fylla reportFieldsStr");
            NSString *beginSQL = [[NSString alloc] init];
            beginSQL = @"SELECT * FROM autotext_categories ORDER BY categorytitle ASC";
            
           //NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            while(sqlite3_step(statement) == SQLITE_ROW) {
                
                NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                
                int i;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[tempDictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    //NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [tempDictionary count]);
                }
                
                [tempArray addObject:tempDictionary];
            }
            /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);

            
            _categoriesArr = tempArray;
            
			begin_stmt = nil;
			beginSQL = nil;
			sqlite3_finalize(statement);

            
            //Gör lista över alla texter för sökfunktionen
            
            /*DOLD*///NSLog(@"Databasen öppnad - viewDidLoad - sys_formtables, för att fylla reportFieldsStr");
            NSString *beginSQL2 = [[NSString alloc] init];
            beginSQL2 = @"SELECT * FROM autotext_texts ORDER BY title ASC";
            
            /*DOLD*///NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt2 = [beginSQL2 UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt2, -1, &statement, NULL);
            while(sqlite3_step(statement) == SQLITE_ROW) {
                
                NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                
                int i;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[tempDictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    //     /*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [tempDictionary count]);
                }
                
                [tempArray2 addObject:tempDictionary];
                
                /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);
            }
            _allTexts = tempArray2;
            
			begin_stmt = nil;
			beginSQL = nil;
            sqlite3_finalize(statement);
            
        } else {
           //NSLog(@"Failed to open/create database");
        }
       //NSLog(@"_categoriesArr längd: %d", [_categoriesArr count]);
       //NSLog(@"_allTexts längd: %d", [_allTexts count]);
        
        

    
        _searchBar.delegate = (id)self;
        isFiltered = false;
    
    
    }


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
   //NSLog(@"Searchbar textdidchange");
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
       //NSLog(@"Sökning börjar");
       //NSLog(@"Söktext: %@", text);
        isFiltered = TRUE;
       //NSLog(@"isfiltered = true");
        
        _allFilteredTexts = [[NSMutableArray alloc] init];
        
        NSDictionary * y = [[NSDictionary alloc] init];
        for (y in _allTexts)
        {
           //NSLog(@"Loopar igenom alltexts: kollar denna text: %@", [y valueForKey:@"title"]);
            NSString * thisTitle = [y valueForKey:@"title"];
            NSString * thisText = [y valueForKey:@"text"];
            
            NSRange adrRange = [thisTitle rangeOfString:text options:NSCaseInsensitiveSearch];
            NSRange stadRange = [thisText rangeOfString:text options:NSCaseInsensitiveSearch];
            if(adrRange.location != NSNotFound || stadRange.location != NSNotFound)
            {
               //NSLog(@"Match hittades: %@", [y valueForKey:@"title"]);
                [_allFilteredTexts addObject:y];
            }
        }
       //NSLog(@"Sökning slutar. Antal sökresultat: %d", [_allFilteredTexts count]);
    }
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}
*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if(isFiltered == TRUE){
        if([_allFilteredTexts count]>0){
            return [_allFilteredTexts count];
        }else{
            return 1;
        }
    }else{
        return [_categoriesArr count];
    }
}

- (AutotextSortCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [[NSString alloc] init];
    AutotextSortCell *cell = [[AutotextSortCell alloc] init];
    
    if(isFiltered == TRUE){
        if([_allFilteredTexts count]>0){
            CellIdentifier = @"TextCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
           //NSLog(@"cellForRowAtIndexPath rad %d, värde %@", indexPath.row, [_allFilteredTexts objectAtIndex:indexPath.row]);
            
            cell.atLabel.text = [[_allFilteredTexts objectAtIndex:indexPath.row] valueForKey:@"title"];
            cell.atText.text = [[_allFilteredTexts objectAtIndex:indexPath.row] valueForKey:@"text"];
        }else{
            CellIdentifier = @"Cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            cell.atLabel.text = @"Inga texter hittades";
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            cell.accessoryView = nil;
        }
    }else{
        if([[[_categoriesArr objectAtIndex:indexPath.row]valueForKey:@"celltype"] isEqualToString:@"FritextCell"]){
            CellIdentifier = @"FritextCell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            cell.atLabel.text = @"Fritext";
            
        }else{
            CellIdentifier = @"Cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
            // Configure the cell...
            
            //NSLog(@"cellForRowAtIndexPath rad %d, värde %@", indexPath.row, [_categoriesArr objectAtIndex:indexPath.row]);
            
            cell.atLabel.text = [[_categoriesArr objectAtIndex:indexPath.row] valueForKey:@"categorytitle"];
        }
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ATselectedSearchResultInCategorySelector"]) {
        NSIndexPath *indexPath = [self.categoryTable indexPathForSelectedRow];
        
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.addTextId = [[_allFilteredTexts objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.addTextDictionary = [_allFilteredTexts objectAtIndex:indexPath.row];
        destViewController.autotextRoomName = _autotextRoomName;
        destViewController.editMode = _editMode;
        
        destViewController.simpleAutotextColname = _simpleAutotextColname;
        
        destViewController.autotextObject = _autotextObject;

		destViewController.tagForRiskInputField = _tagForRiskInputField;
		destViewController.currentRiskText = _currentRiskText;
		/**/NSLog(@"ATobj1-1: %@",_currentRiskText);
       //NSLog(@"roomName1: %@",_autotextRoomName);
    }else if ([segue.identifier isEqualToString:@"ATselectedFritextInCategorySelector"]) {
        
        AutotextSortViewController *destViewController = segue.destinationViewController;
        srandom((unsigned int)time(NULL));
        
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc] init];
        [tempDict setObject:[NSString stringWithFormat:@"fritext%ld",random()] forKey:@"id"];
        [tempDict setObject:@"0" forKey:@"images"];
        [tempDict setObject:@"0" forKey:@"risk"];
        [tempDict setObject:@" " forKey:@"longrisk"];
        [tempDict setObject:@"Fritext" forKey:@"title"];
        [tempDict setObject:@" " forKey:@"text"];
        
        
        destViewController.addTextId = [NSString stringWithFormat:@"fritext%ld",random()];
        destViewController.addTextDictionary = tempDict; //[_allFilteredTexts objectAtIndex:indexPath.row];
        destViewController.autotextRoomName = _autotextRoomName;
        destViewController.editMode = _editMode;
        
        destViewController.simpleAutotextColname = _simpleAutotextColname;
        destViewController.simpleAutotextText = _simpleAutotextText;

        destViewController.autotextObject = _autotextObject;

		destViewController.tagForRiskInputField = _tagForRiskInputField;

        /**/NSLog(@"ATobj1-2: %@",_currentRiskText);
       //NSLog(@"roomName1: %@",_autotextRoomName);
    }else{
        
        NSIndexPath *indexPath = [self.categoryTable indexPathForSelectedRow];
        
        AutotextAddTextSelectorViewController *destViewController = segue.destinationViewController;
        destViewController.category = [[_categoriesArr objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.autotextObject = _autotextObject;
        destViewController.autotextRoomName = _autotextRoomName;
        destViewController.editMode = _editMode;
		
        destViewController.simpleAutotextColname = _simpleAutotextColname;
        destViewController.simpleAutotextText = _simpleAutotextText;

		destViewController.tagForRiskInputField = _tagForRiskInputField;
		destViewController.currentRiskText = _currentRiskText;
		
        /**/NSLog(@"ATobj1-3: %@",_currentRiskText);
       //NSLog(@"roomName1: %@",_autotextRoomName);
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

//Change
//- (void)viewDidUnload {
//    [self setCategoryTable:nil];
//    [super viewDidUnload];
//}
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

-(void) didReceiveMemoryWarning{
    [self setCategoryTable:nil];
    [super didReceiveMemoryWarning];
}
-(void) dealloc{
    [self setCategoryTable:nil];
}

-(BOOL)shouldAutorotate{
    return NO;
}

@end

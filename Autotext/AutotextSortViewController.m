//
//  AutotextSortViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-20.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "AutotextSortViewController.h"
#import "AutotextSortCell.h"
#import <QuartzCore/QuartzCore.h>

@interface AutotextSortViewController ()

@end

@implementation AutotextSortViewController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSLog(@"##### Viewdidload för atSortVC");
    NSLog(@"_simpleAutotextColname: %@",_simpleAutotextColname);
	
	/*
	 //Detta bör hända när man sparar, inte när vyn laddas
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
	if(_autotextObject){
		if(![_autotextObject isKindOfClass:[NSArray class]]){
			NSMutableArray * tempAtArr = [[NSMutableArray alloc] init];
			[tempAtArr addObject:_autotextObject];
			appDelegate.jsonOrigHolder = tempAtArr;
			appDelegate.jsonHolder = tempAtArr;
		}else{
			appDelegate.jsonOrigHolder = _autotextObject;
			appDelegate.jsonHolder = _autotextObject;
		}
		
		NSLog(@"jsonOrigHolder: %@", appDelegate.jsonOrigHolder);
	}
	 //*/
	
	NSLog(@"53: _autotextObject: %@", _autotextObject);
	
    
    
    if([_editMode isEqualToString:@"addInfo"] || [_editMode isEqualToString:@"addNote"] || [_editMode isEqualToString:@"editInfo"] || [_editMode isEqualToString:@"editNote"]){
        
		if([_editMode isEqualToString:@"editInfo"] || [_editMode isEqualToString:@"editNote"]){
        //Lägg till en spara-knapp
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
												  initWithTitle:@"Klar"
												  style:UIBarButtonItemStylePlain
                                                  //initWithBarButtonSystemItem: UIBarButtonSystemItemSave
                                                  target:self
                                                  action:@selector(insertInfo)];
		}else{
			//Lägg till en spara-knapp
			self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
													  initWithTitle:@"Lägg till"
													  style:UIBarButtonItemStylePlain
													  //initWithBarButtonSystemItem: UIBarButtonSystemItemSave
													  target:self
													  action:@selector(insertInfo)];
		}

        NSLog(@"addTextDictionary: %@", _addTextDictionary);
        
        NSMutableArray * tempArr = [[NSMutableArray alloc] init];
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc] init];
        
        //Rubrikrad
        [tempDict setValue:@"ValdRubrik" forKey:@"cellidentifier"];
        [tempDict setValue:[_addTextDictionary valueForKey:@"title"] forKey:@"atlabel"];
        
        NSLog(@"tempDict1: %@",tempDict);
        [tempArr addObject:tempDict];
        tempDict = nil;
        tempDict = [[NSMutableDictionary alloc] init];
        
        //Text
        [tempDict setValue:@"ValdText" forKey:@"cellidentifier"];
        [tempDict setValue:[NSNumber numberWithFloat:150] forKey:@"cellheight"];
        [tempDict setValue:@"Text" forKey:@"atlabel"];
        
        
        NSLog(@"roomName3: %@",_autotextRoomName);
        
		if(_autotextRoomName){
			[tempDict setValue:[[_addTextDictionary valueForKey:@"text"] stringByReplacingOccurrencesOfString:@"#RUM#" withString:_autotextRoomName] forKey:@"attext"];
		}else{
			[tempDict setValue:[_addTextDictionary valueForKey:@"text"] forKey:@"attext"];
		}
        //att ersätta #RUM# med rumsnamn gick inget vidare. Problemet ligger i reportDetailsViewController, variabeln finns inte i dictionaryn som letas i i prepareforsegue. leta fel där infoObj och roomName sätts.
				//OBS!! detta är nu löst :)
        //[tempDict setValue:[[_addTextDictionary valueForKey:@"text"] stringByReplacingOccurrencesOfString:@"#RUM#" withString:_autotextRoomName] forKey:@"attext"];
        
        [tempDict setValue:[NSNumber numberWithInt:1] forKey:@"attexttag"];
        NSLog(@"tempDict2: %@",tempDict);
        [tempArr addObject:tempDict];
        tempDict = nil;
        tempDict = [[NSMutableDictionary alloc] init];
		
		if([_editMode isEqualToString:@"addInfo"] || [_editMode isEqualToString:@"addNote"]){
			//Risk
			[tempDict setValue:@"ValdRisk" forKey:@"cellidentifier"];
			[tempDict setValue:[NSNumber numberWithFloat:150] forKey:@"cellheight"];
			
			[tempDict setValue:@"Risktext" forKey:@"atlabel"];
			[tempDict setValue:[_addTextDictionary valueForKey:@"longrisk"] forKey:@"attext"];
			[tempDict setValue:[NSNumber numberWithInt:2] forKey:@"attexttag"];
			NSLog(@"tempDict3: %@",tempDict);
			[tempArr addObject:tempDict];
			tempDict = nil;
			tempDict = [[NSMutableDictionary alloc] init];
			
			//Risk-checkbox
			[tempDict setValue:@"ValdRiskCheck" forKey:@"cellidentifier"];
			[tempDict setValue:@"Vill du lägga till risktexten i Riskanalys?" forKey:@"atlabel"];
			[tempDict setValue:[NSNumber numberWithInt:3] forKey:@"atchecktag"];
			NSLog(@"tempDict4: %@",tempDict);
			[tempArr addObject:tempDict];
			tempDict = nil;
			tempDict = [[NSMutableDictionary alloc] init];
			
			//Bild-checkbox
			[tempDict setValue:@"ValdBildCheck" forKey:@"cellidentifier"];
			[tempDict setValue:@"Vill du lägga till \"Se bilagan bilder\" på slutet av texten?" forKey:@"atlabel"];
			[tempDict setValue:[NSNumber numberWithInt:4] forKey:@"atchecktag"];
			NSLog(@"tempDict5: %@",tempDict);
			[tempArr addObject:tempDict];
			tempDict = nil;
			//tempDict = [[NSMutableDictionary alloc] init];
			
		}
		
        NSLog(@"temparr count 121: %ld", [tempArr count]);
        _atObjArr = tempArr;
    }else if([_editMode isEqualToString:@"addRoom"] || [_editMode isEqualToString:@"addZone"]){
        NSLog(@"Editmode är addroom eller addzone");
        
        //Lägg till en spara-knapp
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                  initWithTitle:@"Lägg till"
												  style:UIBarButtonItemStylePlain
                                                  //initWithBarButtonSystemItem: UIBarButtonSystemItemSave
                                                  target:self
                                                  action:@selector(insertRoomOrZone)];
        
        NSLog(@"_autotextObject: %@", _autotextObject);
        
        NSMutableArray * tempArr = [[NSMutableArray alloc] init];
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc] init];
        
        //Rubrikrad
        [tempDict setValue:@"ValdRubrik" forKey:@"cellidentifier"];
        [tempDict setValue:[NSString stringWithFormat:@"Lägg till rum i %@",[_autotextObject valueForKey:@"title"]] forKey:@"atlabel"];
        
        NSLog(@"tempDict1: %@",tempDict);
        [tempArr addObject:tempDict];
        tempDict = nil;
        tempDict = [[NSMutableDictionary alloc] init];
        
        //Text
        [tempDict setValue:@"ValdTextEnRad" forKey:@"cellidentifier"];
        [tempDict setValue:@"Namn" forKey:@"atlabel"];
        
        
        NSLog(@"roomName3: %@",_autotextRoomName);
        
        [tempDict setValue:_autotextRoomName forKey:@"attextonerow"];
        
        [tempDict setValue:[NSNumber numberWithInt:1] forKey:@"attexttag"];
        NSLog(@"tempDict2: %@",tempDict);
        [tempArr addObject:tempDict];
        tempDict = nil;
        tempDict = [[NSMutableDictionary alloc] init];
        
        NSLog(@"tempArr count 161: %ld", [tempArr count]);
        _atObjArr = tempArr;
    }else if([_editMode isEqualToString:@"simpleAutotext"]){
		
		NSLog(@"Editmode: simpleAutotext");
		
		NSLog(@"simpleAutotextColname: %@", _simpleAutotextColname);
        
		
        //Lägg till en spara-knapp
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
												  initWithTitle:@"Lägg till"
												  style:UIBarButtonItemStylePlain
                                                  //initWithBarButtonSystemItem: UIBarButtonSystemItemSave
												  target:self
                                                  action:@selector(insertInfo)];
        
        NSLog(@"addTextDictionary: %@", _addTextDictionary);
        
        NSMutableArray * tempArr = [[NSMutableArray alloc] init];
        NSMutableDictionary * tempDict = [[NSMutableDictionary alloc] init];
        
        //Rubrikrad
        [tempDict setValue:@"ValdRubrik" forKey:@"cellidentifier"];
        [tempDict setValue:[_addTextDictionary valueForKey:@"title"] forKey:@"atlabel"];
        
        NSLog(@"tempDict1: %@",tempDict);
        [tempArr addObject:tempDict];
        tempDict = nil;
        tempDict = [[NSMutableDictionary alloc] init];
        
        //Text
        [tempDict setValue:@"ValdText" forKey:@"cellidentifier"];
        [tempDict setValue:[NSNumber numberWithFloat:150] forKey:@"cellheight"];
        [tempDict setValue:@"Text" forKey:@"atlabel"];
        
        
        NSLog(@"roomName3: %@",_autotextRoomName);
        
        [tempDict setValue:[_addTextDictionary valueForKey:@"text"] forKey:@"attext"];
        [tempDict setValue:[NSNumber numberWithInt:1] forKey:@"attexttag"];
        NSLog(@"tempDict2: %@",tempDict);
        [tempArr addObject:tempDict];
        
        NSLog(@"temparr count 210: %ld", [tempArr count]);
        _atObjArr = tempArr;
		
		NSLog(@"_atObjArr %@",_atObjArr);

	}else{
        // Uncomment the following line to preserve selection between presentations.
        // self.clearsSelectionOnViewWillAppear = NO;
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
        
        NSLog(@"autotextObject: %@", _autotextObject);
        
        NSMutableArray * tempArr = [[NSMutableArray alloc] init];
        
        for(NSMutableDictionary *item in _autotextObject) {
            NSLog(@"Viewdidload lägger till barn till %@ i _atObjArr", _autotextParentType);
            [tempArr addObject: item];
			
            //        [_atObjArr addObject: item];
            NSLog(@"temparr count 177: %ld", [tempArr count]);
        }
    _atObjArr = tempArr;
    }
    NSLog(@"##### Slut på Viewdidload för atSortVC");
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"ViewWillAppear för atSortVC (tom funktion)");

}


-(void)insertRoomOrZone{
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSLog(@"NumberOfRows: %ld", [_atObjArr count]);
    return [_atObjArr count];
    
    //return 0;
    
}

- (AutotextSortCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"CellForRowAtIndexPath");
    
    NSString *CellIdentifier = [[NSString alloc] init];
    AutotextSortCell *cell = [[AutotextSortCell alloc] init];

    if([_autotextParentType isEqualToString:@"Room"]){
        CellIdentifier= @"TextCell";
    }else{
        CellIdentifier= @"LabelCell";
    }

    //Om användaren ska lägga till eller redigera en text (dvs inte sortera)
    if([_editMode isEqualToString:@"addInfo"] || [_editMode isEqualToString:@"addNote"] || [_editMode isEqualToString:@"editInfo"] || [_editMode isEqualToString:@"editNote"]){
     
		
		CellIdentifier = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"cellidentifier"];
        cell = [_atTable dequeueReusableCellWithIdentifier:CellIdentifier];
        if ([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"])
          {
            cell.atLabel.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"];
            cell.atLabel.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabeltag"] integerValue];
          }
        else
          {
            NSLog(@"atlabel = nil 224");
          }
        
        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"]){
            cell.atText.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"];
            cell.atText.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attexttag"] integerValue];
			
			//The rounded corner part, where you specify your view's corner radius:
            cell.atText.layer.cornerRadius = 5;
            cell.atText.clipsToBounds = YES;
            [cell.atText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
            [cell.atText.layer setBorderWidth:2.0];
        }else{
			
            NSLog(@"attext = nil 230");
        }//SOKORD
		if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atchecktag"]){
            cell.atRiskSwitch.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atchecktag"] integerValue];
			cell.atImageSwitch.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atchecktag"] integerValue];
			
			NSLog(@"chktag: %ld, %ld",cell.atRiskSwitch.tag,cell.atImageSwitch.tag);
			
			//The rounded corner part, where you specify your view's corner radius:
            cell.atText.layer.cornerRadius = 5;
            cell.atText.clipsToBounds = YES;
            [cell.atText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
            [cell.atText.layer setBorderWidth:2.0];
        }else{
			
            NSLog(@"attext = nil 230");
        }
        
        //Om användaren ska lägga till rum/zon
    }else if([_editMode isEqualToString:@"addRoom"] || [_editMode isEqualToString:@"addZone"]){
        CellIdentifier = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"cellidentifier"];
        cell = [_atTable dequeueReusableCellWithIdentifier:CellIdentifier];

		
		NSLog(@"[_atObjArr objectAtIndex:indexPath.row]: %@",[_atObjArr objectAtIndex:indexPath.row]);
		
        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"]){
            cell.atLabel.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"];
            cell.atLabel.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabeltag"] integerValue];
        }else{
            NSLog(@"atlabel = nil 246");
        }

        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"]){
            cell.atText.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"];
            cell.atText.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attexttag"] integerValue];
			
			//The rounded corner part, where you specify your view's corner radius:
            cell.atText.layer.cornerRadius = 5;
            cell.atText.clipsToBounds = YES;
            [cell.atText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
            [cell.atText.layer setBorderWidth:2.0];
        }else{
            NSLog(@"attext = nil 253");
        }

        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attextonerow"]){
            cell.atTextOneRow.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attextonerow"];
	        cell.atTextOneRow.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attexttag"] integerValue];
	    }else{
            NSLog(@"attextonerow = nil 260");
        }

		//Om användaren ska lägga till en enkel autotext
    }else if([_editMode isEqualToString:@"simpleAutotext"]){

        
		CellIdentifier = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"cellidentifier"];
        cell = [_atTable dequeueReusableCellWithIdentifier:CellIdentifier];
        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"]){
            cell.atLabel.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabel"];
            cell.atLabel.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"atlabeltag"] integerValue];
        }else{
            NSLog(@"atlabel = nil 224");
        }
        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"]){
            cell.atText.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attext"];
            cell.atText.tag = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"attexttag"] integerValue];
			
			
            //The rounded corner part, where you specify your view's corner radius:
            cell.atText.layer.cornerRadius = 5;
            cell.atText.clipsToBounds = YES;
            [cell.atText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
            [cell.atText.layer setBorderWidth:2.0];
        }else{
            NSLog(@"attext = nil 230");
        }
	
        //Om användaren vill sortera infotexter/noteringar
	} else if([_editMode isEqualToString:@"sortInfos"] || [_editMode isEqualToString:@"sortNotes"]){
        CellIdentifier = @"sortinfocell";
        cell = [_atTable dequeueReusableCellWithIdentifier:CellIdentifier];
		
        if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"text"]){
            cell.atText.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"text"];
		}
    }else {
    
		
		cell = [_atTable dequeueReusableCellWithIdentifier:CellIdentifier];
        //    NSLog(@"Objekt som cellen baseras på: %@",_atObjArr);
        if([_autotextParentType isEqualToString:@"Room"]){
            cell.atLabel.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"rubrik"];
            cell.atText.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"text"];
			
			//The rounded corner part, where you specify your view's corner radius:
            cell.atText.layer.cornerRadius = 5;
            cell.atText.clipsToBounds = YES;
            [cell.atText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
            [cell.atText.layer setBorderWidth:2.0];
        }else{
            cell.atLabel.text = [[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"title"];
        }
        NSLog(@"cellForRowAtIndexPath object: %@ \neditMode = %@",[_atObjArr objectAtIndex:indexPath.row],_editMode);
    }
    
    /*
     TODO: Bugg #52
     Hitta rätt if-sats nedan och stoppa in följande rad:
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
     */

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight;
    if([[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"cellheight"]){
        rowHeight = [[[_atObjArr objectAtIndex:indexPath.row] valueForKey:@"cellheight"] floatValue];
    }else{
        rowHeight = 50;
    }
	if ([_editMode isEqualToString:@"sortInfos"] ||[_editMode isEqualToString:@"sortNotes"]) {
		rowHeight = 100;
	}
    return rowHeight;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // remove the original from the data structure
        [_atObjArr removeObjectAtIndex:indexPath.row];

        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

        NSLog(@"result of delete:\n%@", _atObjArr);

    }
}


// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    NSLog(@"move from:%ld to:%ld", fromIndexPath.row, toIndexPath.row);
    // fetch the object at the row being moved
    NSString *r = [_atObjArr objectAtIndex:fromIndexPath.row];
    
    // remove the original from the data structure
    [_atObjArr removeObjectAtIndex:fromIndexPath.row];
    
    // insert the object at the target row
    [_atObjArr insertObject:r atIndex:toIndexPath.row];
    NSLog(@"result of move:\n%@", _atObjArr);
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    if(editing)
    {
        NSLog(@"editMode on");
    }
    else
    {
        NSLog(@"Done leave editmode");
        
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		
		if(_autotextObject){
			if(![_autotextObject isKindOfClass:[NSArray class]]){
				NSMutableArray * tempAtArr = [[NSMutableArray alloc] init];
				[tempAtArr addObject:_autotextObject];
				appDelegate.jsonOrigHolder = tempAtArr;
				appDelegate.jsonHolder = tempAtArr;
			}else{
				appDelegate.jsonOrigHolder = _autotextObject;
				appDelegate.jsonHolder = _autotextObject;
			}
			
			NSLog(@"jsonOrigHolder: %@", appDelegate.jsonOrigHolder);
		}
		
		appDelegate.jsonSortZone = @"false"; //[[NSMutableArray alloc] initWithObjects: _atObjArr, nil];
				
        if([_editMode isEqualToString:@"sortInfos"] || [_editMode isEqualToString:@"sortNotes"]){
			/*
			//Gör en kopia av room-objektet
			NSMutableDictionary * tempDict = [_autotextParentObject mutableCopy];
			NSLog(@"tempDict, [_autotextParentObject mutableCopy], = %@",tempDict);
			
			//Uppdatera sortorder
			NSLog(@"före sortorder: %@",_atObjArr);
			for (int i = 0; i<[_atObjArr count]; i++) {
				[[_atObjArr objectAtIndex:i] setValue:[NSString stringWithFormat:@"%d",i] forKey:@"sortorder"];
			}
			NSLog(@"efter sortorder: %@",_atObjArr);
			
			[tempDict setValue:_atObjArr forKey:@"infos"];
			appDelegate.jsonOrigHolder = [[NSMutableArray alloc] initWithObjects: _autotextParentObject, nil];
			appDelegate.jsonHolder = [[NSMutableArray alloc] initWithObjects: tempDict, nil];
			
			//NSLog(@"ORIGINAL: %@", appDelegate.jsonOrigHolder);
			//NSLog(@"UPPDATERAD: %@", appDelegate.jsonHolder);
			*/
			
			
			
			
			
			//Skicka tillbaka uppdaterat autotext-objekt till formuläret
			
			NSString * arrayToEdit = [[NSString alloc] init];
			if([_editMode isEqualToString:@"sortInfos"]){
				arrayToEdit = @"infos";
			}
			else if([_editMode isEqualToString:@"sortNotes"]){
				arrayToEdit = @"notes";
			}
			
			AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
			/*
			 Används ej
			 NSMutableArray * origParentATobjArr = [[NSMutableArray alloc] init];
			 NSMutableArray * parentATobjArr = [[NSMutableArray alloc] init];
			*/
			NSMutableDictionary * origParentAtDict = [[NSMutableDictionary alloc] init];
			NSMutableDictionary * parentAtDict = [[NSMutableDictionary alloc] init];
			
			//1. Hämta autotextobjektet som innehåller info om hela rummet/föräldraobjektet
			
				parentAtDict = [_autotextParentObject mutableCopy];
				origParentAtDict = [_autotextParentObject mutableCopy];
			
			
			//Se till att objekten har infos() och notes(), så att rätt objekt hittas i search&replace sen
			if(![origParentAtDict valueForKey:@"infos"]){
				[origParentAtDict setValue:[[NSMutableArray alloc] init] forKey:@"infos"];
			}
			if(![origParentAtDict valueForKey:@"notes"]){
				[origParentAtDict setValue:[[NSMutableArray alloc] init] forKey:@"notes"];
			}
			if(![parentAtDict valueForKey:@"infos"]){
				[parentAtDict setValue:[[NSMutableArray alloc] init] forKey:@"infos"];
			}
			if(![parentAtDict valueForKey:@"notes"]){
				[parentAtDict setValue:[[NSMutableArray alloc] init] forKey:@"notes"];
			}
			
			
			
			//2. Ersätt arrayen som editerats med det nya sorterade objektet / Uppdatera föräldraobjektet
			[parentAtDict setValue:_atObjArr forKey:arrayToEdit];
			NSLog(@"parentAtDict: %@",parentAtDict);
			
			//6. Återskapa originalobjektet som en array
			NSLog(@"6.");
			NSMutableArray * origWrapperArr = [[NSMutableArray alloc] init];
			[origWrapperArr addObject:_autotextParentObject];
			
			//7. Stoppa det nya objektet i en array
			NSLog(@"7.");
			NSMutableArray * wrapperArr = [[NSMutableArray alloc] init];
			[wrapperArr addObject:parentAtDict];
			
			//8.
			NSLog(@"8.");
			//Ersätt detta...
			appDelegate.jsonOrigHolder = origWrapperArr;
			//...med detta
			appDelegate.jsonHolder = wrapperArr;
			
			NSLog(@"Sortering sparad. Ersätter %@ med %@ ",_autotextParentObject,wrapperArr);
			
			
			
			
			
			
			
			
			
			
			
			
		}else if([_editMode isEqualToString:@"sortRooms"]){
			//Gör en kopia av zone-objektet
			NSMutableDictionary * tempDict = [_autotextParentObject mutableCopy];
			
			//Uppdatera sortorder
			NSLog(@"före sortorder: %@",_atObjArr);
			for (int i = 0; i<[_atObjArr count]; i++) {
				[[_atObjArr objectAtIndex:i] setValue:[NSString stringWithFormat:@"%d",i] forKey:@"sortorder"];
			}
			NSLog(@"efter sortorder: %@",_atObjArr);
			
			//Uppdatera room-arrayen till den sorterade
			[tempDict setValue:_atObjArr forKey:@"room"];
			
			//Sätt in i jsonholder's SOM ARRAYER (OBS detta medför att det kan bugga när det finns flera objekt, kanske behöver lägga till objectAtIndex:0 för ersättnings-skriptet
			appDelegate.jsonOrigHolder = [[NSMutableArray alloc] initWithObjects: _autotextParentObject, nil];
			appDelegate.jsonHolder = [[NSMutableArray alloc] initWithObjects: tempDict, nil];
			
			//NSLog(@"ORIGINAL: %@", appDelegate.jsonOrigHolder);
			//NSLog(@"UPPDATERAD: %@", appDelegate.jsonHolder);
			
		}else if([_editMode isEqualToString:@"sortZones"]){
			//Gör en kopia av at-objektet
			NSMutableArray * tempArr = [_autotextObject mutableCopy];
			
			NSLog(@"tempdict: %@",tempArr);
			NSLog(@"_atObjArr: %@",_atObjArr);
						
			//Sätt in i jsonholder's SOM ARRAYER (OBS detta medför att det kan bugga när det finns flera objekt, kanske behöver lägga till objectAtIndex:0 för ersättnings-skriptet
			appDelegate.jsonOrigHolder = tempArr;//[[NSMutableArray alloc] initWithObjects: tempArr, nil];
			
			//Uppdatera sortorder
			NSLog(@"före sortorder: %@",_atObjArr);
			for (int i = 0; i<[_atObjArr count]; i++) {
				[[_atObjArr objectAtIndex:i] setValue:[NSString stringWithFormat:@"%d",i] forKey:@"sortorder"];
			}
			NSLog(@"efter sortorder: %@",_atObjArr);
			
			appDelegate.jsonHolder = _atObjArr;//[[NSMutableArray alloc] initWithObjects: _atObjArr, nil];
			
			appDelegate.jsonSortZone = @"true"; //[[NSMutableArray alloc] initWithObjects: _atObjArr, nil];
			
			
			//NSLog(@"ORIGINAL: %@", appDelegate.jsonOrigHolder);
			//NSLog(@"UPPDATERAD: %@", appDelegate.jsonHolder);
			
		}else{
			appDelegate.jsonHolder = _atObjArr;
		}

        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) insertInfo
{
	/*
	NSMutableArray *tempArr = [[NSMutableArray alloc] init];
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
	 */
    
    if([_editMode isEqualToString:@"editInfo"] || [_editMode isEqualToString:@"editNote"]){
        //Ersätt objekt med det som editeras
		
		//Dessa variabler skickas via segue:
		//_addTextDictionary = id, title, text för den text som ska redigeras
		//_autotextObject = rumsobjektet, roomObj (dictionary)
		
		
		NSString * arrayToEdit = [[NSString alloc] init];
		if([_editMode isEqualToString:@"editInfo"]){
			arrayToEdit = @"infos";
		}
		else if([_editMode isEqualToString:@"editNote"]){
			arrayToEdit = @"notes";
		}
		
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		
		NSMutableDictionary * origParentAtDict = [[NSMutableDictionary alloc] init];
		NSMutableDictionary * parentAtDict = [[NSMutableDictionary alloc] init];
		NSMutableDictionary * origTextDict = [_addTextDictionary mutableCopy];
		NSMutableDictionary * updatedTextDict = [_addTextDictionary mutableCopy];
		
		NSMutableArray * newTextArr = [[NSMutableArray alloc] init];
		
		
		origParentAtDict = [[_addTextDictionary valueForKey:@"roomObj"] mutableCopy];
		parentAtDict = [[_addTextDictionary valueForKey:@"roomObj"] mutableCopy];
		
		//NSLog(@"parentAtDict: arraytoedit: %@", [parentAtDict valueForKey:arrayToEdit]);
		

		for (NSMutableDictionary * thisInfoDict in [parentAtDict valueForKey:arrayToEdit]) {
			
			/*NSLog(@"Jämför text i loopen: original: %@ \n\n\n redigerad: %@\n\n%@ vs %@ \n%@ vs %@ \n\n%@ \n\nvs \n\n%@ \n\n\n",
				  thisInfoDict,
				  origTextDict,
				  [thisInfoDict valueForKey:@"id"],
				  [origTextDict valueForKey:@"id"],
				  [thisInfoDict valueForKey:@"title"],
				  [origTextDict valueForKey:@"title"],
				  [thisInfoDict valueForKey:@"text"],
				  [origTextDict valueForKey:@"text"]
				  );
			//*/
			
			if(
			   [[thisInfoDict valueForKey:@"id"] isEqualToString:[origTextDict valueForKey:@"id"]] &&
			   [[thisInfoDict valueForKey:@"text"] isEqualToString:[origTextDict valueForKey:@"text"]]){
				//Hittat texten som ska bytas ut i rumsobjektet, roomObj
    
				//Hämta värdena från input-fälten: //ersätt " med tomt
				NSString *text = [((UITextField*)[self.view viewWithTag:1]).text stringByReplacingOccurrencesOfString:@"\"" withString:@""];
				[updatedTextDict setValue:text forKey:@"text"];
				
				[newTextArr addObject:updatedTextDict];
			}else{
				[newTextArr addObject:thisInfoDict];
			}
		}

		[parentAtDict setValue:newTextArr forKey:arrayToEdit];
	
		//NSLog(@"_addTextDictionary: %@", _addTextDictionary);
		//NSLog(@"_autotextObject: %@", _autotextObject);
		//NSLog(@"_autotextParentObject: %@", _autotextParentObject);
		
		
		
		//6. Återskapa originalobjektet som en array
		NSLog(@"6.");
		NSMutableArray * origWrapperArr = [[NSMutableArray alloc] init];
		[origWrapperArr addObject:_autotextObject];
		
		//7. Stoppa det nya objektet i en array
		NSLog(@"7.");
		NSMutableArray * wrapperArr = [[NSMutableArray alloc] init];
		[wrapperArr addObject:parentAtDict];
		
		//8.
		NSLog(@"8.");
		//Ersätt detta...
		appDelegate.jsonOrigHolder = origWrapperArr;
		//...med detta
		appDelegate.jsonHolder = wrapperArr;
		
		
		
		//Klart!
		
		NSLog(@"FÖRE: %@",origWrapperArr);
		NSLog(@"EFTER: %@",wrapperArr);
		
		//*/
		
		
		
		
		
		
		
		

		
		
		
		
		
		
		
		
		
		
		
		
    }else if([_editMode isEqualToString:@"addInfo"] || [_editMode isEqualToString:@"addNote"]){
        //Lägg till det som editeras
		
		NSString * arrayToEdit = [[NSString alloc] init];
		if([_editMode isEqualToString:@"addInfo"]){
			arrayToEdit = @"infos";
		}
		else if([_editMode isEqualToString:@"addNote"]){
			arrayToEdit = @"notes";
		}
		
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		NSMutableArray * parentATobjArr = [[NSMutableArray alloc] init];
		NSMutableArray * currentATobjArr = [[NSMutableArray alloc] init];
		NSMutableDictionary * parentAtDict = [[NSMutableDictionary alloc] init];
		NSMutableDictionary * newATdict = [[NSMutableDictionary alloc] init];
		
		//1. Hämta autotextobjektet som innehåller info om hela rummet/föräldraobjektet
		if([_autotextObject isKindOfClass:[NSArray class]]){
			//Is array
			NSLog(@"array!");
			parentATobjArr = [_autotextObject mutableCopy];
			parentAtDict = [[parentATobjArr objectAtIndex:0] mutableCopy];
		}else if([_autotextObject isKindOfClass:[NSDictionary class]]){
			NSLog(@"dictionary!");
			parentAtDict = [_autotextObject mutableCopy];
		}
		
		//2. Plocka ut den array som ska läggas till i
		currentATobjArr = [[parentAtDict valueForKey:arrayToEdit] mutableCopy];
		
		//3. Sätt värden för det nya objektet
        [newATdict setValue:[NSString stringWithFormat:@"%@tn%ld-%d",[_autotextObject valueForKey:@"id"],([currentATobjArr count]+1), ((rand() % 10000)+1000)] forKey:@"id"];
		[newATdict setValue:[_addTextDictionary valueForKey:@"title"] forKey:@"title"];
		
		//Hämta värdena från input-fälten: //ersätt " med tomt 
		NSString *text = [((UITextField*)[self.view viewWithTag:1]).text stringByReplacingOccurrencesOfString:@"\"" withString:@""];
		NSString *risk = [((UITextField*)[self.view viewWithTag:2]).text stringByReplacingOccurrencesOfString:@"\"" withString:@""];
		
		[newATdict setValue:text forKey:@"text"];
		
		
		//Är riskcheck på?
		NSString *riskcheck = [[NSString alloc] init];
		if(((UISwitch*)[self.view viewWithTag:3]).on){
			riskcheck = @"1";
		}else{
			riskcheck = @"0";
		}
		
		[newATdict setValue:riskcheck forKey:@"risk"];
		
		
		//Är imagecheck på?
		NSString *image = [[NSString alloc] init];
		if(((UISwitch*)[self.view viewWithTag:4]).on){
			image = @"1";
		}else{
			image = @"0";
		}
		
		[newATdict setValue:image forKey:@"images"];
		
/*
		[
		 [
		  [
		   text stringByReplacingOccurrencesOfString:@"\nSe risk." withString:@""
		   ]
		  stringByReplacingOccurrencesOfString:@"\nSe bild." withString:@""
		  ]
		 stringByReplacingOccurrencesOfString:@"\nSe risk och bild." withString:@""
		 ]
		]
//*/
		
		if([riskcheck isEqualToString:@"1"] && [image isEqualToString:@"0"]){
			[newATdict setValue:[NSString stringWithFormat:@"%@\nSe Riskanalys.",text] forKey:@"text"];
		}else if([riskcheck isEqualToString:@"0"] && [image isEqualToString:@"1"]){
			[newATdict setValue:[NSString stringWithFormat:@"%@\nSe bilagan bilder.",text] forKey:@"text"];
		}else if([riskcheck isEqualToString:@"1"] && [image isEqualToString:@"1"]){
			[newATdict setValue:[NSString stringWithFormat:@"%@\nSe Riskanalys och bilagan bilder.",text] forKey:@"text"];
		}else{
			[newATdict setValue:text forKey:@"text"];
		}
		
		NSLog(@"newATdict: %@",newATdict);
		
		//4. Lägg till det nya objektet i arrayen av t.ex. infotexter
		NSLog(@"4.");
		[currentATobjArr addObject:newATdict];
		
		//5. Uppdatera föräldraobjektet
		NSLog(@"5.");
		[parentAtDict setValue:currentATobjArr forKey:arrayToEdit];
		
		//6. Återskapa originalobjektet som en array
		NSLog(@"6.");
		NSMutableArray * origWrapperArr = [[NSMutableArray alloc] init];
		[origWrapperArr addObject:_autotextObject];
		
		//7. Stoppa det nya objektet i en array
		NSLog(@"7.");
		NSMutableArray * wrapperArr = [[NSMutableArray alloc] init];
		[wrapperArr addObject:parentAtDict];
		
		//8.
		NSLog(@"8.");
		//Ersätt detta...
		appDelegate.jsonOrigHolder = origWrapperArr;
		//...med detta
		appDelegate.jsonHolder = wrapperArr;
		
		
		NSString * curRiskText = [NSString stringWithFormat:@"%@",[[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:@"risk"]];
		
		//Sätt risk i appDelegate om risk = on
		if([riskcheck isEqualToString:@"1"]){
			NSLog(@"RISK = 1, currentRiskText: %@, risk: %@",curRiskText, risk);
			if(curRiskText){
				//appDelegate.riskText = [NSString stringWithFormat:@"%@\n\n%@",appDelegate.riskText,risk];
				[[appDelegate.reportValuesDelegate objectAtIndex:0] setValue:[NSString stringWithFormat:@"%@\n\n%@",curRiskText,risk] forKey:@"risk"];
			}else{
				[[appDelegate.reportValuesDelegate objectAtIndex:0] setValue:[NSString stringWithFormat:@"%@",risk] forKey:@"risk"];
			}
			NSLog(@"appDelegate.riskText efter uppdatering: %@",curRiskText);
		}

		
		//Klart!
		
		NSLog(@"FÖRE: %@",origWrapperArr);
		NSLog(@"EFTER: %@",wrapperArr);
		
    } else if([_editMode isEqualToString:@"simpleAutotext"]){

		//Lägg till texten som ska läggas till i appdelegate forkey: simpleAutotextColname
		
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

		//Hämta värdena från input-fälten:
		NSString *text = ((UITextField*)[self.view viewWithTag:1]).text;
		NSString *newText = [[NSString alloc] init];
		
		NSLog(@"_simpleAutotextText: %@",_simpleAutotextText);
		NSLog(@"text %@",text);
		
		if (_simpleAutotextText && ![_simpleAutotextText isEqualToString:@""]){
			newText = [NSString stringWithFormat:@"%@\n\n%@",_simpleAutotextText,text];
		}else{
			newText = text;
		}
		NSLog(@"newText: %@",newText);
		
		//Sätt appdelegate
		
		NSMutableDictionary* dict = [[NSMutableDictionary alloc] initWithDictionary:appDelegate.simpleAutotextDict];
		
		NSLog(@"Sätter appdelegate.simpleAutotextDict till \n\n%@\n\nFör key: \n\n%@\n",text, _simpleAutotextColname);

		[dict setValue:newText forKey:_simpleAutotextColname];

		appDelegate.simpleAutotextDict = [[NSMutableDictionary alloc] initWithDictionary:dict copyItems:YES];
		dict = nil;

		
		NSLog(@"Sätter appdelegate.simpleAutotextDict Resultat:\n\n%@\n\n", appDelegate.simpleAutotextDict);
		
		//Klart!
		
    } else {
		/*
		//Hämta värdena från input-fälten:
		NSString *text = ((UITextField*)[self.view viewWithTag:1]).text;
		NSString *risk = ((UITextField*)[self.view viewWithTag:2]).text;
		
		//Är riskcheck på?
		NSString *riskcheck = [[NSString alloc] init];
		if(((UISwitch*)[self.view viewWithTag:3]).on){
			riskcheck = @"1";
		}else{
			riskcheck = @"0";
		}
		
		//Är imagecheck på?
		NSString *image = [[NSString alloc] init];
		if(((UISwitch*)[self.view viewWithTag:4]).on){
			image = @"1";
		}else{
			image = @"0";
		}
		
		//Fyll den temporära dictionaryn som utgör den nya autotexten:
		[tempDict setValue:[_addTextDictionary valueForKey:@"id"] forKey:@"id"];
		[tempDict setValue: image forKey:@"images"];
		[tempDict setValue:risk forKey:@"longrisk"];
		[tempDict setValue:riskcheck forKey:@"risk"];
		[tempDict setValue:[_addTextDictionary valueForKey:@"title"] forKey:@"rubrik"];
		[tempDict setValue:text forKey:@"text"];
		
		NSLog(@"Sparar... ");
		
		//Lägg till texten
		NSLog(@"före: %@", tempArr);
		[tempArr addObject:tempDict];
		NSLog(@"efter: %@", tempArr);
		
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		appDelegate.jsonHolder = tempArr;
		*/
    }
	
	//Stäng
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:3] animated:YES];
}


- (void) switchChanged:(id)sender {
    UISwitch *thisSwitch = (UISwitch *)sender;
    UIView *contentView = [thisSwitch superview];
    UITableViewCell *cell = (UITableViewCell *)[contentView superview];
	UITableView * tableView = (UITableView *)[cell superview];
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    // do something
	NSLog(@"###############################");
	NSLog(@"###############################");
	NSLog(@"###############################");
    NSLog(@"Rad nr: %ld",(long)indexPath.row);
	NSLog(@"###############################");
	NSLog(@"###############################");
	NSLog(@"###############################");
}


//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

//- (BOOL)shouldAutorotate {
//
//    return YES;
//}
//
//-(UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}


@end

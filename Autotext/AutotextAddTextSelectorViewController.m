//
//  AutotextAddTextSelectorViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-21.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "AutotextAddTextSelectorViewController.h"

@interface AutotextAddTextSelectorViewController ()

@end

@implementation AutotextAddTextSelectorViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
	//Anslut till appDelegate
	_appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	
   /*VIKTIG*/NSLog(@"addTextSelectorVC viewDidLoad");
    
    //Hämta kategorier från db

	NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
		
	/*	//Lägg till alternativet fritext
		NSMutableDictionary * tempDict2 = [[NSMutableDictionary alloc]init];
		[tempDict2 setObject:@"FritextCell" forKey:@"celltype"];
		[tempArray addObject:tempDict2];
*/
        sqlite3_stmt *statement;
		if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad - viewDidLoad - sys_formtables, för att fylla reportFieldsStr");

            NSString *beginSQL = [[NSString alloc] init];
            if([_editMode isEqualToString:@"addRoom"]){
                beginSQL = [NSString stringWithFormat: @"SELECT * FROM autotext_rooms ORDER BY roomtitle ASC"];
            }else if([_editMode isEqualToString:@"addZone"]){
                beginSQL = [NSString stringWithFormat: @"SELECT * FROM autotext_zones ORDER BY zonetitle ASC"];
            }else{
                if([_editMode isEqualToString:@"addInfo"]){
                    beginSQL = [NSString stringWithFormat: @"SELECT * FROM autotext_texts WHERE category = %@ AND isinfotext = 1  ORDER BY title ASC",_category];
                }else{
                    beginSQL = [NSString stringWithFormat: @"SELECT * FROM autotext_texts WHERE category = %@ ORDER BY title ASC", _category];
                }
            }

           //NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            while(sqlite3_step(statement) == SQLITE_ROW) {
                
                NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
            
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                
                int i;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[tempDictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                   //NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [tempDictionary count]);
                }
                
/*
                //lägg till rubriken som @"rubrik" istället för @"title"
                [tempDictionary setObject:[tempDictionary valueForKey:@"title"] forKey:@"rubrik"];
 */
                
                [tempArray addObject:tempDictionary];
               //NSLog(@"Antal rader: %d",[tempArray count]);
            }
            
            sqlite3_finalize(statement);
			beginSQL = nil;
			begin_stmt = nil;
            
        } else {
           //NSLog(@"Failed to open/create database");
        }
        _textsArr = tempArray;
       //NSLog(@"_textsArr längd: %d", [_textsArr count]);
        
        
        
        
        
        
        //Fyll datasource
        
        
    }
 


#pragma mark - Table view data source
/*
 - (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
 {
 #warning Potentially incomplete method implementation.
 // Return the number of sections.
 return 0;
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_textsArr count];
}

- (AutotextSortCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [[NSString alloc] init];
    AutotextSortCell *cell = [[AutotextSortCell alloc] init];
    
    if([_editMode isEqualToString:@"addRoom"] || [_editMode isEqualToString:@"addZone"]){
//		if([[[_textsArr objectAtIndex:indexPath.row] valueForKey:@"celltype"] isEqualToString:@"FritextCell"]){}
		   
            CellIdentifier = [NSString stringWithFormat:@"%@LabelCell",_editMode];
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }else{
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    //NSLog(@"cellForRowAtIndexPath rad %d, värde %@", indexPath.row, [_textsArr objectAtIndex:indexPath.row]);
    if([_editMode isEqualToString:@"addRoom"]){
        cell.atLabel.text = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"roomtitle"];

    }else if([_editMode isEqualToString:@"addZone"]){
        cell.atLabel.text = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"zonetitle"];
    
    }else{
        cell.atLabel.text = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"title"];
        cell.atText.text = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"text"];
    }
    return cell;
}

#pragma mark didSelectRowAtIndexPath
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

	if( [_editMode isEqualToString:@"addZone"]){
		
		//MATA TILLBAKA SOM EN ARRAY
		
		//Sätt arrayen till jsonHolder
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		NSMutableArray * currentATobjArr = [[NSMutableArray alloc] init];
		NSMutableDictionary * newATdict = [[NSMutableDictionary alloc] init];
		
		//currentATobjArr = [appDelegate.jsonHolder mutableCopy];
		
		[newATdict setValue:[NSString stringWithFormat:@"z%ld",([_autotextObject count]+1)] forKey:@"id"];
		[newATdict setValue:[[NSArray alloc]init] forKey:@"room"];
		//[newATdict setValue:@"" forKey:@"room"];
		NSLog(@"currentATobjArr: %ld",[_autotextObject count]);
		[newATdict setValue:[NSString stringWithFormat:@"%ld",[_autotextObject count]] forKey:@"sortorder"];
		[newATdict setValue:[[_textsArr objectAtIndex:indexPath.row] valueForKey:@"zonetitle"] forKey:@"title"];
		
		//NSLog(@"dict: %@ \n%d",newATdict,indexPath.row);
		NSLog(@"autotextobj: %@",_autotextObject);
		
		currentATobjArr = [_autotextObject mutableCopy];
		[currentATobjArr addObject:newATdict];
		
		//Ersätt detta...
		appDelegate.jsonOrigHolder = _autotextObject;
		//...med detta
		appDelegate.jsonHolder = currentATobjArr;
		
		NSLog(@"före: _autotextObject %@",_autotextObject);
		NSLog(@"efter: currentATobjArr %@",currentATobjArr);
		
		[self.navigationController popViewControllerAnimated:YES];
	}
	
	if( [_editMode isEqualToString:@"addRoom"]){
		
		//MATA TILLBAKA SOM EN ARRAY
		
		//Sätt arrayen till jsonHolder
		AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
		NSMutableArray * parentATobjArr = [[NSMutableArray alloc] init];
		NSMutableArray * currentATobjArr = [[NSMutableArray alloc] init];
		NSMutableDictionary * parentAtDict = [[NSMutableDictionary alloc] init];
		NSMutableDictionary * newATdict = [[NSMutableDictionary alloc] init];
		
		if([_autotextObject isKindOfClass:[NSArray class]]){
			//Is array
			NSLog(@"array!");
			parentATobjArr = [_autotextObject mutableCopy];
			parentAtDict = [[parentATobjArr objectAtIndex:0] mutableCopy];
		}else if([_autotextObject isKindOfClass:[NSDictionary class]]){
			NSLog(@"DICT!!");
			parentAtDict = [_autotextObject mutableCopy];
		}

		NSMutableArray * origWrapperArr = [[NSMutableArray alloc] init];
		[origWrapperArr addObject:[parentAtDict copy]];
			
		currentATobjArr = [[parentAtDict valueForKey:@"room"] mutableCopy];
		
		
		[newATdict setValue:[NSString stringWithFormat:@"%@r%ld",[_autotextObject valueForKey:@"id"],([currentATobjArr count]+1)] forKey:@"id"];
		[newATdict setValue:[NSString stringWithFormat:@"%ld",[_autotextObject count]] forKey:@"sortorder"];
		[newATdict setValue:[[NSArray alloc]init] forKey:@"infos"];
		[newATdict setValue:[[NSArray alloc]init] forKey:@"notes"];
		[newATdict setValue:[[_textsArr objectAtIndex:indexPath.row] valueForKey:@"roomtitle"] forKey:@"title"];
		
		[currentATobjArr addObject:newATdict];
		[parentAtDict setValue:currentATobjArr forKey:@"room"];
		

		NSMutableArray * wrapperArr = [[NSMutableArray alloc] init];
		[wrapperArr addObject:parentAtDict];
		
		//Ersätt detta...
		appDelegate.jsonOrigHolder = origWrapperArr;
		//...med detta
		appDelegate.jsonHolder = wrapperArr;
		
		NSLog(@"origWrapperArr %@",origWrapperArr);
		NSLog(@"wrapperArr %@",wrapperArr);
		
		[self.navigationController popViewControllerAnimated:YES];
	}
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ATselectedRoom"]) {
        NSIndexPath *indexPath = [self.categoryTable indexPathForSelectedRow];
        
        AutotextSortViewController *destViewController = segue.destinationViewController;

        destViewController.autotextObject = _autotextObject;
        destViewController.autotextRoomName = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"roomtitle"];
        destViewController.editMode = _editMode;
        
        destViewController.simpleAutotextColname = _simpleAutotextColname;
        destViewController.simpleAutotextText = _simpleAutotextText;
		NSLog(@"_simpleAutotextColname: %@",_simpleAutotextColname);
		
		destViewController.tagForRiskInputField = _tagForRiskInputField;
		destViewController.currentRiskText = _currentRiskText;
	
		NSLog(@"ATobj2-1: %@",_currentRiskText);
        //NSLog(@"roomName-objekt: %@",[_textsArr objectAtIndex:indexPath.row]);
        
        //NSLog(@"roomName1: %@",_autotextRoomName);
    }else{
        NSIndexPath *indexPath = [self.categoryTable indexPathForSelectedRow];
        
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.addTextId = [[_textsArr objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.addTextDictionary = [_textsArr objectAtIndex:indexPath.row];
        destViewController.autotextRoomName = _autotextRoomName;
        destViewController.editMode = _editMode;
        
        destViewController.simpleAutotextColname = _simpleAutotextColname;
		destViewController.simpleAutotextText = _simpleAutotextText;
		NSLog(@"_simpleAutotextColname: %@",_simpleAutotextColname);

		destViewController.tagForRiskInputField = _tagForRiskInputField;
		destViewController.currentRiskText = _currentRiskText;
		
        destViewController.autotextObject = _autotextObject;
        NSLog(@"ATobj2-2: %@",_currentRiskText);
        //NSLog(@"roomName1: %@",_autotextRoomName);
    }
}

//Change
//- (void)viewDidUnload {
//    [self setCategoryTable:nil];
//    [super viewDidUnload];
//}
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

-(void) didReceiveMemoryWarning{
    [self setCategoryTable:nil];
    [super didReceiveMemoryWarning];
}

-(void) dealloc{
    [self setCategoryTable:nil];
}

- (BOOL)shouldAutorotate {

    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end

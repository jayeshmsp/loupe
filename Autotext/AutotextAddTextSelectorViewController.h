//
//  AutotextAddTextSelectorViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-21.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "AutotextSortCell.h"
#import "AutotextSortViewController.h"
#import "AutotextAddTextSelectorViewController.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"

@interface AutotextAddTextSelectorViewController : UITableViewController{
    
}
@property (retain, nonatomic) NSString * autotextRoomName;
@property (nonatomic, strong) NSString *editMode;   //addZone, addRoom, addInfo, addNote, sort

@property (retain, nonatomic) NSMutableArray * textsArr;

@property (retain, nonatomic) NSString * category;
@property (retain, nonatomic) NSMutableArray * autotextObject;
@property (strong, nonatomic) IBOutlet UITableView *categoryTable;

@property (retain, nonatomic) NSString * simpleAutotextColname;
@property (retain, nonatomic) NSString * simpleAutotextText;

@property (retain, nonatomic) NSString * tagForRiskInputField;
@property (retain, nonatomic) NSString * currentRiskText;


//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

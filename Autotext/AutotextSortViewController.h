//
//  AutotextSortViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-20.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NSString+HTML.h"
#import "AppDelegate.h"

@interface AutotextSortViewController : UITableViewController

@property (nonatomic, retain) IBOutlet UITableView *atTable;


@property (nonatomic, strong) NSNumber *workOrder;
@property (nonatomic, strong) NSNumber *templateid;
@property (nonatomic, strong) NSMutableArray *templateInfo;
@property (nonatomic, strong) NSString *editMode;   //addZone, addRoom, addInfo, addNote, sort
@property (nonatomic, strong) NSString *autotextParentID;   //z1r2
@property (nonatomic, strong) NSString *autotextParentType; //Main, Zone, Room, Info or Note

@property (nonatomic, strong) NSString *addTextId;
@property (nonatomic, strong) NSMutableDictionary *addTextDictionary;
@property (retain, nonatomic) NSString * autotextRoomName;

@property (nonatomic, strong) NSMutableDictionary *addRoomDictionary;


@property (nonatomic, strong) NSMutableArray * autotextObject;
@property (nonatomic, strong) NSMutableArray * autotextParentObject;
@property (nonatomic, strong) NSMutableArray *atObjArr;

@property (retain, nonatomic) NSString * simpleAutotextColname;
@property (retain, nonatomic) NSString * simpleAutotextText;

@property (retain, nonatomic) NSString * tagForRiskInputField;
@property (retain, nonatomic) NSString * currentRiskText;

- (IBAction) insertInfo;
//- (IBAction) insertRoomOrZone;
- (void) insertRoomOrZone;

//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

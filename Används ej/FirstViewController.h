//
//  FirstViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface FirstViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    NSString *databasePath;
    sqlite3 *contactDB;
    
    UITextView *myTextView;
    UITableView *myTableView;
    
    NSMutableArray *myTableViewDataSource;
    
    int i;
    int myCounter;
    int tablesArrCountInt;

    bool fetchDataIsRunning;
    
    NSString *myFetchedData;
    NSMutableString *resultat;
    NSArray *tablesAndChecks;
    NSArray *tablesArr;
    NSArray *checksArr;
    NSMutableString *tablesToRequest;
    NSMutableData *receivedData;
    NSMutableString * receivedDataString;
    
    NSMutableString * SQL;
    NSArray * sqlStatementsArr;
    NSMutableArray * failedSqlStatementsArr;
    NSMutableArray * failedSqlStatementsCodeArr;
    
//    NSMutableString * tableRowCount;
    NSMutableArray * dbloop1;    
}

@property (retain, nonatomic) IBOutlet UITextView *myTextView;
@property (nonatomic, retain) IBOutlet UITableView *myTableView;
@property (nonatomic, retain) NSMutableArray *myTableViewDataSource;

- (IBAction) runFetchData;
- (IBAction) fetchData;

@property (nonatomic, retain) NSString *myFetchedData;
@property (nonatomic, retain) NSMutableString *resultat;
@property (nonatomic, retain) NSArray *tablesAndChecks;
@property (nonatomic, retain) NSArray *tablesArr;
@property (nonatomic, retain) NSArray *checksArr;
@property (nonatomic, retain) NSMutableString *tablesToRequest;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSMutableString * receivedDataString;

@property (nonatomic, retain) NSMutableString * SQL;
@property (nonatomic, retain) NSArray * sqlStatementsArr;
@property (nonatomic, retain) NSMutableArray * failedSqlStatementsArr;
@property (nonatomic, retain) NSMutableArray * failedSqlStatementsCodeArr;

@property (nonatomic) NSString * tableRowCount;
@property (nonatomic, retain) NSMutableArray * dbloop1;



@end

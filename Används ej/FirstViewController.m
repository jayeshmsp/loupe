//
//  FirstViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

//http://stackoverflow.com/questions/2228302/storing-iphone-application-settings-in-app
//http://stackoverflow.com/questions/123503/how-can-i-take-a-photo-with-my-iphone-app


#import "FirstViewController.h"
#import "AppDelegate.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
#import "FirstViewController.h"
#import "AppDelegate.h"

@synthesize myTextView, myTableViewDataSource, myFetchedData, resultat, tablesAndChecks, tablesArr, checksArr, tablesToRequest,receivedData, receivedDataString, SQL, sqlStatementsArr, failedSqlStatementsArr, failedSqlStatementsCodeArr, dbloop1;

@synthesize myTableView = myTableView_;

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    /*DOLD*///NSLog(@"Konfigurerar tableView");
    if(self.myTableView==nil){
        /*DOLD*///NSLog(@"myTableView = nil");
    }else{
        /*DOLD*///NSLog(@"myTableView != nil");
    }
    
    if(self.myTableView.dataSource==nil){
        /*DOLD*///NSLog(@"datasource = nil");
    }else{
        /*DOLD*///NSLog(@"datasource != nil");
    }
    /*DOLD*///NSLog(@"%d",[self.myTableViewDataSource count]);
    
    return [self.myTableViewDataSource count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    /*DOLD*///NSLog(@"calling cellForRowAtIndexPath");
    
    static NSString *CellIdentifier = @"myCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    
    
    // Set up the cell...
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    cell.textLabel.text = [NSString  stringWithFormat:@"Tabell %ld: %@", (long)[indexPath row], [self.myTableViewDataSource objectAtIndex:[indexPath row]]];
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	// open a alert with an OK and cancel button
    NSString *alertString = [NSString stringWithFormat:@"Clicked on row #%ld", (long)[indexPath row]];
	//Change
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//	[alert show];
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                        message:@""
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Done"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
	// open a alert with an OK and cancel button
	NSString *alertString = [NSString stringWithFormat:@"Ojdå! %@", [self.myTableViewDataSource objectAtIndex:[indexPath row]]];
    //Change
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//	[alert show];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                    message:@""
                                                                             preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Done"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alertController dismissViewControllerAnimated:YES completion:nil];
                                                            }];
        
    [alertController addAction:actionCancel];
    [self presentViewController:alertController animated:YES completion:nil];
    
}






#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    /*DOLD*///NSLog(@"Connection-metoderna startar");
    
    receivedData = [[NSMutableData alloc] init];
    receivedDataString = [[NSMutableString alloc] init];
    
    if ([response expectedContentLength] < 0)
    {
        /*DOLD*///NSLog(@"Connection error pga nåt med att längd < 0");
        //here cancel your connection.
        //[connection cancel];
        //return;
    }
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    //[receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    /*DOLD*///NSLog(@"Connectionen tog emot data");
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:(data)];
    /*DOLD*///NSLog(@"connection tog emot data!");
    
    NSString * thisReceivedData = [[NSString alloc] init];
    thisReceivedData = [NSString stringWithUTF8String:[data bytes]];
    if([thisReceivedData length]>0){
        [receivedDataString appendString:(thisReceivedData)];
        /*DOLD*///NSLog(@"thisReceivedData innehåller %d tecken",[thisReceivedData length]);
    }else{
        /*DOLD*///NSLog(@"thisReceivedData innehöll 0 tecken, appendade inte");
    }
    
    /*DOLD*///NSLog(@".");
    /*DOLD*///NSLog(@"receivedData innehåller %d tecken",[receivedData length]);
    /*DOLD*///NSLog(@"receivedDataString är \n\n %@ \n\n",receivedDataString);
    /*DOLD*///NSLog(@"receivedDataString innehåller %d tecken",[receivedDataString length]);
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    /*DOLD*///NSLog(@"Connection failade med error");
    // release the connection, and the data object
    connection = nil;
    // receivedData is declared as a method instance elsewhere
    receivedData = nil;
    
    // inform the user
    /*DOLD*///NSLog(@"Connection failed! Error - %@",[error localizedDescription],[[error userInfo] objectForKey:NSErrorFailingURLStringKey]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    /*DOLD*///NSLog(@"connection did finish loading");
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    
    myTextView.text = receivedDataString;
    
    //Uppdatera databasen
    
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" UPPDATERA DATABASEN i connectionDidFinishLoading");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");

    //om mottagen data > 0
    if([receivedDataString length]>0){
        sqlite3_stmt *statement;
        const char *dbpath = [databasePath UTF8String];
        //öppna db
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            //Kommando: BEGIN
            /*DOLD*///NSLog(@"Databasen öppnad");
            NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
            } else {
                /*DOLD*///NSLog(@"#####%s;   Kommando missly: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB));
            }
            
            //Kommando: Dela upp mottagen data i en array och loopa igenom alla statements.
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@" DELA UPP STATEMENTS I ARRAY OCH LOOPA IGENOM ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");

            NSString * thisSQLstring;
            NSString *insertSQL;
            const char *insert_stmt;
            
       //     NSString * tempSqlTest = @"INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn1\", \"adr1\", \"tel1\");INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn2\", \"adr2\", \"tel2\")";
            /*DOLD*///NSLog(@"tempSqlTest: %@", tempSqlTest);
            
            sqlStatementsArr = [receivedDataString componentsSeparatedByString: @";###;"];
            /*DOLD*///NSLog(@".");
            /*DOLD*///NSLog(@"Antal statements att köra ([sqlStatementsArr count]): %d", [sqlStatementsArr count]);
            /*DOLD*///NSLog(@".");
            /*DOLD*///NSLog(@"Mottagen data: %d tecken", [receivedDataString length]);
            /*DOLD*///NSLog(@".");

            
            failedSqlStatementsArr = [[NSMutableArray alloc] init];
            
            for(i=0;i<[sqlStatementsArr count];i++){
                ///*DOLD*///NSLog(@".");
                ///*DOLD*///NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([sqlStatementsArr count]), [sqlStatementsArr objectAtIndex:i]);
                
                thisSQLstring = nil;
                insertSQL = [NSString stringWithFormat: @"%@", [sqlStatementsArr objectAtIndex:i]];
                insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(contactDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    if ([insertSQL rangeOfString:@"CREATE"].location != NSNotFound) {
                        /*DOLD*///NSLog(@"#####%s; SKAPADE TABELL - Kommando utfört: %@ SQL-svar: %s - Felkod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(contactDB));
                    }else{
                        /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), insertSQL);
                    }
                } else {
                    /*DOLD*///NSLog(@"#####%s;   Kommando: %@ SQL-svar: %s - Felkod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(contactDB));
                    
                    [failedSqlStatementsArr addObject:insertSQL];
                }
            }
            
            sqlStatementsArr = nil;
            
            
            //Kommando: COMMIT
            NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
            const char *commit_stmt = [commitSQL UTF8String];
            sqlite3_prepare_v2(contactDB, commit_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), commitSQL);
            } else {
                /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), commitSQL, sqlite3_errcode(contactDB));
            }

            /*DOLD*///NSLog(@"Finalize: %d", sqlite3_finalize(statement));
            sqlite3_close(contactDB);
        }
    }
    
    //loopa igenom databasen i loggen
    
    
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" LOOPA IGENOM DATABASEN FÖR ATT VISA RESULTATET I LOGGEN");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");

    
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        /*DOLD*///NSLog(@"Listar alla tabeller i databasen:");
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *col1 = (char *)sqlite3_column_text(statement, 0);
            NSString * col1str = nil;
            if (col1 !=NULL)
                col1str = [NSString stringWithUTF8String: col1];
            
            char *col2 = (char *)sqlite3_column_text(statement, 1);
            NSString * col2str = nil;
            if (col2 !=NULL)
                col2str = [NSString stringWithUTF8String: col2];
            
            char *col3 = (char *)sqlite3_column_text(statement, 2);
            NSString * col3str = nil;
            if (col3 !=NULL)
                col3str = [NSString stringWithUTF8String: col3];
            
            /*DOLD*///NSLog(@"ID: %@ sortID: %@ Title: %@",col1str, col2str, col3str);
            
			
        }
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
        } else {
            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB));
        }
                
        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
        sqlite3_close(contactDB);
    }
    
    
    //Loopa igenom alla failedSqlStatements
    /*DOLD*///NSLog(@"\n\n\n#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" FAILADE STATEMENTS ");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################\n\n\n");

    for(i=0;i<[failedSqlStatementsArr count];i++){
        /*DOLD*///NSLog(@"%@",[failedSqlStatementsArr objectAtIndex:i]);
    }
    
    
    /*DOLD*///NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
    
    
    // release the connection, and the data object
    
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" STÄNG CONNECTION OCH NOLLSTÄLL DATA");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");

    connection = nil;
    receivedData = nil;
    /*DOLD*///NSLog(@"Nollställer receivedData");
    
    //Change
//    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [(AppDelegate*)[UIApplication sharedApplication].delegate hideActivity];
    
    /*DOLD*///NSLog(@"Nollställer variabler...");
    
    myFetchedData = nil;
    resultat = nil;
    tablesAndChecks = nil;
    tablesArr = nil;
    checksArr = nil;
    tablesToRequest = nil;
    receivedData = nil;
    receivedDataString = nil;
    
    SQL = nil;
    sqlStatementsArr = nil;
    
    
    
    
    
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        /*DOLD*///NSLog(@"Databasen öppnad");
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            char *col1 = (char *)sqlite3_column_text(statement, 0);
            if (col1 !=NULL){
                
                [dictionary setObject:[NSString stringWithUTF8String: col1] forKey:@"colname"];
                [tempArray addObject:[NSString stringWithUTF8String: col1]];
                
            }
            
            /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);
        }
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
        } else {
            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d Felmeddelande: %s", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB), sqlite3_errmsg(nil));
        }
        
        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
        sqlite3_close(contactDB);
        
        self.myTableViewDataSource = nil;
        /*DOLD*///NSLog(@"Sätter self.myTableViewDataSource = nil");
        self.myTableViewDataSource = [[NSMutableArray alloc] initWithCapacity:0];
        /*DOLD*///NSLog(@"Allokerar myTableViewDataSource");
        self.myTableViewDataSource = tempArray;
        /*DOLD*///NSLog(@"myTableViewDataSource count: %d",[self.myTableViewDataSource count]);
        
        for (i=0; i<[self.myTableViewDataSource count]; i++) {
            /*DOLD*///NSLog(@"%@",[self.myTableViewDataSource objectAtIndex:i]);
        }
        
    }
    
    if(self.myTableView == nil){
        /*DOLD*///NSLog(@"NILCHECK mytableview är nil!");
        

    }else{
        /*DOLD*///NSLog(@"NILCHECK mytableview är INTE nil!");
    }
    /*DOLD*///NSLog(@"myTableViewDataSource count: %d",[self.myTableViewDataSource count]);
    /*DOLD*///NSLog(@"##### RELOADDATA #####");
    
    /*VIKTIG*/NSLog(@"Databasen har uppdaterats.");
    
    [self.myTableView reloadData];
    [self refreshTableView];
//    [self performSelectorOnMainThread:@selector(refreshTableView) withObject:nil waitUntilDone:NO];

}

- (void) getDataOnNewThread
{
    // code here to populate your data source
    // call refreshTableViewOnMainThread like below:
}

- (void) refreshTableView
{
    [self.myTableView reloadData];
}


// Check whether the user has internet
- (bool)hasInternet {
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5.0];
    __block BOOL connectedToInternet = NO;
//    //Change
//    if ([NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil]) {
//        connectedToInternet = YES;
//    }
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
        connectedToInternet = YES;

    }] resume];
    
    //if (connectedToInternet)
    ///*DOLD*///NSLog(@"We Have Internet!");
    return connectedToInternet;
}

- (void)runFetchData
{
    //Här finns eventuellt utrymme för att förflytta fetchdata-metoden till en bakgrundstråd istället
//    if(!fetchDataIsRunning){
//        [self performSelectorInBackground:@selector(fetchData) withObject:nil];
//    }
    //Denna metod fungerar inte, se http://stackoverflow.com/questions/9679754/tried-to-obtain-the-web-lock-from-a-thread-other-than-the-main-thread-or-the-web
    
    
    [self performSelector:@selector(fetchData)];
    
    
}

- (void)fetchData
{
    fetchDataIsRunning = TRUE;
    
    //Kolla om uppkoppling finns
    if(![self hasInternet]){
        /*VIKTIG*/NSLog(@"Ingen uppkoppling hittad. Offline-läge aktivt");
    }else{
        /*VIKTIG*/NSLog(@"Uppdaterar databasen...");

    
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" FETCHDATA KÖRS ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
                
        /*DOLD*///NSLog(@"Start!");
        

        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" HÄMTAR TablesAndChecks ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*VIKTIG*/NSLog(@"Försöker nå internet... (om appen kraschar här, kontrollera uppkopplingen)");

        
        
        /*DOLD*///NSLog(@"Hämtar data...");
        int j = 0;
        bool dataWasFetched = FALSE;
        while (j<=5 && dataWasFetched == FALSE) {
            if([self hasInternet]){
                NSString *url = [NSString stringWithFormat:@"%@/_remote/get_table_checksums.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]];
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                NSString *serverOutput= [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
                myFetchedData = serverOutput;
                /*DOLD*///NSLog(@"Data: %d", [myFetchedData length]);
                /*DOLD*///NSLog(@"Slut på data.");
                dataWasFetched = TRUE;
            }else{
                /*DOLD*///NSLog(@"Försökte hämta data, men hade ingen uppkoppling. Försök %d av 10.", j);
                j++;
            }
        }
        
        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" delar upp TablesAndChecks ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"Försöker nå internet... (om appen kraschar här, så är antagligen mottagen data från tablesandchecks = 0!)");
    
        
        tablesAndChecks = [myFetchedData componentsSeparatedByString: @"&"];
        tablesArr = [[tablesAndChecks objectAtIndex:0] componentsSeparatedByString: @","];
        checksArr = [[tablesAndChecks objectAtIndex:1] componentsSeparatedByString: @","];
        
        resultat = [[NSMutableString alloc] initWithCapacity:0];
        
        for(i=0;i<[tablesArr count];i++){
            [resultat appendString:([tablesArr objectAtIndex:i])];
            [resultat appendString:(@" = ")];
            [resultat appendString:([checksArr objectAtIndex:i])];
            [resultat appendString:(@"; \n")];
        }
        /*DOLD*///NSLog(@"RESULTATET ÄR: %d, tablesArr är %d, checksArr är %d", [resultat length], [tablesArr count], [checksArr count]);
        myTextView.text = resultat;
        
        
        
        NSMutableArray *dbTablesArr = [[NSMutableArray alloc] init];
        //[NSArray arrayWithObjects: @"prot_arbetsorder_desc", @"prot_arbetsorder", @"falskafyran", nil];
        NSMutableArray *dbChecksArr = [[NSMutableArray alloc] init];
        //[NSArray arrayWithObjects: @"100", @"200", @"400", nil];
        
        //Loopa igenom databasens tabell meta_tablechecksums och fyll i arrayerna dbTablesArr och dbChecksArr
        
        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" HÄMTA DATA FRÅN TABELLEN meta_tablechecksums INFÖR JÄMFÖRELSE MED SERVERN ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        
        sqlite3_stmt *statement;
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            /*DOLD*///NSLog(@"Databasen öppnad");
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM meta_tablechecksums"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
            while (sqlite3_step(statement) == SQLITE_ROW) {
                char *col1 = (char *)sqlite3_column_text(statement, 1);
                NSString * col1str = nil;
                if (col1 !=NULL)
                    col1str = [NSString stringWithUTF8String: col1];
                
                char *col2 = (char *)sqlite3_column_text(statement, 2);
                NSString * col2str = nil;
                if (col2 !=NULL)
                    col2str = [NSString stringWithUTF8String: col2];
                
                [dbTablesArr addObject:col1str];
                [dbChecksArr addObject:col2str];
                
                /*DOLD*///NSLog(@"Tabellnamn: %@ Checksum: %@",col1str, col2str);
                
                
            }
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
            } else {
                /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB));
            }
            
            /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
            sqlite3_close(contactDB);
        }
        
        
        
        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" BÖRJAR JÄMFÖRELSE ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        
        NSMutableArray *tablesToDropArr = [[NSMutableArray alloc] init];
        NSMutableArray *tablesToAddArr = [[NSMutableArray alloc] init];
        
        //checksum-tabellen droppas alltid för att sedan skapas på nytt
        [tablesToDropArr addObject:@"meta_tablechecksums"];
        
        for(i=0;i<[tablesArr count];i++){
            NSUInteger indexOfListAinListB = [dbTablesArr indexOfObject: ([tablesArr objectAtIndex: i])]; //finns tabellen lokalt?
            /*DOLD*///NSLog(@"indexOfListAinListB: %d", indexOfListAinListB);
            NSString *checksumA = [checksArr objectAtIndex: i]; //Vad är senaste checksum?
            NSString *checksumB = nil;
            if(indexOfListAinListB>[dbTablesArr count]){
                /*DOLD*///NSLog(@"index för stort");
                /*DOLD*///NSLog(@"finns inte i lista B. Lägg till %@ i tablesToAddArr",[tablesArr objectAtIndex:i]);
                //finns inte i lista B. Lägg till i tablesToAddArr
                [tablesToAddArr addObject:[tablesArr objectAtIndex:i]];
            }else{
                checksumB = [dbChecksArr objectAtIndex: indexOfListAinListB];
                /*DOLD*///NSLog(@"Tabell: %@ checksumA: %@ - checksumB: %@", [tablesArr objectAtIndex: i], checksumA, checksumB);
                if([checksumA isEqualToString:checksumB]){
                    //checksummorna är lika, tabellen behöver inte uppdateras
                    /*DOLD*///NSLog(@"Checksummorna är lika");
                }else{
                    //checksummorna är OLIKA, lägg till tabellnamnet i både tablesToDropArr och tablesToAddArr
                    /*DOLD*///NSLog(@"checksummorna är OLIKA, lägg till tabellnamnet '%@' i både tablesToDropArr och tablesToAddArr", [tablesArr objectAtIndex:i]);
                    
                    [tablesToDropArr addObject:[tablesArr objectAtIndex:i]];
                    [tablesToAddArr addObject:[tablesArr objectAtIndex:i]];
                    
                }
            }
        }
        //Kolla om det finns någon tabell i lista B som inte finns i lista A (ta bort tabeller som finns lokalt med ej på servern)
        for(i=0;i<[dbTablesArr count];i++){
            NSUInteger indexOfListBinListA = [tablesArr indexOfObject: ([dbTablesArr objectAtIndex: i])]; //finns tabellen lokalt?
            if(indexOfListBinListA>[tablesArr count]){
                /*DOLD*///NSLog(@"%@ finns i lista B men inte i lista A. Ta bort den lokalt.",[dbTablesArr objectAtIndex:i]);
                
                //finns inte i lista B. Lägg till i tablesToAddArr
                [tablesToDropArr addObject:[dbTablesArr objectAtIndex:i]];
            }
        }
        
        //JÄMFÖRELSE KLAR. UPPDATERA DATABASEN:
        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" FÖRBERED ATT DROPPA TABELLER ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        
        /*DOLD*///NSLog(@"Drop tables: %@", tablesToDropArr);
        
        
        //DROP TABLES
        //Om den enda tabellen som ska droppas är meta_checksums (dvs om [tablestodrop count]==1), droppa inget! Detta händer om lokala databasen redan är helt uppdaterad.
        if([tablesToDropArr count]>1){
            sqlite3_stmt *statement;
            const char *dbpath = [databasePath UTF8String];
            if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
            {
                /*DOLD*///NSLog(@"Databasen öppnad för att DROPPA TABELLER");
                NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
                const char *begin_stmt = [beginSQL UTF8String];
                sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
                } else {
                    /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB));
                }
                
                NSString * thisSQLstring;
                NSString *insertSQL;
                const char *insert_stmt;
                
                sqlStatementsArr = tablesToDropArr;
                /*DOLD*///NSLog(@".");
                /*DOLD*///NSLog(@"Antal statements att köra: %d", [sqlStatementsArr count]);
                /*DOLD*///NSLog(@".");
                /*DOLD*///NSLog(@"Mottagen data: %d tecken", [receivedDataString length]);
                /*DOLD*///NSLog(@". [sqlStatementsArr count]");
                
                
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" LOOPAR IGENOM TABELLER ATT DROPPA-LISTAN ");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                
                for(i=0;i<[sqlStatementsArr count];i++){
                    ///*DOLD*///NSLog(@".");
                    ///*DOLD*///NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([sqlStatementsArr count]), [sqlStatementsArr objectAtIndex:i]);
                    
                    thisSQLstring = nil;
                    insertSQL = [NSString stringWithFormat: @"DROP TABLE IF EXISTS %@", [sqlStatementsArr objectAtIndex:i]];
                    insert_stmt = [insertSQL UTF8String];
                    sqlite3_prepare_v2(contactDB, insert_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                    } else {
                        /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(contactDB));
                    }
                }
                
                sqlStatementsArr = nil;
                
                NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
                const char *commit_stmt = [commitSQL UTF8String];
                sqlite3_prepare_v2(contactDB, commit_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), commitSQL);
                } else {
                    /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), commitSQL, sqlite3_errcode(contactDB));
                }
                /*DOLD*///NSLog(@"Finalize: %d", sqlite3_finalize(statement));
                sqlite3_close(contactDB);
            }
        }
        
        
        /*DOLD*///NSLog(@"Add tables: %d", [tablesToAddArr count]);
        
        //Gör en array för att fråga servern om sql-script som skapar och fyller de tabeller som saknas på iPaden
        
        
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@" HÄMTA SQL-INSERT-SCRIPT FRÅN SERVERN ");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        /*DOLD*///NSLog(@"#################################");
        
        
        tablesToRequest = [[NSMutableString alloc] initWithCapacity:0];
        for(i=0;i<[tablesToAddArr count];i++){
            [tablesToRequest appendString:([tablesToAddArr objectAtIndex:i])];
            [tablesToRequest appendString:(@",")];
        }
        /*DOLD*///NSLog(@"tablesToRequest: %d", [tablesToRequest length]);
        
        //Hämta data från servern via http POST (om det finns något att hämta, dvs om tablesToRequest > 0
        if([tablesToRequest length]>0){
            NSString * myURL = [NSString stringWithFormat:@"%@/_remote/get_table_insertstring_sqlite.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]];
            
            /*DOLD*///NSLog(@"content-type satt");
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                            initWithURL:[NSURL
                                                         URLWithString: myURL]];
            
            [request setHTTPMethod:@"POST"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            /*DOLD*///NSLog(@"requestedTables=%@",tablesToRequest);
            NSString * tableString = [NSString stringWithFormat:@"requestedTables=%@",tablesToRequest];
            
            [request setValue:[NSString stringWithFormat:@"%lu",
                               (unsigned long)[tableString length]]
           forHTTPHeaderField:@"Content-length"];
            
            [request setHTTPBody:[tableString
                                  dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
            //Change
//            [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
            [(AppDelegate*)[UIApplication sharedApplication].delegate showActivity];
            
            /*DOLD*///NSLog(@"Request-string: %d", [tableString length]);
            //Change
//            NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
//            if (theConnection) {
//                //receivedData = [NSMutableData data];
//                /*DOLD*///NSLog(@"ReceivedData är: %d", [receivedData length]);
//            } else {
//                /*DOLD*///NSLog(@"Connection Failed!");
//            }
            [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
                //handle response

            }] resume];
            
            //Allt klart här! När requesten är färdig uppdateras tabellen.
        }else{
            /*VIKTIG*/NSLog(@"Databasen behövde inte uppdateras.");
            /*DOLD*///NSLog(@"############################### \n############################### \nDatabasen är redan up-to-date!\n###############################\n###############################");
            myTextView.text = @"Databasen är redan up-to-date!";
            
            NSString *alertString = [NSString stringWithFormat:@"Databasen är redan up-to-date!"];
            //Change
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//            [alert show];
//            [alert dismissWithClickedButtonIndex:0 animated:YES];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                            message:@""
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Done"
                                                                   style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                    }];
                
            [alertController addAction:actionCancel];
            [self presentViewController:alertController animated:YES completion:nil];
//            [alertController dismissViewControllerAnimated:YES completion:nil];
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@" SLUT PÅ FETCHDATA! ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            
        }
    }
    fetchDataIsRunning = FALSE;
}


- (void)viewDidLoad
{
    self.title = @"Debug";
    
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" VIEWDIDLOAD: SKAPA DATABAS OM DEN INTE FINNS ");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@"#################################");
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
 
    if ([filemgr fileExistsAtPath: databasePath ] == YES)
        {
        [filemgr removeItemAtPath: (databasePath) error:NULL];
        NSLog(@"Raderade den befintliga databasfilen.");
        }
  //*/
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
		const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS CONTACTS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADDRESS TEXT, PHONE TEXT)";
            /*DOLD*///NSLog(@"Skapar databas (alltså contact-tabellen).");
            if (sqlite3_exec(contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                myTextView.text = @"Failed to create table";
            }

             sqlite3_close(contactDB);
            
        } else {
            myTextView.text = @"Failed to open/create database";
        }
    }
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
//    (void)self.fetchData;
    
    
    //Fyll TableView's datasource ( nsmutablearray) med data från databasen
    
    /*DOLD*///NSLog(@"FYLL MYTABLEVIEWDATASOURCE");
    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        /*DOLD*///NSLog(@"Databasen öppnad");
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            char *col1 = (char *)sqlite3_column_text(statement, 0);
            if (col1 !=NULL){
                
                [dictionary setObject:[NSString stringWithUTF8String: col1] forKey:@"colname"];
                [tempArray addObject:[NSString stringWithUTF8String: col1]];
                
            }
            
            /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);
        }
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
        } else {
            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d Felmeddelande: %s", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(contactDB), sqlite3_errmsg(nil));
        }
        
        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
        sqlite3_close(contactDB);
        
        myTableViewDataSource = [[NSMutableArray alloc] initWithCapacity:0];
        self.myTableViewDataSource = tempArray;
        /*DOLD*///NSLog(@"myTableViewDataSource count: %d",[self.myTableViewDataSource count]);
        
        for (i=0; i<[self.myTableViewDataSource count]; i++) {
            /*DOLD*///NSLog(@"%@",[self.myTableViewDataSource objectAtIndex:i]);
        }
        
    }
    
    self.myTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStylePlain];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    
}

//Change
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//
//
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//    /*DOLD*///NSLog(@"Hejdå!");
//}

- (void) didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Release any retained subviews of the main view.
    
    
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    /*DOLD*///NSLog(@"Hejdå!");
}


//Change
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return YES;
//}
-(BOOL)shouldAutorotate{
    return YES;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end


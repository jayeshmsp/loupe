//
//  AutotextSelector.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-21.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "AutotextSelector.h"

@interface AutotextSelector ()

@end

@implementation AutotextSelector

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

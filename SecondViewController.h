//
//  SecondViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"
#import <sqlite3.h>
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface SecondViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>{
    
    bool isFiltered;
    
    /* FETCHDATA */
    
    int i;
    int myCounter;
    int tablesArrCountInt;
}



@property (nonatomic, retain) NSString *docsDir;
@property (nonatomic, retain) NSArray *dirPaths;


@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSString *databasePath;
@property (nonatomic, retain) NSMutableArray *allSelectedReportsGroupedByDay;
@property (nonatomic, retain) NSMutableString *numberOfSections;

@property (nonatomic, retain) NSMutableArray *allFilteredReports;

@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;

- (IBAction) updateTable;
- (IBAction) updateDatabase;


- (IBAction) toggleViewAll;
@property (nonatomic, retain) NSMutableString *filterOnUsername;

- (IBAction) logoutUser;

@property (nonatomic, retain) IBOutlet UILabel * updatedLabel;
@property (nonatomic, retain) IBOutlet UILabel * poststosendLabel;


/* FETCHDATA */

- (IBAction) runFetchData;
- (IBAction) fetchData;

/* SKICKA RAPPORTER UTAN ATT LADDA NER DATA */
- (IBAction) sendPostsToServerButtonWasPressed;

/* SKICKA DB VIA MAIL */
- (IBAction) sendDBToMailButtonWasPressed;

/* SKICKA LOGGAR VIA MAIL */
- (IBAction) sendLogsToMailButtonWasPressed;

/* SKICKA RAPPORTER... popover-trigger */
- (IBAction) toggleSendreportsPopover;

/* SEGMENTED BUTTON FÖR ATT VÄLJA BACKUP */
@property (nonatomic, strong) IBOutlet UISegmentedControl *changeSegBackupViewController;
- (IBAction) changeSegBackupView;

/* SEGMENTED BUTTON FÖR ATT VÄLJA URVAL */
@property (nonatomic, strong) IBOutlet UISegmentedControl *changeSegUrvalController;
- (IBAction) changeSegUrval;

/* PROGRESS BAR */
@property (weak, nonatomic) IBOutlet UIProgressView *progressbar;

/* LOGGAR UT ANVÄNDARE OM fetchData STARTADE AUTOMATISKT */
@property bool runFetchDataAutomaticallyWasTrue;

@property (nonatomic, retain) NSString *myFetchedData;
@property (nonatomic, retain) NSMutableString *resultat;
@property (nonatomic, retain) NSArray *tablesAndChecks;
@property (nonatomic, retain) NSArray *tablesArr;
@property (nonatomic, retain) NSArray *checksArr;
@property (nonatomic, retain) NSMutableString *tablesToRequest;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSMutableString * receivedDataString;

@property (nonatomic, retain) NSMutableString * SQL;
@property (nonatomic, retain) NSArray * sqlStatementsArr;
@property (nonatomic, retain) NSMutableArray * failedSqlStatementsArr;
@property (nonatomic, retain) NSMutableArray * failedSqlStatementsCodeArr;

@property (nonatomic) NSString * tableRowCount;
@property (nonatomic, retain) NSMutableArray * dbloop1;

@property (nonatomic, retain) NSURLConnection * fetchDataConnection;
@property (nonatomic, retain) NSURLConnection * loginConnection;
@property (nonatomic, retain) NSURLConnection * sendPostConnection;
@property (nonatomic, retain) NSMutableArray * sendPostConnectionArr;

@property (nonatomic, retain) NSString * fetchDataIsRunning;
@property (nonatomic, retain) NSString * logonToServerIsRunning;
@property (nonatomic, retain) NSString * sendPostsToServerIsRunning;

//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@end


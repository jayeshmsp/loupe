//
//  ReportDetailsViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-07.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "AutotextAddTextViewController.h"
#import "AppDelegate.h"

@interface ReportDetailsViewController : UIViewController<UITextFieldDelegate>{
//    NSString * databasePath;
     BOOL           keyboardVisible;
	CGPoint        offset;
	
	int renameZoneButtonWasPressedOnRow;
	int renameRoomButtonWasPressedOnRow;
    
    /*UITapGestureRecognizer *tap;
    BOOL isFullScreen;
    CGRect prevFrame;*/
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *editOrSaveButton;
@property (nonatomic, retain) IBOutlet UITableView *reportForm;


@property (nonatomic, retain) NSString * databasePath;
@property (nonatomic, strong) NSNumber *workOrder;
@property (nonatomic, strong) NSNumber *templateid;
@property (nonatomic, strong) NSMutableArray *templateInfo;
@property (nonatomic, strong) NSNumber *editMode;
@property (nonatomic, strong) NSNumber *autotextMode;

@property (nonatomic, strong) NSMutableArray *reportFields;
@property (nonatomic, strong) NSMutableString * reportFieldsStr;
@property (nonatomic, strong) NSMutableArray *reportInitialValues;
@property (nonatomic, strong) NSMutableArray *defaultReportValues;

@property (nonatomic, retain) NSNumber *workOrderForClickedButton;

@property (nonatomic, retain) NSIndexPath *indexForClickedButton;

@property (nonatomic, retain) NSMutableArray *inputFieldTagCounterArr;
@property (nonatomic, retain) NSMutableDictionary *inputFieldTagDict;
@property (nonatomic, retain) NSMutableDictionary *blobdictionary;

@property (nonatomic, retain) NSString * tagForRiskInputField;


// - (IBAction)showRelatedReports:(id)sender;
- (IBAction)backButtonPressed:(id)sender;

//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

@end

//
//  LoginViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-26.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "LoginViewController.h"
#import <CommonCrypto/CommonDigest.h>

@interface LoginViewController () 

@end

@implementation LoginViewController


@synthesize appDelegate = _appDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    NSLog(@"Login: viewDidLoad");
    
    [_feedbackLabel setText:@""];
    
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [_versionLabel setText:[NSString stringWithFormat:@"Version %@",appVersionString]];

    // Hämta senaste inloggade användarnamnet
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // Get the results out
    NSString *lastServer = [defaults stringForKey:@"siteurl"];
    NSString *lastUserName = [defaults stringForKey:@"lastuser"];
    
    if (lastServer != nil) {
        [_server setText:[lastServer stringByReplacingOccurrencesOfString:@"http://" withString:@""]];
        if (lastUserName != nil) {
            [_username setText:lastUserName];
            [_password becomeFirstResponder];
            [self connectToDbAndStoreConnectionInAppDelegate];
        }else{
            [_username becomeFirstResponder];
            [self connectToDbAndStoreConnectionInAppDelegate];
        }
    }else{
        [_server becomeFirstResponder];
    }
    
	//[_password becomeFirstResponder];
	
    [_password addTarget:_password action:@selector(resignFirstResponder) forControlEvents:UIControlEventEditingDidEndOnExit];

	//Logga in automatiskt
	//[self loginUser];
	
    
    // Ladda alla inställningar... Det kanske ska göras vid login eller i delegate, men det blir senare refactoring...
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //[defaults setObject:@"http://kringdata.loupe.se" forKey:@"siteurl"];
    //[defaults setObject:@"http://www.ohbsys.se" forKey:@"siteurl"];
    [defaults synchronize]; // this method is optional
    
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [self loginUser];
//    [textField resignFirstResponder];
//    return NO;
//}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
}

//Öppna connection och lagra i appdelegate
-(void) connectToDbAndStoreConnectionInAppDelegate{
	_appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSLog(@"Ansluter till DB: connectToDbAndStoreConnectionInAppDelegate körs");
	
	if(dbConnection){
		if(_appDelegate.openDbConnection){
			//Om dbConnection redan håller en connection, stäng den innan variabeln pekas om till databasen som ska öppnas
			sqlite3_close(dbConnection);
			dbConnection = nil;
		}
	}
	
//Kolla om databasen finns
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    NSString * _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
    NSLog(@"Database path%@", _databasePath);
    NSString * _databasePathOld = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]];
    NSFileManager *filemgr = [NSFileManager defaultManager];
	
	NSLog(@"leta db");
	
	//Kolla om det finns en "contacts.db" och döp om den till "LoupeApp.db"
	if ([filemgr fileExistsAtPath: _databasePathOld ] == YES){
		[filemgr moveItemAtPath:_databasePathOld toPath:_databasePath error:NULL];
	}
/*
	if ([filemgr fileExistsAtPath: _databasePath ] == NO){
		NSLog(@"ingen db hittades");
		
		NSString *alertString = [NSString stringWithFormat:@"Ingen databas hittades"];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar." delegate:self cancelButtonTitle:@"Fortsätt" otherButtonTitles:nil];
		[alert show];
	}
*/
	//DB ska ALLTID öppnas här, om filen inte finns så skapas den
	NSLog(@"db hittades");
	const char *dbpath = [_databasePath UTF8String];
	
	//Detta ska vara enda stället i hela appen som sqlite3_open() används! Att öppna och stänga databasen flera gånger orsakar lätt minnesproblem.
	NSLog(@"Öppnar db");
	if (sqlite3_open(dbpath, &dbConnection) == SQLITE_OK)
	{
		NSLog(@"DB öppnades");
		NSLog(@"Finns det nån conn i appDelegate?");
		if(_appDelegate.openDbConnection){
			NSLog(@"Ja, stänger den");
			//Om appDelegate redan håller en connection, stäng den innan variabeln pekas om till databasen som just öppnades
			sqlite3_close(_appDelegate.openDbConnection);
		}else{
			NSLog(@"Nej.");
		}
		NSLog(@"Sparar conn i appDelegate");
		_appDelegate.openDbConnection = dbConnection;

		NSLog(@"# ");
		NSLog(@"# appDelegate.openDbConnection innehåller nu en öppen anslutning till databasen som kan användas i hela appen.");
		NSLog(@"# Använd if(_appDelegate.openDbConnection){} istället för if(sqlite3_open(db, var){}");
		NSLog(@"# ");
	}else{
		NSLog(@"DB kunde inte öppnas.");
	}
	[self checkIfDbIsEmpty];
}

-(void)checkIfDbIsEmpty
{
	NSMutableArray *db_tablesArr = [[NSMutableArray alloc] init];
	sqlite3_stmt *statement;
	
	//Räkna poster i meta_tablechecksums
	if (_appDelegate.openDbConnection)
	{
		/*DOLD*///NSLog(@"Databasen öppnad");
		NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM meta_tablechecksums"];
		const char *begin_stmt = [beginSQL UTF8String];
		sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char *col1 = (char *)sqlite3_column_text(statement, 1);
			NSString * col1str = nil;
			if (col1 !=NULL)
				col1str = [NSString stringWithUTF8String: col1];
			
			char *col2 = (char *)sqlite3_column_text(statement, 2);
			NSString * col2str = nil;
			if (col2 !=NULL)
				col2str = [NSString stringWithUTF8String: col2];
			
			[db_tablesArr addObject:col1str];
			
			/*DOLD*///NSLog(@"Tabellnamn: %@ Checksum: %@",col1str, col2str);
			
			
		}
		if (sqlite3_step(statement) == SQLITE_DONE)
		{
			/*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
		} else {
			/*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
		}
		
		/*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
		beginSQL = nil;
		begin_stmt = nil;
		sqlite3_finalize(statement);
	}
	
	//Om tabellen var tom, tvångs-logga-in användaren och tanka ner db
	if([db_tablesArr count]==0){
		NSLog(@"DB är tom. SELECT * FROM meta_tablechecksums returnerade 0 rader. Visar alert \"Ingen databas hittades\"");

		NSString *alertString = [NSString stringWithFormat:@"Ingen databas hittades"];
       
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                    message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar."
                                                                             preferredStyle:UIAlertControllerStyleAlert];
       //We add buttons to the alert controller by creating UIAlertActions:
       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Fortsätt"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [self okButtonPressed: @"Fortsätt"];
                                                        }]; //You can use a block here to handle a press on this button
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alertController dismissViewControllerAnimated:YES completion:nil];
                                                            }];
        
    [alertController addAction:actionCancel];
       [alertController addAction:actionOk];
        
       [self presentViewController:alertController animated:YES completion:nil];
        
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar." delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Fortsätt", nil];
//		[alert show];
	}
}

-(void)forceLoginUser
{
	_loggedInUser = _username.text;
	_appDelegate.user = _loggedInUser;
	
	NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
	[tempDictionary setValue:_username.text forKey:@"username"];
	[tempDictionary setValue:_password.text forKey:@"password"];
	
	_appDelegate.userDictionary = tempDictionary;
	
	NSLog(@"_appDelegate.userDictionary:");
	NSLog(@"%@",_appDelegate.userDictionary);
	NSLog(@"Kör loginSegue 2");
	[self performSegueWithIdentifier:@"loginSegue" sender:self];
}


//Issue 1 changes
- (IBAction)serverFieldChanged:(UITextField *)sender {
    if (_server.text != nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString *urlWithHttp = [NSString stringWithFormat:@"http://%@", _server.text];
        NSString *cleanUrlWithHttp = [urlWithHttp stringByReplacingOccurrencesOfString:@"http://http://" withString:@"http://"];
        NSString *cleanUrlWithHttps = [urlWithHttp stringByReplacingOccurrencesOfString:@"http://https://" withString:@"https://"];
        if([cleanUrlWithHttp hasPrefix:@"http://http://"]) {
            [defaults setObject: cleanUrlWithHttp forKey:@"siteurl"];
        }else{
            [defaults setObject: cleanUrlWithHttps forKey:@"siteurl"];
        }
        [defaults synchronize];
        NSLog(@"SiteURL updated: %@", [defaults objectForKey:@"siteurl"]);
    }
}














//Funktion som körs när användaren trycker på knappen "radera databasen"
//Raderar databasfilen, och tvångs-loggar-in användaren, och en fetchData körs automatiskt (flagga i appDelegate)
-(void) emptyDB{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([[defaults objectForKey:@"siteurl"] length] > 8){
        NSString * dbLastUpdated = [[NSString alloc] init];
        NSString *docsDir;
        NSArray *dirPaths;
        
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        // Build the path to the database file
        sqlite3_stmt *statement;
        
        NSMutableDictionary * updated = [[NSMutableDictionary alloc] init];
        if(_appDelegate.openDbConnection)
        {
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_dbsyncinfo ORDER BY id DESC LIMIT 1"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            //Ger antalet kolumner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int j = 0;
                for (j=0; j<colCount;j++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, j);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, j);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                updated = dictionary;
                
                dbLastUpdated = [updated valueForKey:@"updated"];
            }
            beginSQL = nil;
            begin_stmt = nil;
            sqlite3_finalize(statement);
        }
        
        
        NSString *alertString = [NSString stringWithFormat:@"Vill du verkligen radera databasen?"];
        
        NSString *alertMsg = [[NSString alloc] init];
        if(![dbLastUpdated isEqualToString:@""]){
            alertMsg = [NSString stringWithFormat:@"Om databasen är korrupt kan den behöva laddas om från servern. \n\nOBS 1:\nDu måste vara ansluten till Internet för att ladda in databasen igen. \n\nOBS 2:\nAlla ändringar sedan senaste synkroniseringen (%@) kommer att försvinna. \n\nDu bör vara säker på vad du gör innan du fortsätter.", dbLastUpdated];
        }else{
            alertMsg = @"Om databasen är korrupt kan den behöva laddas om från servern. \n\nOBS 1:\nDu måste vara ansluten till Internet för att ladda in databasen igen. \n\nOBS 2:\nAlla ändringar sedan senaste synkroniseringen kommer att försvinna. Tidpunkt för senaste synkronisering är tyvärr okänd. \n\nDu bör vara säker på vad du gör innan du fortsätter.";
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                    message:alertMsg
                                                                             preferredStyle:UIAlertControllerStyleAlert];
       //We add buttons to the alert controller by creating UIAlertActions:
       UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Radera"
                                                          style:UIAlertActionStyleDefault
                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                            [self okButtonPressed: @"Radera"];
                                                        }]; //You can use a block here to handle a press on this button
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alertController dismissViewControllerAnimated:YES completion:nil];
                                                             }];
        
        [alertController addAction:actionCancel];
           [alertController addAction:actionOk];
        
       [self presentViewController:alertController animated:YES completion:nil];
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertMsg delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Radera",nil];
//
//        [alert show];
    }else{
        [_feedbackLabel setTextColor:[UIColor colorWithRed:0.8 green:0.15 blue:0 alpha:1]];
        [_feedbackLabel setText:@"Skriv in adress till servern först!"];
    }
}

	
//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//	NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//	if([title isEqualToString:@"Radera"])
//	{
//		NSString *docsDir;
//		NSArray *dirPaths;
//		// Get the documents directory
//		dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//		docsDir = [dirPaths objectAtIndex:0];
//		// Build the path to the database file
//		NSString * _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
//		NSFileManager *filemgr = [NSFileManager defaultManager];
//
//		if ([filemgr fileExistsAtPath: _databasePath ] == YES)
//		{
//			NSLog(@"Finns det nån conn i appDelegate?");
//			if(_appDelegate.openDbConnection){
//				NSLog(@"Ja, stänger den");
//				//Om appDelegate redan håller en connection, stäng den innan variabeln pekas om till databasen som just öppnades
//				sqlite3_close(_appDelegate.openDbConnection);
//			}else{
//				NSLog(@"Nej.");
//			}
//
//			[filemgr removeItemAtPath: (_databasePath) error:NULL];
//			NSLog(@"Raderade den befintliga databasfilen.");
//		}else{
//			NSLog(@"Hittade ingen fil att radera");
//			if([filemgr fileExistsAtPath:[[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]]]){
//				NSLog(@"Men contacts.db finns...");
//			}
//		}
//
//        //Skapa ny DB-fil och prompta användaren om att DB nu kommer laddas ner
//        [self connectToDbAndStoreConnectionInAppDelegate];
//    }
//
//	if([title isEqualToString:@"Fortsätt"])
//	{
//		//Sätt flagga i appdelegate för att synkronisering ska köras direkt när användaren kommer till secondviewcontroller
//		_appDelegate.runFetchDataAutomatically = true;
//		[self forceLoginUser];
//    }
//
//	if([title isEqualToString:@"Försök återställa"])
//	{
//		//Radera databasfilen om den finns och kopiera in säkerhetskopian i dens ställe
//		NSLog(@"Användaren tryckte ''Försök återställa''.");
//		NSLog(@"Försöker ta bort skarpa DB...");
//		NSString *docsDir;
//		NSArray *dirPaths;
//		// Get the documents directory
//		dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//		docsDir = [dirPaths objectAtIndex:0];
//		// Build the path to the database file
//		NSString * _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
//		NSFileManager *filemgr = [NSFileManager defaultManager];
//
//		if ([filemgr fileExistsAtPath: _databasePath ] == YES)
//		{
//			[filemgr removeItemAtPath: (_databasePath) error:NULL];
//			NSLog(@"Raderade den befintliga databasfilen.");
//		}else{
//			NSLog(@"Hittade ingen fil att radera");
//			if([filemgr fileExistsAtPath:[[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]]]){
//				NSLog(@"Men contacts.db finns...");
//			}
//		}
//
//		NSLog(@"Försöker kopiera säkerhetskopian till skarp databas...");
//
//		NSString * _databaseSafeCopyPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ohbsysApp_safecopy.db"]];
//		if ([filemgr fileExistsAtPath: _databaseSafeCopyPath ] == YES){
//			[filemgr copyItemAtPath:_databaseSafeCopyPath toPath:_databasePath error:NULL];
//			NSLog(@"Säkerhetskopian återställdes.");
//
//		}else{
//			NSLog(@"Hittade ingen säkerhetskopia.");
//
//			NSString *alertString = [NSString stringWithFormat:@"Ingen säkerhetskopia hittades"];
//
//			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar." delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Fortsätt", nil];
//			[alert show];
//		}
//    }
//}

- (void)okButtonPressed:(NSString *)title{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if([title isEqualToString:@"Radera"])
    {
        NSString *docsDir;
        NSArray *dirPaths;
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        // Build the path to the database file
        NSString * _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
        NSFileManager *filemgr = [NSFileManager defaultManager];

        if ([filemgr fileExistsAtPath: _databasePath ] == YES)
        {
            NSLog(@"Finns det nån conn i appDelegate?");
            if(_appDelegate.openDbConnection){
                NSLog(@"Ja, stänger den");
                //Om appDelegate redan håller en connection, stäng den innan variabeln pekas om till databasen som just öppnades
                sqlite3_close(_appDelegate.openDbConnection);
            }else{
                NSLog(@"Nej.");
            }

            [filemgr removeItemAtPath: (_databasePath) error:NULL];
            NSLog(@"Raderade den befintliga databasfilen.");
        }else{
            NSLog(@"Hittade ingen fil att radera");
            if([filemgr fileExistsAtPath:[[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]]]){
                NSLog(@"Men contacts.db finns...");
            }
        }

        //Skapa ny DB-fil och prompta användaren om att DB nu kommer laddas ner
        [self connectToDbAndStoreConnectionInAppDelegate];
    }

    if([title isEqualToString:@"Fortsätt"])
    {
        //Sätt flagga i appdelegate för att synkronisering ska köras direkt när användaren kommer till secondviewcontroller
        _appDelegate.runFetchDataAutomatically = true;
        [self forceLoginUser];
    }

    if([title isEqualToString:@"Försök återställa"])
    {
        //Radera databasfilen om den finns och kopiera in säkerhetskopian i dens ställe
        NSLog(@"Användaren tryckte ''Försök återställa''.");
        NSLog(@"Försöker ta bort skarpa DB...");
        NSString *docsDir;
        NSArray *dirPaths;
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        docsDir = [dirPaths objectAtIndex:0];
        // Build the path to the database file
        NSString * _databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
        NSFileManager *filemgr = [NSFileManager defaultManager];

        if ([filemgr fileExistsAtPath: _databasePath ] == YES)
        {
            [filemgr removeItemAtPath: (_databasePath) error:NULL];
            NSLog(@"Raderade den befintliga databasfilen.");
        }else{
            NSLog(@"Hittade ingen fil att radera");
            if([filemgr fileExistsAtPath:[[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]]]){
                NSLog(@"Men contacts.db finns...");
            }
        }

        NSLog(@"Försöker kopiera säkerhetskopian till skarp databas...");

        NSString * _databaseSafeCopyPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ohbsysApp_safecopy.db"]];
        if ([filemgr fileExistsAtPath: _databaseSafeCopyPath ] == YES){
            [filemgr copyItemAtPath:_databaseSafeCopyPath toPath:_databasePath error:NULL];
            NSLog(@"Säkerhetskopian återställdes.");

        }else{
            NSLog(@"Hittade ingen säkerhetskopia.");

            NSString *alertString = [NSString stringWithFormat:@"Ingen säkerhetskopia hittades"];

            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                        message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar."
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
           //We add buttons to the alert controller by creating UIAlertActions:
           UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Fortsätt"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction * _Nonnull action) {
                                                                [self okButtonPressed: @"Fortsätt"];
                                                            }]; //You can use a block here to handle a press on this button
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                 }];
            
            [alertController addAction:actionCancel];
               [alertController addAction:actionOk];
            
           [self presentViewController:alertController animated:YES completion:nil];
            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Din iPad kommer nu ladda ner databasen från servern. När det är klart måste du logga ut och sedan logga in igen.\n\nTryck inte på något förrän iPad har laddat klart och du ser listan över arbetsordrar." delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Fortsätt", nil];
//            [alert show];
        }
    }
   //write your implementation for ok button here
}



//Get SHA-256 hash of the inputted password
-(NSString*) sha256:(NSString *)clear{
    const char *s=[clear cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, (CC_LONG) keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash=[out description];
    hash = [hash stringByReplacingOccurrencesOfString:@" " withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@"<" withString:@""];
    hash = [hash stringByReplacingOccurrencesOfString:@">" withString:@""];
    return hash;
}

-(NSData*) sha256N:(NSString *)clear{
    const char *s=[clear cStringUsingEncoding:NSASCIIStringEncoding];
    NSData *keyData=[NSData dataWithBytes:s length:strlen(s)];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH]={0};
    CC_SHA256(keyData.bytes, (CC_LONG) keyData.length, digest);
    NSData *out=[NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    return out;
}

-(void) loginUser{
	NSLog(@"loginuser start");

	sqlite3_stmt *statement;
	
    
    if(_appDelegate.openDbConnection)
	{
		/*DOLD*///NSLog(@"Databasen öppnad - viewDidLoad - sys_formtables, för att fylla reportFieldsStr");
        //Change
        NSData *rawData = [self sha256N:_password.text];
        NSUInteger capacity = rawData.length * 2;
        NSMutableString *sbuf = [NSMutableString stringWithCapacity:capacity];
        const unsigned char *buf = rawData.bytes;
        NSInteger i;
        for (i=0; i<rawData.length; ++i) {
            [sbuf appendFormat:@"%02lX", (unsigned long)buf[i]];
        }
        NSString *lower = [sbuf lowercaseString];
        //
		NSString *beginSQL = [[NSString alloc] init];
        
//		beginSQL = [NSString stringWithFormat:@"SELECT * FROM sys_users WHERE username = '%@' AND (password = '%@' OR password = '%@')", _username.text, _password.text, [self sha256:_password.text]];
        beginSQL = [NSString stringWithFormat:@"SELECT * FROM sys_users WHERE username = '%@' AND (password = '%@' OR password = '%@')", _username.text, _password.text, lower];
		NSLog(@"SQL: %@", beginSQL);
        
		const char *begin_stmt = [beginSQL UTF8String];
		sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
        NSLog(@"error1: %d",sqlite3_errcode(_appDelegate.openDbConnection));
        NSLog(@"error1 extended: %d",sqlite3_extended_errcode(_appDelegate.openDbConnection));
		if(sqlite3_step(statement) == SQLITE_ROW) {
			
			NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
			NSString * myTempMutStr = [[NSMutableString alloc] init];
			
			//Ger antalet columner för select-statement "pStmt"
			int colCount = sqlite3_column_count(statement);
			
			int i;
			for (i=0; i<colCount;i++) {
				//Ger namnet på kolumn N
				const char *colName = (const char*)sqlite3_column_name(statement, i);
				char *colValue = (char *)sqlite3_column_text(statement, i);
				if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"nil";}
				(void)[tempDictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
				myTempMutStr = nil;
				//NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [tempDictionary count]);
			}
			
			
			_loggedInUser = [tempDictionary valueForKey:@"username"];
            
            // Spara senaste inloggade användarnamnet i rutan
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setObject:_loggedInUser forKey:@"lastuser"];
            [defaults synchronize]; // this method is optional
            
            _appDelegate.user = _loggedInUser;
            _appDelegate.userpassword = _password.text;
			_appDelegate.userId = [NSNumber numberWithInt:[[tempDictionary valueForKey:@"id"] intValue]];
			_appDelegate.userRole = [NSNumber numberWithInt:[[tempDictionary valueForKey:@"role"] intValue]];
			_appDelegate.userDictionary = tempDictionary;
			
            
            [_feedbackLabel setTextColor:[UIColor colorWithRed:0 green:0.65 blue:0.13 alpha:1]];
            [_feedbackLabel setText:@"Välkommen!"];
            //[_password setText:@""];
            [_password resignFirstResponder];
            
			[self performSegueWithIdentifier:@"loginSegue" sender:self];
			NSLog(@"Korrekt användarnamn/lösenord");
			
			NSLog(@"\nKollar appdelegate's variabler: \nuser: %@\nuserid: %@\nuserrole: %@\nuserDict: %@",_appDelegate.user, _appDelegate.userId, _appDelegate.userRole, _appDelegate.userDictionary);
        }else{
            NSLog(@"error2: %d",sqlite3_errcode(_appDelegate.openDbConnection));
            NSLog(@"error2 extended: %d",sqlite3_extended_errcode(_appDelegate.openDbConnection));
            
            [_feedbackLabel setTextColor:[UIColor colorWithRed:0.8 green:0.15 blue:0 alpha:1]];
            [_feedbackLabel setText:@"Fel användarnamn eller lösenord."];
            
            NSLog(@"Försökte logga in");
        }
        /* 
        if([[_password text] length] > 0){
            
            if ([_password.text isEqualToString:@"ho"] || [_password.text isEqualToString:@"sj"]) {
                [_feedbackLabel setTextColor:[UIColor blackColor]];
                [_feedbackLabel setText:@"Välkommen!"];
            }else{
                [_feedbackLabel setTextColor:[UIColor redColor]];
                [_feedbackLabel setText:@"Felaktigt användarnamn eller lösenord"];
            }

        //[_password setText:@""];
        //[_password becomeFirstResponder];

        }
        */

		begin_stmt = nil;
		beginSQL = nil;
		sqlite3_finalize(statement);
		
	} else {
        [self connectToDbAndStoreConnectionInAppDelegate];
        
		NSLog(@"Failed to open/create database");
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  signBezierPath.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-23.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLineDrawingView.h"
#import "AppDelegate.h"
#import <sqlite3.h>

@interface signBezierPath : UIViewController

@property(nonatomic,retain) IBOutlet UIImageView *signImageView;
@property(nonatomic,retain) NSString *FieldColName;
@property(nonatomic,retain) UIImage *BackgroundImage;
@property(nonatomic,retain) MyLineDrawingView *DrawingView;



-(IBAction)startOver:(id)sender;
-(IBAction)saveSignature:(id)sender;
-(IBAction)popView:(id)sender;
-(IBAction)setColor1:(id)sender;
-(IBAction)setColor2:(id)sender;
-(IBAction)setColor3:(id)sender;
-(IBAction)setColor4:(id)sender;
-(IBAction)setColor5:(id)sender;
-(IBAction)setColor6:(id)sender;
-(IBAction)setColor7:(id)sender;


-(IBAction)undo:(id)sender;
-(IBAction)redo:(id)sender;


@end
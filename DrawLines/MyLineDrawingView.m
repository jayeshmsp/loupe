//
//  MyLineDrawingView.m
//  DrawLines
//
//  Created by Reetu Raj on 11/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MyLineDrawingView.h"
#import "AppDelegate.h"


@implementation MyLineDrawingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        BOOL drawOnImage = false;
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if(appDelegate.imageToDrawOn != nil){
            drawOnImage = true;
        }
        
        if (drawOnImage != true) {
            self.backgroundColor=[UIColor whiteColor];
        }else{
            self.backgroundColor=[UIColor colorWithWhite:0 alpha:0];
        }
        
        linesArr = [[NSMutableArray alloc] init];
        colorsArr = [[NSMutableArray alloc] init];
        redoLinesArr = [[NSMutableArray alloc] init];
        redoColorsArr = [[NSMutableArray alloc] init];

        myPath=[[UIBezierPath alloc]init];
        myPath.lineCapStyle=kCGLineCapRound;
        myPath.miterLimit=0;
        myPath.lineWidth=5;
        if (drawOnImage != true) {
            brushPattern=[UIColor blackColor];
        }else{
            brushPattern=[UIColor redColor];
        }

        if (drawOnImage != true) {
            // Draw base line so the customer knows where to write
            myPath_line=[[UIBezierPath alloc]init];
            myPath_line.lineCapStyle=kCGLineCapRound;
            myPath_line.miterLimit=0;
            myPath_line.lineWidth=2;
            brushPattern=[UIColor blackColor];
        
            [myPath_line moveToPoint:CGPointMake(   100.0, 200.0)];
            [myPath_line addLineToPoint:CGPointMake(668.0, 200.0)];
        }

        [self setNeedsDisplay];
        
    }
    return self;
}

-(IBAction)setImage:(UIImage *)image{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 917)];
    imgView.image = image;
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:imgView];
    
    self.backgroundColor=[UIColor blackColor];
    myPath=[[UIBezierPath alloc]init];
    myPath.lineCapStyle=kCGLineCapRound;
    myPath.miterLimit=0;
    myPath.lineWidth=5;
    brushPattern=[UIColor redColor];
    
    // Draw base line so the customer knows where to write
    myPath_line=[[UIBezierPath alloc]init];
    myPath_line.lineCapStyle=kCGLineCapRound;
    myPath_line.miterLimit=0;
    myPath_line.lineWidth=2;
    brushPattern=[UIColor blackColor];
    
    //[myPath_line moveToPoint:CGPointMake(   100.0, 200.0)];
    //[myPath_line addLineToPoint:CGPointMake(668.0, 200.0)];
    [self setNeedsDisplay];
}

-(IBAction)setBrushColor:(UIColor *)brushColor{
    brushPattern = brushColor;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    for(NSInteger i=0;i<[linesArr count];i++){
        [[colorsArr objectAtIndex:i] setStroke];
        [[linesArr objectAtIndex:i] strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
    }

    [brushPattern setStroke];

    [myPath strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
    [myPath_line strokeWithBlendMode:kCGBlendModeNormal alpha:1.0];
    // Drawing code
    //[myPath stroke];
}

#pragma mark - Touch Methods
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
    [myPath moveToPoint:[mytouch locationInView:self]];
    
}
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
    [myPath addLineToPoint:[mytouch locationInView:self]];
    [self setNeedsDisplay];
    
}
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{

    UITouch *mytouch=[[touches allObjects] objectAtIndex:0];
    [myPath addLineToPoint:[mytouch locationInView:self]];

    // Spara linje och färg i array
    [linesArr addObject:[myPath copyWithZone:nil]];
    [colorsArr addObject:[brushPattern copyWithZone:nil]];

    // Initiera ny linje med vald färg
    myPath=[[UIBezierPath alloc]init];
    myPath=[[UIBezierPath alloc]init];
    myPath.lineCapStyle=kCGLineCapRound;
    myPath.miterLimit=0;
    myPath.lineWidth=5;

    //Förverka redo-arrayen när man ritat
    [redoLinesArr removeAllObjects];
    [redoColorsArr removeAllObjects];
    
    [self setNeedsDisplay];
}

-(void)undo{
    //flytta sista elementet i linesArr och colorsArr till undoLinesArr och undoColorsArr
    if([linesArr count]>0 && [colorsArr count] > 0){
        [redoLinesArr addObject:[[linesArr lastObject] copyWithZone:nil]];
        [redoColorsArr addObject:[[colorsArr lastObject] copyWithZone:nil]];
        [linesArr removeLastObject];
        [colorsArr removeLastObject];
    }
    
    [self setNeedsDisplay];
}
-(void)redo{
    if([redoLinesArr count]>0 && [redoColorsArr count] > 0){
        [linesArr addObject:[[redoLinesArr lastObject] copyWithZone:nil]];
        [colorsArr addObject:[[redoColorsArr lastObject] copyWithZone:nil]];
        [redoLinesArr removeLastObject];
        [redoColorsArr removeLastObject];
    }
    
    [self setNeedsDisplay];
}

- (void)dealloc
{
    [brushPattern release];
    [super dealloc];
}

@end

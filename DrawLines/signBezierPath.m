//
//  signBezierPath.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-23.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "signBezierPath.h"
#import "MyLineDrawingView.h"

@interface UIDevice (UndocumentedFeatures)
-(void)setOrientation:(UIInterfaceOrientation)orientation animated:(BOOL)animated;
-(void)setOrientation:(UIInterfaceOrientation)orientation;
@end

@implementation signBezierPath


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
//    if (_BackgroundImage != nil){
//        self.title = @"Rita på bilden";
//        [self.view setBackgroundColor:[UIColor redColor]];
//        MyLineDrawingView *drawScreen=[[MyLineDrawingView alloc]initWithFrame:CGRectMake(0, 0, 768, 917)]; //(0, 0, 768, 917) ; (0, 64, 724, 1024)
//        [self.view addSubview:drawScreen];
//    }else{
//        self.title = @"Signera";
//        [self.view setBackgroundColor:[UIColor blackColor]];
//        MyLineDrawingView *drawScreen=[[MyLineDrawingView alloc]initWithFrame:CGRectMake(0, 270, 768, 300)]; //(0, 0, 768, 917) ; (0, 64, 724, 1024)
//        [self.view addSubview:drawScreen];
//    }

    [self startOver:self];
        
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	
/*	UIInterfaceOrientation o;
	switch ([UIDevice currentDevice].orientation) {
		case UIDeviceOrientationPortrait:
			o = UIInterfaceOrientationPortrait;
			break;
		case UIDeviceOrientationLandscapeLeft:
			o = UIInterfaceOrientationLandscapeLeft;
			break;
		case UIDeviceOrientationLandscapeRight:
			o = UIInterfaceOrientationLandscapeRight;
			break;
		default:
			break;
	}
*/	
//	[self shouldAutorotateToInterfaceOrientation:UIDeviceOrientationPortrait];
//
//	UIDevice *device = [UIDevice currentDevice];
//    UIDeviceOrientation realOrientation = device.orientation;
//
//	/*
//	bool animated = YES;
//	*/
//
//    if ([self shouldAutorotateToInterfaceOrientation:realOrientation]) {
//		/*
//        if (realOrientation != [UIApplication sharedApplication].statusBarOrientation) {
//
//            // Resetting the orientati	on will trigger the application to rotate
//            if ([device respondsToSelector:@selector(setOrientation:animated:)]) {
//                [device setOrientation:realOrientation animated:animated];
//            } else {
//                // Yes if Apple changes the implementation of this undocumented setter,
//                // we're back to square one.
//            }
//        }
//		//*/
//    } else if ([self shouldAutorotateToInterfaceOrientation:UIInterfaceOrientationPortrait]) {
//        /*
//		if ([device respondsToSelector:@selector(setOrientation:animated:)]) {
//
//            // Then set the desired orientation
//            [device setOrientation:UIDeviceOrientationPortrait animated:animated];
//
//            // And set the real orientation back, we don't want to truly mess with the iPhone's balance system.
//            // Because the view does not rotate on this orientation, it won't influence the app visually.
//            [device setOrientation:realOrientation animated:animated];
//        }
//		 */
//    }
}

//-(void)setColorTo:(UIColor*)newColor{
//    for(UIView *view in [self.view subviews])
//    {
//        if([view isKindOfClass:[MyLineDrawingView class]]){
//            MyLineDrawingView *theDrawingView = view;
//            [theDrawingView setBrushColor: newColor];
//        }
//    }
//}

-(IBAction)setColor1:(id)sender{[self.DrawingView setBrushColor:[UIColor redColor]];}
-(IBAction)setColor2:(id)sender{[self.DrawingView setBrushColor:[UIColor yellowColor]];}
-(IBAction)setColor3:(id)sender{[self.DrawingView setBrushColor:[UIColor greenColor]];}
-(IBAction)setColor4:(id)sender{[self.DrawingView setBrushColor:[UIColor blueColor]];}
-(IBAction)setColor5:(id)sender{[self.DrawingView setBrushColor:[UIColor orangeColor]];}
-(IBAction)setColor6:(id)sender{[self.DrawingView setBrushColor:[UIColor whiteColor]];}
-(IBAction)setColor7:(id)sender{[self.DrawingView setBrushColor:[UIColor blackColor]];}


-(IBAction)undo:(id)sender{[self.DrawingView undo];}
-(IBAction)redo:(id)sender{[self.DrawingView redo];}


-(IBAction)startOver:(id)sender
{
    for(UIView *view in [self.view subviews])
    {
        if([view isKindOfClass:[MyLineDrawingView class]])
            [view removeFromSuperview];
    }
    //                                                                               X, Y, Width, Height
    //MyLineDrawingView *drawScreen=[[MyLineDrawingView alloc]initWithFrame:CGRectMake(0, 270, 768, 300)]; //(0, 64, 724, 1024)
    //[self.view addSubview:drawScreen];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if(appDelegate.imageToDrawOn != nil){
        _BackgroundImage = appDelegate.imageToDrawOn;
    }
    
    
    if (_BackgroundImage != nil){
        self.title = @"Rita på bilden";
        [self.view setBackgroundColor:[UIColor blackColor]];
        
        
        CGFloat imgHeight = _BackgroundImage.size.height;
        CGFloat imgWidth = _BackgroundImage.size.width;

        CGFloat frameHeight = _BackgroundImage.size.height;
        CGFloat frameWidth = _BackgroundImage.size.width;

        NSLog(@"Image: %fx%f; Frame: %fx%f", imgWidth, imgHeight, frameWidth, frameHeight);
        
        CGFloat frameLeftOffset = 0;
        
        if (imgWidth >= imgHeight){
            NSLog(@"imgW >= imgH");
            frameWidth = 768;
            frameHeight = imgHeight/imgWidth*768;
            frameLeftOffset = 0;
        }else{
            NSLog(@"imgW < imgH");
            frameWidth = 768;
            frameHeight = 768;
            frameLeftOffset = (768-frameWidth)/2;
        }

        NSLog(@"Frame: %fx%f; FrameLeftOffset: %f", frameWidth, frameHeight, frameLeftOffset);

        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(frameLeftOffset, 120, frameWidth, frameHeight)];
        imgView.image = _BackgroundImage;
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:imgView];
        
        MyLineDrawingView *drawScreen=[[MyLineDrawingView alloc]initWithFrame:CGRectMake(frameLeftOffset, 120, frameWidth, frameHeight)]; //(0, 0, 768, 917) ; (0, 64, 724, 1024)
        //[drawScreen setImage: _BackgroundImage];
        
        self.DrawingView = drawScreen;
        
        [self.view addSubview:drawScreen];
    }else{
        self.title = @"Signera";
        [self.view setBackgroundColor:[UIColor blackColor]];
        MyLineDrawingView *drawScreen=[[MyLineDrawingView alloc]initWithFrame:CGRectMake(0, 270, 768, 300)]; //(0, 0, 768, 917) ; (0, 64, 724, 1024)

        self.DrawingView = drawScreen;

        [self.view addSubview:drawScreen];
    }
	
	
	
}


-(IBAction)saveSignature:(id)sender
{
    //signImageView;
    
    if(_BackgroundImage != nil){
        [self performSelector:@selector(saveDrawnOnPhotoToDb)];
    }else{
        [self performSelector:@selector(saveImageToDb)];
    }
	
	
    /*
	NSString *alertString = [NSString stringWithFormat:@"Sparat!"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
	 */
    
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.imageToDrawOn = nil;
    appDelegate.imageToDrawOnFieldName = nil;
    appDelegate.imageToDrawOnFieldValue = nil;
    appDelegate.imageToDrawOnFieldSelectedIndex = nil;
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)saveImageToDb {
    
    NSLog(@"%s", __FUNCTION__);
    // UIImageWriteToSavedPhotosAlbum(drawImage.image, self, nil, nil);
    //return;
    
    //				GET IMAGE TO SAVE
    //###########################################
    //CGImageRef screen = [self snapshotViewAfterScreenUpdates:true]; //UIGetScreenImage();
    //UIImage *img = [UIImage imageWithCGImage:screen];
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0);
    
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Image cropping
    
    CGImageRef imageRef;
    
    // Cropping measures depend on screen resolution. Check for retina display:
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 540, 1536, 600));// CGRectMake(0, 64, 768, 917));
    } else {
        // non-Retina display
        //imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 334, 768, 300));// CGRectMake(0, 64, 768, 917));
        imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 270, 768, 300));// CGRectMake(0, 64, 768, 917));
    }
    
    // TODO: Adapt to iphone screen size as well...
    
    
    UIImage *img2Save = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    NSData * signatureImageData = [[NSData alloc] init];
    
    // Request to save the image to camera roll
    UIImageWriteToSavedPhotosAlbum(img2Save, self, nil, nil);
    
    
    // Write a UIImage to JPEG with minimum compression (best quality)
    // The value 'image' must be a UIImage object
    // The value '1.0' represents image compression quality as value from 0.0 to 1.0
    
    signatureImageData = UIImageJPEGRepresentation(img2Save, 0.5);
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //appDelegate.signatureImageData = signatureImageData;
    if(appDelegate.signatureImagesDict == nil){
        appDelegate.signatureImagesDict = [NSMutableDictionary new];
    }
    [appDelegate.signatureImagesDict setObject:signatureImageData forKey:_FieldColName];
    
    UIImage *image = [UIImage imageWithData:signatureImageData];
    NSLog(@"%@", image.description);
    NSLog(@"Kollar bilddata");
    /*
     //					SAVE TO DB
     //###########################################
     sqlite3 *database;
     
     //Ladda array från databasen
     NSString *docsDir;
     NSArray *dirPaths;
     // Get the documents directory
     dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     docsDir = [dirPaths objectAtIndex:0];
     // Build the path to the database file
     NSString * databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]];
     NSFileManager *filemgr = [NSFileManager defaultManager];
     
     NSLog(@"leta db");
     
     if ([filemgr fileExistsAtPath: databasePath ] == YES){
     
     // Open a connection to the database given its file path.
     if (sqlite3_open([databasePath UTF8String], &database) != SQLITE_OK) {
     // error handling...
     }
     
     // Construct the query and empty prepared statement.
     const char *sql = "INSERT INTO `my_table` (`name`, `data`) VALUES (?, ?)";
     sqlite3_stmt *statement;
     
     // Prepare the data to bind.
     NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:@"something"]);
     NSString *nameParam = @"Some name";
     
     // Prepare the statement.
     if (sqlite3_prepare_v2(database, sql, -1, &statement, NULL) == SQLITE_OK) {
     // Bind the parameters (note that these use a 1-based index, not 0).
     sqlite3_bind_text(statement, 1, nameParam);
     sqlite3_bind_blob(statement, 2, [imageData bytes], [imageData length], SQLITE_STATIC);
     // SQLITE_STATIC tells SQLite that it doesn't have to worry about freeing the binary data.
     }
     
     // Execute the statement.
     if (sqlite3_step(statement) != SQLITE_DONE) {
     // error handling...
     }
     
     // Clean up and delete the resources used by the prepared statement.
     sqlite3_finalize(statement);
     
     // Now let's try to query! Just select the data column.
     const char *selectSql = "SELECT `data` FROM `my_table` WHERE `name` = ?";
     sqlite3_stmt *selectStatement;
     
     if (sqlite3_prepare_v2(database, selectSql, -1, &selectStatement, NULL) == SQLITE_OK) {
     // Bind the name parameter.
     sqlite3_bind_text(selectStatement, 1, nameParam);
     }
     
     // Execute the statement and iterate over all the resulting rows.
     while (sqlite3_step(selectStatement) == SQLITE_ROW) {
     // We got a row back. Let's extract that BLOB.
     // Notice the columns have 0-based indices here.
     const void *blobBytes = sqlite3_column_blob(selectStatement, 0);
     int blobBytesLength = sqlite3_column_bytes(selectStatement, 0); // Count the number of bytes in the BLOB.
     NSData *blobData = [NSData dataWithBytes:blobBytes length:blobBytesLength];
     NSLog("Here's that data!\n%@", blobData);
     }
     
     // Clean up the select statement
     sqlite3_finalize(selectStatement);
     
     // Close the connection to the database.
     sqlite3_close(database);
     */
}

-(void)saveDrawnOnPhotoToDb {
    
    NSLog(@"%s", __FUNCTION__);
    // UIImageWriteToSavedPhotosAlbum(drawImage.image, self, nil, nil);
    //return;
    
    //				GET IMAGE TO SAVE
    //###########################################
    //CGImageRef screen = [self snapshotViewAfterScreenUpdates:true]; //UIGetScreenImage();
    //UIImage *img = [UIImage imageWithCGImage:screen];
    
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0);
    
    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Image cropping
    
    CGImageRef imageRef;
    
    CGFloat imgHeight = _BackgroundImage.size.height;
    CGFloat imgWidth = _BackgroundImage.size.width;
    
    CGFloat frameHeight = _BackgroundImage.size.height;
    CGFloat frameWidth = _BackgroundImage.size.width;
    CGFloat frameLeftOffset = 0;
    
    if (imgWidth >= imgHeight){
        frameWidth = 768;
        frameHeight = imgHeight/imgWidth*768;
        frameLeftOffset = 0;
    }else{
        frameWidth = imgWidth/imgHeight*768;
        frameHeight = 768;
        frameLeftOffset = (768-frameWidth)/2;
    }
    
    // Cropping measures depend on screen resolution. Check for retina display:
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        // Retina display
        
        imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(frameLeftOffset*2, 240, frameWidth*2, frameHeight*2));// CGRectMake(0, 64, 768, 917));
    } else {
        // non-Retina display
        //imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 334, 768, 300));// CGRectMake(0, 64, 768, 917));
            imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(frameLeftOffset, 120, frameWidth, frameHeight));// CGRectMake(0, 64, 768, 917));
    }
    
    // TODO: Adapt to iphone screen size as well...
    
    
    UIImage *img2Save = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    NSData * drawnOnImageData = [[NSData alloc] init];
    
    // Request to save the image to camera roll
    UIImageWriteToSavedPhotosAlbum(img2Save, self, nil, nil);
    
    
    // Write a UIImage to JPEG with minimum compression (best quality)
    // The value 'image' must be a UIImage object
    // The value '1.0' represents image compression quality as value from 0.0 to 1.0
    
    drawnOnImageData = UIImageJPEGRepresentation(img2Save, 0.5);
  
    UIImage *image = [UIImage imageWithData:drawnOnImageData];
    NSLog(@"Kollar bilddata");
    
    
    
    //Spara bild till fil
    
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    //NSLog(@"UUID: %@",uuidStringRef);
    NSString * fileName = [NSString stringWithFormat:@"%@.jpg",(__bridge NSString *)(uuidStringRef)];
    
    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
    
    //[self saveImage:image withFileName:fileName ofType:@"jpg" inDirectory:@"Documents"];
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:jpgPath atomically:YES];
    
				// Check if image was saved
				NSError *error;
				NSFileManager *fileMgr = [NSFileManager defaultManager];
				
				// Point to Document directory
				NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
				
				// Write out the contents of home directory to console
				NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
				
    
    //Uppdatera fieldvalue
    
    //Add image to field value string
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *fieldName = appDelegate.imageToDrawOnFieldName;
    NSString *fieldValue = appDelegate.imageToDrawOnFieldValue;
    
    if (appDelegate.attachImagesDict == nil) {
        appDelegate.attachImagesDict = [NSMutableDictionary new];
    }
    if ([appDelegate.attachImagesDict valueForKey:fieldName] == nil) {
        [appDelegate.attachImagesDict setValue: fieldValue forKey:fieldName];
    }
    if ([fieldValue length] > 0 ) {
        [appDelegate.attachImagesDict setValue:[NSString stringWithFormat:@"%@###;###%@###&###", [appDelegate.attachImagesDict valueForKey:fieldName], fileName] forKey:fieldName];
    }else{
        [appDelegate.attachImagesDict setValue:[NSString stringWithFormat:@"%@###&###", fileName] forKey:fieldName];
    }
    fieldValue = [appDelegate.attachImagesDict valueForKey:fieldName];
    
    //Uppdatera rapporten
  
    [appDelegate.reportValuesDelegate setValue:fieldValue forKey:fieldName];
  }


-(IBAction)popView:(id)sender{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.imageToDrawOn = nil;
    appDelegate.imageToDrawOnFieldName = nil;
    appDelegate.imageToDrawOnFieldValue = nil;
    appDelegate.imageToDrawOnFieldSelectedIndex = nil;
    
    //Change
//   [self.navigationController dismissModalViewControllerAnimated:YES];
	[[self navigationController] popViewControllerAnimated:YES];
}


//Change
//- (void)viewDidUnload
//{
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
//    // e.g. self.myOutlet = nil;
//}
//
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation {
//	//return NO;
//
//    //return UIInterfaceOrientationIsLandscape(UIInterfaceOrientationLandscapeRight);
//    return UIInterfaceOrientationIsLandscape(UIDeviceOrientationPortrait);
//}

-(void) dealloc{
    
}
-(BOOL) shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


@end

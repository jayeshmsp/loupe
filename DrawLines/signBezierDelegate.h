//
//  signBezierDelegate.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-23.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface signBezierDelegate : NSObject <UIApplicationDelegate> {
    
    UINavigationController *navigationController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) UINavigationController *navigationController;
@end
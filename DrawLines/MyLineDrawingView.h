//
//  MyLineDrawingView.h
//  DrawLines
//
//  Created by Reetu Raj on 11/05/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyLineDrawingView : UIView {
 
    UIBezierPath *myPath;
    UIBezierPath *myPath_line;
    UIColor *brushPattern;
    UIColor *newLinesColor;
    NSMutableArray *linesArr;
    NSMutableArray *colorsArr;
    NSMutableArray *redoLinesArr;
    NSMutableArray *redoColorsArr;
}

-(IBAction)setImage:(UIImage *)image;
-(IBAction)setBrushColor:(UIColor *)brushColor;

-(IBAction)undo;
-(IBAction)redo;

@end

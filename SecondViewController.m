//
//  SecondViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//


#import "SecondViewController.h"
#import "ReportDetailsViewController.h"
#import "ActionSheetPicker.h"
#import "ASIFormDataRequest.h"
#import "NSString+HTML.h"
#import "webservice.h"

@interface SecondViewController ()<UIPopoverPresentationControllerDelegate>

@end

@implementation SecondViewController{
//    __weak UIPopoverController *sendreportsPopover;
    __weak UIPopoverPresentationController *sendreportsPopover;

    //Change
//    UIAlertView *fetchReportsInProgress;
    UIAlertController *fetchReportsInProgress;
}

@synthesize tableView = _tableView;
@synthesize dirPaths = _dirPaths;
@synthesize docsDir = _docsDir;


//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
//
//    //Fråga om användaren vill maila kraschrapporten som hittades
//    if([title isEqualToString:@"Skicka nu..."])
//    {
//        [self sendLogByMail];
//    }
//    if([title isEqualToString:@"Skicka inte"]){
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
//        NSFileManager *filemgr = [NSFileManager defaultManager];
//        if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
//        {
//            [filemgr removeItemAtPath: (crashLogPath) error:NULL];
//            NSLog(@"Raderade kraschloggfilen eftersom användaren tryckte på 'skicka inte'.");
//        }
//    }
//
//    //Fråga om användaren verkligen vill synka databasen
//    if([title isEqualToString:@"Uppdatera"]){
//        NSLog(@"Anv valde Uppdatera. Kolla internetaccess och kör runFetchData.");
//
//        if([self hasInternet]){
//            NSLog(@"Användaren kan nå internet. Kör runFetchData.");
//            [self runFetchData];
//        }else{
//            NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
//            NSString *alertString = [NSString stringWithFormat:@"Kunde inte hämta de senaste rapporterna eftersom iPad inte fick kontakt med servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
//    }
//
//    //Logga ut om användare = (null)
//    if([title isEqualToString:@"Dölj"]){
//        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//        if([appDelegate.userDictionary valueForKey:@"fullname"] == nil){
//            [self logoutUser];
//        }
//    }
//
//}

- (void)alertButtonAction:(NSString *) title{
//    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    //Fråga om användaren vill maila kraschrapporten som hittades
    if([title isEqualToString:@"Skicka nu..."])
    {
        [self sendLogByMail];
    }
    if([title isEqualToString:@"Skicka inte"]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
        NSFileManager *filemgr = [NSFileManager defaultManager];
        if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
        {
            [filemgr removeItemAtPath: (crashLogPath) error:NULL];
            NSLog(@"Raderade kraschloggfilen eftersom användaren tryckte på 'skicka inte'.");
        }
    }
    
    //Fråga om användaren verkligen vill synka databasen
    if([title isEqualToString:@"Uppdatera"]){
        NSLog(@"Anv valde Uppdatera. Kolla internetaccess och kör runFetchData.");
        
        if([self hasInternet]){
            NSLog(@"Användaren kan nå internet. Kör runFetchData.");
            [self runFetchData];
        }else{
            NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
            NSString *alertString = [NSString stringWithFormat:@"Kunde inte hämta de senaste rapporterna eftersom iPad inte fick kontakt med servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertHeader
                                                                                        message:alertString
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                 }];
            
            [alertController addAction:actionCancel];
            
           [self presentViewController:alertController animated:YES completion:nil];
        }
    }
    
    //Logga ut om användare = (null)
    if([title isEqualToString:@"Dölj"]){
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        if([appDelegate.userDictionary valueForKey:@"fullname"] == nil){
            [self logoutUser];
        }
    }

}

NSMutableArray * getColoredFieldsCache;
- (NSMutableArray *) getColoredFields {
    if (getColoredFieldsCache == nil) {
        if (_appDelegate.openDbConnection)
        {
            sqlite3_shutdown();
                sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                sqlite3_initialize();
            sqlite3_stmt *statement;
            NSMutableArray * colorFieldArray = [[NSMutableArray alloc] initWithCapacity:0];
            
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT id, sortorder, fieldcolname, fieldcolor, fieldtype FROM prot_arbetsorder_desc WHERE active = 1 AND fieldcolor NOT NULL ORDER BY sortorder DESC"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            
            
            int colCount = sqlite3_column_count(statement);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                /*DOLD*///NSLog(@"while(sqlite3_step(statement) == SQLITE_ROW); SQLite errorkod 2: %d", sqlite3_errcode(_appDelegate.openDbConnection));
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int j = 0;
                for (j=0; j<colCount;j++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, j);
                    char *colValue = (char *)sqlite3_column_text(statement, j);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    
                    //Undvik att fylla dictionary med nil
                    if (myTempMutStr) {
                        //Alla values passerar myTempMutStr här. Gör globala ersättningar här.
                        (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    }
                    myTempMutStr = nil;
                }
                [colorFieldArray addObject:[dictionary mutableCopy]];
                dictionary = nil;
            }
            getColoredFieldsCache = colorFieldArray;
            return colorFieldArray;
        }else{
            return nil;
        }
    }else{
        return getColoredFieldsCache;
    }
}

- (void)toggleViewAll{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString * loggedInUser = appDelegate.user;     //[NSString stringWithFormat:@"Lars"];

    if([_filterOnUsername isEqualToString:@"viewAll"]){
        _filterOnUsername = [NSMutableString stringWithString:loggedInUser];
    }else{
        _filterOnUsername = [NSMutableString stringWithString:@"viewAll"];
    }
    
    [self updateTable];
}

- (void)updateDatabase
{
    NSString *alertString = [NSString stringWithFormat:@"Uppdatera databas"];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Vill du hämta de senaste rapporterna från servern?") delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Uppdatera",nil];
//    [alert show];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                message:(@"Vill du hämta de senaste rapporterna från servern?")
                                                                         preferredStyle:UIAlertControllerStyleAlert];
   //We add buttons to the alert controller by creating UIAlertActions:
   UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Uppdatera"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
       
                                                        [self alertButtonAction:@"Uppdatera"];
       
                                                    }]; //You can use a block here to handle a press on this button
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                       style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                                         }];
    
    [alertController addAction:actionCancel];
       [alertController addAction:actionOk];
    
   [self presentViewController:alertController animated:YES completion:nil];
    
/*
    if([self hasInternet]){
        NSString *alertString = [NSString stringWithFormat:@"Uppdatera databas"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Vill du hämta de senaste rapporterna från servern?") delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"Uppdatera",nil];
        [alert show];
        
        NSLog(@"Knappen 'Uppdatera DB' trycktes ned. Visar prompt 'Vill du synkronisera?' 'Uppdatera'/'Avbryt'");
    }else{
        NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
        NSString *alertString = [NSString stringWithFormat:@"Kunder inte hämta de senaste rapporterna eftersom iPad inte fick kontakt med servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
 */
}


- (void)updateTable{
    //Ange vilken användare som är inloggad (för SQL-select)
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  
    NSString * loggedInUser = appDelegate.user;
    NSString * loggedInUserFullName = [appDelegate.userDictionary valueForKey:@"fullname"];
    
    //Rensa färg-cachen, så att ändringar i databasen återspeglas direkt
    if(getColoredFieldsCache != nil) {
        getColoredFieldsCache = nil;
    }
    
    [self.tableView setSeparatorColor: [self colorWithHexString:@"999999"]];
    
    if([_filterOnUsername isEqualToString:@"viewAll"]){
        self.title = @"Visar alla arbetsordrar";
        self.navigationController.navigationBar.tintColor = [UIColor brownColor];
    }else if([_filterOnUsername isEqualToString:@"usersToday"]){
        self.title = @"Visar användarens arbetsordrar för idag";
        self.navigationController.navigationBar.tintColor = [UIColor brownColor];
    }else if([_filterOnUsername isEqualToString:@"usersAlphabetical"]){
        self.title = @"Visar användarens arbetsordrar alfabetiskt";
        self.navigationController.navigationBar.tintColor = [UIColor brownColor];
    }else{
        self.title = [NSString stringWithFormat:@"Arbetsordrar för %@", loggedInUserFullName];
        self.navigationController.navigationBar.tintColor = [UIColor grayColor];
    }
    
    /*DOLD*///NSLog(@"Uppdaterar tabellen");
    
    if (_appDelegate.openDbConnection)
    {
        NSMutableArray * daysArray = [[NSMutableArray alloc] initWithCapacity:0];
        NSMutableArray * reportsPerDayArray = [[NSMutableArray alloc] initWithCapacity:0];
        NSString * previousDate = [[NSString alloc] init];
        sqlite3_shutdown();
            sqlite3_config(SQLITE_CONFIG_SERIALIZED);
            sqlite3_initialize();
        sqlite3_stmt *statement;
        NSString *beginSQL = [[NSString alloc] init];
        
        NSMutableArray * coloredFields = [self getColoredFields];
        NSString * coloredFieldsStr = @"";
        for (int idx = 0; idx<[coloredFields count]; idx++) {
            coloredFieldsStr = [coloredFieldsStr stringByAppendingString:@", "];
            coloredFieldsStr = [coloredFieldsStr stringByAppendingString:[[coloredFields objectAtIndex:idx] valueForKey:@"fieldcolname"]];
        }
        
        if([_filterOnUsername isEqualToString:@"viewAll"]){
            beginSQL = [NSString stringWithFormat: @"SELECT prot_arbetsorder.id, projekt || '  –  ' || gatuadress as gatuadress, gatuadress as gatuadress2, postnummer, stad, ordermottagare, avtaladtid_1, notis%@ FROM prot_arbetsorder INNER JOIN sys_users ON ansvarigbesiktningsman_1=sys_users.id ORDER BY prot_arbetsorder.avtaladtid_1 DESC;", coloredFieldsStr];
            NSLog(@"%@", beginSQL);
            
            
            
        }else if([_filterOnUsername isEqualToString:@"usersToday"]){
            NSDate* date = [NSDate date];
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            NSTimeZone *destinationTimeZone = [NSTimeZone systemTimeZone];
            formatter.timeZone = destinationTimeZone;
            [formatter setDateStyle:NSDateFormatterLongStyle];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSString* dateString = [formatter stringFromDate:date];
            
            beginSQL = [NSString stringWithFormat: @"SELECT prot_arbetsorder.id, projekt || '  –  ' || gatuadress as gatuadress, gatuadress as gatuadress2, postnummer, stad, ordermottagare, avtaladtid_1, notis%@ FROM prot_arbetsorder INNER JOIN sys_users ON ansvarigbesiktningsman_1=sys_users.id WHERE username = '%@' AND avtaladtid_1 LIKE '%@%%' ORDER BY prot_arbetsorder.avtaladtid_1 DESC;", coloredFieldsStr, [loggedInUser lowercaseString], dateString];
            
            
            
        }else if([_filterOnUsername isEqualToString:@"usersAlphabetical"]){
            beginSQL = [NSString stringWithFormat: @"SELECT prot_arbetsorder.id, projekt || '  –  ' || gatuadress as gatuadress, gatuadress as gatuadress2, postnummer, stad, ordermottagare, avtaladtid_1, notis%@ FROM prot_arbetsorder INNER JOIN sys_users ON ansvarigbesiktningsman_1=sys_users.id WHERE username = '%@' ORDER BY gatuadress ASC;", coloredFieldsStr, [loggedInUser lowercaseString]];
            
            
            
        }else{
            beginSQL = [NSString stringWithFormat: @"SELECT prot_arbetsorder.id, projekt || '  –  ' || gatuadress as gatuadress, gatuadress as gatuadress2, postnummer, stad, ordermottagare, avtaladtid_1, notis%@ FROM prot_arbetsorder INNER JOIN sys_users ON ansvarigbesiktningsman_1=sys_users.id WHERE username = '%@' ORDER BY prot_arbetsorder.avtaladtid_1 DESC;", coloredFieldsStr, [loggedInUser lowercaseString]];
        }
        
        
        
        /*#dolt#*////*DOLD*///NSLog(@"#################################################");
        /*#dolt#*////*DOLD*///NSLog(@"#################################################");
        /*#dolt#*////*DOLD*///NSLog(@"beginSQL för arbetsorder-listan: %@", beginSQL);
        /*#dolt#*////*DOLD*///NSLog(@"#################################################");
        /*#dolt#*////*DOLD*///NSLog(@"#################################################");
        
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
        
        //Ger antalet columner för select-statement "pStmt"
        int colCount = sqlite3_column_count(statement);
        /*#dolt#*////*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
        
        while(sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *tempDictionary = [NSMutableDictionary dictionary];
            NSString * myTempMutStr = [[NSMutableString alloc] init];
            
            
            int j = 0;
            for (j=0; j<colCount;j++) {
                //Ger namnet på kolumn N
                const char *colName = (const char*)sqlite3_column_name(statement, j);
                char *colValue = (char *)sqlite3_column_text(statement, j);
                if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                (void)[tempDictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                myTempMutStr = nil;
                /*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [tempDictionary count]);
            }
            
            NSString *thisDate;
            if([tempDictionary valueForKey:@"avtaladtid_1"]){
                if([[tempDictionary valueForKey:@"avtaladtid_1"] length]>=10){
                    thisDate= [[tempDictionary valueForKey:@"avtaladtid_1"] substringToIndex:11];
                }
            }
            /*DOLD*///NSLog(@"############################################################################## thisdate är: %@",thisDate);
            
            if (![thisDate isEqualToString:previousDate]) {
                /*DOLD*///NSLog(@"Nytt datum! %@", thisDate);
                if([reportsPerDayArray count]>0){
                    [daysArray addObject:[reportsPerDayArray mutableCopy]];
                    /*DOLD*///NSLog(@"Lägger till ihopsamlade reportsPerDayArray (%d st) i daysArray. Count: %d",[reportsPerDayArray count], [daysArray count]);
                    [reportsPerDayArray removeAllObjects];
                    /*DOLD*///NSLog(@"Tömmer reportsPerDay. Count: %d", [reportsPerDayArray count]);
                }
                [reportsPerDayArray addObject:[tempDictionary mutableCopy]];
                /*DOLD*///NSLog(@"Lägger till dictionary(id: %@) i en ny, tom och fräsch reportsPerDayArray. Count: %d", [tempDictionary valueForKey:@"id"],[reportsPerDayArray count]);
            }else{
                /*DOLD*///NSLog(@"Samma datum! %@", thisDate);
                [reportsPerDayArray addObject:[tempDictionary mutableCopy]];
                /*DOLD*///NSLog(@"Lägger till dictionary(id: %@) i reportsPerDayArray. Count: %d", [tempDictionary valueForKey:@"id"], [reportsPerDayArray count]);
            }
            previousDate = thisDate;
        }
        if([reportsPerDayArray count]>0){
            [daysArray addObject:[reportsPerDayArray mutableCopy]];
            /*DOLD*///NSLog(@"Lägger till sista omgången av ihopsamlade reportsPerDayArray (%d st) i daysArray. Count: %d",[reportsPerDayArray count], [daysArray count]);
        }
        
        
        
        
        while(sqlite3_step(statement) == SQLITE_ROW) {
            
            /* Oklart när denna while-sats körs/används? Men variabeln reportsPerDayArray används ovan */
            
            NSLog(@"Loopar igenom databasens innehåll för att lägga till i tabellen");
            
            
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            
            char *col1 = (char *)sqlite3_column_text(statement, 0);
            if (col1 !=NULL){
                //Alla values passerar thisTempString här. Gör globala ersättningar här.
                NSString * thisTempString = [NSString stringWithUTF8String: col1];
                [dictionary setObject:[thisTempString stringByDecodingHTMLEntities] forKey:@"id"];
            }
            char *col2 = (char *)sqlite3_column_text(statement, 1);
            if (col2 !=NULL){
                //Alla values passerar thisTempString här. Gör globala ersättningar här.
                NSString * thisTempString = [NSString stringWithUTF8String: col2];
                [dictionary setObject:[thisTempString stringByDecodingHTMLEntities] forKey:@"name"];
            }else{
                [dictionary setObject:@"" forKey:@"name"];
            }
            char *col3 = (char *)sqlite3_column_text(statement, 2);
            if (col3 !=NULL){
                //Alla values passerar thisTempString här. Gör globala ersättningar här.
                NSString * thisTempString = [NSString stringWithUTF8String: col3];
                [dictionary setObject:[thisTempString stringByDecodingHTMLEntities] forKey:@"adress"];
            }
            char *col4 = (char *)sqlite3_column_text(statement, 3);
            if (col4 !=NULL){
                //Alla values passerar thisTempString här. Gör globala ersättningar här.
                NSString * thisTempString = [NSString stringWithUTF8String: col4];
                [dictionary setObject:[thisTempString stringByDecodingHTMLEntities] forKey:@"postnr"];
            }
            char *col5 = (char *)sqlite3_column_text(statement, 4);
            if (col5 !=NULL){
                //Alla values passerar thisTempString här. Gör globala ersättningar här.
                NSString * thisTempString = [NSString stringWithUTF8String: col5];
                [dictionary setObject:[thisTempString stringByDecodingHTMLEntities] forKey:@"stad"];
            }
            NSLog(@"%@",dictionary);
            [reportsPerDayArray addObject:dictionary];
            
        }
        
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            /*#dolt#*////*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
        } else {
            /*#dolt#*////*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
        }
        statement = nil;
        beginSQL = nil;
        sqlite3_finalize(statement);
         
        /*
        NSLog(@"Skapar datalista för arbetsorder-listan:");
        NSLog(@"%@",daysArray);
        //*/
        
        _allSelectedReportsGroupedByDay = daysArray;
        _numberOfSections = [NSMutableString stringWithFormat:@"%lu",(unsigned long)[daysArray count]];
        //*/
        /*#dolt#*////*DOLD*///NSLog(@"_allSelectedReportsGroupedByDay count: %d", [_allSelectedReportsGroupedByDay count]);
        /*#dolt#*////*DOLD*///NSLog(@"daysArray count: %d", [daysArray count]);
        /*#dolt#*////*DOLD*///NSLog(@"reportsPerDay count: %d", [reportsPerDayArray count]);
        
        ///*#dolt#*////*DOLD*///NSLog(@"################ Värde 1: %@",[[[_allSelectedReportsGroupedByDay objectAtIndex:0] objectAtIndex:0] valueForKey:@"gatuadress"]);
        
        [self.tableView reloadData];
    }
}

- (void)updateDbInfoBar {
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" UPPDATERA SENAST-UPPDATERAD-DATUM ");
    /*DOLD*///NSLog(@"#################################");
    
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    sqlite3_shutdown();
        sqlite3_config(SQLITE_CONFIG_SERIALIZED);
        sqlite3_initialize();
    sqlite3_stmt *statement;
    
    
    NSMutableDictionary * updated = [[NSMutableDictionary alloc] init];
    if (_appDelegate.openDbConnection)
    {
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_dbsyncinfo ORDER BY id DESC LIMIT 1"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
        
        //Ger antalet kolumner för select-statement "pStmt"
        int colCount = sqlite3_column_count(statement);
        
        while(sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            NSString * myTempMutStr = [[NSMutableString alloc] init];
            
            int j = 0;
            for (j=0; j<colCount;j++) {
                //Ger namnet på kolumn N
                const char *colName = (const char*)sqlite3_column_name(statement, j);
                
                char *colValue = (char *)sqlite3_column_text(statement, j);
                if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                myTempMutStr = nil;
                /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
            }
            updated = dictionary;
            
            _updatedLabel.text = [NSString stringWithFormat:@"Databasen uppdaterades senast %@",[updated valueForKey:@"updated"]];
        }
        beginSQL = nil;
        begin_stmt = nil;
        sqlite3_finalize(statement);
         
    }
    
    NSMutableArray * poststosend = [[NSMutableArray alloc] init];
    if (_appDelegate.openDbConnection)
    {
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
        
        //Ger antalet kolumner för select-statement "pStmt"
        int colCount = sqlite3_column_count(statement);
        
        while(sqlite3_step(statement) == SQLITE_ROW) {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            NSString * myTempMutStr = [[NSMutableString alloc] init];
            
            int j = 0;
            for (j=0; j<colCount;j++) {
                //Ger namnet på kolumn N
                const char *colName = (const char*)sqlite3_column_name(statement, j);
                
                char *colValue = (char *)sqlite3_column_text(statement, j);
                if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                myTempMutStr = nil;
                /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
            }
            [poststosend addObject:dictionary];
        }
        beginSQL = nil;
        begin_stmt = nil;
        sqlite3_finalize(statement);
         
    }
    
    
    //Ta bort dubletter från poststosendArr
    NSArray* originalArray = poststosend;
    NSMutableSet* existingNames = [NSMutableSet set];
    NSMutableArray* filteredArray = [NSMutableArray array];
    for (id object in originalArray) {
        if (![existingNames containsObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]]) {
            [existingNames addObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]];
            [filteredArray addObject:object];
        }
    }
    
    poststosend = filteredArray;
    
    _appDelegate.postsToSend = poststosend;
    
    if([poststosend count] == 0){
        _poststosendLabel.text = [NSString stringWithFormat:@"Inga rapporter behöver laddas upp till servern."];
    }else if([poststosend count] == 0){
        _poststosendLabel.text = [NSString stringWithFormat:@"%lu rapport har ändrats med inte laddats upp till servern.",(unsigned long)[poststosend count]];
    }else{
    _poststosendLabel.text = [NSString stringWithFormat:@"%lu rapporter har ändrats med inte laddats upp till servern.",(unsigned long)[poststosend count]];
    }
   
    /*DOLD*///NSLog(@"#################################");
    /*DOLD*///NSLog(@" SLUT PÅ UPPDATERA SENAST-UPPDATERAD-DATUM ");
    /*DOLD*///NSLog(@"#################################");
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSLog(@"SecondVC PrepareForSegue...");
    if ([segue.identifier isEqualToString:@"showReportDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ReportDetailsViewController *destViewController = segue.destinationViewController;
        if(isFiltered == TRUE){
            destViewController.workOrder = [[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"id"];
        }else{
            destViewController.workOrder = [[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"id"];
        }
        destViewController.templateid = [NSNumber numberWithInt:0];
        destViewController.editMode = [NSNumber numberWithInt:0];
        /*#dolt#*////*DOLD*///NSLog(@"PrepareForSegue: %@",[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"id"]);
    }
    if ([segue.identifier isEqualToString:@"showSendreportsPopover"]) {
        
        NSLog(@"PrepForSeg för showSendreportsPopover");
//        sendreportsPopover = [(UIStoryboardPopoverSegue *)segue popoverController];
        UIViewController *dvc = segue.destinationViewController;
        sendreportsPopover = dvc.popoverPresentationController;
        if (sendreportsPopover) {
             if ([sender isKindOfClass:[UIButton class]]) { // Assumes the popover is being triggered by a UIButton
                 sendreportsPopover.sourceView = (UIButton *)sender;
                 sendreportsPopover.sourceRect = [(UIButton *)sender bounds];
             }
            sendreportsPopover.delegate = self;
         }
    }
        
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(isFiltered==TRUE){
        return 1;
    }else{
        /*DOLD*///NSLog(@"numberOfSections: %d",[_allSelectedReportsGroupedByDay count]);
        return [_allSelectedReportsGroupedByDay count];
    }
}



- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(isFiltered==TRUE){
        return [NSString stringWithFormat:@"Sökresultat"];
    }else{
        NSString * headerTitle = [[NSString alloc] init];
        if([[_allSelectedReportsGroupedByDay objectAtIndex:section] count]>0){
            if([[[_allSelectedReportsGroupedByDay objectAtIndex:section] objectAtIndex:0] valueForKey:@"avtaladtid_1"]){
                if([[[[_allSelectedReportsGroupedByDay objectAtIndex:section] objectAtIndex:0] valueForKey:@"avtaladtid_1"] length]>=10){
                    headerTitle = [[[[_allSelectedReportsGroupedByDay objectAtIndex:section] objectAtIndex:0] valueForKey:@"avtaladtid_1"] substringToIndex:11];
                }
            }
        }
        /*DOLD*///NSLog(@"headertitle: %@",headerTitle);
        return [NSString stringWithFormat:@"%@", headerTitle];
    }
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(isFiltered==TRUE){
        if([_allFilteredReports count]>0){
            return [_allFilteredReports count];
        }else{
            return 1;
        }
    }else{
        /*DOLD*///NSLog(@"numberOfRowsInSection: %d",[[_allSelectedReportsGroupedByDay objectAtIndex:section] count]);
        return [[_allSelectedReportsGroupedByDay objectAtIndex:section] count];
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"RecipeCell";
    UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];

    if(isFiltered==TRUE){
        if([_allFilteredReports count]>0){
            
            /*#dolt#*////*DOLD*///NSLog(@"Sektion: %ld Rad: %ld",(long)indexPath.section, (long)indexPath.row);
            //NSLog(@"%@", [_allFilteredReports objectAtIndex:indexPath.row]);
            
            NSString * thisId = [[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"id"];
            NSString * thisAdr = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"gatuadress2"] stringByDecodingHTMLEntities];
            NSString * thisAdr2 = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"gatuadress"] stringByDecodingHTMLEntities];
            NSString * thisAdrLen = [NSString stringWithFormat:@"%lu", (unsigned long)[thisAdr length]];
            NSLog(@"%@", thisAdrLen);
            if(![thisAdr2 isEqualToString: @""]) {
                thisAdr = thisAdr2;
            }
            NSString * thisStad = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"stad"] stringByDecodingHTMLEntities];
            NSString * thisPostnr = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"postnummer"] stringByDecodingHTMLEntities];
            //NSString * thisName = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"ordermottagare"] stringByDecodingHTMLEntities];
            NSString * thisTime = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"avtaladtid_1"] stringByDecodingHTMLEntities];
            NSString * thisSubtitle = [[[_allFilteredReports objectAtIndex:indexPath.row] valueForKey:@"notis"] stringByDecodingHTMLEntities];
            
            if(thisTime){
                if([thisTime length]>=10){
                    thisTime = [thisTime substringToIndex:16];
                    //thisTime = [thisTime substringFromIndex:11];
                }
            }
            
            NSString * thisLabel = [NSString stringWithFormat:@"%@ %@, %@ %@ (#%@)", thisTime, thisAdr,thisPostnr,thisStad,thisId];
            
            cell.userInteractionEnabled = YES;
            cell.textLabel.text = thisLabel;
            cell.detailTextLabel.text = thisSubtitle;

            NSLog(@"%ld",(long)indexPath.section);
            NSLog(@"%ld",(long)indexPath.row);
            NSString * cellColor = [self getCellColorFromData: [_allFilteredReports objectAtIndex:indexPath.row] byColorObject: [self getColoredFields]];
            cell.backgroundColor = [self colorWithHexString: cellColor];
        }else{
            NSString * thisLabel = [NSString stringWithFormat:@"Inga resultat hittades."];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            cell.accessoryView = nil;
            cell.textLabel.text = thisLabel;
            cell.detailTextLabel.text = [NSString stringWithFormat:@"Tyvärr."];
            cell.backgroundColor = [self colorWithHexString: @"ffffff"];
        }
    }else{
        
        /*#dolt#*//*DOLD*///NSLog(@"Sektion: %d Rad: %d",indexPath.section, indexPath.row);
        
        NSString * thisId = [[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"id"];
        NSString * thisAdr = [[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"gatuadress2"] stringByDecodingHTMLEntities];
        NSString * thisAdr2 = [[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"gatuadress"] stringByDecodingHTMLEntities];
        
        //if([_allSelectedReportsGroupedByDay objectAtIndex:indexPath.row][@"gatuadress2"] && [thisAdr isEqualToString: @""]) {
        if(![thisAdr2 isEqualToString: @""]) {
            thisAdr = thisAdr2;
            }
        
        
        NSString * thisStad = [[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"stad"] stringByDecodingHTMLEntities];
        NSString * thisPostnr = [[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"postnummer"] stringByDecodingHTMLEntities];
        //NSString * thisName = [[[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"ordermottagare"] stringByDecodingHTMLEntities];
        NSString * thisTime = [[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"avtaladtid_1"];
        NSString * thisSubtitle = [[[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] valueForKey:@"notis"];
        //NSLog(@"Sektion: %d Rad: %d Subtitle: %@",indexPath.section, indexPath.row, thisSubtitle);
        
        if(thisTime){
            if([thisTime length]>=10){
                thisTime = [thisTime substringToIndex:16];
                thisTime = [thisTime substringFromIndex:11];
            }
        }
        
        NSString * thisLabel = [NSString stringWithFormat:@"%@   %@, %@ %@ (#%@)", thisTime, thisAdr,thisPostnr,thisStad,thisId];
        
        cell.textLabel.text = thisLabel;
        cell.detailTextLabel.text = thisSubtitle;

        NSString * cellColor = [self getCellColorFromData: [[_allSelectedReportsGroupedByDay objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] byColorObject: [self getColoredFields]];
        cell.backgroundColor = [self colorWithHexString: cellColor];
    }
    
    
    return cell;
}

- (NSString *)getCellColorFromData:(NSMutableDictionary*)cellDict byColorObject:(NSMutableArray*)colorFieldArr {
    //Om inget fält matchar, sätt färg till vit.
    NSString * cellColor = @"ffffff";
    
    NSLog(@"-");
    NSLog(@"getCellColorFromData START");
    NSLog(@"-");
    NSLog(@"getCellColorFromData cellDict: %@", cellDict);
    NSLog(@"getCellColorFromData colorFieldArr: %@", colorFieldArr);
    NSLog(@"-");
    
    //Loopa igenom färgfält
    for (int j = 0; j < [colorFieldArr count]; j++) {
        //Om fältet finns i cell-dict, och har ett värde som inte är tomt, null
        NSString * colorFieldValue = [cellDict valueForKey:[[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolname"]];
        NSLog(@"Key: %@, value: %@", [[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolname"], colorFieldValue);
        
        if (colorFieldValue != nil) {
            //eller standardvärdet,
            if (![colorFieldValue isEqualToString:@"ÅÅÅÅ-MM-DD"] && ![colorFieldValue isEqualToString:@""] && ![colorFieldValue isEqualToString:@" "]) {
                if(![[[colorFieldArr objectAtIndex:j] valueForKey:@"fieldtype"] isEqualToString:@"signatureSimple"] &&
                   ![[[colorFieldArr objectAtIndex:j] valueForKey:@"fieldtype"] isEqualToString:@"attachImages"]){
                    //sätt bakgrundsfärg till fältets färg och avsluta loopen
                    NSLog(@"Woo, fältet %@ är ifyllt! Då väljer vi den här färgen (%@) och går till nästa arbetsorder.", [[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolname"], [[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolor"]);
                    cellColor = [[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolor"];
                    break;
                }else{
                    NSLog(@"Skippar fältet eftersom det är en signatur, och dom vill vi ignorera just nu eftersom vi inte orkar hitta på en smartare fix");
                }
                //Annars, gå vidare
            }
        }
        NSLog(@"Nope, fältet %@ är tomt. Gå vidare till nästa fält.", [[colorFieldArr objectAtIndex:j] valueForKey:@"fieldcolname"]);
    }
    
    NSLog(@"-");
    NSLog(@"getCellColorFromData END");
    NSLog(@"-");

    return cellColor;
}


//Convert hex color to UIColor
- (UIColor *) colorWithHexString: (NSString *) hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //Avmarkera raden användaren trycker på, så den inte är blå när man pop:ar subview:n
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    //Style:a backup-valets segmentbutton
    //_changeSegBackupViewController.segmentedControlStyle = UISegmentedControlStyleBar;
    //_changeSegBackupViewController.tintColor = [UIColor lightGrayColor];
    //[[[_changeSegBackupViewController subviews] objectAtIndex:0] setTintColor:[UIColor darkGrayColor]];

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([appDelegate.tableBackup isEqualToString:@""]){
        _changeSegBackupViewController.selectedSegmentIndex = 0;
    }
    else if([appDelegate.tableBackup isEqualToString:@"_bkp1"]){
        _changeSegBackupViewController.selectedSegmentIndex = 1;
    }
    else if([appDelegate.tableBackup isEqualToString:@"_bkp2"]){
        _changeSegBackupViewController.selectedSegmentIndex = 2;
    }
    else if([appDelegate.tableBackup isEqualToString:@"_bkp3"]){
        _changeSegBackupViewController.selectedSegmentIndex = 3;
    }
    else if([appDelegate.tableBackup isEqualToString:@"_bkp4"]){
        _changeSegBackupViewController.selectedSegmentIndex = 4;
    }
    else if([appDelegate.tableBackup isEqualToString:@"_bkp5"]){
        _changeSegBackupViewController.selectedSegmentIndex = 5;
    }
    

    for (int j=0; j<[_changeSegUrvalController.subviews count]; j++){
        UIColor *tintcolor=[UIColor blackColor];
        [[_changeSegUrvalController.subviews objectAtIndex:j] setTintColor:tintcolor];
    }
    if (_changeSegUrvalController.selectedSegmentIndex != UISegmentedControlNoSegment){
        UIColor *tintcolor=[UIColor blueColor];
        [[_changeSegUrvalController.subviews objectAtIndex:_changeSegUrvalController.selectedSegmentIndex] setTintColor:tintcolor];
    }

    
    [self updateDbInfoBar];
    [self updateTable];
    
    //Uppdatera databasen
//    [self performSelector:@selector(runFetchData)];
}

- (void)viewDidLoad
{
    
    NSLog(@"viewDidLoad för secondViewController START");
    [super viewDidLoad];
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"########################### VIEW DID LOAD ###########################");
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"#####################################################################");
    // Do any additional setup after loading the view, typically from a nib.

    [_progressbar setProgress:0];
    
    _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    _logonToServerIsRunning = @"0";
    _sendPostsToServerIsRunning = @"0";
    _fetchDataIsRunning = @"0";
    
    self.title = @"Arbetsordrar";
/*
    [[self.tabBarController.viewControllers objectAtIndex:0] setTitle:@"Arbetsordrar"];
    [[self.tabBarController.viewControllers objectAtIndex:1] setTitle:@"Signera"];
    [[self.tabBarController.viewControllers objectAtIndex:2] setTitle:@"Debug"];
*/
    
    if(_appDelegate.runFetchDataAutomatically == true){
        _runFetchDataAutomaticallyWasTrue = true;
        _appDelegate.runFetchDataAutomatically = false;
        [self fetchData];
    }
    
    [self updateDbInfoBar];
    [self updateTable];
    
    _searchBar.delegate = (id)self;
    isFiltered = false;

    //Uppdatera databasen - OBS! Kör i _antingen_ Vdidload eller Vwillappear, annars kan det bli dubbelt
    //[self performSelector:@selector(runFetchData)];
    
    //Om en crash.log existerar, försök maila den
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
    {
        if ([MFMailComposeViewController canSendMail]) {
            if([self hasInternet]){
                NSString *alertString = [NSString stringWithFormat:@"Logg hittad"];
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Hittade en kraschlogg. Vill du skicka in den för analys? Fyll gärna i vad det sista du gjorde var.") delegate:self cancelButtonTitle:@"Skicka nu..." otherButtonTitles:@"Skicka senare",@"Skicka inte",nil];
//                [alert show];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                            message:(@"Hittade en kraschlogg. Vill du skicka in den för analys? Fyll gärna i vad det sista du gjorde var.")
                                                                                     preferredStyle:UIAlertControllerStyleAlert];
               //We add buttons to the alert controller by creating UIAlertActions:
               UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Skicka inte"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                   
                   [self alertButtonAction:@"Skicka inte"];
                   
                                                                }]; //You can use a block here to handle a press on this button
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Skicka nu..."
                                                                   style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                    [self alertButtonAction:@"Skicka nu..."];
//                                                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                     }];
                
                [alertController addAction:actionCancel];
                   [alertController addAction:actionOk];
                
               [self presentViewController:alertController animated:YES completion:nil];
                
            }
        } else {
            // Handle the error
            NSLog(@"Ipaden är inte konfigurerad för att kunna skicka mail");
        }
    }
    
    NSLog(@"viewDidLoad för secondViewController SLUT");
    
}


- (void)sendLogByMail{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"ohbsyslog@gmail.com"]];
        [controller setSubject:@"Logg från iPad"];
        [controller setMessageBody:@"Vad var det sista du gjorde? <br><br>" isHTML:YES];
        [controller addAttachmentData:[NSData dataWithContentsOfFile:crashLogPath] mimeType:@"text/plain" fileName:@"crash.log"];
//        if (controller) [self presentModalViewController:controller animated:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    }
}

//- (void)viewDidUnload
//{
//    [self setProgressbar:nil];
//    [super viewDidUnload];
//    // Release any retained subviews of the main view.
///*
//_docsDir = nil;
//_dirPaths = nil;
//_tableView = nil;
//_databasePath = nil;
//_allSelectedReportsGroupedByDay = nil;
//_numberOfSections = nil;
//_allFilteredReports = nil;
//_searchBar = nil;
//_filterOnUsername = nil;
//_updatedLabel = nil;
//_poststosendLabel = nil;
//_changeSegBackupViewController = nil;
//_myFetchedData = nil;
//_resultat = nil;
//_tablesAndChecks = nil;
//_tablesArr = nil;
//_checksArr = nil;
//_tablesToRequest = nil;
//_receivedData = nil;
//_receivedDataString = nil;
//_SQL = nil;
//_sqlStatementsArr = nil;
//_failedSqlStatementsArr = nil;
//_failedSqlStatementsCodeArr = nil;
//_tableRowCount = nil;
//_dbloop1 = nil;
//_fetchDataConnection = nil;
//_loginConnection = nil;
//_sendPostConnection = nil;
//_sendPostConnectionArr = nil;
//_fetchDataIsRunning = nil;
//_logonToServerIsRunning = nil;
//_sendPostsToServerIsRunning = nil;
//*/
//}

- (void) didReceiveMemoryWarning
{
    [self setProgressbar:nil];
    //Change
//    [super viewDidUnload];
    [super didReceiveMemoryWarning];
    // Release any retained subviews of the main view.
/*
_docsDir = nil;
_dirPaths = nil;
_tableView = nil;
_databasePath = nil;
_allSelectedReportsGroupedByDay = nil;
_numberOfSections = nil;
_allFilteredReports = nil;
_searchBar = nil;
_filterOnUsername = nil;
_updatedLabel = nil;
_poststosendLabel = nil;
_changeSegBackupViewController = nil;
_myFetchedData = nil;
_resultat = nil;
_tablesAndChecks = nil;
_tablesArr = nil;
_checksArr = nil;
_tablesToRequest = nil;
_receivedData = nil;
_receivedDataString = nil;
_SQL = nil;
_sqlStatementsArr = nil;
_failedSqlStatementsArr = nil;
_failedSqlStatementsCodeArr = nil;
_tableRowCount = nil;
_dbloop1 = nil;
_fetchDataConnection = nil;
_loginConnection = nil;
_sendPostConnection = nil;
_sendPostConnectionArr = nil;
_fetchDataIsRunning = nil;
_logonToServerIsRunning = nil;
_sendPostsToServerIsRunning = nil;
*/
}

-(void) dealloc{
    [self setProgressbar:nil];
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
//
//    //    return NO;
//}

-(BOOL)shouldAutorotate{
    return YES;
}


-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text
{
    if(text.length == 0)
    {
        isFiltered = FALSE;
    }
    else
    {
        isFiltered = TRUE;
        _allFilteredReports = [[NSMutableArray alloc] init];
        
        NSMutableArray * x = [[NSMutableArray alloc] init];
        NSDictionary * y = [[NSDictionary alloc] init];
        for (x in _allSelectedReportsGroupedByDay)
        {
            for (y in x)
            {
                NSString * thisAdr = [NSString stringWithFormat:@"%@ %@", [y valueForKey:@"gatuadress"], [y valueForKey:@"gatuadress2"]];
                NSString * thisStad = [y valueForKey:@"stad"];
   
                NSRange adrRange = [thisAdr rangeOfString:text options:NSCaseInsensitiveSearch];
                NSRange stadRange = [thisStad
                                     rangeOfString:text options:NSCaseInsensitiveSearch];
                if(adrRange.location != NSNotFound || stadRange.location != NSNotFound)
                {
                    [_allFilteredReports addObject:y];
                }
            }
        }
    }
    [self.tableView reloadData];
}



-(void) logoutUser{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.user = nil;
    appDelegate.userId = nil;
    appDelegate.userRole = nil;
    appDelegate.userDictionary = nil;
    
    [self performSegueWithIdentifier:@"logoutSegue" sender:self];
    NSLog(@"Loggade ut");
    
    NSLog(@"\nKollar appdelegate's variabler: \nuser: %@\nuserid: %@\nuserrole: %@\nuserDict: %@",appDelegate.user, appDelegate.userId, appDelegate.userRole, appDelegate.userDictionary);
}



/* HÄR LÅG DELEGATE-METODERNA FÖR NSURLCONNECTION */



// Check whether the user has internet
- (bool)hasInternet {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->fetchReportsInProgress setTitle:@"Kontrollerar anslutningen..."];
    });
    
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/ping.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
//    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/ping.asp", @"https://devkringdata.loupe.se"]];
    
    NSLog(@"URL: %@", url);
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:3.0];
     BOOL connectedToInternet = NO;
    if ([NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil]) {
        connectedToInternet = YES;
    }
    
//    __block BOOL connectedToInternet = NO;
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSLog(@"URL String = %@", [url absoluteString]);
//
//    [[session dataTaskWithURL:[NSURL URLWithString:[url absoluteString]]
//              completionHandler:^(NSData *data,
//                                  NSURLResponse *response,
//                                  NSError *error) {
//        NSLog(@"Response = %@", response);
//
//        connectedToInternet = YES;
//                // handle response
//
//      }] resume];
    
//
//    //if (connectedToInternet)
//    ///*DOLD*///NSLog(@"We Have Internet!");
    
    return connectedToInternet;
}

- (void)runFetchData
{
    //Här finns eventuellt utrymme för att förflytta fetchdata-metoden till en bakgrundstråd istället
    //    if(!fetchDataIsRunning){
    //        [self performSelectorInBackground:@selector(fetchData) withObject:nil];
    //    }
    //Denna metod fungerar inte, se http://stackoverflow.com/questions/9679754/tried-to-obtain-the-web-lock-from-a-thread-other-than-the-main-thread-or-the-web
    
    
    //    [self performSelector:@selector(clearPostsToSendQueue)];
    
    
    //[self runFetchData];
    [self logonToServer];
    //[self sendPostsToServer];
    //[self fetchData];
    
    
}



- (void) logonToServer
{
    NSLog(@"logontoserver! logonToServerIsRunning: %@", _logonToServerIsRunning);
    if([_logonToServerIsRunning isEqualToString:@"0"]){
        _logonToServerIsRunning = @"1";
        NSLog(@"logontoserver! logonToServerIsRunning: %@", _logonToServerIsRunning);
        
        /*VIKTIG*/NSLog(@"Gör request för att logga in på servern...");
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/login.asp?admin=%@&pass=%@&format=json", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],[appDelegate.userDictionary valueForKey:@"username"],[appDelegate.userDictionary valueForKey:@"password"]]];

    // Create NSURLSession object
        NSURLSession *session = [NSURLSession sharedSession];
        [(AppDelegate*)[UIApplication sharedApplication].delegate showActivity];
        // Create NSURLSessionDataTask task object by url and session object.
        NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

            if (data != nil){
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                if (parseError) {
                    NSLog(@"Admin data: %@",parseError);
                }
                else{
//                    NSLog(@"data: %@",[responseDictionary valueForKey:@"login"]);
                    bool _isLoggedIn = [responseDictionary valueForKey:@"login"];
                    if (_isLoggedIn == true){
                        self->_logonToServerIsRunning = @"0";
                        [self sendPostsToServer:true];
                    }else{
                        [(AppDelegate*)[UIApplication sharedApplication].delegate hideActivity];
//                        NSError *error = [request error];

                        NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
                        NSString *alertString = [NSString stringWithFormat:@"Kunder inte synkronisera med databasen eftersom iPad inte fick kontakt med servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertHeader
                                                                                                            message:alertString
                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                                               style:UIAlertActionStyleDefault
                                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                                }];

                        [alertController addAction:actionCancel];

                        [self presentViewController:alertController animated:YES completion:nil];

                        NSLog(@"Error: %@", error.localizedDescription);
                        self->_logonToServerIsRunning = @"0";
                    }
                    [(AppDelegate*)[UIApplication sharedApplication].delegate hideActivity];
//                    self->_logonToServerIsRunning = @"0";
//                    [self sendPostsToServer:true];
                    
                }
            }else{
                [(AppDelegate*)[UIApplication sharedApplication].delegate hideActivity];
//                NSError *error = [request error];

                NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
                NSString *alertString = [NSString stringWithFormat:@"Kunder inte synkronisera med databasen eftersom iPad inte fick kontakt med servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertHeader
                                                                                                    message:alertString
                                                                                             preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                                       style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                        }];

                [alertController addAction:actionCancel];

                [self presentViewController:alertController animated:YES completion:nil];

                NSLog(@"Error: %@", error.localizedDescription);
                self->_logonToServerIsRunning = @"0";
            }

        }];

        // Begin task.
        [task resume];
        
    }
}

//Delegate Callback från mail-funktionen
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
        NSFileManager *filemgr = [NSFileManager defaultManager];
        //Om en crash.log existerar, maila den vid nästa db-synk.
        if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
        {
            [filemgr removeItemAtPath: (crashLogPath) error:NULL];
            NSLog(@"Raderade kraschloggfilen.");
        }
    }
//    [self dismissModalViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)sendPostsToServerButtonWasPressed
{
    [self sendPostsToServer:false];
}

- (void)toggleSendreportsPopover {
    NSLog(@"toggleSendReportsPopover körs");
    if (sendreportsPopover){
        NSLog(@"Döljer popover...");
//        [sendreportsPopover dismissPopoverAnimated:YES];
        [[sendreportsPopover presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    }else{
        NSLog(@"Visar popover...");
        [self performSegueWithIdentifier:@"showSendreportsPopover" sender:nil];
    }
    
    //Passa på att uppdatera dbInfoBar när man öppnar/stänger popovern
    [self updateDbInfoBar];
    
}

- (void)sendDBToMailButtonWasPressed
{
    NSLog(@"Användaren tryckte på knappen \"Maila db\"");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"LoupeApp.db"];
    NSString *crashPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: dbPath ] == YES)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"ohbsyslog@gmail.com"]];
        [controller setSubject:@"Databasfil från iPad"];
        [controller setMessageBody:@"Vilken information vill du få utplockad manuellt? <br>(Adress eller ärendenummer (ordernummer) och vilken rapportmall) <br><br>" isHTML:YES];
        [controller addAttachmentData:[NSData dataWithContentsOfFile:dbPath] mimeType:@"application/x-sqlite3" fileName:@"LoupeApp.db"];
        
        if ([filemgr fileExistsAtPath: crashPath ] == YES){
            [controller addAttachmentData:[NSData dataWithContentsOfFile:crashPath] mimeType:@"plain/text" fileName:@"crash.log"];
        }
        if ([filemgr fileExistsAtPath: logPath ] == YES){
            [controller addAttachmentData:[NSData dataWithContentsOfFile:logPath] mimeType:@"plain/text" fileName:@"console.log"];
        }
        
//        if (controller) [self presentModalViewController:controller animated:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    }else{
        NSLog(@"Hittade inte databasfilen \"%@\"", dbPath);
    }
}

- (void)sendLogsToMailButtonWasPressed
{
    NSLog(@"Användaren tryckte på knappen \"Maila db\"");
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:@"LoupeApp.db"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *logFiles = [filemgr contentsOfDirectoryAtPath:documentsDirectory error:nil];
    
    if ([logFiles count]>0)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"ohbsyslog@gmail.com"]];
        [controller setSubject:@"Databasfil från iPad"];
        [controller setMessageBody:@"Vilken information vill du få utplockad manuellt? <br>(Adress eller ärendenummer (ordernummer) och vilken rapportmall) <br><br>" isHTML:YES];
        
        for(int j=0; j<([logFiles count]-1); j++){
            NSString * thispath = [documentsDirectory stringByAppendingPathComponent:[logFiles objectAtIndex:j]];
            //if(![[logFiles objectAtIndex:j] isEqualToString:@"LoupeApp.db"]){ //Ta inte med db-filen
            //    if(![[[logFiles objectAtIndex:j] substringWithRange:NSMakeRange([[logFiles objectAtIndex:j] length]-3, [[logFiles objectAtIndex:j]  length])] isEqualToString:@"jpg"]){ //Ta inte med bilder
            
            NSLog(@"Längd på filnamn: %lu, Range: %lu till%lui",(unsigned long)[[logFiles objectAtIndex:j] length],[[logFiles objectAtIndex:j] length]-4, [[logFiles objectAtIndex:j] length]-1);
            
            NSRange r = NSMakeRange([[logFiles objectAtIndex:j] length]-4, 4);
            NSString *fileExt = [[logFiles objectAtIndex:j] substringWithRange: r];
            NSLog(@"%@", fileExt);
            if ([fileExt isEqualToString:@".log"]){
                [controller addAttachmentData:[NSData dataWithContentsOfFile:thispath] mimeType:@"plain/text" fileName:[logFiles objectAtIndex:j]];
            }
            
            //}
        }
        
//        if (controller) [self presentModalViewController:controller animated:YES];
        if (controller) [self presentViewController:controller animated:YES completion:nil];
    }else{
        NSLog(@"Hittade inte databasfilen \"%@\"", dbPath);
    }
}

- (void)sendPostsToServer:(bool)runFetchDataWhenDone
{
    NSLog(@"sendPostsToServer! sendPostsToServerIsRunning: %@", _sendPostsToServerIsRunning);
    
    /*
    NSLog(@"SENDPOSTSTOSERVER INAKTIVERAT");
    _sendPostsToServerIsRunning = @"1";
    [self performSelector:@selector(fetchData)];
    */
    
    if([_sendPostsToServerIsRunning isEqualToString:@"0"]){
        _sendPostsToServerIsRunning = @"1";
        /*DOLD*///NSLog(@"#### sendpoststoserver #####");
        /*VIKTIG*/NSLog(@"Skickar uppdateringsposter till servern...");
        
        
        //1. Hämta en array över vilka poster som ska skickas
        
        NSString *docsDir;
        NSArray *dirPaths;
        
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        docsDir = [dirPaths objectAtIndex:0];
        
        
        sqlite3_shutdown();
            sqlite3_config(SQLITE_CONFIG_SERIALIZED);
            sqlite3_initialize();
        
        sqlite3_stmt *statement;
        
        
        NSMutableArray * poststosendArr = [[NSMutableArray alloc] init];
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
            /*DOLD*///NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"Listar prot_tabell");
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                
                
                int j = 0;
                for (j=0; j<colCount;j++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, j);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, j);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                [poststosendArr addObject:dictionary];
            }
            beginSQL = nil;
            begin_stmt = nil;
            sqlite3_finalize(statement);
             
        }
        
        //Ta bort dubletter från poststosendArr
        NSArray* originalArray = poststosendArr;
        NSMutableSet* existingNames = [NSMutableSet set];
        NSMutableArray* filteredArray = [NSMutableArray array];
        for (id object in originalArray) {
            if (![existingNames containsObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]]) {
                [existingNames addObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]];
                [filteredArray addObject:object];
            }
        }
        
        poststosendArr = filteredArray;
        
        /*DOLD*/NSLog(@"posts: %@",poststosendArr);
        
        
        
        if([poststosendArr count]>0){
            
            //2. Hämta en array med alla poster i sys_formtables (för att få tabellnamnet som lagrar rapporten)
            
            NSMutableArray * formTablesArr = [[NSMutableArray alloc] init];
            if (_appDelegate.openDbConnection)
            {
                NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_formtables"];
                
                const char *begin_stmt = [beginSQL UTF8String];
                sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    NSString * myTempMutStr = [[NSMutableString alloc] init];
                    
                    
                    
                    int j = 0;
                    for (j=0; j<colCount;j++) {
                        //Ger namnet på kolumn N
                        const char *colName = (const char*)sqlite3_column_name(statement, j);
                        
                        char *colValue = (char *)sqlite3_column_text(statement, j);
                        if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                        (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                        myTempMutStr = nil;
                        /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                    }
                    [formTablesArr addObject:dictionary];
                }
                beginSQL = nil;
                begin_stmt = nil;
                sqlite3_finalize(statement);
                 
            }
            
            //NSLog(@"formtables: %@", formTablesArr);
            
            
            
            
            
            
            //3. Hämta de poster som ska skickas från sina faktiska lagringsplatser
            
            NSMutableArray * colnamesArr = [[NSMutableArray alloc] init];
            NSMutableArray * postsContentArr = [[NSMutableArray alloc] init];
            
            NSMutableDictionary * reportDictionary = [[NSMutableDictionary alloc] init];
            
            for (int j=0; j< [poststosendArr count]; j++){
                reportDictionary = [poststosendArr objectAtIndex:j];
                
                
                NSLog(@"reporttemplate: %d",[[reportDictionary valueForKey:@"reporttemplate"] intValue]);
                
                //Hoppa över om reporttemplate = 0 i tabellen sys_poststosend
                if([[reportDictionary valueForKey:@"reporttemplate"] intValue] != 0){
                    NSLog(@"TEST");
                    if (_appDelegate.openDbConnection)
                    {
                        //Hitta tabellen som har id = template-id i formTablesArr
                        NSString * reportTable = [[NSString alloc] init];
                        for (NSMutableDictionary * k in formTablesArr) {
                            if([[k valueForKey:@"id"] isEqualToString:[reportDictionary valueForKey:@"reporttemplate"]])
                            {
                                reportTable = [NSString stringWithFormat:@"%@",[k valueForKey:@"table_name"]];
                            }
                        }
                        
                        //SELECT * från prot_tabell där id = workorder-id
                        NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE workorder_id = %d", reportTable,[[reportDictionary valueForKey:@"workorderid"] intValue]];
                        
                        /*DOLD*///NSLog(@"SQL: %@", beginSQL);
                        
                        const char *begin_stmt = [beginSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                        
                        /*DOLD*///NSLog(@"Listar prot_tabell");
                        //Ger antalet columner för select-statement "pStmt"
                        int colCount = sqlite3_column_count(statement);
                        /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
                        
                        while(sqlite3_step(statement) == SQLITE_ROW) {
                            /*DOLD*///NSLog(@"sqlite_row...");
                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                            [dictionary setObject:[[NSMutableDictionary alloc] init] forKey:@"blobdictionary"];
                            
                            NSString * myTempMutStr = [[NSMutableString alloc] init];
                            
                            //töm arrayen - vi vill bara ha en uppsättning kolumn-namn, så vi tar den från sista raden
                            NSMutableArray * tempColnamesArr = [[NSMutableArray alloc] init];
                            
                            
                            int j = 0;
                            for (j=0; j<colCount;j++) {
                                //Ger namnet på kolumn N
                                const char *colName = (const char*)sqlite3_column_name(statement, j);
                                //Lägg till namnet i colnamesArr
                                [tempColnamesArr addObject:[NSString stringWithUTF8String:colName]];
                                
                                char *colValue = (char *)sqlite3_column_text(statement, j);
                                if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                                
                                //Undvik att fylla dictionary med nil
                                if (myTempMutStr) {
                                    (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                                }else{
                                    NSLog(@"TEMPMUTSTR var tom för %s", colName);
                                    //Hittat en blob? Om det är en blob, så är colvalue inte NULL men blir ändå NIL.
                                    
                                    /* The pointer returned by sqlite3_column_blob() points to memory
                                     ** that is owned by the statement handle (pStmt). It is only good
                                     ** until the next call to an sqlite3_XXX() function (e.g. the
                                     ** sqlite3_finalize() below) that involves the statement handle.
                                     ** So we need to make a copy of the blob into memory obtained from
                                     ** malloc() to return to the caller.
                                     */
                                    
                                    NSData *imgData = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, j) length:sqlite3_column_bytes(statement, j)];
                                    
                                    NSLog(@"BLOBDATA HITTAT i rapporten: %lu", (unsigned long)[imgData length]);
                                    NSLog(@"Lagrar... Resultat på nästa rad:");
                                    
                                    (void)[[dictionary valueForKey:@"blobdictionary"] setObject:imgData forKey:[NSString stringWithUTF8String:colName]];
                                    
                                    NSLog(@"blobdictionary forkey %@: %lu",[NSString stringWithUTF8String:colName], (unsigned long)[[[dictionary valueForKey:@"blobdictionary"] valueForKey:[NSString stringWithUTF8String:colName]] length]);
                                    
                                }
                                myTempMutStr = nil;
                                /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                            }
                            [colnamesArr addObject:[tempColnamesArr copy]];
                            [postsContentArr addObject:dictionary];
                        }
                        
                        beginSQL = nil;
                        begin_stmt = nil;
                    sqlite3_finalize(statement);
                         
                    }
                }
            }
            
            /*DOLD*/NSLog(@"colnames: %@",colnamesArr);
            /*DOLD*///NSLog(@"postsContentArr: %@", postsContentArr);
            
#pragma mark Skapa POST-datasträng
            //4. Skapa en datasträng till POST-requesten
            //Loopar igenom alla rapporter som ska postas, och i varje iteration loopas varje kolumn igenom
            //Skapa en request för varje stort varv och fyll requesten i den lilla loopen
            
            for (int j=0; j<[poststosendArr count]; j++)
            {
                if([[[poststosendArr objectAtIndex:j] valueForKey:@"reporttemplate"] intValue] != 0)
                {
                    //Sätt URL för requesten
//                    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_scripts/report_ajax-save-report.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                    NSString *url = [NSString stringWithFormat:@"%@/_scripts/report_ajax-save-report.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]];
                    
                    NSString * dbstr = [NSString stringWithFormat:@"template=%@",[[poststosendArr objectAtIndex:j] valueForKey:@"reporttemplate"]];
                    NSString *postParam = @"";
                    
                    if([colnamesArr count]>j){
                    
                        //Fyll requesten
                        for (int k=0; k<[[colnamesArr objectAtIndex:j] count]; k++)
                        {
                            NSString * colstr = [[NSString alloc] init];
                            NSString * valstr = [[NSString alloc] init];
                            colstr = [[colnamesArr objectAtIndex:j] objectAtIndex:k];
                            if([colstr isEqualToString:@"workorder_id"]){
                                colstr = @"workorderID";
                            }
                            if([colstr isEqualToString:@"id"]){
                                colstr = @"reportID";
                            }
                            
                            valstr = [[postsContentArr objectAtIndex:j] valueForKey:[[colnamesArr objectAtIndex:j] objectAtIndex:k]];
                        
                            if(valstr){
                                /*DOLD*///NSLog(@"Valstr är inte nil");
                                /*DOLD*///NSLog(@"Lägger till värde: %@ = %@", colstr, valstr);
//                            [request addPostValue:valstr forKey:colstr];
//                                NSLog(@"Request = %@", request);
                                NSString *stringNew = [NSString stringWithFormat:@"&%@=%@",colstr,valstr];
                                NSString *myString = [NSString stringWithFormat:@"%@%@", postParam, stringNew];
                                postParam = myString;
                                
                                //BILDUPPLADDNING AV SIGNATUR:
                                //Om valstr är filnamnet på en bild, kolla om den finns i filsystemet, ladda upp den till servern, verifiera att den    existerar på servern, och radera filen lokalt på ipaden.
                            
                                if([valstr rangeOfString:@".jpg"].location != NSNotFound){
                                //Fieldvaluen innehöll .jpg, försök ladda upp bild
                                
                                NSString * fileName = valstr;
                                /*sokord*/
                                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
                                
                                // Create file manager
                                NSFileManager *fileMgr = [NSFileManager defaultManager];
                                
                                //Finns filen?
                                if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                                    
                                    //TEST: Ladda upp bilden på servern
                                    //Sätt URL för requesten
                                    
                                    //API For IMAGE UPLOAD
                                    NSURL *signatureUploadUrl = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_inc/signatureUpload.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                                    
////                                    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:signatureUploadUrl];
////                                    [request setHTTPMethod:@"POST"];//Adding Data is Url With Json
////                                    NSData * imageData1 = [[NSData alloc] initWithContentsOfFile:jpgPath];
////                                    NSString *templateParam = [NSString stringWithFormat:@"template=%@",nil];
////
//
//                                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:signatureUploadUrl];
//
//                                    NSData *imageData = [[NSData alloc] initWithContentsOfFile:jpgPath];
//
//                                        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
//                                        [request setHTTPShouldHandleCookies:NO];
//                                        [request setTimeoutInterval:60];
//                                        [request setHTTPMethod:@"POST"];
//
//                                        NSString *boundary = @"YOUR_BOUNDARY_STRING";
//                                        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
//                                        [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
//
//                                        NSMutableData *body = [NSMutableData data];
//
//                                        [body appendData:[[NSString stringWithFormat:@"template=%@", nil] dataUsingEncoding:NSUTF8StringEncoding]];
//                                        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//                                        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"file\"; filename=\"%@.jpg\"\r\n", @"file"] dataUsingEncoding:NSUTF8StringEncoding]];
//                                        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
//                                        [body appendData:[NSData dataWithData:imageData]];
//
//                                        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
////                                        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"token\"\r\n\r\n%@", kImageUploadToken] dataUsingEncoding:NSUTF8StringEncoding]];
////
////                                        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
//
//
//                                        [request setHTTPBody:body];
//                                         NSLog(@"Request body %@", [[NSString alloc] initWithData:[request HTTPBody] encoding:NSUTF8StringEncoding]);
//
//                                        // set the content-length
//                                        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
//                                        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//                                        NSLog(@"%@", [request allHTTPHeaderFields]);
//
//                                        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//                                            if(data.length > 0)
//                                            {
//                                                //success
//                                                NSString* responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                                                NSLog(@"Response %@",responseString);
//                                                NSError *error;
//                                                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
////                                                if(error == nil){
////                                                    if([[responseDictionary objectForKey:@"ok"]boolValue]) {
////                                                        NSString *fileURL = [[responseDictionary objectForKey:@"file"] objectForKey:@"url_download"];
////                                                        NSLog(@"File URL %@", fileURL);
////                                                    }
////                                                    else {
////                                                        NSLog(@"Could Not upload file ");
////                                                    }
////                                                }
//
//
//                                            }
//                                        }];
                                    
                                
                                    //Skapa requesten
                                    ASIFormDataRequest *_signatureUploadRequest = [ASIFormDataRequest requestWithURL:signatureUploadUrl];
                                    __weak ASIFormDataRequest *signatureUploadRequest = _signatureUploadRequest;


                                    [signatureUploadRequest addPostValue:nil forKey:@"template"];

                                    NSData * imageData = [[NSData alloc] initWithContentsOfFile:jpgPath];

                                    [signatureUploadRequest setData:imageData withFileName:fileName andContentType:@"image/jpeg" forKey:@"file"];

                                    [signatureUploadRequest setCompletionBlock:^{
                                        NSString *responseString = [[NSString alloc] initWithData:[signatureUploadRequest responseData] encoding:NSUTF8StringEncoding];
                                        /*VIKTIG*/NSLog(@"Fil skickad! Response: %@", responseString);

                                        //Filen skickades. Nu kollar vi om bilden finns på servern, och i så fall raderar vi den lokalt.
                                        NSURL *signatureCheckUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/_inc/signatureImageExists.asp?format=json&filename=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],fileName]];

                                        NSURLSession *sessionCheck = [NSURLSession sharedSession];
                                        NSURLSessionDataTask *task = [sessionCheck dataTaskWithURL:signatureCheckUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

                                            if (data != nil){
//                                                NSError *parseError = nil;
                                                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                                if (error) {
                                                    NSLog(@"Admin data: %@",error);
                                                    NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                                }
                                                else{
                                                    bool _isExists = [responseDictionary valueForKey:@"exists"];

                                                    NSLog(@"Response Signature check = %@",data);
//                                                    if([responseString isEqualToString:@"TRUE"]){
                                                    if(_isExists == true){
                                                        NSLog(@"Bilden existerar på servern. Uppladdningen är komplett och färdig. Raderar bilden på iPaden.");

                                                        [fileMgr removeItemAtPath: (jpgPath) error:NULL];
                                                        NSLog(@"Raderade filen.");

                                                        //NSString *alertString = [NSString stringWithFormat:@"Fil skickad!"];
                                                        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Filen laddades upp och servern bekräftade att den kom fram.") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                        //[alert show];

                                                    }
                                                }
                                            }else{
                                                NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                            }
                                        }];

                                        // Begin task.
                                        [task resume];

                                    }];
                                    [signatureUploadRequest setFailedBlock:^{
                                        NSError *error = [signatureUploadRequest error];
                                        /*VIKTIG*/NSLog(@"Error när filen som innehåller signaturen skulle skickas: %@", error.localizedDescription);

                                        NSString *alertString = [NSString stringWithFormat:@"Ett fel uppstod, försök igen senare."];
                                        NSString *alertString2 = [NSString stringWithFormat:@"Ett fel uppstod när signaturen skulle skickas. Det kan till exempel bero på dålig uppkoppling/täckning. Försök igen senare. Felmeddelande: %@", error.localizedDescription];
//                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertString2 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                        [alert show];
                                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                                                            message:alertString2
                                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                                            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                                                                   style:UIAlertActionStyleDefault
                                                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                                                    }];

                                            [alertController addAction:actionCancel];

                                            [self presentViewController:alertController animated:YES completion:nil];

                                    }];
                                    NSLog(@"Skickar fil... Filnamn: %@",fileName);

                                    //NSString *alertString = [NSString stringWithFormat:@"Skickar fil... Filnamn: %@",fileName];
                                    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    //[alert show];

                                    [signatureUploadRequest startAsynchronous];
                                }
                            }
                            
                            
                            }else{
                                /*DOLD*/NSLog(@"Valstr är nil, går vidare till nästa fält/kolumn");
                            }
                        
                        }
                    
                    }else{
                        NSLog(@"[colnamesArr objectAtIndex:j] var nil");
                    }
                    NSLog(@"%@", postParam);
                    NSString *newParameters = [NSString stringWithFormat:@"%@%@", dbstr, postParam];
                    NSLog(@"Appended string = %@", newParameters);

                    [webservice executequery:url strpremeter:newParameters withblock:^(NSData * dbdata, NSError *error) {
                        NSLog(@"Data: %@", dbdata);
                        if (error){
                            /*VIKTIG*/NSLog(@"Error när post %d av %lu skulle skickas: %@",  j+1, (unsigned long)[poststosendArr count], error.localizedDescription);
                            self->_sendPostsToServerIsRunning = @"0";
                        }
                        if (dbdata!=nil)
                        {
                            NSDictionary *maindic = [NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
                            NSLog(@"Response Data: %@", maindic);
//                            NSString *responseString = [[NSString alloc] initWithData:[dbdata responseData] encoding:NSUTF8StringEncoding];
                            /*VIKTIG*/NSLog(@"Post %d av %lu skickad! Response: %@", j+1, (unsigned long)[poststosendArr count], dbdata);
                            if(j+1 == [poststosendArr count]){
                                //Alla poster har skickats
                                /*VIKTIG*/NSLog(@"Alla poster har skickats!");
                                self->_sendPostsToServerIsRunning = @"0";
                                [self clearPostsToSendQueue];
                                if(runFetchDataWhenDone){
                                    NSLog(@"funktionen sendPosts kallades med runFetchDataWhenDone = true");
                                    [self fetchData];
                                }
                            }
                        }else{
                            /*VIKTIG*/NSLog(@"Error när post %d av %lu skulle skickas: %@",  j+1, (unsigned long)[poststosendArr count], error.localizedDescription);
                            self->_sendPostsToServerIsRunning = @"0";
                        }
                    }];
                    
                    
//                    [request setCompletionBlock:^{
//                        NSString *responseString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
//                        /*VIKTIG*/NSLog(@"Post %d av %lu skickad! Response: %@", j+1, (unsigned long)[poststosendArr count], responseString);
//                        if(j+1 == [poststosendArr count]){
//                            //Alla poster har skickats
//                            /*VIKTIG*/NSLog(@"Alla poster har skickats!");
//                            self->_sendPostsToServerIsRunning = @"0";
//                            [self clearPostsToSendQueue];
//                            if(runFetchDataWhenDone){
//                                NSLog(@"funktionen sendPosts kallades med runFetchDataWhenDone = true");
//                                [self fetchData];
//                            }
//                        }
//                    }];
//                    [request setFailedBlock:^{
//                        NSError *error = [request error];
//                        /*VIKTIG*/NSLog(@"Error när post %d av %lu skulle skickas: %@",  j+1, (unsigned long)[poststosendArr count], error.localizedDescription);
//                        self->_sendPostsToServerIsRunning = @"0";
//                    }];
//
//                    NSLog(@"Hela request-objektet: %@", request);
//                    [request setStringEncoding:NSUTF8StringEncoding];
//                    [request startAsynchronous];
                    
                }
            }
            
        }else{
            //Om uppladdnings-kön är tom, kör fetchData och uppdatera databasen.
            /*VIKTIG*/NSLog(@"Inga rapporter att skicka.");
            _sendPostsToServerIsRunning = @"0";
            if(runFetchDataWhenDone){
                NSLog(@"funktionen sendPosts kallades med runFetchDataWhenDone = true");
                [self fetchData];
            }
        }
    }
}




- (void)clearPostsToSendQueue
{
    NSLog(@"Tömmer uppladdningskön");
    //Tömmer uppladdnings-kön när en synkronisering har genomförts
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    
    sqlite3_shutdown();
        sqlite3_config(SQLITE_CONFIG_SERIALIZED);
        sqlite3_initialize();
    sqlite3_stmt *statement;
    
    if (_appDelegate.openDbConnection)
    {
        /*DOLD*///NSLog(@"Kör kommando: %@", sql);
        NSString *beginSQL = @"DROP TABLE IF EXISTS sys_poststosend";
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            /*VIKTIG*/NSLog(@"Uppladdningskön tömd. (%@)", beginSQL);
            /*DOLD*///NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
        } else {
            /*VIKTIG*/NSLog(@"Uppladdningskön tömdes inte! Kommando: %@; Felkod: %d", beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
        }
        
        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
        beginSQL = nil;
        begin_stmt = nil;
        sqlite3_finalize(statement);
         
        //        [self performSelector:@selector(fetchData)];
    }
    [self updateDbInfoBar];
    [self updateTable];

}

#pragma mark
#pragma mark fetchData

- (void)openLoadingPopup:(NSString*)customMsg{
    NSString *popupMsg = @"Uppdaterar databasen...";
    if ([customMsg length] > 0) {
        popupMsg = customMsg;
    }
//    fetchReportsInProgress = [[UIAlertView alloc] initWithTitle:popupMsg message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
//    NSLog([NSString stringWithFormat: @"Visar sendprogressAlert, '%@'", popupMsg]);
    fetchReportsInProgress = [UIAlertController alertControllerWithTitle:popupMsg message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleMedium];
    loading.frame=CGRectMake(140, 60, 16, 16);
    [loading startAnimating];
//    [fetchReportsInProgress addSubview:loading];
    [fetchReportsInProgress.view addSubview:loading];
    [self presentViewController:fetchReportsInProgress animated:YES completion:nil];
    
    /*
    UIProgressView *alertProgressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
    alertProgressBar.frame=CGRectMake(40,90,210,16);
    [alertProgressBar setProgress:0.5];
    [fetchReportsInProgress addSubview:alertProgressBar];
    */
    
    
    
//    [fetchReportsInProgress show];
    
    
}
- (void)closeLoadingPopupWithError:(bool)withError showMessage:(bool)showMessage{
    //Dölj alertview och meddela användaren att rapporten skickats
    NSLog(@"%@", [NSString stringWithFormat: @"Döljer sendprogressAlert, '%@'", fetchReportsInProgress.title]);
//    [fetchReportsInProgress dismissWithClickedButtonIndex:0 animated:true];
    [fetchReportsInProgress dismissViewControllerAnimated:true completion:nil];
    NSString * alertHeader;
    NSString * alertText;
//    UIAlertView * alert;
    UIAlertController * alert;

    if(withError){
        // Om ett fel uppstod, meddela anvandaren
        alertHeader = @"Hämtningen misslyckades";
        alertText = @"Hämtningen tog för lång tid - över två minuter. Kontrollera att du kan surfa till ohbsys.se och försök igen. Om problemet kvarstår, kontakta supporten.";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Dölj" otherButtonTitles: nil];
        alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Dölj" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                                            [self alertButtonAction:@"Dölj"];
                                                                    }];
                
        [alert addAction:actionCancel];
    }else{
        alertHeader = @"Hämtningen slutfördes";
        alertText = @"Databasen har uppdaterats.";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Dölj" otherButtonTitles: nil];
        alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Dölj" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                                                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                                                            [self alertButtonAction:@"Dölj"];
                                                                    }];
        [alert addAction:actionCancel];
    }

    if (showMessage) {
        NSLog(@"Visar alert från postWasCompleted");
//        [alert show];
        [self presentViewController:alert animated:YES completion:nil];
    }

    
    // Om ingen error uppstod, uppdatera DbInfoBar och listan med arbetsordrar
    if(!withError){
        [self updateDbInfoBar];
        [self updateTable];
    }

    //Här kan man lägga in en funktion som kollar om synk startats automatiskt och i så fall logga ut igen.
    /*
    if(_runFetchDataAutomaticallyWasTrue == true){
        _runFetchDataAutomaticallyWasTrue = false;
        [self logoutUser];
    }
    */
    
}

-(void)makeDatabaseSafeCopy{
    /*
    NSString *docsDir;
    NSArray *dirPaths;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    // Build the path to the database file
    NSString * _thisdatabasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"LoupeApp.db"]];
    NSString * _thisdatabaseSafeCopyPath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ohbsysApp_safecopy.db"]];

    if ([filemgr fileExistsAtPath: _thisdatabaseSafeCopyPath ] == YES)
    {
        [filemgr removeItemAtPath: (_thisdatabaseSafeCopyPath) error:NULL];
        NSLog(@"Raderade den befintliga säkerhetskopian.");
    }else{
        NSLog(@"Hittade ingen fil att radera");
    }
    
    NSLog(@"Försöker skapa en säkerhetskopia...");
    
    if ([filemgr fileExistsAtPath: _thisdatabasePath ] == YES){
        [filemgr copyItemAtPath:_thisdatabasePath toPath:_thisdatabaseSafeCopyPath error:NULL];
        NSLog(@"Säkerhetskopian skapades.");
    }else{
        NSLog(@"Hittade inte originaldatabasen.");
    }
*/
}

- (void)fetchData
{
    NSLog(@"FetchData kallades. Körs funktionen redan? _fetchDataIsRunning: %@",_fetchDataIsRunning);
    if([_fetchDataIsRunning intValue] == 0){
        _fetchDataIsRunning = @"1";
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openLoadingPopup:@"Hämtar data från servern..."];
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self->fetchReportsInProgress setTitle:@"Hämtar data..."];
        });
        
        //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: YES];
        
        //Kolla om uppkoppling finns
        NSLog(@"%d",[self hasInternet]);
        if(![self hasInternet]){
            /*VIKTIG*/NSLog(@"Ingen uppkoppling hittad. Offline-läge aktivt");
        }else{
            /*VIKTIG*/NSLog(@"Uppdaterar databasen...");
            dispatch_async(dispatch_get_main_queue(), ^{
                self->_poststosendLabel.text = [NSString stringWithFormat:@"Uppdaterar databasen..."];
            });
            
            //Detta funkar inte. Funktionens innehåll utkommenterad. Tillagt 10 nov 2013
            [self makeDatabaseSafeCopy];
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" FETCHDATA KÖRS ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            
            /*DOLD*///NSLog(@"Start!");
            
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" HÄMTAR _tablesAndChecks ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*VIKTIG*///NSLog(@"Försöker nå internet... (om appen kraschar här, kontrollera uppkopplingen)");
            
            
            
            /*DOLD*///NSLog(@"Hämtar data...");
            int j = 0;
            bool dataWasFetched = FALSE;
            while (j<=5 && dataWasFetched == FALSE) {
                if([self hasInternet]){
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self->fetchReportsInProgress setTitle:@"Hämtar checksummor..."];
                    });

                    
                    NSString *url = [NSString stringWithFormat:@"%@/_remote/get_table_checksums.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]];
                    
                    NSLog(@"URL: %@", url);
                    
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
                    NSString *serverOutput= [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
                    _myFetchedData = serverOutput;
                    /*DOLD*/NSLog(@"Data: %@", _myFetchedData);
                    /*DOLD*/NSLog(@"Slut på data.");
                    dataWasFetched = TRUE;
                    
                }else{
                    /*DOLD*/NSLog(@"Försökte hämta data, men hade ingen uppkoppling. Försök %d av 10.", j);
                    j++;
                }
            }
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" delar upp _tablesAndChecks ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@"Försöker nå internet... (om appen kraschar här, så är antagligen mottagen data från _tablesAndChecks = 0!)");
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->fetchReportsInProgress setTitle:@"Läser in checksummor..."];
            });
            
            if (_myFetchedData.length < 1) {
                NSLog(@"Mottagen data innehåller färre än 1 tecken.");
            }
            
            _tablesAndChecks = [_myFetchedData componentsSeparatedByString: @"&"];
            _tablesArr = [[_tablesAndChecks objectAtIndex:0] componentsSeparatedByString: @","];
            _checksArr = [[_tablesAndChecks objectAtIndex:1] componentsSeparatedByString: @","];
            
            _resultat = [[NSMutableString alloc] initWithCapacity:0];
            
            for(i=0;i<[_tablesArr count];i++){
                [_resultat appendString:([_tablesArr objectAtIndex:i])];
                [_resultat appendString:(@" = ")];
                [_resultat appendString:([_checksArr objectAtIndex:i])];
                [_resultat appendString:(@"; \n")];
            }
            /*DOLD*///NSLog(@"_resultatET ÄR: %d, _tablesArr är %d, _checksArr är %d", [_resultat length], [_tablesArr count], [_checksArr count]);
            //     myTextView.text = _resultat;
            
            
            
            NSMutableArray *db_tablesArr = [[NSMutableArray alloc] init];
            //[NSArray arrayWithObjects: @"prot_arbetsorder_desc", @"prot_arbetsorder", @"falskafyran", nil];
            NSMutableArray *db_checksArr = [[NSMutableArray alloc] init];
            //[NSArray arrayWithObjects: @"100", @"200", @"400", nil];
            
            //Loopa igenom databasens tabell meta_tablechecksums och fyll i arrayerna db_tablesArr och db_checksArr
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" HÄMTA DATA FRÅN TABELLEN meta_tablechecksums INFÖR JÄMFÖRELSE MED SERVERN ");
            //Retrives data before comparision
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            sqlite3_shutdown();
                sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                sqlite3_initialize();
            sqlite3_stmt *statement;
            
            if (_appDelegate.openDbConnection)
            {
                /*DOLD*///NSLog(@"Databasen öppnad");
                NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM meta_tablechecksums"];
                const char *begin_stmt = [beginSQL UTF8String];
                sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                while (sqlite3_step(statement) == SQLITE_ROW) {
                    char *col1 = (char *)sqlite3_column_text(statement, 1);
                    NSString * col1str = nil;
                    if (col1 !=NULL)
                        col1str = [NSString stringWithUTF8String: col1];
                    
                    char *col2 = (char *)sqlite3_column_text(statement, 2);
                    NSString * col2str = nil;
                    if (col2 !=NULL)
                        col2str = [NSString stringWithUTF8String: col2];
                    
                    [db_tablesArr addObject:col1str];
                    [db_checksArr addObject:col2str];
                    
                    /*DOLD*///NSLog(@"Tabellnamn: %@ Checksum: %@",col1str, col2str);
                    
                    
                }
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
                } else {
                    /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                }
                
                /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                beginSQL = nil;
                begin_stmt = nil;
                sqlite3_finalize(statement);
                 
            }
            
            
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" BÖRJAR JÄMFÖRELSE ");
            //Begins comparision
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            
            NSMutableArray *tablesToDropArr = [[NSMutableArray alloc] init];
            NSMutableArray *tablesToAddArr = [[NSMutableArray alloc] init];
            
            //checksum-tabellen droppas alltid för att sedan skapas på nytt
            [tablesToDropArr addObject:@"meta_tablechecksums"];
            
            for(i=0;i<[_tablesArr count];i++){
                NSUInteger indexOfListAinListB = [db_tablesArr indexOfObject: ([_tablesArr objectAtIndex: i])]; //finns tabellen lokalt? (Is table local)
                /*DOLD*///NSLog(@"indexOfListAinListB: %d", indexOfListAinListB);
                NSString *checksumA = [_checksArr objectAtIndex: i]; //Vad är senaste checksum? (What is the latest checksum?)
                NSString *checksumB = nil;
                if(indexOfListAinListB>[db_tablesArr count]){
                    /*DOLD*///NSLog(@"index för stort");
                    /*DOLD*///NSLog(@"finns inte i lista B. Lägg till %@ i tablesToAddArr",[_tablesArr objectAtIndex:i]);
                    //finns inte i lista B. Lägg till i tablesToAddArr
                    [tablesToAddArr addObject:[_tablesArr objectAtIndex:i]];
                }else{
                    checksumB = [db_checksArr objectAtIndex: indexOfListAinListB];
                    /*DOLD*///NSLog(@"Tabell: %@ checksumA: %@ - checksumB: %@", [_tablesArr objectAtIndex: i], checksumA, checksumB);
                    if([checksumA isEqualToString:checksumB]){
                        //checksummorna är lika, tabellen behöver inte uppdateras
                        /*DOLD*///NSLog(@"Checksummorna är lika");
                    }else{
                        //checksummorna är OLIKA, lägg till tabellnamnet i både tablesToDropArr och tablesToAddArr
                        /*DOLD*///NSLog(@"checksummorna är OLIKA, lägg till tabellnamnet '%@' i både tablesToDropArr och tablesToAddArr", [_tablesArr objectAtIndex:i]);
                        
                        [tablesToDropArr addObject:[_tablesArr objectAtIndex:i]];
                        [tablesToAddArr addObject:[_tablesArr objectAtIndex:i]];
                        
                    }
                }
            }
            //Kolla om det finns någon tabell i lista B som inte finns i lista A (ta bort tabeller som finns lokalt med ej på servern)
            for(i=0;i<[db_tablesArr count];i++){
                NSUInteger indexOfListBinListA = [_tablesArr indexOfObject: ([db_tablesArr objectAtIndex: i])]; //finns tabellen lokalt?
                if(indexOfListBinListA>[_tablesArr count]){
                    /*DOLD*///NSLog(@"%@ finns i lista B men inte i lista A. Ta bort den lokalt.",[db_tablesArr objectAtIndex:i]);
                    
                    //finns inte i lista B. Lägg till i tablesToAddArr
                    [tablesToDropArr addObject:[db_tablesArr objectAtIndex:i]];
                }
            }
            
            //JÄMFÖRELSE KLAR. UPPDATERA DATABASEN:
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" FÖRBERED ATT DROPPA TABELLER ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
             
            /*DOLD*/NSLog(@"Drop tables: %@", tablesToDropArr);
            
            
            //DROP TABLES
            //Om den enda tabellen som ska droppas är meta_checksums (dvs om [tablestodrop count]==1), droppa inget! Detta händer om lokala databasen redan är helt uppdaterad.
            if([tablesToDropArr count]>1){
                sqlite3_shutdown();
                    sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                    sqlite3_initialize();
                sqlite3_stmt *statement;
                
                if (_appDelegate.openDbConnection)
                {
                    /*DOLD*///NSLog(@"Databasen öppnad för att DROPPA TABELLER");
                    NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
                    } else {
                        /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    sqlite3_finalize(statement);
                     
                    begin_stmt = nil;
                    beginSQL = nil;
                    
                    NSString * thisSQLstring;
                    NSString *insertSQL;
                    const char *insert_stmt;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _poststosendLabel.text = [NSString stringWithFormat:@"%lu tabeller uppdateras...",[tablesToDropArr count]+[tablesToAddArr count]-2];
                    });
                    
                    _sqlStatementsArr = tablesToDropArr;
                    
                    /*DOLD*///NSLog(@".");
                    /*DOLD*///NSLog(@"Antal statements att köra: %d", [sqlStatementsArr count]);
                    /*DOLD*///NSLog(@".");
                    /*DOLD*///NSLog(@"Mottagen data: %d tecken", [receivedDataString length]);
                    /*DOLD*///NSLog(@". [sqlStatementsArr count]");
                    
                    
                    /*DOLD*///NSLog(@"#################################");
                    /*DOLD*///NSLog(@"#################################");
                    /*DOLD*/NSLog(@" LOOPAR IGENOM TABELLER ATT DROPPA-LISTAN ");
                    /*DOLD*///NSLog(@"#################################");
                    /*DOLD*///NSLog(@"#################################");
                    
                    for(i=0;i<[_sqlStatementsArr count];i++){
                        ///*DOLD*///NSLog(@".");
                        ///*DOLD*///NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([sqlStatementsArr count]), [sqlStatementsArr objectAtIndex:i]);
                        
                        thisSQLstring = nil;
                        insertSQL = [NSString stringWithFormat: @"DROP TABLE IF EXISTS %@_bkp5", [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                        
                        insertSQL = [NSString stringWithFormat: @"ALTER TABLE %@_bkp4 RENAME TO %@_bkp5", [_sqlStatementsArr objectAtIndex:i], [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                        
                        insertSQL = [NSString stringWithFormat: @"ALTER TABLE %@_bkp3 RENAME TO %@_bkp4", [_sqlStatementsArr objectAtIndex:i], [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                        
                        insertSQL = [NSString stringWithFormat: @"ALTER TABLE %@_bkp2 RENAME TO %@_bkp3", [_sqlStatementsArr objectAtIndex:i], [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                        
                        insertSQL = [NSString stringWithFormat: @"ALTER TABLE %@_bkp1 RENAME TO %@_bkp2", [_sqlStatementsArr objectAtIndex:i], [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                        
                        insertSQL = [NSString stringWithFormat: @"ALTER TABLE %@ RENAME TO %@_bkp1", [_sqlStatementsArr objectAtIndex:i], [_sqlStatementsArr objectAtIndex:i]];
                        insert_stmt = [insertSQL UTF8String];
                        sqlite3_prepare_v2(_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
                        if (sqlite3_step(statement) == SQLITE_DONE)
                        {
                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %d", sqlite3_errmsg(nil), [insertSQL length]);
                            /*DOLD*///NSLog(@"\n\nKommando utfört: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        } else {
                            /*DOLD*///NSLog(@"\n\nKommando misslyckades: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                        }
                        sqlite3_finalize(statement);
                         
                    }
                    
                    _sqlStatementsArr = nil;
                    
                    NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
                    const char *commit_stmt = [commitSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, commit_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), commitSQL);
                    } else {
                        /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), commitSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    /*DOLD*///NSLog(@"Finalize: %d", sqlite3_finalize(statement));
                    beginSQL = nil;
                    begin_stmt = nil;
                    insertSQL = nil;
                    insert_stmt = nil;
                    commitSQL = nil;
                    commit_stmt = nil;
                    sqlite3_finalize(statement);
                     
                }
            }
            

            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self->fetchReportsInProgress setTitle:@"Hämtar data..."];
            });
            
            /*DOLD*///NSLog(@"Add tables: %d", [tablesToAddArr count]);
            
            //Gör en array för att fråga servern om sql-script som skapar och fyller de tabeller som saknas på iPaden
            
            
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*/NSLog(@" HÄMTA SQL-INSERT-SCRIPT FRÅN SERVERN ");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            /*DOLD*///NSLog(@"#################################");
            dispatch_async(dispatch_get_main_queue(), ^{
            _updatedLabel.text = @"Laddar ner data från servern...";
            });
            _poststosendLabel.text = @"";
            
            _tablesToRequest = [[NSMutableString alloc] initWithCapacity:0];
            for(i=0;i<[tablesToAddArr count];i++){
                [_tablesToRequest appendString:([tablesToAddArr objectAtIndex:i])];
                [_tablesToRequest appendString:(@",")];
            }
            /*DOLD*///NSLog(@"tablesToRequest: %d", [tablesToRequest length]);
            
            //Hämta data från servern (om det finns något att hämta, dvs om tablesToRequest > 0)
            if([_tablesToRequest length]>0){
                NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
//                NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_remote/get_table_insertstring_sqlite_limited.asp?appversion=%@&user=%@&pass=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], appVersion, _appDelegate.user, _appDelegate.userpassword]];
                
                NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_remote/get_table_insertstring_sqlite_limited.asp?appversion=%@&user=%@&pass=%@&requestedTables=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], appVersion, _appDelegate.user, _appDelegate.userpassword,_tablesToRequest]];

                // Create NSURLSession object
                    NSURLSession *session = [NSURLSession sharedSession];
                    [(AppDelegate*)[UIApplication sharedApplication].delegate showActivity];
                    // Create NSURLSessionDataTask task object by url and session object.
                    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                        NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                        NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);

                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
                        if ([httpResponse statusCode] == 200){
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    [self->fetchReportsInProgress setTitle:@"Sparar data i iPad..."];
                                    self->_poststosendLabel.text = @"All data från servern hämtad. Läser in data i databasen...";
                                });
                                /*SOKORD FETCHDATA*/
    //                            NSString *responseString = [[NSString alloc] initWithData:[data responseData] encoding:NSUTF8StringEncoding];

                                NSLog(@"Response: %lu", (unsigned long)[data length]);
                                NSLog(@"Response: %@", data);

                                self->_receivedDataString = [[NSMutableString alloc] initWithData:data encoding:NSUTF8StringEncoding];

                                //om mottagen data > 0
                                if([self->_receivedDataString length]>0){
                                    sqlite3_shutdown();
                                        sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                                        sqlite3_initialize();
                                    sqlite3_stmt *statement;

                                    //öppna db
                                    if (self->_appDelegate.openDbConnection)
                                    {
                                        //Kommando: BEGIN
                                        /*DOLD*///NSLog(@"Databasen öppnad");
                                        NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
                                        const char *begin_stmt = [beginSQL UTF8String];
                                        sqlite3_prepare_v2(self->_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                                        if (sqlite3_step(statement) == SQLITE_DONE)
                                        {
                                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
                                        } else {
                                            /*DOLD*///NSLog(@"#####%s;   Kommando missly: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                                        }

                                        sqlite3_finalize(statement);

                                        //Kommando: Dela upp mottagen data i en array och loopa igenom alla statements.

                                        /*DOLD*///NSLog(@"#################################");
                                        /*DOLD*///NSLog(@"#################################");
                                        /*DOLD*/NSLog(@" DELA UPP STATEMENTS I ARRAY OCH LOOPA IGENOM ");
                                        /*DOLD*///NSLog(@"#################################");
                                        /*DOLD*///NSLog(@"#################################");

                                        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
                                        dispatch_async(queue, ^{

                                            NSString * thisSQLstring;
                                            NSString *insertSQL;
                                            const char *insert_stmt;

                                            // NSString * tempSqlTest = @"INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn1\", \"adr1\", \"tel1\");INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn2\", \"adr2\", \"tel2\")";
                                            /*DOLD*///NSLog(@"tempSqlTest: %@", tempSqlTest);

                                            self->_sqlStatementsArr = [self->_receivedDataString componentsSeparatedByString: @";###;"];
                                            /*DOLD*///NSLog(@".");
                                            /*DOLD*/NSLog(@"Antal statements att köra self->([sqlStatementsArr count]): %lu", (unsigned long)[self->_sqlStatementsArr count]);
                                            /*DOLD*///NSLog(@".");
                                            /*DOLD*/NSLog(@"Mottagen data: %lu tecken", (unsigned long)[self->_receivedDataString length]);
                                            /*DOLD*/NSLog(@"Mottagen data: %@", self->_receivedDataString);
                                            /*DOLD*///NSLog(@".");


                                            self->_failedSqlStatementsArr = [[NSMutableArray alloc] init];

                                            //Visa alert medan statements körs och appen är upptagen
                                            /*
                                             NSString *alertHeader = [NSString stringWithFormat:@"Kör databaskommandon"];
                                             NSString *alertString = [NSString stringWithFormat:@"Skriver till databasen..."];
                                             UIAlertView *progressAlert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                             [progressAlert show];
                                             */

                                            //Skapa en kö för alla event som ska uppdatera GUI.
                                            //Läs mer: http://stackoverflow.com/questions/10034045/ui-does-not-update-when-main-thread-is-blocked-in-cocoa-app

                                            //                            dispatch_async(queue, ^{
                                            //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:0];});

                                            //                                // Doing some stuff
                                            //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:.25];});

                                            //                                // Doing more stuff
                                            //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:.75];});
                                            //                            });

                                            for(self->i=0;self->i<[self->_sqlStatementsArr count];self->i++){
                                                /*DOLD*///NSLog(@".");
                                                /*DOLD*///NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([_sqlStatementsArr count]), [_sqlStatementsArr objectAtIndex:i]);

                                                float j = (float)self->i;

                                                float k = (float)[self->_sqlStatementsArr count];
                                                //NSLog(@"Loop %d av %d. %d%% färdigt. Kommando: %@", i, [_sqlStatementsArr count], (int)ceil(j/k*100), [[_sqlStatementsArr objectAtIndex:i] substringToIndex:40]);
                                                //NSString * alertText = [NSString stringWithFormat:@"Loop %d av %d. %d%% färdigt.", i, [_sqlStatementsArr count], (i/[_sqlStatementsArr count])];
                                                //[progressAlert setMessage:alertText];
                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                    [self->_progressbar setProgress:(j/k)];
                                                    __weak NSString * progressLabel = [NSString stringWithFormat:@"Kör kommando %d av %lu... %d%% färdigt.", self->i, (unsigned long)[self->_sqlStatementsArr count], (int)ceil(j/k*100)];

                                                    if (j == k-1 || j == k){
                                                        self->_updatedLabel.text = @"Avslutar databasförändringen...";
                                                        self->_poststosendLabel.text = @"";
                                                        [self->_progressbar setProgress:1];
                                                        //[[fetchReportsInProgress alertProgressBar] setProgress:1]; //Funkar ej
                                                    }else{
                                                        self->_updatedLabel.text = @"Sparar in datan i databasen:";
                                                        self->_poststosendLabel.text = progressLabel;
                                                    }
                                                    progressLabel = nil;

                                                });

                                                thisSQLstring = nil;
                                                insertSQL = [NSString stringWithFormat: @"%@", [self->_sqlStatementsArr objectAtIndex:self->i]];
                                                insert_stmt = [insertSQL UTF8String];
                                                sqlite3_prepare_v2(self->_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);

                                                if (sqlite3_step(statement) == SQLITE_DONE)
                                                {
                                                    if ([insertSQL rangeOfString:@"CREATE"].location != NSNotFound) {
                                                        /*DOLD*///5NSLog(@"#####%s; SKAPADE TABELL - Kommando utfört: %@ SQL-svar: %s - Svarskod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
                                                    }else{
                                                        /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), insertSQL);
                                                    }
                                                } else {
                                                    /*DOLD*///NSLog(@"#####%s;   Kommando: %@ SQL-svar: %s - Felkod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));

                                                    [self->_failedSqlStatementsArr addObject:insertSQL];
                                                }

                                                sqlite3_finalize(statement);

                                            }

                                            insert_stmt = nil;
                                            insertSQL = nil;

                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                [self->_progressbar setProgress:0];

                                                [self closeLoadingPopupWithError:false showMessage:true];
                                                [self updateDbInfoBar];

                                            });
                                        });


                                        self->_sqlStatementsArr = nil;


                                        //Kommando: COMMIT
                                        NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
                                        const char *commit_stmt = [commitSQL UTF8String];
                                        sqlite3_prepare_v2(self->_appDelegate.openDbConnection, commit_stmt, -1, &statement, NULL);
                                        if (sqlite3_step(statement) == SQLITE_DONE)
                                        {
                                            sqlite3_finalize(statement);
                                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), commitSQL);
                                        } else {
                                            /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), commitSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                                            sqlite3_finalize(statement);
                                        }

                                        /*DOLD*///NSLog(@"Finalize: %d", sqlite3_finalize(statement));
                                        beginSQL = nil;
                                        begin_stmt = nil;
                                        commit_stmt = nil;
                                        commitSQL = nil;

            //                            sqlite3_finalize(statement);
                                    }
                                }else{
                                    NSLog(@"Fetched data length: 0");
                                }

                                //loopa igenom databasen i loggen


                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@" LOOPA IGENOM DATABASEN FÖR ATT VISA RESULTATET I LOGGEN");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");

                                //Bortkommenterat, rest från begynnelsen
                                //
                                //                    sqlite3_stmt *statement;
                                //
                                //                    if (_appDelegate.openDbConnection)
                                //                    {
                                //                        /*DOLD*///NSLog(@"Listar alla tabeller i databasen:");
                                //                        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name"];
                                //                        const char *begin_stmt = [beginSQL UTF8String];
                                //                        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                                //                        while (sqlite3_step(statement) == SQLITE_ROW) {
                                //                            char *col1 = (char *)sqlite3_column_text(statement, 0);
                                //                            NSString * col1str = nil;
                                //                            if (col1 !=NULL)
                                //                                col1str = [NSString stringWithUTF8String: col1];
                                //
                                //                            char *col2 = (char *)sqlite3_column_text(statement, 1);
                                //                            NSString * col2str = nil;
                                //                            if (col2 !=NULL)
                                //                                col2str = [NSString stringWithUTF8String: col2];
                                //
                                //                            char *col3 = (char *)sqlite3_column_text(statement, 2);
                                //                            NSString * col3str = nil;
                                //                            if (col3 !=NULL)
                                //                                col3str = [NSString stringWithUTF8String: col3];
                                //
                                //                            /*DOLD*/NSLog(@"ID: %@ sortID: %@ Title: %@",col1str, col2str, col3str);
                                //
                                //
                                //                        }
                                //                        if (sqlite3_step(statement) == SQLITE_DONE)
                                //                        {
                                //                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
                                //                        } else {
                                //                            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                                //                        }
                                //
                                //                        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                                //                        beginSQL = nil;
                                //                        begin_stmt = nil;
                                //                        sqlite3_finalize(statement);
                                //                    }


                                //Loopa igenom alla failedSqlStatements
                                /*DOLD*/NSLog(@"\n\n\n#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@" FAILADE STATEMENTS ");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################\n\n\n");

                                NSLog(@"Antal kommandon som misslyckades: %lu", (unsigned long)[self->_failedSqlStatementsArr count]);

                                for(self->i=0;self->i<[self->_failedSqlStatementsArr count];self->i++){
                                    /*DOLD*/NSLog(@"%@",[self->_failedSqlStatementsArr objectAtIndex:self->i]);
                                }


                                //NSLog(@"Succeeded! Received %d bytes of data",[_receivedData length]);


                                // release the connection, and the data object

                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@" STÄNG CONNECTION OCH NOLLSTÄLL DATA");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");
                                /*DOLD*/NSLog(@"#################################");

                                //                connection = nil;
                                self->_receivedData = nil;
                                /*DOLD*///NSLog(@"Nollställer receivedData");
                                //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

                                /*DOLD*///NSLog(@"Nollställer variabler...");

                                /*
                                 _myFetchedData = nil;
                                 _resultat = nil;
                                 _tablesAndChecks = nil;
                                 _tablesArr = nil;
                                 _checksArr = nil;
                                 _tablesToRequest = nil;
                                 _receivedData = nil;
                                 _receivedDataString = nil;

                                 _SQL = nil;
                                 _sqlStatementsArr = nil;
                                 */



                                //Även detta en rest från begynnelsen
                                //                    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
                                //
                                //                    if (_appDelegate.openDbConnection)
                                //                    {
                                //                        /*DOLD*///NSLog(@"Databasen öppnad");
                                //                        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"];
                                //                        const char *begin_stmt = [beginSQL UTF8String];
                                //                        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                                //                        while(sqlite3_step(statement) == SQLITE_ROW) {
                                //                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                                //                            char *col1 = (char *)sqlite3_column_text(statement, 0);
                                //                            if (col1 !=NULL){
                                //
                                //                                [dictionary setObject:[NSString stringWithUTF8String: col1] forKey:@"colname"];
                                //                                [tempArray addObject:[NSString stringWithUTF8String: col1]];
                                //
                                //                            }
                                //
                                //                            /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);
                                //                        }
                                //                        if (sqlite3_step(statement) == SQLITE_DONE)
                                //                        {
                                //                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
                                //                        } else {
                                //                            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d Felmeddelande: %s", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection), sqlite3_errmsg(nil));
                                //                        }
                                //
                                //                        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                                //                        beginSQL = nil;
                                //                        begin_stmt = nil;
                                //                        sqlite3_finalize(statement);
                                //                    }


                                self->_fetchDataIsRunning = @"0";

                                //dispatch_async(dispatch_get_main_queue(), ^{
                                //    [self closeLoadingPopupWithError:false showMessage:false];

                                    [self updateTable];
                                    [self updateDbInfoBar];
                                //});


                                /*VIKTIG*/NSLog(@"Databasen har uppdaterats.");
                            });
                        }else{
//                            NSError *error = &error;
                            NSLog(@"Error: %@", error.localizedDescription);
                            self->_fetchDataIsRunning = @"0";
                            [self closeLoadingPopupWithError:true showMessage:true];
                            [self updateDbInfoBar];
                        }
                    }];

                    // Begin task.
                    [task resume];
//                ASIFormDataRequest *_request = [ASIFormDataRequest requestWithURL:url];
//                __weak ASIFormDataRequest *request = _request;
//                [request setTimeOutSeconds:120];
//
//                [request addPostValue:_tablesToRequest forKey:@"requestedTables"];
//                NSLog(@"Tabeller som hämtas (POST-string): \nrequestedTables=%@",_tablesToRequest);
//
//
//
//
//
//#pragma mark fetchData Request Complete
//
//                [request setCompletionBlock:^{
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self->fetchReportsInProgress setTitle:@"Sparar data i iPad..."];
//                    });
//
//                    self->_poststosendLabel.text = @"All data från servern hämtad. Läser in data i databasen...";
//                    /*SOKORD FETCHDATA*/
//                    NSString *responseString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
//
//                    NSLog(@"Response: %lu", (unsigned long)[responseString length]);
//
//                    self->_receivedDataString = [responseString mutableCopy];
//
//                    //om mottagen data > 0
//                    if([self->_receivedDataString length]>0){
//                        sqlite3_shutdown();
//                            sqlite3_config(SQLITE_CONFIG_SERIALIZED);
//                            sqlite3_initialize();
//                        sqlite3_stmt *statement;
//
//                        //öppna db
//                        if (self->_appDelegate.openDbConnection)
//                        {
//                            //Kommando: BEGIN
//                            /*DOLD*///NSLog(@"Databasen öppnad");
//                            NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
//                            const char *begin_stmt = [beginSQL UTF8String];
//                            sqlite3_prepare_v2(self->_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
//                            if (sqlite3_step(statement) == SQLITE_DONE)
//                            {
//                                /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
//                            } else {
//                                /*DOLD*///NSLog(@"#####%s;   Kommando missly: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
//                            }
//
//                            sqlite3_finalize(statement);
//
//                            //Kommando: Dela upp mottagen data i en array och loopa igenom alla statements.
//
//                            /*DOLD*///NSLog(@"#################################");
//                            /*DOLD*///NSLog(@"#################################");
//                            /*DOLD*/NSLog(@" DELA UPP STATEMENTS I ARRAY OCH LOOPA IGENOM ");
//                            /*DOLD*///NSLog(@"#################################");
//                            /*DOLD*///NSLog(@"#################################");
//
//                            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//                            dispatch_async(queue, ^{
//
//                                NSString * thisSQLstring;
//                                NSString *insertSQL;
//                                const char *insert_stmt;
//
//                                // NSString * tempSqlTest = @"INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn1\", \"adr1\", \"tel1\");INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn2\", \"adr2\", \"tel2\")";
//                                /*DOLD*///NSLog(@"tempSqlTest: %@", tempSqlTest);
//
//                                self->_sqlStatementsArr = [self->_receivedDataString componentsSeparatedByString: @";###;"];
//                                /*DOLD*///NSLog(@".");
//                                /*DOLD*/NSLog(@"Antal statements att köra self->([sqlStatementsArr count]): %lu", (unsigned long)[self->_sqlStatementsArr count]);
//                                /*DOLD*///NSLog(@".");
//                                /*DOLD*/NSLog(@"Mottagen data: %lu tecken", (unsigned long)[self->_receivedDataString length]);
//                                /*DOLD*/NSLog(@"Mottagen data: %@", self->_receivedDataString);
//                                /*DOLD*///NSLog(@".");
//
//
//                                self->_failedSqlStatementsArr = [[NSMutableArray alloc] init];
//
//                                //Visa alert medan statements körs och appen är upptagen
//                                /*
//                                 NSString *alertHeader = [NSString stringWithFormat:@"Kör databaskommandon"];
//                                 NSString *alertString = [NSString stringWithFormat:@"Skriver till databasen..."];
//                                 UIAlertView *progressAlert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                 [progressAlert show];
//                                 */
//
//                                //Skapa en kö för alla event som ska uppdatera GUI.
//                                //Läs mer: http://stackoverflow.com/questions/10034045/ui-does-not-update-when-main-thread-is-blocked-in-cocoa-app
//
//                                //                            dispatch_async(queue, ^{
//                                //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:0];});
//
//                                //                                // Doing some stuff
//                                //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:.25];});
//
//                                //                                // Doing more stuff
//                                //                                dispatch_async(dispatch_get_main_queue(), ^{[self.progress setProgress:.75];});
//                                //                            });
//
//                                for(self->i=0;self->i<[self->_sqlStatementsArr count];self->i++){
//                                    /*DOLD*///NSLog(@".");
//                                    /*DOLD*///NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([_sqlStatementsArr count]), [_sqlStatementsArr objectAtIndex:i]);
//
//                                    float j = (float)self->i;
//
//                                    float k = (float)[self->_sqlStatementsArr count];
//                                    //NSLog(@"Loop %d av %d. %d%% färdigt. Kommando: %@", i, [_sqlStatementsArr count], (int)ceil(j/k*100), [[_sqlStatementsArr objectAtIndex:i] substringToIndex:40]);
//                                    //NSString * alertText = [NSString stringWithFormat:@"Loop %d av %d. %d%% färdigt.", i, [_sqlStatementsArr count], (i/[_sqlStatementsArr count])];
//                                    //[progressAlert setMessage:alertText];
//                                    dispatch_async(dispatch_get_main_queue(), ^{
//                                        [self->_progressbar setProgress:(j/k)];
//                                        __weak NSString * progressLabel = [NSString stringWithFormat:@"Kör kommando %d av %lu... %d%% färdigt.", self->i, (unsigned long)[self->_sqlStatementsArr count], (int)ceil(j/k*100)];
//
//                                        if (j == k-1 || j == k){
//                                            self->_updatedLabel.text = @"Avslutar databasförändringen...";
//                                            self->_poststosendLabel.text = @"";
//                                            [self->_progressbar setProgress:1];
//                                            //[[fetchReportsInProgress alertProgressBar] setProgress:1]; //Funkar ej
//                                        }else{
//                                            self->_updatedLabel.text = @"Sparar in datan i databasen:";
//                                            self->_poststosendLabel.text = progressLabel;
//                                        }
//                                        progressLabel = nil;
//
//                                    });
//
//                                    thisSQLstring = nil;
//                                    insertSQL = [NSString stringWithFormat: @"%@", [self->_sqlStatementsArr objectAtIndex:self->i]];
//                                    insert_stmt = [insertSQL UTF8String];
//                                    sqlite3_prepare_v2(self->_appDelegate.openDbConnection, insert_stmt, -1, &statement, NULL);
//
//                                    if (sqlite3_step(statement) == SQLITE_DONE)
//                                    {
//                                        if ([insertSQL rangeOfString:@"CREATE"].location != NSNotFound) {
//                                            /*DOLD*///5NSLog(@"#####%s; SKAPADE TABELL - Kommando utfört: %@ SQL-svar: %s - Svarskod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
//                                        }else{
//                                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), insertSQL);
//                                        }
//                                    } else {
//                                        /*DOLD*///NSLog(@"#####%s;   Kommando: %@ SQL-svar: %s - Felkod: %d", sqlite3_errmsg(nil), insertSQL, sqlite3_errmsg(nil), sqlite3_errcode(_appDelegate.openDbConnection));
//
//                                        [self->_failedSqlStatementsArr addObject:insertSQL];
//                                    }
//
//                                    sqlite3_finalize(statement);
//
//                                }
//
//                                insert_stmt = nil;
//                                insertSQL = nil;
//
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    [self->_progressbar setProgress:0];
//
//                                    [self closeLoadingPopupWithError:false showMessage:true];
//                                    [self updateDbInfoBar];
//
//                                });
//                            });
//
//
//                            self->_sqlStatementsArr = nil;
//
//
//                            //Kommando: COMMIT
//                            NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
//                            const char *commit_stmt = [commitSQL UTF8String];
//                            sqlite3_prepare_v2(self->_appDelegate.openDbConnection, commit_stmt, -1, &statement, NULL);
//                            if (sqlite3_step(statement) == SQLITE_DONE)
//                            {
//                                sqlite3_finalize(statement);
//                                /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), commitSQL);
//                            } else {
//                                /*DOLD*///NSLog(@"#####%s;   Kommando misslyckades: %@; Felkod: %d", sqlite3_errmsg(nil), commitSQL, sqlite3_errcode(_appDelegate.openDbConnection));
//                                sqlite3_finalize(statement);
//                            }
//
//                            /*DOLD*///NSLog(@"Finalize: %d", sqlite3_finalize(statement));
//                            beginSQL = nil;
//                            begin_stmt = nil;
//                            commit_stmt = nil;
//                            commitSQL = nil;
//
////                            sqlite3_finalize(statement);
//                        }
//                    }else{
//                        NSLog(@"Fetched data length: 0");
//                    }
//
//                    //loopa igenom databasen i loggen
//
//
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@" LOOPA IGENOM DATABASEN FÖR ATT VISA RESULTATET I LOGGEN");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//
//                    //Bortkommenterat, rest från begynnelsen
//                    //
//                    //                    sqlite3_stmt *statement;
//                    //
//                    //                    if (_appDelegate.openDbConnection)
//                    //                    {
//                    //                        /*DOLD*///NSLog(@"Listar alla tabeller i databasen:");
//                    //                        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name"];
//                    //                        const char *begin_stmt = [beginSQL UTF8String];
//                    //                        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
//                    //                        while (sqlite3_step(statement) == SQLITE_ROW) {
//                    //                            char *col1 = (char *)sqlite3_column_text(statement, 0);
//                    //                            NSString * col1str = nil;
//                    //                            if (col1 !=NULL)
//                    //                                col1str = [NSString stringWithUTF8String: col1];
//                    //
//                    //                            char *col2 = (char *)sqlite3_column_text(statement, 1);
//                    //                            NSString * col2str = nil;
//                    //                            if (col2 !=NULL)
//                    //                                col2str = [NSString stringWithUTF8String: col2];
//                    //
//                    //                            char *col3 = (char *)sqlite3_column_text(statement, 2);
//                    //                            NSString * col3str = nil;
//                    //                            if (col3 !=NULL)
//                    //                                col3str = [NSString stringWithUTF8String: col3];
//                    //
//                    //                            /*DOLD*/NSLog(@"ID: %@ sortID: %@ Title: %@",col1str, col2str, col3str);
//                    //
//                    //
//                    //                        }
//                    //                        if (sqlite3_step(statement) == SQLITE_DONE)
//                    //                        {
//                    //                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
//                    //                        } else {
//                    //                            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
//                    //                        }
//                    //
//                    //                        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
//                    //                        beginSQL = nil;
//                    //                        begin_stmt = nil;
//                    //                        sqlite3_finalize(statement);
//                    //                    }
//
//
//                    //Loopa igenom alla failedSqlStatements
//                    /*DOLD*/NSLog(@"\n\n\n#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@" FAILADE STATEMENTS ");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################\n\n\n");
//
//                    NSLog(@"Antal kommandon som misslyckades: %lu", (unsigned long)[self->_failedSqlStatementsArr count]);
//
//                    for(self->i=0;self->i<[self->_failedSqlStatementsArr count];self->i++){
//                        /*DOLD*/NSLog(@"%@",[self->_failedSqlStatementsArr objectAtIndex:self->i]);
//                    }
//
//
//                    //NSLog(@"Succeeded! Received %d bytes of data",[_receivedData length]);
//
//
//                    // release the connection, and the data object
//
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@" STÄNG CONNECTION OCH NOLLSTÄLL DATA");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//                    /*DOLD*/NSLog(@"#################################");
//
//                    //                connection = nil;
//                    self->_receivedData = nil;
//                    /*DOLD*///NSLog(@"Nollställer receivedData");
//                    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
//
//                    /*DOLD*///NSLog(@"Nollställer variabler...");
//
//                    /*
//                     _myFetchedData = nil;
//                     _resultat = nil;
//                     _tablesAndChecks = nil;
//                     _tablesArr = nil;
//                     _checksArr = nil;
//                     _tablesToRequest = nil;
//                     _receivedData = nil;
//                     _receivedDataString = nil;
//
//                     _SQL = nil;
//                     _sqlStatementsArr = nil;
//                     */
//
//
//
//                    //Även detta en rest från begynnelsen
//                    //                    NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
//                    //
//                    //                    if (_appDelegate.openDbConnection)
//                    //                    {
//                    //                        /*DOLD*///NSLog(@"Databasen öppnad");
//                    //                        NSString *beginSQL = [NSString stringWithFormat: @"SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"];
//                    //                        const char *begin_stmt = [beginSQL UTF8String];
//                    //                        sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
//                    //                        while(sqlite3_step(statement) == SQLITE_ROW) {
//                    //                            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
//                    //                            char *col1 = (char *)sqlite3_column_text(statement, 0);
//                    //                            if (col1 !=NULL){
//                    //
//                    //                                [dictionary setObject:[NSString stringWithUTF8String: col1] forKey:@"colname"];
//                    //                                [tempArray addObject:[NSString stringWithUTF8String: col1]];
//                    //
//                    //                            }
//                    //
//                    //                            /*DOLD*///NSLog(@"Antal rader: %d",[tempArray count]);
//                    //                        }
//                    //                        if (sqlite3_step(statement) == SQLITE_DONE)
//                    //                        {
//                    //                            /*DOLD*///NSLog(@"#####%s;   Kommando utfört: %@", sqlite3_errmsg(nil), beginSQL);
//                    //                        } else {
//                    //                            /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d Felmeddelande: %s", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection), sqlite3_errmsg(nil));
//                    //                        }
//                    //
//                    //                        /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
//                    //                        beginSQL = nil;
//                    //                        begin_stmt = nil;
//                    //                        sqlite3_finalize(statement);
//                    //                    }
//
//
//                    self->_fetchDataIsRunning = @"0";
//
//                    //dispatch_async(dispatch_get_main_queue(), ^{
//                    //    [self closeLoadingPopupWithError:false showMessage:false];
//
//                        [self updateTable];
//                        [self updateDbInfoBar];
//                    //});
//
//
//                    /*VIKTIG*/NSLog(@"Databasen har uppdaterats.");
//                }];
//
//#pragma mark fetchData Request Failed
//                [request setFailedBlock:^{
//                    NSError *error = [request error];
//                    NSLog(@"Error: %@", error.localizedDescription);
//                    self->_fetchDataIsRunning = @"0";
//                    [self closeLoadingPopupWithError:true showMessage:true];
//                    [self updateDbInfoBar];
//
//                }];

//                [request startAsynchronous];
                //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

                //Allt klart här! När requesten är färdig uppdateras databasen.

                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" UPPDATERA SENAST-UPPDATERAD-DATUM ");
                /*DOLD*///NSLog(@"#################################");
                
                
                NSString *docsDir;
                NSArray *dirPaths;
                
                // Get the documents directory
                dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                docsDir = [dirPaths objectAtIndex:0];
                
                
                
                sqlite3_shutdown();
                    sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                    sqlite3_initialize();
                
                sqlite3_stmt *statement;
                
                if (_appDelegate.openDbConnection)
                {
                    /*DOLD*///NSLog(@"Kör kommando: %@", sql);
                    NSString *beginSQL = @"CREATE TABLE IF NOT EXISTS sys_dbsyncinfo (id integer primary key autoincrement, updated datetime default (datetime('now', 'localtime')))";
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
                    } else {
                        /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    
                    /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                    beginSQL = nil;
                    begin_stmt = nil;
                    sqlite3_finalize(statement);
                     
                }
                if (_appDelegate.openDbConnection)
                {
                    /*DOLD*///NSLog(@"Kör kommando: %@", sql);
                    NSString *beginSQL = @"INSERT INTO sys_dbsyncinfo DEFAULT VALUES";
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
                    } else {/*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    
                    /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                    beginSQL = nil;
                    begin_stmt = nil;
                    sqlite3_finalize(statement);
                     
                }
                
                if(![_appDelegate.userDictionary valueForKey:@"fullname"]){
                    //                NSString *alertString = [NSString stringWithFormat:@"Databasen har uppdaterats"];
                    //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Du loggas nu ut för att kunna logga in som rätt användare." delegate:self cancelButtonTitle:@"Fortsätt" otherButtonTitles:nil];
                    //[alert show];
                    //[self performSelector:@selector(logoutUser)];
                }
                
                
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" SLUT PÅ UPPDATERA SENAST-UPPDATERAD-DATUM ");
                /*DOLD*///NSLog(@"#################################");
                
                
            }else{
                /*VIKTIG*/NSLog(@"Databasen behövde inte uppdateras.");
                /*DOLD*///NSLog(@"############################### \n############################### \nDatabasen är redan up-to-date!\n###############################\n###############################");
                //     myTextView.text = @"Databasen är redan up-to-date!";
                
                //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                
                
                //NSString *alertString = [NSString stringWithFormat:@"Databasen behövde inte uppdateras."];
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
                //[alert show];
                //[alert dismissWithClickedButtonIndex:0 animated:YES];
                
                
                
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" UPPDATERA SENAST-UPPDATERAD-DATUM ");
                /*DOLD*///NSLog(@"#################################");
                
                
                NSString *docsDir;
                NSArray *dirPaths;
                
                // Get the documents directory
                dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                
                docsDir = [dirPaths objectAtIndex:0];
                
                
                
                sqlite3_shutdown();
                    sqlite3_config(SQLITE_CONFIG_SERIALIZED);
                    sqlite3_initialize();
                
                sqlite3_stmt *statement;
                
                if (_appDelegate.openDbConnection)
                {
                    /*DOLD*///NSLog(@"Kör kommando: %@", sql);
                    NSString *beginSQL = //@"drop table if exists sys_dbsyncinfo";//
                    @"CREATE TABLE IF NOT EXISTS sys_dbsyncinfo (id integer primary key autoincrement, updated datetime default (datetime('now', 'localtime')))";
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
                    } else {
                        /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    
                    /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                    beginSQL = nil;
                    begin_stmt = nil;
                    sqlite3_finalize(statement);
                }
                if (_appDelegate.openDbConnection)
                {
                    /*DOLD*///NSLog(@"Kör kommando: %@", sql);
                    NSString *beginSQL = @"INSERT INTO sys_dbsyncinfo DEFAULT VALUES";
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    if (sqlite3_step(statement) == SQLITE_DONE)
                    {
                        /*DOLD*///NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
                    } else {/*DOLD*///                    NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
                    }
                    
                    /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
                    beginSQL = nil;
                    begin_stmt = nil;
                    sqlite3_finalize(statement);
                }
                
                
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" SLUT PÅ UPPDATERA SENAST-UPPDATERAD-DATUM ");
                /*DOLD*///NSLog(@"#################################");
                
                _fetchDataIsRunning = @"0";
                
                [self updateTable];
                [self updateDbInfoBar];
                
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@" SLUT PÅ FETCHDATA! ");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                /*DOLD*///NSLog(@"#################################");
                
            }
        }
        // Detta sätts när all data är mottagen och införd i databasen.
        //        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible: NO];
    }
}


#pragma mark
#pragma mark IAMultipartRequestGenerator delegate methods

-(void)requestDidFailWithError:(NSError *)error {
    
    NSLog(@"request failed");
    
}
-(void)requestDidFinishWithResponse:(NSData *)responseData {
    
    NSString *response = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    
    NSLog(@"SERVER RESPONSE: %@", response);
    
}

#pragma mark
#pragma mark Välj backup-läge

-(IBAction)changeSegBackupView{
    NSLog(@"changeSegBackupView, tab %ld", (long)_changeSegBackupViewController.selectedSegmentIndex);
    
    // _changeSegBackupViewController.tintColor = [UIColor lightGrayColor];
    //    [[[_changeSegBackupViewController subviews] objectAtIndex:_changeSegBackupViewController.selectedSegmentIndex] setTintColor:[UIColor darkGrayColor]];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    for (int j=0; j<[_changeSegUrvalController.subviews count]; j++){
        UIColor *tintcolor=[UIColor blackColor];
        [[_changeSegUrvalController.subviews objectAtIndex:j] setTintColor:tintcolor];
    }
    if (_changeSegUrvalController.selectedSegmentIndex != UISegmentedControlNoSegment){
        UIColor *tintcolor=[UIColor blueColor];
        [[_changeSegUrvalController.subviews objectAtIndex:_changeSegUrvalController.selectedSegmentIndex] setTintColor:tintcolor];
    }
    
    
    if(_changeSegBackupViewController.selectedSegmentIndex == 0){
        NSLog(@"Backup: inget tabellsuffix valt");
        appDelegate.tableBackup = @"";
    }
    if(_changeSegBackupViewController.selectedSegmentIndex == 1){
        NSLog(@"Backup: tabellsuffix '_bkp1' valt");
        appDelegate.tableBackup = @"_bkp1";
    }
    if(_changeSegBackupViewController.selectedSegmentIndex == 2){
        NSLog(@"Backup: tabellsuffix '_bkp2' valt");
        appDelegate.tableBackup = @"_bkp2";
    }
    if(_changeSegBackupViewController.selectedSegmentIndex == 3){
        NSLog(@"Backup: tabellsuffix '_bkp3' valt");
        appDelegate.tableBackup = @"_bkp3";
    }
    if(_changeSegBackupViewController.selectedSegmentIndex == 4){
        NSLog(@"Backup: tabellsuffix '_bkp4' valt");
        appDelegate.tableBackup = @"_bkp4";
    }
    if(_changeSegBackupViewController.selectedSegmentIndex == 5){
        NSLog(@"Backup: tabellsuffix '_bkp5' valt");
        appDelegate.tableBackup = @"_bkp5";
    }
}


-(IBAction)changeSegUrval{
    NSLog(@"changeSegUrval, tab %ld", (long)_changeSegUrvalController.selectedSegmentIndex);
    
    // _changeSegBackupViewController.tintColor = [UIColor lightGrayColor];
    //    [[[_changeSegBackupViewController subviews] objectAtIndex:_changeSegBackupViewController.selectedSegmentIndex] setTintColor:[UIColor darkGrayColor]];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString * loggedInUser = appDelegate.user;     //[NSString stringWithFormat:@"Lars"];
    
    
    for (int j=0; j<[_changeSegUrvalController.subviews count]; j++){
        UIColor *tintcolor=[UIColor blackColor];
        [[_changeSegUrvalController.subviews objectAtIndex:j] setTintColor:tintcolor];
    }
    if (_changeSegUrvalController.selectedSegmentIndex != UISegmentedControlNoSegment){
        UIColor *tintcolor=[UIColor blueColor];
        [[_changeSegUrvalController.subviews objectAtIndex:_changeSegUrvalController.selectedSegmentIndex] setTintColor:tintcolor];
    }
    
    if(_changeSegUrvalController.selectedSegmentIndex == 0){
        _filterOnUsername = [NSMutableString stringWithString:loggedInUser];
    }
    if(_changeSegUrvalController.selectedSegmentIndex == 1){
        _filterOnUsername = [NSMutableString stringWithString:@"usersToday"];
    }
    if(_changeSegUrvalController.selectedSegmentIndex == 2){
        _filterOnUsername = [NSMutableString stringWithString:@"usersAlphabetical"];
    }
    if(_changeSegUrvalController.selectedSegmentIndex == 3){
        _filterOnUsername = [NSMutableString stringWithString:@"viewAll"];
    }
    
    [self updateTable];
}


@end

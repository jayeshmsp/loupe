//
//  sendReportsPopoverViewController.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2013-07-04.
//  Copyright (c) 2013 David Forsberg. All rights reserved.
//

#import "SecondViewController.h"

@interface sendReportsPopoverViewController : SecondViewController
{
	//Senaste raden som klickades på, sätts i didSelectRowAtIndexPath
	int selectedRowNumber;
	//Den klickade radens templ. och woid, används i funktionen som öppnar uppladdad rapport i webbläsaren
	int selectedRowNumberTemplate;
	int selectedRowNumberWorkorderId;
	
	//Bool som håller koll på om någon synk körs och blockerar andra isf
	bool httpRequestIsRunning;
}
//appDelegate
@property (strong, nonatomic) AppDelegate *appDelegate;

//TableView:en
@property (nonatomic, strong) IBOutlet UITableView *tableView;

//Array som håller tabellens värden, fylls i cellforrowatindexpath
@property (nonatomic, retain) NSMutableArray *sendReportsTableContent;


@end

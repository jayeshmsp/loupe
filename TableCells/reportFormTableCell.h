//
//  reportFormTableCell.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-07.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface reportFormTableCell : UITableViewCell<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) IBOutlet UITextView *longLabel;
@property (nonatomic, weak) IBOutlet UILabel *primaryLabel;
@property (nonatomic, weak) IBOutlet UILabel *secondaryLabel;

@property (nonatomic, weak) IBOutlet UILabel *htmltext;

@property (nonatomic, weak) IBOutlet UITextField *inputField;
@property (nonatomic, strong) IBOutlet UITextView *longText;
//@property (nonatomic, retain) IBOutlet UIWebView *htmlTextWebView;
@property (nonatomic, retain) IBOutlet WKWebView *htmlTextWebView;
@property (nonatomic, retain) IBOutlet UISwitch *chkBox;
@property (nonatomic, retain) IBOutlet UITextView *autotextTextView;

@property (nonatomic, weak) IBOutlet UILabel *getCustomerNamn;
@property (nonatomic, weak) IBOutlet UILabel *getCustomerAdress;
@property (nonatomic, weak) IBOutlet UILabel *getCustomerPostnr;
@property (nonatomic, weak) IBOutlet UILabel *getCustomerStad;
@property (nonatomic, weak) IBOutlet UILabel *getCustomerTelefon;
@property (nonatomic, weak) IBOutlet UILabel *getCustomerMail;

@property (nonatomic, weak) IBOutlet UITextField *getCustomerNamnField;
@property (nonatomic, weak) IBOutlet UITextField *getCustomerAdressField;
@property (nonatomic, weak) IBOutlet UITextField *getCustomerPostnrField;
@property (nonatomic, weak) IBOutlet UITextField *getCustomerStadField;
@property (nonatomic, weak) IBOutlet UITextField *getCustomerTelefonField;
@property (nonatomic, weak) IBOutlet UITextField *getCustomerMailField;

@property (nonatomic, retain) IBOutlet UIButton *renameZoneButton;
@property (nonatomic, retain) IBOutlet UIButton *renameRoomButton;

@property (nonatomic, retain) IBOutlet UIImageView *signatureImageView;

@property (nonatomic, retain) IBOutlet UIImageView *imageView1;
@property (nonatomic, retain) IBOutlet UIImageView *imageView2;
@property (nonatomic, retain) IBOutlet UIImageView *imageView3;

- (IBAction)takePhoto:  (id)sender;
- (IBAction)selectPhoto:(id)sender;
- (IBAction)deletePhoto:(id)sender;

- (IBAction)takePhoto2:  (id)sender;
- (IBAction)selectPhoto2:(id)sender;
- (IBAction)deletePhoto2:(id)sender;

- (IBAction)takePhoto3:  (id)sender;
- (IBAction)selectPhoto3:(id)sender;
- (IBAction)deletePhoto3:(id)sender;


@end

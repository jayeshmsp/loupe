//
//  AttachImagesCollectionViewCell.m
//  OHBSYS Storyboards iOS8
//
//  Created by David Forsberg on 2015-09-27.
//  Copyright (c) 2015 David Forsberg. All rights reserved.
//

#import "AttachImagesCollectionViewCell.h"

@implementation AttachImagesCollectionViewCell : UICollectionViewCell

@synthesize imageView = _imageView;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        [_imageView setClipsToBounds:YES];
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_imageView];
    }
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
}
@end
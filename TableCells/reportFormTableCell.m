//
//  reportFormTableCell.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-07.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "reportFormTableCell.h"

@implementation reportFormTableCell

@synthesize primaryLabel, secondaryLabel, htmltext, inputField, longText;



- (void) changeState: (id) sender
{
    _chkBox = (UISwitch*)sender;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) takePicture:(id) sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    else
    {
        [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    
    [imagePicker setDelegate:self];
    //[self presentModalViewController:imagePicker animated:YES];
}


- (IBAction)takePhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    //[self presentViewController:picker animated:YES completion:NULL];
}

- (void)takePhoto2:(id)sender{
    
}

- (void)takePhoto3:(id)sender{
    
}

- (IBAction)selectPhoto:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    //[self presentViewController:picker animated:YES completion:NULL];
}

- (void)selectPhoto2:(id)sender{
    
}

- (void)selectPhoto3:(id)sender{
    
}

- (IBAction)deletePhoto:(id)sender {
    NSLog(@"deletePhoto not implemented!");
}

- (void)deletePhoto2:(id)sender{
    
}

- (void)deletePhoto3:(id)sender{
    
}

@end

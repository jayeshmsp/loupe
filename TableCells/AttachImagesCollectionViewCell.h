//
//  AttachImagesCollectionViewCell.h
//  OHBSYS Storyboards iOS8
//
//  Created by David Forsberg on 2015-09-27.
//  Copyright (c) 2015 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttachImagesCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@end

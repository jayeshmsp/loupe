//
//  AutotextSortCell.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-11-20.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AutotextSortCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *atLabel;
@property (nonatomic, strong) IBOutlet UITextView *atText;
@property (weak, nonatomic) IBOutlet UITextField *atTextOneRow;
@property (weak, nonatomic) IBOutlet UISwitch *atRiskSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *atImageSwitch;

@end

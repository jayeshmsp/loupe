//
//  ReportDetailsViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-07.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "ReportDetailsViewController.h"
#import "reportFormTableCell.h"
#import "AutotextSortViewController.h"
#import "AttachImagesCollectionViewController.h"
#import "signBezierPath.h"
#import <QuartzCore/QuartzCore.h>
#import "NSString+HTML.h"

@interface ReportDetailsViewController ()

@end

@implementation ReportDetailsViewController

@synthesize workOrder, reportFieldsStr;
@synthesize templateInfo = _templateInfo;
@synthesize reportFields = _reportFields;
@synthesize reportInitialValues = _reportInitialValues;
@synthesize editMode = _editMode;
@synthesize workOrderForClickedButton = _workOrderForClickedButton;

#pragma mark numberOfRowsInSection
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _inputFieldTagCounterArr = [[NSMutableArray alloc] init];
    _inputFieldTagDict = [[NSMutableDictionary alloc] init];

    return [_reportFields count];
}


#pragma mark heightForRowAtIndexPath
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int thisRowHeight;
    NSString * thisFieldTypeIdentifier = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"];
    NSString * thisFieldLook = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldlook"];
    
	
	
    //Kolla efter fetchFromWorkorder, som har formatet "fetchForWorkorder,kolumnnamn,fieldtype"
    if ([thisFieldTypeIdentifier rangeOfString:@","].location == NSNotFound) {
       /*DOLD*///NSLog(@"inget kommatecken i fieldtype");
        
    } else {
       /*DOLD*///NSLog(@"kommatecken i fieldtype, gör array");
        NSMutableArray * fieldtypeArr = [[NSMutableArray alloc] init];
        fieldtypeArr = [[thisFieldTypeIdentifier componentsSeparatedByString:@","] mutableCopy];
        thisFieldTypeIdentifier = [fieldtypeArr objectAtIndex:0];
        if ([fieldtypeArr count] > 2){
            //Om målfältets fieldtype är angiven, använd den för att beräkna höjden.
            thisFieldTypeIdentifier = [fieldtypeArr objectAtIndex:2];
        }
    }
    
    
    if(
       [thisFieldTypeIdentifier isEqualToString:@"inputLongText"] || [thisFieldTypeIdentifier isEqualToString:@"simpleAutotext"]
       ){
		NSString *thisFieldColName = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"] lowercaseString];
//		if([_appDelegate.reportValuesDelegate count]>0 && 1==2){
        if([_appDelegate.reportValuesDelegate count]>0){
			
			NSString *thisFieldName =  [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldname"];
			NSString *thisFieldValue = [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName];
			
            
//			CGSize labelSize = [thisFieldName sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(220-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize labelSize = [thisFieldName boundingRectWithSize:CGSizeMake(220-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
//			CGSize textSize = [thisFieldValue sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(480-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisFieldValue boundingRectWithSize:CGSizeMake(480-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;

            
			thisRowHeight = MAX(130.0f, MAX(labelSize.height+20,textSize.height+20));
		}else{
			thisRowHeight = 130;
		}
    }else if(
             [thisFieldTypeIdentifier isEqualToString:@"signatureSimple"] || [thisFieldTypeIdentifier isEqualToString:@"htmltext"]
             ){
        if([thisFieldLook isEqualToString:@"25"]){
            thisRowHeight = 130*6;
        }else{
            thisRowHeight = 130;
        }
	}else if(
             [thisFieldTypeIdentifier isEqualToString:@"getCustomer"] ||
             [thisFieldTypeIdentifier isEqualToString:@"getCustomerBuyer"] ||
             [thisFieldTypeIdentifier isEqualToString:@"getCustomerSeller"] ||
             [thisFieldTypeIdentifier isEqualToString:@"getCustomerBroker"] ||
             [thisFieldTypeIdentifier isEqualToString:@"getCustomerEstate"] ||
             [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerBuyer"] ||
             [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerSeller"] ||
             [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerBroker"] ||
             [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerEstate"]
             ){
        thisRowHeight = 160;
    }else if(
             [thisFieldTypeIdentifier isEqualToString:@"getCustomer"]
             ){
        if([_editMode intValue]==1){
            thisRowHeight = 160; //Om man vill använda input-versionen av getCustomer, sätt 160 (+ ev extramarginal);
        }else{
            thisRowHeight = 160;
        }
	}else if([thisFieldTypeIdentifier isEqualToString:@"autotextinfo"] || [thisFieldTypeIdentifier isEqualToString:@"autotextinfo-view"]){
		NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"text"];
		
		//Räkna ut höjden på textview. Default är 10px margin runtom, därför +/- 20 för att beräkningarna ska stämma
		
		NSString *text = thisLabelText;
//		CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
        CGSize textSize = [text boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
        
        thisRowHeight = MAX(textSize.height+20, 50.0f);
        
    }else if([thisFieldTypeIdentifier isEqualToString:@"autotextnote"] || [thisFieldTypeIdentifier isEqualToString:@"autotextnote-view"]){
        NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"text"];
        
        NSString *text = thisLabelText;
//        CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
        CGSize textSize = [text boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
        thisRowHeight = MAX(textSize.height+20, 50.0f);
        
    //}else if([thisFieldTypeIdentifier isEqualToString:@"attachImages"] || [thisFieldTypeIdentifier isEqualToString:@"attachImages-view"]){
      //  thisRowHeight = 160;
    }else{
        thisRowHeight = 50;
    }
    
    
    /*DOLD*///NSLog(@"Räknar ut höjd för %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"]);

        
    return thisRowHeight;
}


#pragma mark didSelectRowAtIndexPath
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
	reportFormTableCell * cell = [tableView cellForRowAtIndexPath:indexPath];
	
	// get the reference to the text field
	if(cell.inputField){
		[cell.inputField becomeFirstResponder];
	}
	if(cell.longText){
		[cell.longText becomeFirstResponder];
	}
	
//    [textField selectText:self];
//    [[textField currentEditor] setSelectedRange:NSMakeRange([[textField stringValue] length], 0)];
}












#pragma mark
#pragma mark
#pragma mark CellForRowAtIndexPath
/*
 #####################################################################
 #####################################################################
 ###################### CELL FOR ROW AT INDEX PATH ###################
 #####################################################################
 #####################################################################

 #######################################################################
 ##### longLabel = till vänster, fältnamn på flera rader           #####
 ##### primaryLabel = till vänster, fältnamn eller rubrik          #####
 ##### secondaryLabel = till höger, fältvärde                      #####
 ##### longText = till höger, fältvärde inputLongText och SimpleAT #####
 #######################################################################

 */
- (reportFormTableCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	
	
    /*DOLD*/NSLog(@"cellforrowatindexpath");
    //Om view:en fått info (via segue) att man inte ska kunna redigera (0), välj den cellprototyp som heter XXXXX-view
    NSString * simpleTableIdentifier;
    NSString *thisFieldTypeIdentifier = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"];
    NSString *thisFieldFetchColumn = [[NSString alloc] init];
    NSString *thisFieldFetchColumnType = [[NSString alloc] init];

    //Kolla efter fetchFromWorkorder, som har formatet "fetchForWorkorder,kolumnnamn,fieldtype"
    if ([thisFieldTypeIdentifier rangeOfString:@","].location == NSNotFound) {
       /*DOLD*///NSLog(@"inget kommatecken i fieldtype");
        
        if([_editMode intValue]==1){
            simpleTableIdentifier = thisFieldTypeIdentifier;
        }else{
            simpleTableIdentifier = [NSString stringWithFormat:@"%@-view", thisFieldTypeIdentifier];
        }
        
        thisFieldFetchColumn = @"";
        thisFieldFetchColumnType = @"";
        
    } else {
       /*DOLD*///NSLog(@"kommatecken i fieldtype, gör array");
        NSMutableArray * fieldtypeArr = [[NSMutableArray alloc] init];
        fieldtypeArr = [[thisFieldTypeIdentifier componentsSeparatedByString:@","] mutableCopy];
        thisFieldTypeIdentifier = [fieldtypeArr objectAtIndex:0];
        thisFieldFetchColumn = [[fieldtypeArr objectAtIndex:1] lowercaseString];
        if ([fieldtypeArr count] > 2){
            thisFieldFetchColumnType = [fieldtypeArr objectAtIndex:2];
        }
    }
    
    if([thisFieldTypeIdentifier isEqualToString:@"fetchFromWorkorder"]){
        if([thisFieldFetchColumnType isEqualToString:@"getCustomer"] ||
           [thisFieldFetchColumnType isEqualToString:@"getCustomerBuyer"] ||
           [thisFieldFetchColumnType isEqualToString:@"getCustomerSeller"] ||
           [thisFieldFetchColumnType isEqualToString:@"getCustomerBroker"] ||
           [thisFieldFetchColumnType isEqualToString:@"getCustomerEstate"] ||
           [thisFieldFetchColumnType isEqualToString:@"fetchCustomerBuyer"] ||
           [thisFieldFetchColumnType isEqualToString:@"fetchCustomerSeller"] ||
           [thisFieldFetchColumnType isEqualToString:@"fetchCustomerBroker"] ||
           [thisFieldFetchColumnType isEqualToString:@"fetchCustomerEstate"]
           ){
            //Välj getcustomer-view-mall
            simpleTableIdentifier = [NSString stringWithFormat:@"getCustomer-view"];
        } else {
            simpleTableIdentifier = [NSString stringWithFormat:@"%@-view",thisFieldFetchColumnType];
        }
    }
    
	NSLog(@"simpleTableIdentifier: %@", simpleTableIdentifier);
	
    //Initiera cell; om ingen återvinningsbar finns, skapa den (alloc + init)
    reportFormTableCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        cell = [[reportFormTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
    }
    
    NSString *thisFieldType =       thisFieldTypeIdentifier; // [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"];
    NSString *thisFieldName =       [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldname"];
    NSString *thisFieldFritext =    [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fritext"];
    NSString *thisFieldColName =    [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"] lowercaseString];
    NSString * thisFieldValue;
    if([_appDelegate.reportValuesDelegate count]>0){
		if([[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName]){
			thisFieldValue =     [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName];
		}else{
			thisFieldValue = @"";
		}
    }else{
		if([_defaultReportValues count]>0 && [_editMode intValue] == 1){
			NSLog(@"Använder värde från defaultReportValues för colname %@: %@",thisFieldColName, [[_defaultReportValues objectAtIndex:0] valueForKey: thisFieldColName]);
			thisFieldValue = [[_defaultReportValues objectAtIndex:0] valueForKey: thisFieldColName];
		}else{
			thisFieldValue = [NSString stringWithFormat:@""];
		}
    }
	//filtrera bort nil
	if(!thisFieldValue){
		thisFieldValue = @"";
	}

	/*DOLD3*/NSLog(@"Objekt för raden: %@",[_reportFields objectAtIndex:indexPath.row]);
    
    /*DOLD*///NSLog(@"##### %@ #####", thisFieldType);
    
	/*
	 ##################### DEFAULT-cell ###################
	 */
	
	 cell.primaryLabel.text = thisFieldName;
	 cell.longLabel.text = thisFieldName ;
	 cell.secondaryLabel.text = thisFieldValue;
	 cell.longText.text = thisFieldValue;
	 cell.inputField.text = thisFieldValue;
    
    //Set cellens bakgrundsfärg om någon färg är satt på i desc-tabellen
    
    NSString *thisFieldBackgroundColor = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolor"];
    if(thisFieldBackgroundColor != nil){
        thisFieldBackgroundColor = [thisFieldBackgroundColor stringByReplacingOccurrencesOfString:@"#" withString:@""];
        if(![thisFieldBackgroundColor isEqualToString:@""]){
            cell.backgroundColor = [self colorWithHexString: thisFieldBackgroundColor];
        }else{
            cell.backgroundColor = [self colorWithHexString: @"ffffff"];
        }
    }else{
        cell.backgroundColor = [self colorWithHexString: @"ffffff"];
    }
	


#pragma mark FetchFromWorkorder
    /*
     #####################################################################
     ################ Formatera cell för FetchFromWorkorder ##############
     #####################################################################
     */
    if([thisFieldType isEqualToString:@"fetchFromWorkorder"]){
        cell.userInteractionEnabled = FALSE;
        
        //sätt thisfieldvalue till värdet av arbetsorderns värde för fältet
        sqlite3_stmt *statement;
        
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM prot_arbetsorder WHERE id = %@",workOrder];
            /*DOLD*///NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"Listar prot_tabell");
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int i = 0;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    /*#dolt#*//*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                thisFieldValue = [[dictionary mutableCopy] valueForKey:thisFieldFetchColumn];
				dictionary = nil;
               /*DOLD3*///NSLog(@"FFW: thisFieldColName: %@ thisfieldvalue: %@", thisFieldColName, thisFieldValue);
            }

        beginSQL = nil;
        begin_stmt = nil;
        sqlite3_finalize(statement);
		}else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 8");
		}


        
        
#pragma mark ffw: selectBesiktningsman
        /*
         ##################### selectBesiktningsman ###################
         */
		if([thisFieldFetchColumnType isEqualToString:@"selectBesiktningsman"]){
            
            //Hämta kund från DB
            NSMutableArray * besmanArray = [[NSMutableArray alloc] initWithCapacity:0];
            sqlite3_stmt *statement;
            
            if (_appDelegate.openDbConnection)
            {
                /*DOLD*///NSLog(@"Databasen öppnad - selectbesiktningsman - view");
                NSString *beginSQL = [NSString stringWithFormat: @"select sys_users.id, sys_users.username, sys_users.fullname, sys_userroles.role from sys_users INNER JOIN sys_userroles ON sys_users.role = sys_userroles.ID WHERE sys_users.id = %@;", thisFieldValue];
                /*DOLD*///NSLog(@"SQL: %@", beginSQL);
                const char *begin_stmt = [beginSQL UTF8String];
                sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                
                /*DOLD*///NSLog(@"Listar prot_tabell");
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    NSString * myTempMutStr = [[NSMutableString alloc] init];
                    
                    int i = 0;
                    for (i=0; i<colCount;i++) {
                        //Ger namnet på kolumn N
                        const char *colName = (const char*)sqlite3_column_name(statement, i);
                        
                        char *colValue = (char *)sqlite3_column_text(statement, i);
                        if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                        (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                        myTempMutStr = nil;
                        /*DOLD3*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                    }
                    [besmanArray addObject:dictionary];
                }
                        
            beginSQL = nil;
            begin_stmt = nil;
            sqlite3_finalize(statement);
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 9");
			}

            
			
			NSString * thisCellLabel;
			if([besmanArray count]>0){
				thisCellLabel = [[besmanArray objectAtIndex:0] valueForKey:@"fullname"];
			}
            cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
			cell.secondaryLabel.text = thisCellLabel;
            /*DOLD*///NSLog(@"%@",[[besmanArray objectAtIndex:0] valueForKey:@"fullname"]);
        
			
        }else if([thisFieldFetchColumnType isEqualToString:@"getCustomer"]) {
#pragma mark ffw: getCustomer
        
            NSString * custId = thisFieldValue;
            NSMutableArray * custArray = [[NSMutableArray alloc] init];
            
            //Hämta kunden från db
            if (_appDelegate.openDbConnection)
            {
                
                /*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
                NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_customers WHERE id = %@;", custId];
                /*DOLD*///NSLog(@"SQL: %@", beginSQL);
                const char *begin_stmt = [beginSQL UTF8String];
                sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                
                /*DOLD*///NSLog(@"Listar prot_tabell");
                //Ger antalet columner för select-statement "pStmt"
                int colCount = sqlite3_column_count(statement);
                /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
                
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    NSString * myTempMutStr = [[NSMutableString alloc] init];
                    
                    int i = 0;
                    for (i=0; i<colCount;i++) {
                        //Ger namnet på kolumn N
                        const char *colName = (const char*)sqlite3_column_name(statement, i);
                        
                        char *colValue = (char *)sqlite3_column_text(statement, i);
                        if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                        (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                        myTempMutStr = nil;
                        /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                    }
                    [custArray addObject:dictionary];
                }
    beginSQL = nil;
    begin_stmt = nil;
        sqlite3_finalize(statement);

        
            }else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 10");
			}
            
            //NSLog(@"custArray: %@", custArray);
            //NSLog(@"custArray count: %d", [custArray count]);
            
            if([custArray count]>0){
                if (![[[custArray objectAtIndex:0] valueForKey:@"name"] isEqualToString:@""]) {
                    cell.getCustomerNamn.text = [[custArray objectAtIndex:0] valueForKey:@"name"];
                }else{
                    cell.getCustomerNamn.text = @"";
                }
                if (![[[custArray objectAtIndex:0] valueForKey:@"address"] isEqualToString:@""]) {
                    cell.getCustomerAdress.text = [[custArray objectAtIndex:0] valueForKey:@"address"];
                }else{
                    cell.getCustomerAdress.text = @"";
                }
                if (![[[custArray objectAtIndex:0] valueForKey:@"postnr"] isEqualToString:@""]) {
                    cell.getCustomerPostnr.text = [[custArray objectAtIndex:0] valueForKey:@"postnr"];
                }else{
                    cell.getCustomerPostnr.text = @"";
                }
                if (![[[custArray objectAtIndex:0] valueForKey:@"city"] isEqualToString:@""]) {
                    cell.getCustomerStad.text = [[custArray objectAtIndex:0] valueForKey:@"city"];
                }else{
                    cell.getCustomerStad.text = @"";
                }
                if (![[[custArray objectAtIndex:0] valueForKey:@"phone"] isEqualToString:@""]) {
                    cell.getCustomerTelefon.text = [[custArray objectAtIndex:0] valueForKey:@"phone"];
                }else{
                    cell.getCustomerTelefon.text = @"";
                }
                if (![[[custArray objectAtIndex:0] valueForKey:@"mail"] isEqualToString:@""]) {
                    cell.getCustomerMail.text = [[[custArray objectAtIndex:0] valueForKey:@"mail"] stringByReplacingOccurrencesOfString:@"&#64;" withString:@"@"];
                }
            }else{
                cell.getCustomerNamn.text = @"Ingen kund vald";
            }
            cell.longLabel.text = thisFieldName ;
		cell.secondaryLabel.text = thisFieldName;
            
            
            
        }else
		
			
		if([thisFieldFetchColumnType isEqualToString:@"XXXXXXX"]) {
#pragma mark ffw: XXXXXXX
            
        
        } else {
#pragma mark ffw: DEFAULT
            /*
             ##################### DEFAULT-cell ###################
             */
            cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
			cell.secondaryLabel.text = thisFieldValue;
            cell.longText.text = thisFieldValue;
        }
    }

    /*
     if([thisFieldTypeIdentifier isEqualToString:@"getCustomer"] ||
       [thisFieldTypeIdentifier isEqualToString:@"getCustomerBuyer"] ||
       [thisFieldTypeIdentifier isEqualToString:@"getCustomerSeller"] ||
       [thisFieldTypeIdentifier isEqualToString:@"getCustomerBroker"] ||
       [thisFieldTypeIdentifier isEqualToString:@"getCustomerEstate"] ||
       [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerBuyer"] ||
       [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerSeller"] ||
       [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerBroker"] ||
       [thisFieldTypeIdentifier isEqualToString:@"fetchCustomerEstate"]
       ){
        simpleTableIdentifier = @"getCustomer-view";
    }else{
        if([_editMode intValue]==1){
            simpleTableIdentifier = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"];
        }else{
            simpleTableIdentifier = [NSString stringWithFormat:@"%@-view",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"]];
        }
    }
    */
    
#pragma mark
#pragma mark ##### Formatera celler övriga fälttyper
#pragma mark

	NSDate* sourceDate = [NSDate date]; // your NSDate
	
	NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
	NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
	
	NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
	NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
	NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
	
	NSDate* destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
	
	//NSLog(@"Timestamp: %@",destinationDate);

	
#pragma mark addDate
	if([_editMode intValue]!=1){
        //NSLog(@"Timestamp: %@",destinationDate);
		
        /*
         ##################### addedDate ###################
         */
        if([thisFieldType isEqualToString:@"addedDate"]){
            /*DOLD*///NSLog(@"Hittade addedDate");
            
            cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
            if ([thisFieldValue isEqualToString:@""]){
                /*DOLD*///NSLog(@"addedDate = nil");
                cell.secondaryLabel.text = [[NSString stringWithFormat:@"%@", destinationDate] substringWithRange:NSMakeRange(0, 11)];
            }else{
                /*DOLD*///NSLog(@"addedDate har ett värde satt: %@",thisFieldValue);
                cell.secondaryLabel.text = thisFieldValue;
            }
        }
		
	}else{
#pragma mark -edit
		
		/*
		 ##################### addedDate ###################
		 */
		if([thisFieldType isEqualToString:@"addedDate"]){
			/*DOLD*///NSLog(@"Hittade AddedDate");
			
			cell.longLabel.text = thisFieldName ;
			cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
			if ([thisFieldValue isEqualToString:@""]){
				cell.inputField.text = [[NSString stringWithFormat:@"%@", destinationDate] substringWithRange:NSMakeRange(0, 11)];
			}else{
				cell.inputField.text = thisFieldValue;
			}
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:[thisFieldColName lowercaseString]];
			/*DOLD*///NSLog(@"_inputFieldTagCounterArr: %@",_inputFieldTagCounterArr);
			//Sätt fältets tag till räknarens count
			cell.inputField.tag = [_inputFieldTagCounterArr count];
			cell.inputField.delegate = self;
			/*DOLD*///NSLog(@"TAG: %d",cell.inputField.tag);
			
			//Spara fältets tag som kolumnens namn i dictionaryt
            [_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			
		}
	}
#pragma mark updateDate
	if([_editMode intValue]!=1){
		/*
		 ##################### updatedDate ###################
		 */
		if([thisFieldType isEqualToString:@"updatedDate"]){
			cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
			if ([thisFieldValue isEqualToString:@""]){
				cell.secondaryLabel.text = [[NSString stringWithFormat:@"%@", destinationDate] substringWithRange:NSMakeRange(0, 11)];
			}else{
				cell.secondaryLabel.text = thisFieldValue;
			}
		}
	}else{
#pragma mark -edit
		/*
		 ##################### updatedDate ###################
		 */
		if([thisFieldType isEqualToString:@"updatedDate"]){
			/*DOLD*///NSLog(@"Hittade updatedDate");
			
			cell.longLabel.text = thisFieldName ;
			cell.primaryLabel.text = thisFieldName;
			cell.inputField.text = [[NSString stringWithFormat:@"%@", destinationDate] substringWithRange:NSMakeRange(0, 11)];
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:[thisFieldColName lowercaseString]];
			/*DOLD*///NSLog(@"_inputFieldTagCounterArr: %@",_inputFieldTagCounterArr);
			//Sätt fältets tag till räknarens count
			cell.inputField.tag = [_inputFieldTagCounterArr count];
			cell.inputField.delegate = self;
			
			/*DOLD*///NSLog(@"TAG: %d",cell.inputField.tag);
			//Spara fältets tag som kolumnens namn i dictionaryt
			[_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			
		}
	}
#pragma mark inputShortText
	if([_editMode intValue]!=1){
		/*
		 ##################### inputShortText ###################
		 */
		if([thisFieldType isEqualToString:@"inputShortText"]){
			cell.primaryLabel.text = thisFieldName ;
			cell.longLabel.text = thisFieldName ;
			cell.secondaryLabel.text = thisFieldValue;
            cell.inputField.text = thisFieldValue;
            cell.userInteractionEnabled = FALSE;
		}
	}else{
#pragma mark -edit
		/*
		 ##################### inputShortText ###################
		 */
		if([thisFieldType isEqualToString:@"inputShortText"]){
			cell.primaryLabel.text = thisFieldName ;
			cell.longLabel.text = thisFieldName ;
			cell.secondaryLabel.text = thisFieldValue;
			cell.inputField.text = thisFieldValue;
			
			/*DOLD*///NSLog(@"##### INPUTSHORTTEXT: %@",thisFieldColName);
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:[thisFieldColName lowercaseString]];
			/*DOLD*///NSLog(@"_inputFieldTagCounterArr: %@",_inputFieldTagCounterArr);
			//Sätt fältets tag till räknarens count
			cell.inputField.tag = [_inputFieldTagCounterArr count];
			cell.inputField.delegate = self;
			//Spara fältets tag som kolumnens namn i dictionaryt
			[_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			[_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:[thisFieldColName lowercaseString]];
		}
	}
#pragma mark inputLongText
	if([_editMode intValue]!=1){
		/*
		 ##################### inputLongText ###################
		 */
		if([thisFieldType isEqualToString:@"inputLongText"]){
			cell.longLabel.text = thisFieldName;
			cell.longText.text = thisFieldValue;
            cell.userInteractionEnabled = TRUE;
		}
	}else{
#pragma mark -edit
		/*
		 ##################### inputLongText ###################
		 */
		if([thisFieldType isEqualToString:@"inputLongText"]){
			cell.longLabel.text = thisFieldName;
			cell.longText.text = thisFieldValue;
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:thisFieldColName];
			//Sätt fältets tag till räknarens count
			cell.longText.tag = [_inputFieldTagCounterArr count];
			cell.longText.delegate = self;
			//Spara fältets tag som kolumnens namn i dictionaryt
            [_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			
			//Kolla om detta är inputfält för risk-texter
			if([[thisFieldColName lowercaseString] isEqualToString:@"risk"]){
				//Spara tag:en
                NSLog(@"_tagForRiskInputField: %lu",(unsigned long)[_inputFieldTagCounterArr count]);
                _tagForRiskInputField = [NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]];
			}
			
			cell.longLabel.text = thisFieldName ;
			// cell.longText.text = thisFieldValue;
			
//			CGSize labelSize = [thisFieldName sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(220-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize labelSize = [thisFieldName boundingRectWithSize:CGSizeMake(220-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
//			CGSize textSize = [thisFieldValue sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(480-24, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisFieldValue boundingRectWithSize:CGSizeMake(480-24, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
			
			//Sätt höjd till fältet som behöver mest utrymme, eller 130 som minimum
            //[cell.longLabel setFrame:CGRectMake(20, 0, 220, MAX(130.0f, MAX(labelSize.height+24,textSize.height+24)))];
            //[cell.longText setFrame:CGRectMake(250, 0, 480, MAX(130.0f, MAX(labelSize.height+24,textSize.height+24)))];
            [cell.longLabel setFrame:CGRectMake(20, 0, 220, 130)];
            [cell.longText setFrame:CGRectMake(250, 0, 480, 130)];
            
            
            
			//[cell.longLabel setBackgroundColor:[UIColor greenColor]];
			//[cell.longText setBackgroundColor:[UIColor blueColor]];
			
			NSLog(@"labelsize: %f valuesize: %f MAX: %f",labelSize.height, textSize.height, MAX(130.0f, MAX(labelSize.height+20,textSize.height+20)));
			
			
			//The rounded corner part, where you specify your view's corner radius:
			cell.longText.layer.cornerRadius = 5;
			cell.longText.clipsToBounds = YES;
			[cell.longText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
			[cell.longText.layer setBorderWidth:2.0];
		}
		
	}
#pragma mark simpleAutotext
	if([_editMode intValue]!=1){
		/*
		 ##################### simpleAutotext ###################
		 */
		if([thisFieldType isEqualToString:@"simpleAutotext"]){
			
			cell.longLabel.text = thisFieldName;
            cell.longText.text = thisFieldValue;
            cell.userInteractionEnabled = FALSE;
			
		}
	}else{
#pragma mark -edit
		/*
		 ##################### simpleAutotext ###################
		 */
		if([thisFieldType isEqualToString:@"simpleAutotext"]){
			/*DOLD3*///NSLog(@"Hittade simpleAutotext");
			
			
			//NSLog(@"appdelegate-objekt: %@ \n\n%@ \n\n%@", appDelegate.simpleAutotextDict,thisFieldColName, [appDelegate.simpleAutotextDict valueForKey:thisFieldColName]);
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:thisFieldColName];
			//Sätt fältets tag till räknarens count
			cell.longText.tag = [_inputFieldTagCounterArr count];
			cell.longText.delegate = self;
			//Spara fältets tag som kolumnens namn i dictionaryt
            [_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			
			//Kolla om detta är inputfält för risk-texter
			if([[thisFieldColName lowercaseString] isEqualToString:@"risk"]){
				//Spara tag:en
                NSLog(@"_tagForRiskInputField: %lu",(unsigned long)[_inputFieldTagCounterArr count]);
				_tagForRiskInputField = [NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]];
			}
			
			
			if([_appDelegate.simpleAutotextDict valueForKey:thisFieldColName] && ![[_appDelegate.simpleAutotextDict valueForKey:thisFieldColName] isEqualToString:@""]){
				
				cell.longText.text = [_appDelegate.simpleAutotextDict valueForKey:thisFieldColName];
				[_appDelegate.simpleAutotextDict setValue:nil forKey:thisFieldColName];
				
				//Spara fältets nya värde i reportValuesDelegate
				//Uppdatera reportValuesDelegate med fältets innehåll
				if([_appDelegate.reportValuesDelegate count]>0){
					[[_appDelegate.reportValuesDelegate objectAtIndex:0] setValue:cell.longText.text forKey:thisFieldColName];
					
					NSLog(@"Uppdaterade reportValuesDelegate: %@ = \"%@\" \nVärde i delegate: %@",thisFieldColName, cell.longText.text, [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:thisFieldColName]);
					
					//NSLog(@"Hela reportValuesDelegate: %@", appDelegate.reportValuesDelegate);
				}else{
					NSLog(@"Kunde inte uppdatera reportValuesDelegate med det nya simpleAutotext-värdet - reportValuesDelegate.count var <1! Fält: %@; Text: \"%@\"",thisFieldColName, cell.longText.text);
				}
				
				NSLog(@"Become first responder...");
				[cell.longText becomeFirstResponder];
				NSLog(@"Resigning first responder...");
				[cell.longText resignFirstResponder];
				NSLog(@"Slut på tilläggning av simpleautotext");
				
				
			}else{
				//Kolla om detta är inputfält för risk-texter
				if([[thisFieldColName lowercaseString] isEqualToString:@"risk"]){
					
					NSLog(@"RISKTEXT! Text: %@ + risktext: %@ = %@",thisFieldValue, _appDelegate.riskText, [NSString stringWithFormat:@"%@\n\n%@",thisFieldValue, _appDelegate.riskText]);
					
					if (_appDelegate.riskText) {
						//Lägg till risktexten som ligger i appDelegate
						if(thisFieldValue && ![thisFieldValue isEqualToString:@""]){
							cell.longText.text = [NSString stringWithFormat:@"%@\n\n%@",thisFieldValue, _appDelegate.riskText];
						}else{
							cell.longText.text = [NSString stringWithFormat:@"%@", _appDelegate.riskText];
						}
						
						//Spara fältets nya värde i reportValuesDelegate
						//Uppdatera reportValuesDelegate med fältets innehåll
						if([_appDelegate.reportValuesDelegate count]>0){
							[[_appDelegate.reportValuesDelegate objectAtIndex:0] setValue:cell.longText.text forKey:thisFieldColName];
							
							NSLog(@"Uppdaterade reportValuesDelegate: %@ = \"%@\" \nVärde i delegate: %@",thisFieldColName, cell.longText.text, [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:thisFieldColName]);
							
							//NSLog(@"Hela reportValuesDelegate: %@", appDelegate.reportValuesDelegate);
						}
						
						//Töm risktext i appdelegate
						_appDelegate.riskText = nil;
					}else{
						cell.longText.text = thisFieldValue;
					}
				}else{
					//NSLog(@"inte risktext");
					cell.longText.text = thisFieldValue;
				}
				
			}
			
			
			
			//The rounded corner part, where you specify your view's corner radius:
			
			cell.longText.layer.cornerRadius = 5;
			cell.longText.clipsToBounds = YES;
			[cell.longText.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
			[cell.longText.layer setBorderWidth:2.0];
			
		}
	}
#pragma mark signatureSimple
    if([_editMode intValue]!=1){
        /*
         ##################### signatureSimple ###################
         */
        if([thisFieldType isEqualToString:@"signatureSimple"]){
            /*DOLD3*///NSLog(@"Hittade signatureSimple");
            
            
            //NSLog(@"appdelegate-objekt blob count: %u", [appDelegate.signatureImageData length]);
            
            //NSLog(@"value: %@", thisFieldValue);
            
            cell.longLabel.text = [NSString stringWithFormat:@"Signatur: %@",thisFieldName];
            cell.secondaryLabel.text = @"";
            cell.userInteractionEnabled = FALSE;
            
            if(thisFieldValue){
                NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",thisFieldValue]];
                
                // Create file manager
                NSFileManager *fileMgr = [NSFileManager defaultManager];
                
                UIImage *image = [[UIImage alloc] init];
                
                //Finns filen?
                if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                    image = [UIImage imageWithContentsOfFile:jpgPath];
                }else{
                    image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_signatures/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],thisFieldValue]]]];
                }
                
                if(!image){
                    //cell.secondaryLabel.text = @"Signatur finns, men kunde inte laddas från servern just nu. Är iPad uppkopplad mot";
                }
                NSLog(@"%@",image);
                
                [cell.signatureImageView setImage:image];
            }
            
        }
    }else{
#pragma mark -edit
        /*
         ##################### signatureSimple ###################
         */
        if([thisFieldType isEqualToString:@"signatureSimple"]){
            /*DOLD3*///NSLog(@"Hittade signatureSimple");
            
            cell.longLabel.text = [NSString stringWithFormat:@"Signatur: %@",thisFieldName];
            cell.secondaryLabel.text = @"";
            
            NSLog(@"appdelegate-objekt blob count: %lu", (unsigned long)[_appDelegate.signatureImageData length]);
            
            NSLog(@"value: %@", thisFieldValue);
            if ([thisFieldValue isEqualToString:@"nil"] || [thisFieldValue isEqualToString:@"null"] || [thisFieldValue isEqualToString:@"(null)"]) {
                thisFieldValue = nil;
            }
            
            /*if([[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName]){
             NSData * imageData = [[NSData alloc] initWithBytes:[[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName] length:[[[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName] length]];
             UIImage *image = [UIImage imageWithData:imageData];
             [cell.signatureImageView setImage:image];
             }*/
            
            /*
             if([_blobdictionary valueForKey:thisFieldColName]){
             NSData * imageData = [_blobdictionary valueForKey:thisFieldColName];
             UIImage *image = [UIImage imageWithData:imageData];
             [cell.signatureImageView setImage:image];
             }
             */
//            if(_appDelegate.signatureImageData ){
//                NSData * imageData = _appDelegate.signatureImageData;
//                UIImage *image = [UIImage imageWithData:imageData];
//                [cell.signatureImageView setImage:image];
//            }else{
//                if(thisFieldValue && ![thisFieldValue isEqualToString:@"nil"]){
//                    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",thisFieldValue]];
//                    
//                    // Create file manager
//                    NSFileManager *fileMgr = [NSFileManager defaultManager];
//                    
//                    UIImage *image = [[UIImage alloc] init];
//                    
//                    //Finns filen?
//                    if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
//                        image = [UIImage imageWithContentsOfFile:jpgPath];
//                    }else{
//                        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_signatures/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],thisFieldValue]]]];
//                    }
//                    
//                    if(thisFieldValue && !image){
//                        //Visas när den inte ska!
//                        //	cell.secondaryLabel.text = @"Signatur finns, men kunde inte laddas!";
//                    }
//                    NSLog(@"%@",image);
//                    [cell.signatureImageView setImage:image];
//                }
//            }
            
            
            
            if([_appDelegate.signatureImagesDict objectForKey:thisFieldColName] ){
                NSData * imageData = [_appDelegate.signatureImagesDict objectForKey:thisFieldColName];
                UIImage *image = [UIImage imageWithData:imageData];
                [cell.signatureImageView setImage:image];
            }else{
                if(thisFieldValue && ![thisFieldValue isEqualToString:@"nil"]){
                    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",thisFieldValue]];
                    
                    // Create file manager
                    NSFileManager *fileMgr = [NSFileManager defaultManager];
                    
                    UIImage *image = [[UIImage alloc] init];
                    
                    
                    
                    // Check if image was saved
                    NSError *error;
                    //NSFileManager *fileMgr = [NSFileManager defaultManager];
                    
                    // Point to Document directory
                    NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                    
                    // Write out the contents of home directory to console
                    NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
                    
                    
                    
                    
                    
                    //Finns filen?
                    if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                        image = [UIImage imageWithContentsOfFile:jpgPath];
                    }else{
                        image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/_signatures/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],thisFieldValue]]]];
                    }
                    
                    if(thisFieldValue && !image){
                        //Visas när den inte ska!
                        //	cell.secondaryLabel.text = @"Signatur finns, men kunde inte laddas!";
                    }
                    NSLog(@"%@",image);
                    [cell.signatureImageView setImage:image];
                }
            }
        }
    }
#pragma mark attachImages
#pragma mark -edit
    if([_editMode intValue]!=1 || [_editMode intValue]==1){
        /*
         ##################### attachImages ###################
         */
        if([thisFieldType isEqualToString:@"attachImages"]){
            
            if ([_appDelegate.attachImagesDict valueForKey: thisFieldColName]) {
                //Uppdatera fältet med tillagda bilder
                thisFieldValue = [_appDelegate.attachImagesDict valueForKey: thisFieldColName];
                
            }

            cell.longLabel.text = [NSString stringWithFormat:@"Bilder: %@",thisFieldName];
            cell.secondaryLabel.text = @"";
            
            NSString *msg = @"Inga bilder inlagda";
            
            NSArray *imageAndTextList = nil;
            
            if ([thisFieldValue length] > 0){
                if([thisFieldValue rangeOfString:@"###;###"].location != NSNotFound){
                    imageAndTextList = [thisFieldValue componentsSeparatedByString:@"###;###"];
                }else{
                    imageAndTextList = [NSArray arrayWithObjects: thisFieldValue, nil];
                }
            }
            if([imageAndTextList count] > 0){
                if([imageAndTextList count] == 1){
                    msg = [NSString stringWithFormat:@"%lu bild inlagd", (unsigned long)[imageAndTextList count]];
                }else{
                    msg = [NSString stringWithFormat:@"%lu bilder inlagda", (unsigned long)[imageAndTextList count]];
                }
                
                //Ladda ner alla bilder i cache
                
                //Kolla så cache-mapp finns eller skapa den
                NSError *error;
                NSString *cacheFolderPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//Cache"]];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:cacheFolderPath]){
                    [[NSFileManager defaultManager] createDirectoryAtPath:cacheFolderPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
                }
                
                
                NSLog(@"Loopar alla filer i cachen för att radera bilder äldre än 7 dagar...");
                int count;
                
                NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//Cache"]] error:NULL];
                for (count = 0; count < (int)[directoryContent count]; count++)
                {
                    NSString *curPath = [NSString stringWithString:[NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//Cache//%@", [directoryContent objectAtIndex:count]]]];
                    
                    NSDictionary* attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:curPath error:nil];
                    NSDate *date = (NSDate*)[attrs objectForKey: NSFileCreationDate];
                    
                    double age = [[NSDate date] timeIntervalSince1970] - [date timeIntervalSince1970];
                    
                    if(age > 60*60*24*7){
                        //File is older than a week
                        //Delete file
                        NSError *error;
                        if ([[NSFileManager defaultManager] isDeletableFileAtPath:curPath]) {
                            BOOL success = [[NSFileManager defaultManager] removeItemAtPath:curPath error:&error];
                            if (!success) {
                                NSLog(@"Error removing file at path: %@", error.localizedDescription);
                            }else{
                                //NSLog(@"File deleted, older than 7 days: %@", [directoryContent objectAtIndex:count]);
                            }
                        }
                    }else{
                        //Behåll filen, mindre än 7 dygn gammal
                        //NSLog(@"File %d: %@, age: %f", (count + 1), [directoryContent objectAtIndex:count], age);
                    }
                    
                }
                
                
                
                
                NSLog(@"Klart. Laddar ner aktuella bilder till cache-mappen... %lu bilder", (unsigned long)[imageAndTextList count]);
                //Loopa
                for (int i = 0; i < [imageAndTextList count]; i++) {
                    
                    // Get file name
                    NSString *imageVal = [imageAndTextList objectAtIndex:i];
                    NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
                    NSString *imageFilename = [imageObject objectAtIndex:0];
                    
                    NSString  *jpgCachePath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//Cache//cached_%@", imageFilename]];
                    
                    //Kolla om filen finns, annars ladda ner och spara den
                    
                    //Finns filen?
                    NSFileManager *fileMgr = [NSFileManager defaultManager];
                    if ([fileMgr fileExistsAtPath: jpgCachePath ] == YES){
                        //NSLog(@"Bilden %@ finns redan i cache.", imageFilename);
                    }else{
                        //Ladda ner filen och spara i cache-mappen
                        //NSLog(@"Bilden %@ ska laddas ner till cachen.", imageFilename);
                        
                        // Use GCD's background queue
                        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                            // Save it into file system
                            //NSLog(@"Sparar bild... ");
                            NSString *imgUrl = [NSString stringWithFormat:@"%@/_images/%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], imageFilename];
                            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString: imgUrl]];
                            
                            [imgData writeToFile:jpgCachePath atomically:YES];
                        });

                    }
                    
                } //Slut loop
                
                
                NSLog(@"Slut på loop, cachen uppdaterad.");
                
                //Loopa igenom hela mappen och radera filer som är äldre än 7 dagar
                
                /*
                // Use GCD's background queue
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    // Generate the file path
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectory = [paths objectAtIndex:0];
                    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"yourfilename.dat"];
                    
                    // Save it into file system
                    [imgData writeToFile:dataPath atomically:YES];
                });
                */
                
                
                
            }else{
                msg = [NSString stringWithFormat:@"Inga bilder inlagda"];
            }
            
            [cell.secondaryLabel setText:msg];
        }
    }
#pragma mark HTML
		/*
		 ##################### HTML-text ###################
		 */
		if([thisFieldType isEqualToString:@"htmltext"]){
			/*DOLD*///NSLog(@"htmlmode");
			
			NSString *path = [[NSBundle mainBundle] bundlePath];
			NSURL *baseURL = [NSURL fileURLWithPath:path];
			
			NSString * thisCss = [NSString stringWithFormat:@"<style type=\"text/css\">body,h1,h2,h3,p{font-family:helvetica;}</style>"];
            NSString * thisHTML = [NSString stringWithFormat:@"%@ %@",thisCss,thisFieldFritext];
            /*DOLD3*/NSLog(@"################### HTML ###############");
            /*DOLD3*/NSLog(@"%@", thisHTML);
            /*DOLD3*/NSLog(@"################### /HTML ###############");
			[cell.htmlTextWebView loadHTMLString:thisHTML baseURL:baseURL];
			cell.htmlTextWebView.scrollView.scrollEnabled = YES;
			cell.htmlTextWebView.scrollView.bounces = YES;
		}
#pragma mark chkBox
	if([_editMode intValue]!=1){
		/*
		 ##################### chkBox ###################
		 */
        if([thisFieldType isEqualToString:@"chkBox"]){
            cell.userInteractionEnabled = FALSE;
			if([thisFieldValue isEqualToString:@"0"]){
				[cell.chkBox setOn:NO];
			}else{
				[cell.chkBox setOn:YES];
			}
			[cell.chkBox setEnabled:NO];
		}
	}else{
#pragma mark -edit
		/*
		 ##################### chkBox ###################
		 */
        if([thisFieldType isEqualToString:@"chkBox"]){
            cell.userInteractionEnabled = FALSE;
			/*DOLD*///NSLog(@"CHECKBOX: %@",thisFieldValue);
			
			if(thisFieldValue == 0){
				/*DOLD*///NSLog(@"checkbox off");
				[cell.chkBox setOn:NO];
			}else{
				/*DOLD*///NSLog(@"checkbox on");
				[cell.chkBox setOn:YES];
			}
			[cell.chkBox setEnabled:YES];
			
			//Lägg till fält i räknaren
			[_inputFieldTagCounterArr addObject:[thisFieldColName lowercaseString]];
			/*DOLD*///NSLog(@"_inputFieldTagCounterArr: %@",_inputFieldTagCounterArr);
			//Sätt fältets tag till räknarens count
			[cell.chkBox setTag:[_inputFieldTagCounterArr count]];
			//            cell.chkBox.tag = [_inputFieldTagCounterArr count];
			//            cell.chkBox.delegate = self;
			
			//http://stackoverflow.com/questions/5415653/how-to-catch-event-of-switch-changed-in-custom-tableviewcell
			
			/*DOLD*///NSLog(@"Checkbox count tag : %d", [_inputFieldTagCounterArr count]);
			/*DOLD*///NSLog(@"Checkbox tag: %d", cell.chkBox.tag);
			//Spara fältets tag som kolumnens namn i dictionaryt
            [_inputFieldTagDict setValue:[NSString stringWithFormat:@"%lu",(unsigned long)[_inputFieldTagCounterArr count]] forKey:thisFieldColName];
			
			
			//Det finns ingen funktion som sparar UIswitchars värde/läge i reportValuesDelegate,
			//så fälttypen är inaktiverad på ipad tills detta är löst
			[cell.chkBox setEnabled:NO];
		}
	}
#pragma mark datePicker
	/*
	 ##################### datepicker ###################
	 */
	if([thisFieldType isEqualToString:@"datepicker"]){
		NSString * thisLabelText;
		if ([thisFieldValue length]>1) {
			thisLabelText = [[thisFieldValue componentsSeparatedByString:@","] objectAtIndex:0];
		}
		cell.primaryLabel.text = thisLabelText;
	}
	
#pragma mark 5
	
	//Edit-läget inaktiverat
	if(/* DISABLES CODE */ (1) == 1 || [_editMode intValue]!=1){
		/*
		 ##################### getCustomer ###################
		 */
		if([thisFieldType isEqualToString:@"getCustomer"] ||
		   [thisFieldType isEqualToString:@"getCustomerBuyer"] ||
		   [thisFieldType isEqualToString:@"getCustomerSeller"] ||
		   [thisFieldType isEqualToString:@"getCustomerBroker"] ||
		   [thisFieldType isEqualToString:@"getCustomerEstate"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerBuyer"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerSeller"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerBroker"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerEstate"]
		   ){
            
            // Dölj redigeringsknappen
            self.navigationItem.rightBarButtonItem = nil;
			
			//Sätt kund-id att hämta (beroende på get/fetch)
			NSString * custId;
			
			if([thisFieldType isEqualToString:@"getCustomer"] ||
			   [thisFieldType isEqualToString:@"getCustomerBuyer"] ||
			   [thisFieldType isEqualToString:@"getCustomerSeller"] ||
			   [thisFieldType isEqualToString:@"getCustomerBroker"] ||
			   [thisFieldType isEqualToString:@"getCustomerEstate"]){
				custId = thisFieldValue;
			}
			
			/*DOLD*///NSLog(@"VIEW get-/fetch-customer-fält. custId: %@", custId);
			
			
			NSMutableArray * custArray = [[NSMutableArray alloc] initWithCapacity:0];
			sqlite3_stmt *statement;
			
			
			if([thisFieldType isEqualToString:@"fetchCustomerBuyer"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerSeller"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerBroker"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerEstate"]){
				
				//Hämta kund-id från arbetsordern
				if (_appDelegate.openDbConnection)
				{
					/*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
					NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM prot_arbetsorder WHERE id = %@",workOrder];
					/*DOLD*///NSLog(@"SQL: %@", beginSQL);
					const char *begin_stmt = [beginSQL UTF8String];
					sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
					
					/*DOLD*///NSLog(@"Listar prot_tabell");
					//Ger antalet columner för select-statement "pStmt"
					int colCount = sqlite3_column_count(statement);
					/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
					
					while(sqlite3_step(statement) == SQLITE_ROW) {
						NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
						NSString * myTempMutStr = [[NSMutableString alloc] init];
						
						int i = 0;
						for (i=0; i<colCount;i++) {
							//Ger namnet på kolumn N
							const char *colName = (const char*)sqlite3_column_name(statement, i);
							
							char *colValue = (char *)sqlite3_column_text(statement, i);
							if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
							(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
							myTempMutStr = nil;
							/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
						}
						if([thisFieldType isEqualToString:@"fetchCustomerBuyer"]){
							custId = [dictionary valueForKey:@"kopare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerSeller"]){
							custId = [dictionary valueForKey:@"saljare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerBroker"]){
							custId = [dictionary valueForKey:@"maklare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerEstate"]){
							custId = [dictionary valueForKey:@"fastighet"];
						}
					}
    beginSQL = nil;
    begin_stmt = nil;
        sqlite3_finalize(statement);

        
				}else{
					NSLog(@"Ingen _appDelegate.openDbConnection! 11");
				}
			}
			
			//Hämta kunden från db
			if (_appDelegate.openDbConnection)
			{
				
				/*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
				NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_customers WHERE id = %@;", custId];
				/*DOLD*///NSLog(@"SQL: %@", beginSQL);
				const char *begin_stmt = [beginSQL UTF8String];
				sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
				
				/*DOLD*///NSLog(@"Listar prot_tabell");
				//Ger antalet columner för select-statement "pStmt"
				int colCount = sqlite3_column_count(statement);
				/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
				
				while(sqlite3_step(statement) == SQLITE_ROW) {
					NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
					NSString * myTempMutStr = [[NSMutableString alloc] init];
					
					int i = 0;
					for (i=0; i<colCount;i++) {
						//Ger namnet på kolumn N
						const char *colName = (const char*)sqlite3_column_name(statement, i);
						
						char *colValue = (char *)sqlite3_column_text(statement, i);
						if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
						myTempMutStr = nil;
						/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
					}
					[custArray addObject:dictionary];
				}
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 12");
			}
			
			//NSLog(@"custArray: %@", custArray);
			//NSLog(@"custArray count: %d", [custArray count]);
			
			if([custArray count]>0){
				if (![[[custArray objectAtIndex:0] valueForKey:@"name"] isEqualToString:@""]) {
					cell.getCustomerNamn.text = [[custArray objectAtIndex:0] valueForKey:@"name"];
				}else{
					cell.getCustomerNamn.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"address"] isEqualToString:@""]) {
					cell.getCustomerAdress.text = [[custArray objectAtIndex:0] valueForKey:@"address"];
				}else{
					cell.getCustomerAdress.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"postnr"] isEqualToString:@""]) {
					cell.getCustomerPostnr.text = [[custArray objectAtIndex:0] valueForKey:@"postnr"];
				}else{
					cell.getCustomerPostnr.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"city"] isEqualToString:@""]) {
					cell.getCustomerStad.text = [[custArray objectAtIndex:0] valueForKey:@"city"];
				}else{
					cell.getCustomerStad.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"phone"] isEqualToString:@""]) {
					cell.getCustomerTelefon.text = [[custArray objectAtIndex:0] valueForKey:@"phone"];
				}else{
					cell.getCustomerTelefon.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"mail"] isEqualToString:@""]) {
					cell.getCustomerMail.text = [[[custArray objectAtIndex:0] valueForKey:@"mail"] stringByReplacingOccurrencesOfString:@"&#64;" withString:@"@"];
				}
			}else{
				cell.getCustomerNamn.text = @"Ingen kund vald";
			}
			cell.longLabel.text = thisFieldName ;
			cell.secondaryLabel.text = thisFieldName;
		}
		
	}else{
#pragma mark -edit
		
		/*
		 ##################### getCustomer ###################
		 */
		if([thisFieldType isEqualToString:@"getCustomer"] ||
		   [thisFieldType isEqualToString:@"getCustomerBuyer"] ||
		   [thisFieldType isEqualToString:@"getCustomerSeller"] ||
		   [thisFieldType isEqualToString:@"getCustomerBroker"] ||
		   [thisFieldType isEqualToString:@"getCustomerEstate"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerBuyer"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerSeller"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerBroker"] ||
		   [thisFieldType isEqualToString:@"fetchCustomerEstate"]
		   ){
			
			//Sätt kund-id att hämta (beroende på get/fetch)
			NSString * custId;
			
			if([thisFieldType isEqualToString:@"getCustomer"] ||
			   [thisFieldType isEqualToString:@"getCustomerBuyer"] ||
			   [thisFieldType isEqualToString:@"getCustomerSeller"] ||
			   [thisFieldType isEqualToString:@"getCustomerBroker"] ||
			   [thisFieldType isEqualToString:@"getCustomerEstate"]){
				custId = thisFieldValue;
			}
			
			/*DOLD*///NSLog(@"EDIT get-/fetch-customer-fält. custId: %@", custId);
			
			
			NSMutableArray * custArray = [[NSMutableArray alloc] initWithCapacity:0];
			sqlite3_stmt *statement;
			
			
			if([thisFieldType isEqualToString:@"fetchCustomerBuyer"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerSeller"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerBroker"] ||
			   [thisFieldType isEqualToString:@"fetchCustomerEstate"]){
				
				//Hämta kund-id från arbetsordern
				if (_appDelegate.openDbConnection)
				{
					/*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
					NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM prot_arbetsorder WHERE id = %@",workOrder];
					/*DOLD*///NSLog(@"SQL: %@", beginSQL);
					const char *begin_stmt = [beginSQL UTF8String];
					sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
					
					/*DOLD*///NSLog(@"Listar prot_tabell");
					//Ger antalet columner för select-statement "pStmt"
					int colCount = sqlite3_column_count(statement);
					/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
					
					while(sqlite3_step(statement) == SQLITE_ROW) {
						NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
						NSString * myTempMutStr = [[NSMutableString alloc] init];
						
						int i = 0;
						for (i=0; i<colCount;i++) {
							//Ger namnet på kolumn N
							const char *colName = (const char*)sqlite3_column_name(statement, i);
							
							char *colValue = (char *)sqlite3_column_text(statement, i);
							if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
							(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
							myTempMutStr = nil;
							/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
						}
						if([thisFieldType isEqualToString:@"fetchCustomerBuyer"]){
							custId = [dictionary valueForKey:@"kopare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerSeller"]){
							custId = [dictionary valueForKey:@"saljare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerBroker"]){
							custId = [dictionary valueForKey:@"maklare"];
						}else if([thisFieldType isEqualToString:@"fetchCustomerEstate"]){
							custId = [dictionary valueForKey:@"fastighet"];
						}
					}
                beginSQL = nil;
                begin_stmt = nil;
                    sqlite3_finalize(statement);

                    
				}else{
					NSLog(@"Ingen _appDelegate.openDbConnection! 13");
				}
			}
			
			//Hämta kunden från db
			if (_appDelegate.openDbConnection)
			{
				
				/*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
				NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_customers WHERE id = %@;", custId];
				/*DOLD*///NSLog(@"SQL: %@", beginSQL);
				const char *begin_stmt = [beginSQL UTF8String];
				sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
				
				/*DOLD*///NSLog(@"Listar prot_tabell");
				//Ger antalet columner för select-statement "pStmt"
				int colCount = sqlite3_column_count(statement);
				/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
				
				while(sqlite3_step(statement) == SQLITE_ROW) {
					NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
					NSString * myTempMutStr = [[NSMutableString alloc] init];
					
					int i = 0;
					for (i=0; i<colCount;i++) {
						//Ger namnet på kolumn N
						const char *colName = (const char*)sqlite3_column_name(statement, i);
						
						char *colValue = (char *)sqlite3_column_text(statement, i);
						if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
						myTempMutStr = nil;
						/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
					}
					[custArray addObject:dictionary];
				}

            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 14");
			}
			
			//NSLog(@"custArray: %@", custArray);
			//NSLog(@"custArray count: %d", [custArray count]);
			
			if([custArray count]>0){
				if (![[[custArray objectAtIndex:0] valueForKey:@"name"] isEqualToString:@""]) {
					cell.getCustomerNamn.text = [[custArray objectAtIndex:0] valueForKey:@"name"];
				}else{
					cell.getCustomerNamn.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"address"] isEqualToString:@""]) {
					cell.getCustomerAdress.text = [[custArray objectAtIndex:0] valueForKey:@"address"];
				}else{
					cell.getCustomerAdress.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"postnr"] isEqualToString:@""]) {
					cell.getCustomerPostnr.text = [[custArray objectAtIndex:0] valueForKey:@"postnr"];
				}else{
					cell.getCustomerPostnr.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"city"] isEqualToString:@""]) {
					cell.getCustomerStad.text = [[custArray objectAtIndex:0] valueForKey:@"city"];
				}else{
					cell.getCustomerStad.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"phone"] isEqualToString:@""]) {
					cell.getCustomerTelefon.text = [[custArray objectAtIndex:0] valueForKey:@"phone"];
				}else{
					cell.getCustomerTelefon.text = @"";
				}
				if (![[[custArray objectAtIndex:0] valueForKey:@"mail"] isEqualToString:@""]) {
					cell.getCustomerMail.text = [[[custArray objectAtIndex:0] valueForKey:@"mail"] stringByReplacingOccurrencesOfString:@"&#64;" withString:@"@"];
				}
			}else{
				cell.getCustomerNamn.text = @"Ingen kund vald";
			}
			cell.longLabel.text = thisFieldName ;
			cell.secondaryLabel.text = thisFieldName;
		}
	}
	
	
	
#pragma mark SelectBesiktningsman
	//edit-läget inaktiverat
	if(/* DISABLES CODE */ (1) == 1 || [_editMode intValue]!=1){
		/*
		 ##################### selectBesiktningsman ###################
		 */
        if([thisFieldType isEqualToString:@"selectBesiktningsman"]){
            cell.userInteractionEnabled = FALSE;
			
			//Hämta kund från DB
			NSMutableArray * besmanArray = [[NSMutableArray alloc] initWithCapacity:0];
			sqlite3_stmt *statement;
			
			if (_appDelegate.openDbConnection)
			{
				/*DOLD*///NSLog(@"Databasen öppnad - selectbesiktningsman - view");
				NSString *beginSQL = [NSString stringWithFormat: @"select sys_users.id, sys_users.username, sys_users.fullname, sys_userroles.role                                       from sys_users INNER JOIN sys_userroles ON sys_users.role = sys_userroles.ID WHERE sys_users.id = %@;", thisFieldValue];
				/*DOLD*///NSLog(@"SQL: %@", beginSQL);
				const char *begin_stmt = [beginSQL UTF8String];
				sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
				
				/*DOLD*///NSLog(@"Listar prot_tabell");
				//Ger antalet columner för select-statement "pStmt"
				int colCount = sqlite3_column_count(statement);
				/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
				
				while(sqlite3_step(statement) == SQLITE_ROW) {
					NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
					NSString * myTempMutStr = [[NSMutableString alloc] init];
					
					int i = 0;
					for (i=0; i<colCount;i++) {
						//Ger namnet på kolumn N
						const char *colName = (const char*)sqlite3_column_name(statement, i);
						
						char *colValue = (char *)sqlite3_column_text(statement, i);
						if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
						myTempMutStr = nil;
						/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
					}
					[besmanArray addObject:dictionary];
				}
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 15");
			}
			cell.primaryLabel.text = thisFieldName;
			NSString * thisCellLabel;
			if([besmanArray count]>0){
				thisCellLabel =[[besmanArray objectAtIndex:0] valueForKey:@"fullname"];
			}
			cell.secondaryLabel.text = thisCellLabel;
			/*DOLD*///NSLog(@"%@",[[besmanArray objectAtIndex:0] valueForKey:@"fullname"]);
		}
		
	}else{
#pragma mark -edit
	}
#pragma mark Autotext
	if([_editMode intValue]!=1){
		/*
		 ##################### Autotexter ###################
		 */
		if([thisFieldType isEqualToString:@"autotext"]){
			//NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisFieldName;
			cell.secondaryLabel.text = nil;
		}
		if([thisFieldType isEqualToString:@"autotextzone"]){
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisLabelText;
			cell.primaryLabel.textColor = [UIColor whiteColor];
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor darkGrayColor];
			cell.backgroundView = myView;
			
			//            cell.contentView.backgroundColor = [UIColor darkGrayColor];
		}
		if([thisFieldType isEqualToString:@"autotextroom"]){
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisLabelText;
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor lightGrayColor];
			cell.backgroundView = myView;
			
			//            cell.contentView.backgroundColor = [UIColor lightGrayColor];
			
			//Appenda alla allmänna informationer och noteringar till en textsträng, och visa dem sen i textfältet
			/*
			 NSString * roomNotes = [NSString  ]
			 cell.autotextTextView.text = [[[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"] valueForKey:@"notes"];
			 //*/
		}
		if([thisFieldType isEqualToString:@"autotextinfo"]){
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"text"];
			cell.autotextTextView.text = [thisLabelText stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
			
			
			
			
			
			
			
//			CGSize textSize = [thisLabelText sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisLabelText boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
			[cell.autotextTextView setFrame:CGRectMake(50, 0, 685, MAX(textSize.height+20, 50.0f))];
			
			
			
			
			
			
			
		}
		if([thisFieldType isEqualToString:@"autotextnote"]){
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"text"];
			cell.autotextTextView.text = [thisLabelText stringByReplacingOccurrencesOfString:@"\\\"" withString:@"\""];
			
			
			
			
			
			
			
			
//			CGSize textSize = [thisLabelText sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisLabelText boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
			[cell.autotextTextView setFrame:CGRectMake(50, 0, 685, MAX(textSize.height+20, 50.0f))];
			
			
			
			
			
			
		}
		if([thisFieldType isEqualToString:@"autotextinfoheader"]){
			NSString * thisLabelText = @"Allmän information";
			cell.primaryLabel.text = thisLabelText;
		}
		if([thisFieldType isEqualToString:@"autotextnotesheader"]){
			NSString * thisLabelText = @"Noteringar";
			cell.primaryLabel.text = thisLabelText;
		}
		if([thisFieldType isEqualToString:@"autotextaddtext"]){
			NSString * thisLabelText = @"Lägg till text";
			cell.primaryLabel.text = thisLabelText;
		}
		if([thisFieldType isEqualToString:@"autotextaddroom"]){
			NSString * thisLabelText = @"Lägg till rum";
			cell.primaryLabel.text = thisLabelText;
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor lightGrayColor];
			cell.backgroundView = myView;
			
			//cell.contentView.backgroundColor = [UIColor lightGrayColor];
		}
		if([thisFieldType isEqualToString:@"autotextaddzone"]){
			NSString * thisLabelText = @"Lägg till zon";
			cell.primaryLabel.text = thisLabelText;
			cell.primaryLabel.textColor = [UIColor whiteColor];
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor darkGrayColor];
			cell.backgroundView = myView;
			
			//cell.contentView.backgroundColor = [UIColor darkGrayColor];
		}
		
		
	}else{
#pragma mark -edit
		/*
		 ##################### Autotexter ###################
		 */
		
		/* BYT NAMN-KNAPPAR: http://stackoverflow.com/questions/1802707/detecting-which-uibutton-was-pressed-in-a-uitableview (använd nåt i stil med "button superview superview indexPathForCell:") */
		
		if([thisFieldType isEqualToString:@"autotext"]){
			//NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisFieldName; //@"Rad med fieldtypen autotext";
			
			/*DOLD*///NSLog(@"##### RAD: %d ##### autotext-objekt: %@",indexPath.row,[_reportFields objectAtIndex:indexPath.row]);
			
			
		}
		if([thisFieldType isEqualToString:@"autotextzone"]){
			/*DOLD*///NSLog(@"##### RAD: %d ##### autotextzone-objekt: %@",indexPath.row,[_reportFields objectAtIndex:indexPath.row]);
			
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisLabelText;
			cell.primaryLabel.textColor = [UIColor whiteColor];
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor darkGrayColor];
			cell.backgroundView = myView;
			
			//http://code.coneybeare.net/how-to-make-custom-drawn-gradient-backgrounds
			//cell.backgroundView = [[UACellBackgroundView alloc] initWithFrame:CGRectZero];
			
			[cell.renameZoneButton addTarget:self action:@selector(renameZone:) forControlEvents:UIControlEventTouchUpInside];
		}
		if([thisFieldType isEqualToString:@"autotextroom"]){
			/*DOLD*///NSLog(@"##### RAD: %d ##### autotextroom-objekt: %@",indexPath.row,[_reportFields objectAtIndex:indexPath.row]);
			
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"] valueForKey:@"title"];
			cell.primaryLabel.text = thisLabelText;
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor lightGrayColor];
			cell.backgroundView = myView;

			cell.userInteractionEnabled = YES;
			[cell.renameRoomButton addTarget:self action:@selector(renameRoom:) forControlEvents:UIControlEventTouchUpInside];

		}
		if([thisFieldType isEqualToString:@"autotextinfo"]){
			/*DOLD*///NSLog(@"##### RAD: %d ##### autotextinfo-objekt: %@",indexPath.row,[_reportFields objectAtIndex:indexPath.row]);
			
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"text"];
			cell.autotextTextView.text = thisLabelText;
			
			
			
			
			
			
			
			
//			CGSize textSize = [thisLabelText sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisLabelText boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
			[cell.autotextTextView setFrame:CGRectMake(50, 0, 685, MAX(textSize.height+20, 50.0f))];
			
			
			
			
			
			
			cell.userInteractionEnabled = YES;
			
		}
		if([thisFieldType isEqualToString:@"autotextnote"]){
			/*DOLD*///NSLog(@"##### RAD: %d ##### autotextnote-objekt: %@",indexPath.row,[_reportFields objectAtIndex:indexPath.row]);
			
			NSString * thisLabelText = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"text"];
			cell.autotextTextView.text = thisLabelText;
			
			
			
			
			
			
			
			
//			CGSize textSize = [thisLabelText sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(685-20, 20000.0f) lineBreakMode:UILineBreakModeWordWrap];
            CGSize textSize = [thisLabelText boundingRectWithSize:CGSizeMake(685-20, 20000.0f) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:14.0]} context:nil].size;
			[cell.autotextTextView setFrame:CGRectMake(50, 0, 685, MAX(textSize.height+20, 50.0f))];
			
			
			
			
			
			
			cell.userInteractionEnabled = YES;
			
		}
		if([thisFieldType isEqualToString:@"autotextinfoheader"]){
			NSString * thisLabelText = @"Allmän information";
			cell.primaryLabel.text = thisLabelText;
			cell.userInteractionEnabled = YES;
		}
		if([thisFieldType isEqualToString:@"autotextnotesheader"]){
			NSString * thisLabelText = @"Noteringar";
			cell.primaryLabel.text = thisLabelText;
			cell.userInteractionEnabled = YES;
		}
		if([thisFieldType isEqualToString:@"autotextaddinfo"]){
			NSString * thisLabelText = @"Lägg till info";
			cell.primaryLabel.text = thisLabelText;
		}
		if([thisFieldType isEqualToString:@"autotextaddnote"]){
			NSString * thisLabelText = @"Lägg till notering";
			cell.primaryLabel.text = thisLabelText;
		}
		if([thisFieldType isEqualToString:@"autotextaddroom"]){
			NSString * thisLabelText = @"Lägg till rum";
			cell.primaryLabel.text = thisLabelText;
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor lightGrayColor];
			cell.backgroundView = myView;
			
			//cell.contentView.backgroundColor = [UIColor lightGrayColor];
		}
		if([thisFieldType isEqualToString:@"autotextaddzone"]){
			NSString * thisLabelText = @"Lägg till zon";
			cell.primaryLabel.text = thisLabelText;
			cell.primaryLabel.textColor = [UIColor whiteColor];
			
			UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
			myView.backgroundColor = [UIColor darkGrayColor];
			cell.backgroundView = myView;
			
			//cell.contentView.backgroundColor = [UIColor darkGrayColor];
		}
		
	}
#pragma mark includedReports
	//Edit-läge inaktiverat
	if(/* DISABLES CODE */ (1) == 1 ||[_editMode intValue]!=1){
		/*
		 ##################### includedReports ###################
		 */
        if([thisFieldType isEqualToString:@"includedReports"]){
            cell.userInteractionEnabled = FALSE;
            
			NSLog(@"includedReports");
			//Hämta mall-namn från DB
			NSMutableArray * allReportsArray = [[NSMutableArray alloc] initWithCapacity:0];
			sqlite3_stmt *statement;
			
			if (_appDelegate.openDbConnection)
			{
				/*DOLD*///NSLog(@"Databasen öppnad - includedReports - view");
				NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_formtables"];
				/*DOLD*///NSLog(@"SQL: %@", beginSQL);
				const char *begin_stmt = [beginSQL UTF8String];
				sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
				
				/*DOLD*///NSLog(@"Listar prot_tabell");
				//Ger antalet columner för select-statement "pStmt"
				int colCount = sqlite3_column_count(statement);
				/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
				
				while(sqlite3_step(statement) == SQLITE_ROW) {
					NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
					NSString * myTempMutStr = [[NSMutableString alloc] init];
					
					int i = 0;
					for (i=0; i<colCount;i++) {
						//Ger namnet på kolumn N
						const char *colName = (const char*)sqlite3_column_name(statement, i);
						
						char *colValue = (char *)sqlite3_column_text(statement, i);
						if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
						myTempMutStr = nil;
						/*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
					}
					[allReportsArray addObject:dictionary];
				}
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 16");
			}
			/*DOLD*///NSLog(@"Sätter thisCellLabel = allreportsArray[0].fullname");
			NSString * thisCellLabel;
			if([allReportsArray count]>0){
				thisCellLabel = [[allReportsArray objectAtIndex:0] valueForKey:@"fullname"];
			}
			cell.primaryLabel.text = thisCellLabel;
			/*DOLD*///NSLog(@"%@",[[allReportsArray objectAtIndex:0] valueForKey:@"form_name"]);
			
			NSArray * reportsArr = [thisFieldValue componentsSeparatedByString:@","];
            cell.primaryLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)[reportsArr count]];
			
			NSMutableString * allReportsStr = [[NSMutableString alloc] init];
			
			int i = 0;
			int j = 0;
			
			for (i=0;i<[allReportsArray count];i++) {
				for (j=0;j<[reportsArr count];j++) {
					int thisListedReportId = [[[allReportsArray objectAtIndex:i] valueForKey:@"id"] intValue];
					int thisChosenReportId = [[reportsArr objectAtIndex:j] intValue];
					if(thisListedReportId == thisChosenReportId){
						if([allReportsStr length]>0){
							[allReportsStr appendString:@", "];
						}
						[allReportsStr appendString:[[allReportsArray objectAtIndex:i] valueForKey:@"form_name"]];
						
						UIView* view = [[UIView alloc] initWithFrame: _window.bounds];
						
						CGRect frame = CGRectMake(10, 10, 200, 100);
						UIButton *button = [[UIButton alloc] initWithFrame:frame];
						[view addSubview:button];
						[button setTitle:[[allReportsArray objectAtIndex:i] valueForKey:@"form_name"] forState: UIControlStateNormal];
						//                      [button setTarget: self];
						//                      [button setAction: @selector(myButtonWasHit:)];
						
					}
				}
			}
			cell.primaryLabel.text = thisFieldName; //allReportsStr;
			
		}
	}else{
#pragma mark -edit
	}
#pragma mark includedReportsLink
	//Edit-läge inaktiverat
	if(/* DISABLES CODE */ (1) == 1 || [_editMode intValue]!=1){
		/*
		 ##################### includedReportsLink ###################
		 */
		if([thisFieldType isEqualToString:@"includedReportsLink"]){
			NSLog(@"includedReports-LINK");

			//Hämta mall-namn från DB
			NSMutableArray * allReportsArray = [[NSMutableArray alloc] initWithCapacity:0];
			sqlite3_stmt *statement;
			
			if (_appDelegate.openDbConnection)
			{
				/*DOLD*///NSLog(@"Databasen öppnad - includedReportsLink - view");
				NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_formtables WHERE id = %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"incRepId"]];
				/*DOLD*///NSLog(@"SQL: %@", beginSQL);
				const char *begin_stmt = [beginSQL UTF8String];
				sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
				
				/*DOLD*///NSLog(@"Listar sys_formtables för valt id-nummer");
				//Ger antalet columner för select-statement "pStmt"
				int colCount = sqlite3_column_count(statement);
				/*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
				/*VIKTIG*/NSLog(@"Om appen kraschar här, så försöker den hämta namnet på en includedreport som inte finns i sys_formtables");
				while(sqlite3_step(statement) == SQLITE_ROW) {
					NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
					NSString * myTempMutStr = [[NSMutableString alloc] init];
					
					int i = 0;
					for (i=0; i<colCount;i++) {
						//Ger namnet på kolumn N
						const char *colName = (const char*)sqlite3_column_name(statement, i);
						
						char *colValue = (char *)sqlite3_column_text(statement, i);
						if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
						myTempMutStr = nil;
						/*#dolt#*//*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
					}
					[allReportsArray addObject:dictionary];
				}
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
			}else{
				NSLog(@"Ingen _appDelegate.openDbConnection! 16");
			}
			cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
			
			if([allReportsArray count]>0){
				cell.secondaryLabel.text = [[allReportsArray objectAtIndex:0] valueForKey:@"form_name"];

                UIView *myView = [[UIView alloc] initWithFrame:cell.frame];
                myView.backgroundColor = [UIColor colorWithRed:214.00/255.00 green:233.00/255.00 blue:247.00/255.00 alpha:1.0];
                cell.backgroundView = myView;
			}else{
                cell.secondaryLabel.text = [NSString stringWithFormat:@"Inga bilagor"];
                
                if(![[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"incRepId"] isEqualToString:@""]){
                    cell.secondaryLabel.text = [NSString stringWithFormat:@"Inga bilagor (%@)",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"incRepId"]];
                }

                cell.userInteractionEnabled = FALSE;
                cell.accessoryType = UITableViewCellAccessoryNone;
			}
			
			cell.longLabel.textColor = [UIColor blackColor];
			//            cell.contentView.superview.backgroundColor = [UIColor colorWithRed:0.07 green:0.24 blue:0.54 alpha:1];
			
		}
	}else{
#pragma mark -edit
	}

#pragma mark
#pragma mark ##### Slut på formatering
#pragma mark

    return cell;
}

//Convert hex color to UIColor
- (UIColor *) colorWithHexString: (NSString *) hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

//Get Image From URL
-(UIImage *) getImageFromURL:(NSString *)fileURL {
    UIImage * result;
    
    NSData * data = [NSData dataWithContentsOfURL:[NSURL URLWithString:fileURL]];
    result = [UIImage imageWithData:data];
    
    return result;
}

//Save Image
-(void) saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    if ([[extension lowercaseString] isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}

//Load Image
-(UIImage *) loadImage:(NSString *)fileName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    UIImage * result = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@.%@", directoryPath, fileName, extension]];
    
    return result;
}

/*
//How-To

//Definitions
NSString * documentsDirectoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

//Get Image From URL
UIImage * imageFromURL = [self getImageFromURL:@"http://www.yourdomain.com/yourImage.png"];

//Save Image to Directory
[self saveImage:imageFromURL withFileName:@"My Image" ofType:@"png" inDirectory:documentsDirectoryPath];

//Load Image From Directory
UIImage * imageFromWeb = [self loadImage:@"My Image" ofType:@"png" inDirectory:documentsDirectoryPath];
*/


//Change
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//	/*
//	 NSString *path = [[NSBundle mainBundle] bundlePath];
//	NSURL *baseURL = [NSURL fileURLWithPath:path];
//	[webView loadHTMLString:htmlString baseURL:baseURL];
//	*/
//
//	NSString * css = @"var cssChild=\"<style type='text/css'>body,h1,h2,h3,p{font-family:\'helvetica\';}</style>\";";
//	NSString * js = [NSString stringWithFormat:@"%@ document.getElementsByTagName('head')[0].appendChild(cssChild);", css];
//
//	NSLog(@"JAVSCRIPT: %@",js);
//
//	[webView stringByEvaluatingJavaScriptFromString:js];
//}


#pragma mark rename-knappar
- (void)renameZone:(id)sender
{
	UIButton *button = (UIButton *)sender;
	// Get the UITableViewCell which is the superview of the UITableViewCellContentView which is the superview of the UIButton
	reportFormTableCell * cell = (reportFormTableCell *)[[button superview] superview];
	int row = (int) [_reportForm indexPathForCell:cell].row;
	renameZoneButtonWasPressedOnRow = row;
	
	NSMutableDictionary * zoneObj = [[[_reportFields objectAtIndex:row] valueForKey:@"zoneObj"] mutableCopy];
	
	
	NSString*title = @"Ange nytt namn för zonen:";
	NSString*value = [zoneObj valueForKey:@"title"];
	[self promptUserForZoneName:sender title:title textValue:value];
}

- (void)promptUserForZoneName:(id)sender title:(NSString *)title textValue:(NSString *)textValue{
    
    //Change
    UIAlertController *passwordAlert = [UIAlertController alertControllerWithTitle:title
                                                                                message:@"\n\n"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
    [passwordAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"name";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleRoundedRect;
        [textField becomeFirstResponder];
            if(textValue){
                textField = passwordAlert.textFields.firstObject;
                [textField setText: textValue];

            }
        }];

   //We add buttons to the alert controller by creating UIAlertActions:
   UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
       [self alertButtonClicker:@"OK" alertTitle:title textfield:passwordAlert.textFields.firstObject];

//                                                        [self okButtonPressed: @"Fortsätt"];
                                                    }]; //You can use a block here to handle a press on this button
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                       style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            [passwordAlert dismissViewControllerAnimated:YES completion:nil];
                                                        }];

    [passwordAlert addAction:actionCancel];
   [passwordAlert addAction:actionOk];

   [self presentViewController:passwordAlert animated:YES completion:nil];
	//Change Complete
    
    
//	UIAlertView *passwordAlert = [[UIAlertView alloc] initWithTitle:title message:@"\n\n" delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"OK", nil];
//
//    passwordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//    UITextField *passwordField;
//    /*	UITextField *passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0,0,252,25)];
//     passwordField.font = [UIFont systemFontOfSize:18];
//     passwordField.backgroundColor = [UIColor whiteColor];
//     passwordField.borderStyle = UITextBorderStyleRoundedRect;
//     passwordField.secureTextEntry = NO;
//     passwordField.keyboardAppearance = UIKeyboardAppearanceAlert;
//     */	//passwordField.delegate = self;
//    if(textValue){
//        passwordField = [passwordAlert textFieldAtIndex:0];
//        [passwordField setText: textValue];
//
//    }
//    // [passwordField becomeFirstResponder];
//    // [passwordAlert addSubview:passwordField];
//    // [passwordAlert setTransform:CGAffineTransformMakeTranslation(0,109)];
//
//    [[passwordAlert textFieldAtIndex:0] becomeFirstResponder];
//
//	[passwordAlert show];
}


/*
- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
//	[reportfo performSegueWithIdentifier:@"GroupToGroupMembers" sender:self];
}
*/



#pragma mark rename-knappar
- (void)renameRoom:(id)sender
{
	UIButton *button = (UIButton *)sender;
	// Get the UITableViewCell which is the superview of the UITableViewCellContentView which is the superview of the UIButton
	reportFormTableCell * cell = (reportFormTableCell*) [[button superview] superview];
	int row = (int) [_reportForm indexPathForCell:cell].row;
	renameRoomButtonWasPressedOnRow = row;
	
	NSMutableDictionary * roomObj = [[[_reportFields objectAtIndex:row] valueForKey:@"roomObj"] mutableCopy];
	
	
	NSString*title = @"Ange nytt namn för rummet:";
	NSString*value = [roomObj valueForKey:@"title"];
	[self promptUserForRoomName:sender title:title textValue:value];
}
#pragma mark

- (void)promptUserForRoomName:(id)sender title:(NSString *)title textValue:(NSString *)textValue{
    
    //Change
    UIAlertController *passwordAlert = [UIAlertController alertControllerWithTitle:title
                                                                                message:@"\n\n"
                                                                         preferredStyle:UIAlertControllerStyleAlert];
    [passwordAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//            textField.placeholder = @"name";
//            textField.textColor = [UIColor blueColor];
//            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//            textField.borderStyle = UITextBorderStyleRoundedRect;
        [textField becomeFirstResponder];
            if(textValue){
                textField = passwordAlert.textFields.firstObject;
                [textField setText: textValue];

            }
        }];

   //We add buttons to the alert controller by creating UIAlertActions:
   UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [self alertButtonClicker:@"OK" alertTitle:title textfield:passwordAlert.textFields.firstObject];
                                                    }]; //You can use a block here to handle a press on this button
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avbryt"
                                                       style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            [passwordAlert dismissViewControllerAnimated:YES completion:nil];
                                                        }];

    [passwordAlert addAction:actionCancel];
   [passwordAlert addAction:actionOk];

   [self presentViewController:passwordAlert animated:YES completion:nil];
    
//    NSLog(@"promptUserForRoomName...");
//	UIAlertView *passwordAlert = [[UIAlertView alloc] initWithTitle:title message:@"\n\n" delegate:self cancelButtonTitle:@"Avbryt" otherButtonTitles:@"OK", nil];
//	passwordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//
//    UITextField *passwordField;
//
//    /*	UITextField *passwordField = [[UITextField alloc] initWithFrame:CGRectMake(0,0,252,25)];
//	passwordField.font = [UIFont systemFontOfSize:18];
//	passwordField.backgroundColor = [UIColor whiteColor];
//	passwordField.borderStyle = UITextBorderStyleRoundedRect;
//	passwordField.secureTextEntry = NO;
//	passwordField.keyboardAppearance = UIKeyboardAppearanceAlert;
//*/	//passwordField.delegate = self;
//	if(textValue){
//        passwordField = [passwordAlert textFieldAtIndex:0];
//        [passwordField setText: textValue];
//    }
//    // [passwordField becomeFirstResponder];
//    // [passwordAlert addSubview:passwordField];
//    // [passwordAlert setTransform:CGAffineTransformMakeTranslation(0,109)];
//
//    [[passwordAlert textFieldAtIndex:0] becomeFirstResponder];
//
//    NSLog(@"Visar alert");
//	[passwordAlert show];

}



//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//	if(buttonIndex == 1){
//		//Delegate för "Vill du lämna sidan"-bekräftelse
//		NSString *btntitle = [alertView buttonTitleAtIndex:buttonIndex];
//		if([btntitle isEqualToString:@"Lämna sidan"]){
//
//			[self performSelector:@selector(emptyVars)];
//
//			[self.navigationController popViewControllerAnimated:YES];
//		}else{
//
//			NSLog(@"alertView subviews: %@",[alertView subviews]);
////            UILabel * promptLabel = [alertView title]; //[[alertView subviews] objectAtIndex:1];
//			NSString * promptTitle = [alertView title];
//
//			//UITextView * promptText = [[alertView subviews] objectAtIndex:5];
//
//
//            UITextField *promptText = [alertView textFieldAtIndex:0];
//
//			NSString * newName = promptText.text;
//			NSLog(@"alertView textField text: %@",promptText.text);
//
//			if(promptTitle && ![promptTitle isEqualToString:@""]){
//
//				if([promptTitle isEqualToString:@"Ange nytt namn för zonen:"]){
//					int row = renameZoneButtonWasPressedOnRow;
//
//					NSMutableDictionary * zoneObj = [[[_reportFields objectAtIndex:row] valueForKey:@"zoneObj"] mutableCopy];
//					//NSLog(@"KNAPP TRYCKTES PÅ RAD %d!",row);
//					//NSLog(@"zoneObj: %@",zoneObj);
//
////					NSMutableDictionary * newZoneObj = [zoneObj mutableCopy];
//                    //Change
//                    NSMutableArray * newZoneObj = [zoneObj mutableCopy];
//
//					[newZoneObj setValue:[newName stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:@"title"];
//					_appDelegate.jsonHolder = newZoneObj;
//
//					NSMutableArray * newCompleteJsonHolder = [[NSMutableArray alloc] init];
//
//					for (NSDictionary*zoneDict in _appDelegate.completeJsonHolder) {
//						//Ersätt detta...
//						if([zoneDict isEqualToDictionary:zoneObj]){
//							//...med detta
//							[newCompleteJsonHolder addObject:newZoneObj];
//						}else{
//							[newCompleteJsonHolder addObject:zoneDict];
//						}
//					}
//
//					NSLog(@"newCompleteJsonHolder: %@",newCompleteJsonHolder);
//					NSLog(@"RenameZone: ersätt %@ med %@",zoneObj,newZoneObj);
//
//					//newCompleteJson
//					_appDelegate.completeJsonHolder = newCompleteJsonHolder;
//					[self performSelector:@selector(viewStartFunction)];
//
//
//				}else if([promptTitle isEqualToString:@"Ange nytt namn för rummet:"]){
//					int row = renameRoomButtonWasPressedOnRow;
//
//					NSMutableDictionary * roomObj = [[[_reportFields objectAtIndex:row] valueForKey:@"roomObj"] mutableCopy];
//					//NSLog(@"KNAPP TRYCKTES PÅ RAD %d!",row);
//					NSLog(@"roomObj: %@",roomObj);
//
//					_appDelegate.jsonOrigHolder = [[NSMutableArray alloc] initWithObjects: [roomObj mutableCopy], nil];
//					[roomObj setValue:[newName stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:@"title"];
//					_appDelegate.jsonHolder = [[NSMutableArray alloc] initWithObjects:roomObj, nil];
//
//					NSLog(@"RenameRoom: ersätt %@ med %@",_appDelegate.jsonOrigHolder,_appDelegate.jsonHolder);
//					[self performSelector:@selector(viewStartFunction)];
//				}
//			}
//		}
//	}
//}

-(void)alertButtonClicker:(NSString * )btntitle alertTitle:(NSString * )alertTitle textfield:(UITextField *)textField{
    //Delegate för "Vill du lämna sidan"-bekräftelse
//    NSString *btntitle = [alertView buttonTitleAtIndex:buttonIndex];
    if([btntitle isEqualToString:@"Lämna sidan"]){

        [self performSelector:@selector(emptyVars)];

        [self.navigationController popViewControllerAnimated:YES];
    }else{
        
//        NSLog(@"alertView subviews: %@",[alertView subviews]);
////            UILabel * promptLabel = [alertView title]; //[[alertView subviews] objectAtIndex:1];
//        NSString * promptTitle = [alertView title];
        NSString * promptTitle = alertTitle;
//
//        UITextField *promptText = [alertView textFieldAtIndex:0];
        UITextField *promptText = textField;
        
        NSString * newName = promptText.text;
        NSLog(@"alertView textField text: %@",promptText.text);
        
        if(promptTitle && ![promptTitle isEqualToString:@""]){
            
            if([promptTitle isEqualToString:@"Ange nytt namn för zonen:"]){
                int row = renameZoneButtonWasPressedOnRow;
                
                NSMutableDictionary * zoneObj = [[[_reportFields objectAtIndex:row] valueForKey:@"zoneObj"] mutableCopy];
                //NSLog(@"KNAPP TRYCKTES PÅ RAD %d!",row);
                //NSLog(@"zoneObj: %@",zoneObj);
                
                NSMutableArray * newZoneObj = [zoneObj mutableCopy];
                
                
                [newZoneObj setValue:[newName stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:@"title"];
                _appDelegate.jsonHolder = newZoneObj;
                
                NSMutableArray * newCompleteJsonHolder = [[NSMutableArray alloc] init];
                
                for (NSDictionary*zoneDict in _appDelegate.completeJsonHolder) {
                    //Ersätt detta...
                    if([zoneDict isEqualToDictionary:zoneObj]){
                        //...med detta
                        [newCompleteJsonHolder addObject:newZoneObj];
                    }else{
                        [newCompleteJsonHolder addObject:zoneDict];
                    }
                }
                
                NSLog(@"newCompleteJsonHolder: %@",newCompleteJsonHolder);
                NSLog(@"RenameZone: ersätt %@ med %@",zoneObj,newZoneObj);
                
                //newCompleteJson
                _appDelegate.completeJsonHolder = newCompleteJsonHolder;
                [self performSelector:@selector(viewStartFunction)];
                
                
            }else if([promptTitle isEqualToString:@"Ange nytt namn för rummet:"]){
                int row = renameRoomButtonWasPressedOnRow;
                
                NSMutableDictionary * roomObj = [[[_reportFields objectAtIndex:row] valueForKey:@"roomObj"] mutableCopy];
                //NSLog(@"KNAPP TRYCKTES PÅ RAD %d!",row);
                NSLog(@"roomObj: %@",roomObj);
                
                _appDelegate.jsonOrigHolder = [[NSMutableArray alloc] initWithObjects: [roomObj mutableCopy], nil];
                [roomObj setValue:[newName stringByReplacingOccurrencesOfString:@"\"" withString:@""] forKey:@"title"];
                _appDelegate.jsonHolder = [[NSMutableArray alloc] initWithObjects:roomObj, nil];
                
                NSLog(@"RenameRoom: ersätt %@ med %@",_appDelegate.jsonOrigHolder,_appDelegate.jsonHolder);
                [self performSelector:@selector(viewStartFunction)];
            }
        }
    }
}






/*
 #####################################################################
 #####################################################################
 ########################### VIEW DID LOAD ###########################
 #####################################################################
 #####################################################################
 */


#pragma mark
#pragma mark ViewWillAppear
- (void)viewWillAppear:(BOOL)animated
{
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #");
    /*DOLD*///NSLog(@"######################## VIEW WILL APPEAR ###########################");
    /*DOLD*///NSLog(@"# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #");
    /*DOLD*///NSLog(@"#####################################################################");

    [self viewStartFunction];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector (keyboardDidShow:)
												 name: UIKeyboardDidShowNotification object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector (keyboardDidHide:)
												 name: UIKeyboardDidHideNotification object:nil];

	if(_editMode == [NSNumber numberWithInt:1]){
		UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"Gå ur redigeringsläge"
																	 style:UIBarButtonItemStylePlain
																	target:self
																	action:@selector(backButtonPressed:)];
		self.navigationItem.leftBarButtonItem = backButton;
	}
}

- (void)backButtonPressed:(id)sender{
	NSString *alertString = [NSString stringWithFormat:@"Har du sparat?"];
	
	NSString *alertMsg = @"Om du inte sparar innan du lämnar sidan kan information gå förlorad!";
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertMsg delegate:self cancelButtonTitle:@"Stanna på sidan" otherButtonTitles:@"Lämna sidan",nil];
//
//	[alert show];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                message:alertMsg
                                                                         preferredStyle:UIAlertControllerStyleAlert];
   //We add buttons to the alert controller by creating UIAlertActions:
   UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Lämna sidan"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                        [self alertButtonClicker:@"Lämna sidan" alertTitle:alertString textfield:nil];
                                                    }]; //You can use a block here to handle a press on this button
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Stanna på sidan"
                                                       style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                                        }];
    
    [alertController addAction:actionCancel];
   [alertController addAction:actionOk];
    
   [self presentViewController:alertController animated:YES completion:nil];

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

	
	// Do any additional setup after loading the view.
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"########################### VIEW DID LOAD ###########################");
    /*DOLD*///NSLog(@"#####################################################################");
    /*DOLD*///NSLog(@"#####################################################################");
    
}

- (void)viewStartFunction
{
    /*DOLD*/NSLog(@"ViewStartFunction");
    
    /*DOLD*/NSLog(@"######################### WorkOrder: %d TemplateID: %d", [workOrder intValue], [_templateid intValue]);
	if(_appDelegate.openDbConnection){
		NSLog(@"DB ÖPPEN!");
	}
	
	
    //Kolla template
    int thisTemplateId;
    if([_templateid intValue]==0){
		NSLog(@"TemplateId = 0, så templateId = 135");
        thisTemplateId = 135;
    }else{
		NSLog(@"TemplateId satt");
        thisTemplateId = [_templateid intValue];
    }
	//NSLog(@"TemplateId: %d", thisTemplateId);

    //Kolla om editMode är aktivt och uppdatera knappen "editOrSaveButton" till "redigera" eller "spara"
    if([_editMode isEqualToNumber:[NSNumber numberWithInt:1]]){
        self.navigationItem.rightBarButtonItem = nil;
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemSave target:self action:@selector(saveReportAndPop)];
//        self.navigationItem.rightBarButtonItem.title = @"Spara";
//        _editOrSaveButton.title = @"Spara";
    }else{
        _editOrSaveButton.title = @"Redigera";
    }
	
	//Inaktivera redigera-läget för arbetsorder - disable:at för KD
	//if(thisTemplateId == 135){
	//	self.navigationItem.rightBarButtonItem = nil;
	//}
    
    //Inaktivera redigera-läget för användare som bara har läs-rättigheter
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if([[NSString stringWithFormat:@"%@",[appDelegate.userDictionary valueForKey:@"role"]] isEqualToString:@"4"]){
        self.navigationItem.rightBarButtonItem = nil;
    }

    reportFieldsStr = [[NSMutableString alloc] init];

	NSLog(@"TemplateId: %d", thisTemplateId);
    
	if (_appDelegate.openDbConnection)
	{
		//info från sys_formtables - blir templateinfo
        NSMutableArray * tempArray = [[NSMutableArray alloc] initWithCapacity:0];
        //info från prot_ - blir reportValues
        NSMutableArray * tempArray2 = [[NSMutableArray alloc] initWithCapacity:0];
        //info från prot_desc - blir reportFields
        NSMutableArray * tempArray3 = [[NSMutableArray alloc] initWithCapacity:0];
        //info från prot_ - blir defaultReportValues
        NSMutableArray * tempArray4 = [[NSMutableArray alloc] initWithCapacity:0];
        sqlite3_stmt *statement;
        
            /*DOLD*/NSLog(@"Databasen öppnad - viewDidLoad - sys_formtables, för att fylla reportFieldsStr");

			NSLog(@"TemplateId: %d", thisTemplateId);
            
			NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_formtables WHERE published = 1 AND id = %d;", thisTemplateId];
            /*DOLD*/NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

                NSString * myTempMutStr = [[NSMutableString alloc] init];

                char *col1 = (char *)sqlite3_column_text(statement, 0);
                if (col1 !=NULL){myTempMutStr = [NSString stringWithUTF8String: col1];}else{myTempMutStr = @"";}
                [dictionary setObject:myTempMutStr forKey:@"id"];
                /*DOLD*/NSLog(@"myTempMutStr för id: %@",myTempMutStr);
                myTempMutStr = nil;
                
                char *col2 = (char *)sqlite3_column_text(statement, 1);
                if (col2 !=NULL){myTempMutStr = [NSString stringWithUTF8String: col2];}else{myTempMutStr = @"";}
                [dictionary setObject:myTempMutStr forKey:@"form_name"];
                /*DOLD*/NSLog(@"myTempMutStr för formname: %@",myTempMutStr);
                myTempMutStr = nil;
                
                char *col3 = (char *)sqlite3_column_text(statement, 2);
                if (col3 !=NULL){myTempMutStr = [NSString stringWithUTF8String: col3];}else{myTempMutStr = @"";}
                [dictionary setObject:myTempMutStr forKey:@"table_name"];
                /*DOLD*/NSLog(@"myTempMutStr för tablename: %@",myTempMutStr);
                myTempMutStr = nil;
                
                char *col4 = (char *)sqlite3_column_text(statement, 3);
                if (col4 !=NULL){myTempMutStr = [NSString stringWithUTF8String: col4];}else{myTempMutStr = @"";}
                [dictionary setObject:myTempMutStr forKey:@"desc_table_name"];
                /*DOLD*/NSLog(@"myTempMutStr för desctablename: %@",myTempMutStr);
                myTempMutStr = nil;
                
                /*DOLD*/NSLog(@"Namn från dictionary: %@",[dictionary valueForKey:@"form_name"]);
                /*DOLD*/NSLog(@"Namn från dictionary: %@",[dictionary valueForKey:@"table_name"]);
                
                [tempArray addObject:[dictionary mutableCopy]];
				dictionary = nil;
				
                /*DOLD*/NSLog(@"Antal rader: %lu",(unsigned long)[tempArray count]);
            }
            
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
         
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
        _templateInfo = tempArray;
        /*DOLD*///NSLog(@"_templateInfo längd: %d", [_templateInfo count]);
        
        
        NSString * tableSuffix = @"";
		if(_appDelegate.tableBackup){tableSuffix = _appDelegate.tableBackup;}
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad: reportvalues. templateInfo: %@",_templateInfo);
            NSString *beginSQL = [[NSString alloc] init];
            if(_templateInfo.count > 0){
                if([[[_templateInfo objectAtIndex:0] valueForKey:@"id"] isEqualToString:@"135"]){
                beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@%@ WHERE id = %d;", [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"], tableSuffix, [workOrder intValue]];
                //beginSQL = [NSString stringWithFormat: @"SELECT * FROM prot_overlatelsebesiktning WHERE id = 1;"];
            }else{
                beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@%@ WHERE workorder_id = %d;", [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"], tableSuffix, [workOrder intValue]];
                //beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@_desc;", [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"]];
            }
            /*DOLD*/NSLog(@"Öppnar prot_-tabell. SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"Listar prot_tabell");
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
			
            /*DOLD*///NSLog(@"SQLite errorkod 1: %d", sqlite3_errcode(_appDelegate.openDbConnection));
			
			_blobdictionary = [[NSMutableDictionary alloc] init];
			
            while(sqlite3_step(statement) == SQLITE_ROW) {
                /*DOLD*///NSLog(@"while(sqlite3_step(statement) == SQLITE_ROW); SQLite errorkod 2: %d", sqlite3_errcode(_appDelegate.openDbConnection));
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int i = 0;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    
					//Undvik att fylla dictionary med nil
					if (myTempMutStr) {
						
						//Alla values passerar myTempMutStr här. Gör globala ersättningar här.
						(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    }else{
						NSLog(@"TEMPMUTSTR var tom för %s", colName);
						//Hittat en blob? Om det är en blob, så är colvalue inte NULL men blir ändå NIL.
						
						/* The pointer returned by sqlite3_column_blob() points to memory
						 ** that is owned by the statement handle (pStmt). It is only good
						 ** until the next call to an sqlite3_XXX() function (e.g. the
						 ** sqlite3_finalize() below) that involves the statement handle.
						 ** So we need to make a copy of the blob into memory obtained from
						 ** malloc() to return to the caller.
						 */
						
						NSData *imgData = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, i) length:sqlite3_column_bytes(statement, i)];
						
                        NSLog(@"BLOBDATA HITTAT: %lu", (unsigned long)[imgData length]);
						
						(void)[_blobdictionary setObject:imgData forKey:[NSString stringWithUTF8String:colName]];
						
                        NSLog(@"blobdictionary forkey %@: %lu",[NSString stringWithUTF8String:colName], (unsigned long)[[_blobdictionary valueForKey:[NSString stringWithUTF8String:colName]] length]);
						
						
						
					}
					
					
					myTempMutStr = nil;
                    /*DOLD3*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                
                
                [tempArray3 addObject:[dictionary mutableCopy]];
				dictionary = nil;
				
                /*DOLD*///NSLog(@"Antal rader i temparray3: %d",[tempArray3 count]);
            }
            /*DOLD*///NSLog(@"SQLite errorkod 3: %d", sqlite3_errcode(_appDelegate.openDbConnection));
			sqlite3_finalize(statement);
			beginSQL = nil;
			begin_stmt = nil;
            }
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 2");
		}
		
		//De värden som ligger i databasen
		_reportInitialValues = [tempArray3 mutableCopy];
		
		//Om reportValuesDelegate är tom, dvs rapporten just öppnades, sätt den till rapportens sparade värden från databasen.
		//Om reportValuesDelegate inte är tom, betyder det att rapporten öppnades tidigare och inte har sparats (t.ex. efter att man lagt till en autotext och återvänt till denna view)
		if([_appDelegate.reportValuesDelegate count]<1){
			NSLog(@"appDelegate.reportValuesDelegate = tempArray3;");
			
			/*
			if ([tempArray3 count]<1) {
				NSLog(@"temparray3 var tom, lägger till dictionary");
				NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
				[dict setValue:@"Värde för initiering av dictionary" forKey:@"1111111111111111111111111"];
				[tempArray3 addObject:dict];
			}
			 */
			if(tempArray3){
				NSLog(@"Kopierar temparray3 till delegate");
				_appDelegate.reportValuesDelegate = [tempArray3 mutableCopy];
			}else{
				NSLog(@"Temparray3 existerade inte - initierar reportsValueDelegate som ny mutablearray");
				_appDelegate.reportValuesDelegate = [[NSMutableArray alloc]init];
			}
		}else{
			NSLog(@"appDelegate.reportValuesDelegate sattes INTE eftersom det redan innehöll data: %@",_appDelegate.reportValuesDelegate);
		}
        /*DOLD3*///NSLog(@"_reportInitialValues count: %d", [_reportInitialValues count]);
        /*DOLD3*/NSLog(@"_reportInitialValues: %@", _reportInitialValues);
        /*DOLD3*///NSLog(@"appDelegate.reportValuesDelegate count: %d", [appDelegate.reportValuesDelegate count]);
        /*DOLD3*/NSLog(@"appDelegate.reportValuesDelegate: %@", _appDelegate.reportValuesDelegate);
        
		
		
		
		
		
		
		
		
		if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad: reportvalues");
			NSString *beginSQL = [[NSString alloc] init];
			beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE id = 1 LIMIT 1;", [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"]];
			
			/*DOLD*///NSLog(@"def rep vals SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"Listar prot_tabell");
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
			
            /*DOLD*///NSLog(@"SQLite errorkod 1: %d", sqlite3_errcode(_appDelegate.openDbConnection));
			
            while(sqlite3_step(statement) == SQLITE_ROW) {
                /*DOLD*///NSLog(@"while(sqlite3_step(statement) == SQLITE_ROW); SQLite errorkod 2: %d", sqlite3_errcode(_appDelegate.openDbConnection));
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int i = 0;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    
					(void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    
					myTempMutStr = nil;
                    /*DOLD*///NSLog(@"DEFAULTREPORTVALUES colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                
                
				[tempArray4 addObject:[dictionary mutableCopy]];
				dictionary = nil;
				
                /*DOLD*///NSLog(@"Antal rader i temparray3: %d",[tempArray3 count]);
            }
            /*DOLD*///NSLog(@"SQLite errorkod 3: %d", sqlite3_errcode(_appDelegate.openDbConnection));
			
			sqlite3_finalize(statement);
			begin_stmt = nil;
			beginSQL = nil;
            
        } else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 3");
		}
        _defaultReportValues = tempArray4;
        /*DOLD*///NSLog(@"defaultReportValues count: %d", [_defaultReportValues count]);
		/*DOLD*/NSLog(@"defaultReportValues: %@", _defaultReportValues);
		if([_appDelegate.reportValuesDelegate count]<1 && [_editMode intValue] == 1){
			NSLog(@"reportValuesDelegate var tom, så defaultValues kopierades");
			_appDelegate.reportValuesDelegate = [_defaultReportValues mutableCopy];
			/*DOLD*/NSLog(@"appDelegate.reportValuesDelegate satt till defaultValues: %@", _appDelegate.reportValuesDelegate);
		}
			
		
		
		
        
		
		
		
		
		
        
        
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad - viewdidload - hämta namnet på desc-tabell");
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE active = 1 ORDER BY sortorder ASC;", [[_templateInfo objectAtIndex:0] valueForKey:@"desc_table_name"]];
            /*DOLD*///NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"");
            /*DOLD*///NSLog(@"#####################################################################");
            /*DOLD*///NSLog(@"########################### LOOPAR FÄLT #############################");
            /*DOLD*///NSLog(@"#####################################################################");

            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSLog(@"dictinary = %@", dictionary);
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                NSLog(@"myTempMutStr = %@", myTempMutStr);
                
                int colCount = sqlite3_column_count(statement);
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    /*DOLD*///NSLog(@"while(sqlite3_step(statement) == SQLITE_ROW); SQLite errorkod 2: %d", sqlite3_errcode(_appDelegate.openDbConnection));
                    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                    NSString * myTempMutStr = [[NSMutableString alloc] init];
                    
                    int i = 0;
                    for (i=0; i<colCount;i++) {
                        //Ger namnet på kolumn N
                        const char *colName = (const char*)sqlite3_column_name(statement, i);
                        char *colValue = (char *)sqlite3_column_text(statement, i);
                        if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}

                        //Undvik att fylla dictionary med nil
                        if (myTempMutStr) {
                            //Alla values passerar myTempMutStr här. Gör globala ersättningar här.
                            (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                        }
                        myTempMutStr = nil;
                    }
                    
                    if([[dictionary valueForKey:@"fieldtype"] isEqualToString:@"includedReports"]){
                        //lägg till original-objektet för en ev. rubrik
                        [tempArray2 addObject:dictionary];
                        
                        if([_appDelegate.reportValuesDelegate count]>0){
                            //Hämta värdet för includereports-kolumnen
                            NSString * incRepVal = [[NSString alloc] init];
                            incRepVal = [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]];
                            
                            NSArray * incRepArr = [[NSArray alloc] init];
                            incRepArr = [incRepVal componentsSeparatedByString:@","];
                            int i;
                            for (i=0; i<[incRepArr count]; i++) {
                                //Lägg till ett nytt dictionary-objekt i _reportFields för varje rapport som hör till arbetsordern
                                NSMutableDictionary *thisIncRep = [NSMutableDictionary dictionary];
                                //Säg till tabell-renderaren vilken mall som ska användas
                                [thisIncRep setObject:[NSString stringWithFormat:@"includedReportsLink"] forKey:@"fieldtype"];
                                //Mall att länka till
                                [thisIncRep setObject:incRepArr[i] forKey:@"incRepId"];
                                //mall x av y (om man i tabellrenderingen vill veta vilken som är första och sista posten)
                                [thisIncRep setObject:[NSNumber numberWithInt:i] forKey:@"incRepNumber"];
                                [thisIncRep setObject:[NSNumber numberWithInt:(int) [incRepArr count]] forKey:@"incRepTotal"];
                                //Lägg till i array
                                [tempArray2 addObject:thisIncRep];
                            }
                        }
                        
#pragma mark Json Autotexter - tolka objekt
                        /*
                         #####################################################################
                         #####################################################################
                         ########################### JSON AUTOTEXTER #########################
                         #####################################################################
                         #####################################################################
                         */
                    }else if([[dictionary valueForKey:@"fieldtype"] isEqualToString:@"autotext"]){
                        /*DOLD3*///NSLog(@"Hittat autotext-fält");
                        
                        /*DOLD*///NSLog(@"1. [appDelegate.reportValuesDelegate objectAtIndex:0]: %@",[appDelegate.reportValuesDelegate objectAtIndex:0]);
                        /*DOLD*///NSLog(@"2. [dictionary valueForKey:@'fieldcolname']: %@",[dictionary valueForKey:@"fieldcolname"]);
                        /*DOLD*///NSLog(@"3. [[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@'fieldcolname']]: %@",[[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]]);
                        
                        /*
                         Flyttar denna till efter det har fyllts med data. OBS! om objektet är tomt blir det ingen rubrik. Lös det med en "lägg till original-objektet efter hela json-tolknings-grejen om det inte fanns nåt värde"
                         //lägg till original-objektet för en ev. rubrik
                         //[tempArray2 addObject:dictionary];
                         */
                        
                        /*DOLD*///NSLog(@"La till dictionary i array");
                        /*DOLD*///NSLog(@"reportvalues count: %d",[appDelegate.reportValuesDelegate count]);
                        
                        
                        if([_appDelegate.reportValuesDelegate count]>0 || [_defaultReportValues count]>0){
                            /*
                             ###########################################
                             ########## Kolla om objektet är tomt ######
                             ###########################################
                             */
                            
                            /*
                             if([[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]]==nil || [[[appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]] isEqualToString:@""]){
                             //NSLog(@"Objektet som skulle innehållit json-strängen var nil");
                             [tempArray2 addObject:dictionary];
                             
                             //Lägg till "Lägg till zon"-rad
                             NSMutableDictionary *thisAutoAddZoneRow = [NSMutableDictionary dictionary];
                             [thisAutoAddZoneRow setObject:[NSString stringWithFormat:@"autotextaddzone"] forKey:@"fieldtype"];
                             [tempArray2 addObject:thisAutoAddZoneRow];
                             
                             //NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till zon'-rad: %d",[tempArray2 count]);
                             //NSLog(@"TEMPARRAY2 COUNT vid slut: %d",[tempArray2 count]);
                             
                             //}else{*/
                            
                            /*DOLD*///NSLog(@"reportValues count > 0");
                            
                            //Sätt källan för json-objektet - fältets värde, eller rapportmallens standardvärde?
                            
                            NSLog(@"_editMode: %d", [_editMode intValue]);
                            
                            NSString * autoJsonStr = [[NSString alloc] init];
                            NSLog(@"Sätt json-källa");
                            if([_appDelegate.reportValuesDelegate count]>0){
                                NSLog(@"reportValuesDelegate count > 0, rapporten har laddats från DB");
                                if([[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]]){
                                    autoJsonStr = [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]];
                                    NSLog(@"Sätter autoJsonStr = %@",autoJsonStr);
                                    
                                }
                            }else if([_defaultReportValues count]>0){
                                NSLog(@"reportValuesDelegate count = 0, rapporten har inte laddats från DB. Hämtar värde från defaultReportValues");
                                if([_editMode intValue] == 1){
                                    NSLog(@"_editMode = 1, sätter autotexten till default-värdet");
                                    if([[_defaultReportValues objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]]){
                                        autoJsonStr = [[_defaultReportValues objectAtIndex:0] valueForKey:[dictionary valueForKey:@"fieldcolname"]];
                                        NSLog(@"Sätter autoJsonStr = %@",autoJsonStr);
                                    }
                                }else{
                                    NSLog(@"_editMode = 0, sätter inte autotexten till default-värdet");
                                }
                            }
                            
                            /*DOLD3*///NSLog(@"autoJsonStr: %@",autoJsonStr);
                            if([autoJsonStr isEqualToString:@""] || !autoJsonStr){
                                NSLog(@"autoJsonStr är tom, sätt värdet till [] (tom array)");
                                autoJsonStr = @"[]";
                            }
                            
                            NSData * jsonData = [[NSData alloc] initWithData:[[autoJsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                            
                            /*DOLD3*///NSLog(@"jsonData: %@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
                            
                            
                            NSMutableArray* jsonArr = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
                            /*DOLD3*///NSLog(@"jsonArr-objektet är: %@",jsonArr );
                            
                            //Hämta json-objektet från completejsonholder om editmode är aktivt, annars, töm jsonholders
                            if([_editMode intValue] == 1){
                                if (_appDelegate.completeJsonHolder != nil) {
                                    NSLog(@"appDelegate.completeJsonHolder = %@",_appDelegate.completeJsonHolder);
                                    jsonArr = _appDelegate.completeJsonHolder;
                                    /*DOLD3*/NSLog(@"sätter json till appDelegate.completeJsonHolder");
                                }else{
                                    _appDelegate.completeJsonHolder = jsonArr;
                                    /*DOLD3*/NSLog(@"sätter appDelegate.completeJsonHolder första gången");
                                    
                                }
                            }else{
                                //töm json
                                
                                _appDelegate.completeJsonHolder = nil;
                                _appDelegate.jsonOrigHolder = nil;
                                _appDelegate.jsonHolder = nil;
                                
                            }
                            
                            
                            //För varje item(zon) i json, lägg till en rad med zon-mall, loopa sedan igenom zon-itemets array och lägg till rader för varje rum som har en dictionary.
                            
                            /*
                             AUTOTEXT____Sortera zoner__Redigera namn/text
                             -----------------------------
                             Zon 1       Sortera rum     >
                             -----------------------------
                             Rum 1       Sortera texter  >
                             Allmän text
                             Text 1                  >
                             Text 2                  >
                             Notering
                             Text 3                  >
                             Lägg till text              >
                             -----------------------------
                             Rum 2       Sortera texter  >
                             Text 1                      >
                             Text 2                      >
                             Lägg till text              >
                             Lägg till rum               >
                             -----------------------------
                             Zon 2       Sortera rum     >
                             -----------------------------
                             Rum 1       Sortera texter	>
                             Text 1                      >
                             Lägg till text              >
                             -----------------------------
                             Rum 2       Sortera texter	>
                             Text 1                      >
                             Text 2                      >
                             Text 3                      >
                             Lägg till text              >
                             Lägg till rum               >
                             -----------------------------
                             Lägg till zon               >
                             -----------------------------
                             
                             Totalt 8 typer av rader: Zon, Rum, Allmän text, Notering, lägg till allmän text, lägg till notering, lägg till rum, lägg till zon.
                             //*/
                            
                            
                            if (!jsonArr) {
                                /*VIKTIG*/NSLog(@"Error parsing JSON");
                                
                                // 								[self.navigationController popToRootViewControllerAnimated:YES];
                                
                                //self.navigationItem.rightBarButtonItem = nil;
                                
                                
                                
                                NSString *alertString = [NSString stringWithFormat:@"Kunde inte läsa autotext-objektet. Försök att uppdatera databasen, och prova igen."];
//                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
//                                [alert show];
//
                                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                                            message:@""
                                                                                                     preferredStyle:UIAlertControllerStyleAlert];
                               //We add buttons to the alert controller by creating UIAlertActions:
                                UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Done"
                                                                                   style:UIAlertActionStyleDefault
                                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                                    }];
                                
                            [alertController addAction:actionCancel];
                                
                               [self presentViewController:alertController animated:YES completion:nil];

                                
                                
                                NSLog(@"jsonArr kunde inte läsas, autoJsonStr är tom, sätt värdet till [] (tom array)");
                                autoJsonStr = @"[]";
                                jsonData = [[NSData alloc] initWithData:[[autoJsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                                jsonArr = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:nil];
                            }
                            
                            
                            
                            if(jsonArr){
                                
                                NSMutableArray *editedJsonOrig = _appDelegate.jsonOrigHolder;
                                NSMutableArray *editedJson = _appDelegate.jsonHolder;
                                _appDelegate.jsonOrigHolder = nil;
                                _appDelegate.jsonHolder = nil;
                                /*DOLD*///NSLog(@"#####################################################");
                                /*DOLD*///NSLog(@"#####################################################");
                                /*DOLD*///NSLog(@"#####################################################");
                                /*DOLD*///NSLog(@"#####################################################");
                                /*DOLD*///NSLog(@"completeJsonHolder count: %d", [appDelegate.completeJsonHolder count]);
                                /*DOLD*///NSLog(@"editedJsonOrig: %@", editedJsonOrig);
                                /*DOLD*///NSLog(@"editedJson: %@", editedJson);
                                
                                //lägg till original-objektet för en ev. rubrik och för att man ska kunna sortera zoner
                                //Uppdatera först
                                //Resten av objektet
                                NSMutableArray * updatedJsonArr = [[NSMutableArray alloc] init];
                                
                                //Se om data har uppdaterats via sorteringsmodulen
                                //NSLog(@"room %d: %@",j, room);
                                //NSLog(@"editedJsonOrig: %@",editedJsonOrig);
                                
                                
                                NSLog(@"JÄMFÖR HELA OBJEKTET (söker efter tillagd zon)");
                                NSLog(@"####################################################");
                                
                                //Eftersom hela objektet kan vara identiskt med objektet som skickas när man lägger till ett rum (det objektet är hela zonen, och om man bara har en zon inlagd så är objektet likadant som hela json-objektet), så om det matchar "zon lades till" så ska inte rums-uppdateringen köras. Om man lägger till ett rum när man bara har en zon är det av samma anledning egentligen denna funktion som körs, men det spelar ingen roll. Hela objektet ersätts ändå med det nya. Felet blir när båda körs.
                                int wasZonesJustSortedOrAdded = 0;
                                
                                //NSString * oldJsonArr = [jsonArr copy];
                                
                                NSLog(@"jsonArr count: %lu", (unsigned long)[jsonArr count]);
                                if([jsonArr isEqualToArray:editedJsonOrig] || [jsonArr count]<1){
                                    //NSLog(@"Infos: editedJsonOrig = room! %@", [zone valueForKey:@"infos"]);
                                    updatedJsonArr = editedJson;
                                    
                                    wasZonesJustSortedOrAdded = 1;
                                    
                                    NSLog(@"MATCHADE (en zon har lagts till eller sorterats) - wasZonesJustSortedOrAdded: %d", wasZonesJustSortedOrAdded);
                                    NSLog(@"####################################################");
                                    NSLog(@"ORIGINAL jsonArr: %@", jsonArr);
                                    NSLog(@"####################################################");
                                    NSLog(@"ORIGINAL editedJsonOrig: %@", editedJsonOrig);
                                    NSLog(@"####################################################");
                                    NSLog(@"UPPDATERAD: %@", editedJson);
                                    NSLog(@"####################################################");
                                    
                                }else{
                                    updatedJsonArr = jsonArr;
                                    
                                    NSLog(@"MATCHADE INTE (en zon har INTE lagts till) - wasZonesJustSortedOrAdded: %d", wasZonesJustSortedOrAdded);
                                    NSLog(@"####################################################");
                                    NSLog(@"ORIGINAL jsonArr: %@", jsonArr);
                                    NSLog(@"####################################################");
                                    NSLog(@"ORIGINAL editedJsonOrig: %@", editedJsonOrig);
                                    NSLog(@"####################################################");
                                    NSLog(@"UPPDATERAD: %@", editedJson);
                                    NSLog(@"####################################################");
                                }
                                
                                jsonArr = updatedJsonArr;
                                _appDelegate.completeJsonHolder = jsonArr;
                                /*DOLD3*///NSLog(@"uppdaterar appDelegate.completeJsonHolder");
                                if(_appDelegate.completeJsonHolder){
                                    [dictionary setObject:_appDelegate.completeJsonHolder forKey:@"atObj"];
                                }else{
                                    NSLog(@"Kunde inte, completeJsonHolder var nil");
                                }
                                
                                NSLog(@"########################### RUM-OBJ som läggs till i AT-header-raden för zon-sortering: %@",[dictionary valueForKey:@"atObj"]);
                                
                                
                                [tempArray2 addObject:[dictionary mutableCopy]];
                                dictionary = nil;
                                
                                
                                /*
                                 ###########################################
                                 ########## START ZON ######################
                                 ###########################################
                                 */
                                //NSLog(@"start zon");
                                
                                /*DOLD*///NSLog(@"TEMPARRAY2 COUNT vid start: %d",[tempArray2 count]);
                                int i = 0;
                                NSMutableArray * updatedZoneArr = [[NSMutableArray alloc] init];
                                for(NSDictionary *zone in jsonArr) {
                                    /*DOLD*///NSLog(@"Zon: %@", zone);
                                    
                                    //Skapa ett nytt dictionary-objekt för att lägga i _reportFields
                                    NSMutableDictionary *thisAutoZoneRow = [NSMutableDictionary dictionary];
                                    
                                    //Säg till tabell-renderaren vilken mall som ska användas, zon eller vanlig rad?
                                    [thisAutoZoneRow setObject:[NSString stringWithFormat:@"autotextzone"] forKey:@"fieldtype"];
                                    
                                    //Resten av objektet
                                    
                                    //Skapa en ny dictionary för att passa vidare i kedjan till nästa loop (och i slutändan till json-objektet)
                                    NSMutableDictionary * updatedZone = [[NSMutableDictionary alloc] init];
                                    
                                    [updatedZone setObject:[zone objectForKey:@"id"] forKey:@"id"];
                                    [updatedZone setValue:[zone valueForKey:@"title"] forKey:@"title"];
                                    [updatedZone setValue:[NSString stringWithFormat:@"%d",i] forKey:@"sortorder"];
                                    
                                    /*DOLD3*///NSLog(@"JÄMFÖR ZON-OBJEKTET (söker efter tillagt rum)");
                                    /*
                                     NSLog(@"####################################################");
                                     
                                     //Se om rums-data har uppdaterats via sorteringsmodulen
                                     if([[zone valueForKey:@"room"] isEqualToArray:editedJsonOrig]){
                                     [updatedZone setValue:editedJson forKey:@"room"];
                                     
                                     NSLog(@"MATCHADE (ett rum har lagts till)");
                                     NSLog(@"####################################################");
                                     NSLog(@"ORIGINAL: %@", editedJsonOrig);
                                     NSLog(@"####################################################");
                                     NSLog(@"UPPDATERAD: %@", editedJson);
                                     NSLog(@"####################################################");
                                     
                                     }else{
                                     [updatedZone setValue:[zone valueForKey:@"room"] forKey:@"room"];
                                     
                                     NSLog(@"MATCHADE INTE (ett rum har INTE lagts till)");
                                     NSLog(@"####################################################");
                                     NSLog(@"ORIGINAL: %@", editedJsonOrig);
                                     NSLog(@"####################################################");
                                     NSLog(@"UPPDATERAD: %@", editedJson);
                                     NSLog(@"####################################################");
                                     
                                     }
                                     */
                                    
                                    
                                    NSLog(@"JÄMFÖR ZON-OBJEKTET 2 (söker efter tillagt rum) - wasZonesJustSortedOrAdded: %d", wasZonesJustSortedOrAdded);
                                    NSLog(@"####################################################");
                                    
                                    //Se om rums-data har uppdaterats via sorteringsmodulen
                                    //if([[editedJsonOrig objectAtIndex:0] isKindOfClass:[NSDictionary class]]){
                                    
                                    if(wasZonesJustSortedOrAdded == 0 && [zone isEqualToDictionary:[editedJsonOrig objectAtIndex:0]]){ // && ![jsonArr isEqualToArray:editedJsonOrig]){
                                        if([editedJson count]>0){
                                            [updatedZone setValue:[[editedJson objectAtIndex:0] valueForKey:@"room"] forKey:@"room"];
                                        }
                                        
                                        NSLog(@"MATCHADE (ett rum har lagts till)");
                                        NSLog(@"####################################################");
                                        NSLog(@"ORIGINAL: %@", [zone valueForKey:@"room"]);
                                        NSLog(@"####################################################");
                                        NSLog(@"UPPDATERAD: %@", [[editedJson objectAtIndex:0] valueForKey:@"room"]);
                                        NSLog(@"####################################################");
                                        
                                    }else{
                                        [updatedZone setValue:[zone valueForKey:@"room"] forKey:@"room"];
                                        
                                        NSLog(@"MATCHADE INTE (ett rum har INTE lagts till)");
                                        NSLog(@"####################################################");
                                        //NSLog(@"ORIGINAL: %@", editedJsonOrig);
                                        //NSLog(@"####################################################");
                                        //NSLog(@"UPPDATERAD: %@", editedJson);
                                        NSLog(@"####################################################");
                                        
                                    }
                                    //}else{
                                    //	[updatedZone setValue:[zone valueForKey:@"room"] forKey:@"room"];
                                    //									}
                                    
                                    
                                    //NSLog(@"updatedZone %d: %@",j, updatedZone);
                                    
                                    //rad: ID-nummer
                                    [thisAutoZoneRow setObject:[updatedZone objectForKey:@"id"] forKey:@"id"];
                                    //rad: zon-objekt
                                    [thisAutoZoneRow setObject:updatedZone forKey:@"zoneObj"];
                                    //rad: zon x av y (om man i tabellrenderingen vill veta vilken som är första och sista posten)
                                    [thisAutoZoneRow setObject:[NSNumber numberWithInt:i+1] forKey:@"zoneNumber"];
                                    [thisAutoZoneRow setObject:[NSNumber numberWithInt:(int) [jsonArr count]] forKey:@"zoneTotal"];
                                    
                                    //Lägg till i array
                                    //NSLog(@"updatedZone (som blir zoneObj): %@",updatedZone);
                                    //NSLog(@"thisAutoZoneRow %@",thisAutoZoneRow);
                                    [tempArray2 addObject:thisAutoZoneRow];
                                    
                                    
                                    /*
                                     ###########################################
                                     ########## START RUM ######################
                                     ###########################################
                                     */
                                    //NSLog(@"start rum");
                                    
                                    int j = 0;
                                    NSMutableArray * updatedRoomArr = [[NSMutableArray alloc] init];
                                    for(NSDictionary *room in [updatedZone objectForKey:@"room"]){
                                        
                                        /*DOLD*///NSLog(@"Rum: %@", room);
                                        
                                        //Skapa ett nytt dictionary-objekt för att lägga i _reportFields
                                        NSMutableDictionary *thisAutoRoomRow = [NSMutableDictionary dictionary];
                                        
                                        //Säg till tabell-renderaren vilken mall som ska användas, zon eller vanlig rad?
                                        [thisAutoRoomRow setObject:[NSString stringWithFormat:@"autotextroom"] forKey:@"fieldtype"];
                                        
                                        //Resten av objektet
                                        
                                        //Skapa en ny dictionary för att passa vidare i kedjan till nästa loop (och i slutändan till json-objektet)
                                        NSMutableDictionary * updatedRoom = [[NSMutableDictionary alloc] init];
                                        
                                        [updatedRoom setObject:[room objectForKey:@"id"] forKey:@"id"];
                                        [updatedRoom setValue:[room valueForKey:@"title"] forKey:@"title"];
                                        [updatedRoom setValue:[NSString stringWithFormat:@"%d",j] forKey:@"sortorder"];
                                        
                                        //Se om text-data har uppdaterats via sorteringsmodulen
                                        
                                        /*
                                         NSLog(@"JÄMFÖR RUM-OBJEKTET (söker efter uppdaterat rumsobjekt, tillagd text eller sorterat rum) - metoden funkar både med sortering och tilläggning");
                                         NSLog(@"####################################################");
                                         */
                                        if([room isEqualToDictionary:[editedJsonOrig objectAtIndex:0]]){
                                            /*DOLD*///NSLog(@"UPPDATERAR!!");
                                            if([editedJson count]>0){
                                                updatedRoom  = [editedJson objectAtIndex:0];
                                            }
                                            
                                            //[updatedRoom setValue:editedJson forKey:@"notes"];
                                            /*DOLD3*///NSLog(@"MATCHADE (text har lagts till)");
                                            /*DOLD3*///NSLog(@"####################################################");
                                            /*DOLD3*///NSLog(@"ORIGINAL: %@", editedJsonOrig);
                                            /*DOLD3*///NSLog(@"####################################################");
                                            /*DOLD3*///NSLog(@"UPPDATERAD: %@", editedJson);
                                            /*DOLD3*///NSLog(@"####################################################");
                                            
                                        }else{
                                            updatedRoom = [room mutableCopy];
                                            /*DOLD3*///NSLog(@"MATCHADE INTE (text har INTE lagts till eller sorterats)");
                                            /*DOLD3*///NSLog(@"####################################################");
                                            //NSLog(@"ORIGINAL: %@", editedJsonOrig);
                                            //NSLog(@"####################################################");
                                            //NSLog(@"UPPDATERAD: %@", editedJson);
                                            /*DOLD3*///NSLog(@"####################################################");
                                            
                                        }
                                        
                                        
                                        //rad: ID-nummer
                                        [thisAutoRoomRow setObject:[updatedRoom objectForKey:@"id"] forKey:@"id"];
                                        //rad: zon-objekt
                                        [thisAutoRoomRow setObject:updatedRoom forKey:@"roomObj"];
                                        //rad: zon x av y (om man i tabellrenderingen vill veta vilken som är första och sista posten)
                                        [thisAutoRoomRow setObject:[NSNumber numberWithInt:j+1] forKey:@"roomNumber"];
                                        [thisAutoRoomRow setObject:[NSNumber numberWithInt:(int) [[updatedZone objectForKey:@"room"] count]] forKey:@"roomTotal"];
                                        
                                        //Lägg till i array
                                        [tempArray2 addObject:thisAutoRoomRow];
                                        /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt rum: %d",[tempArray2 count]);
                                        
                                        if([[updatedRoom valueForKey:@"infos"] count]>0){
                                            
                                            //Lägg till "Allmän information"
                                            NSMutableDictionary *thisAutoRow = [NSMutableDictionary dictionary];
                                            [thisAutoRow setObject:[NSString stringWithFormat:@"autotextinfoheader"] forKey:@"fieldtype"];
                                            //Hela infotext-objektet för sortering
                                            [thisAutoRow setObject:updatedRoom forKey:@"roomObj"];
                                            [thisAutoRow setObject:[updatedRoom valueForKey:@"infos"] forKey:@"infoObj"];
                                            
                                            [tempArray2 addObject:thisAutoRow];
                                            
                                            /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt info-header-rad: %d",[tempArray2 count]);
                                            
                                            /*
                                             ###########################################
                                             ########## START INFO #####################
                                             ###########################################
                                             */
                                            //NSLog(@"start infos");
                                            
                                            int k = 0;
                                            for(NSDictionary *infoText in [updatedRoom valueForKey:@"infos"]){
                                                /*DOLD3*///NSLog(@"info %d: %@", k, infoText);
                                                
                                                //Skapa ett nytt dictionary-objekt för att lägga i _reportFields
                                                NSMutableDictionary *thisAutoInfoRow = [NSMutableDictionary dictionary];
                                                
                                                //Säg till tabell-renderaren vilken mall som ska användas, zon eller vanlig rad?
                                                [thisAutoInfoRow setObject:[NSString stringWithFormat:@"autotextinfo"] forKey:@"fieldtype"];
                                                
                                                //ID-nummer
                                                [thisAutoInfoRow setObject:[infoText objectForKey:@"id"] forKey:@"id"];
                                                
                                                //Resten av objektet
                                                //NSLog(@"########################## infoText före: %@", infoText);
                                                //NSLog(@"\nnoteText: %@ \n\neditedJsonOrig: %@ \n\neditedJson: %@", infoText, editedJsonOrig, editedJson);
                                                
                                                /*
                                                 if([infoText isEqual:[editedJsonOrig objectAtIndex:0]]){
                                                 
                                                 //DOLD3//NSLog(@"editedJsonOrig = infotext!");
                                                 //                                              [thisAutoinfoRow setObject:editedJson forKey:@"infoObj"];
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:[editedJson objectAtIndex:0] forKey:@"infoObj"];
                                                 [thisAutoInfoRow setObject:[tempDict valueForKey:@"infoObj"] forKey:@"infoObj"];
                                                 
                                                 //                                                [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"infos"] forKey:@"infoObj"];
                                                 }else if([infoText isEqual:editedJsonOrig]){
                                                 
                                                 //DOLD3//NSLog(@"editedJsonOrig = notetext!");
                                                 //                                              [thisAutoNoteRow setObject:editedJson forKey:@"noteObj"];
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:editedJson forKey:@"infoObj"];
                                                 [thisAutoInfoRow setObject:[tempDict valueForKey:@"infoObj"] forKey:@"infoObj"];
                                                 
                                                 //                                                [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"infos"] forKey:@"infoObj"];
                                                 //Om man uppdaterar en fält med flera texter
                                                 }else{
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:infoText forKey:@"infoObj"];
                                                 [thisAutoInfoRow setObject:[tempDict valueForKey:@"infoObj"] forKey:@"infoObj"];
                                                 
                                                 //                                                [thisAutoinfoRow setObject:infoText forKey:@"infoObj"];
                                                 *//*DOLD3*//*//NSLog(@"editedJsonOrig != infotext...");
                                                             }
                                                             //*/
                                                
                                                //NSLog(@"########################## infoText efter: %@", infoText);
                                                
                                                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                
                                                //[tempDict setObject:[editedJson objectAtIndex:0] forKey:@"infoObj"];
                                                //[tempDict setObject:editedJson forKey:@"infoObj"];
                                                [tempDict setObject:[infoText copy] forKey:@"infoObj"];
                                                
                                                //NSLog(@"#1: %@\n\n#2: %@\n\n #3: %@\n\n", [editedJson objectAtIndex:0],editedJson,infoText);
                                                
                                                [thisAutoInfoRow setObject:updatedRoom forKey:@"roomObj"];
                                                [thisAutoInfoRow setObject:[tempDict valueForKey:@"infoObj"] forKey:@"infoObj"];
                                                
                                                
                                                
                                                //rum x av y (om man i tabellrenderingen vill veta vilken som är första och sista posten)
                                                [thisAutoInfoRow setObject:[NSNumber numberWithInt:k+1] forKey:@"infoNumber"];
                                                
                                                [thisAutoInfoRow setObject:[NSNumber numberWithInt:(int) [[room valueForKey:@"infos"] count]] forKey:@"infoTotal"];
                                                
                                                //Lägg till i array
                                                //NSLog(@"thisAutoInfoRow: %@",thisAutoInfoRow);
                                                [tempArray2 addObject:thisAutoInfoRow];
                                                //NSLog(@"TEMPARRAY2 COUNT tillagt infotext: %d",[tempArray2 count]);
                                                
                                                k++;
                                            }
                                        }
                                        
                                        if([[updatedRoom valueForKey:@"notes"] count]>0){
                                            
                                            //Lägg till "Noteringar:"
                                            NSMutableDictionary *thisAutoRow = [NSMutableDictionary dictionary];
                                            [thisAutoRow setObject:[NSString stringWithFormat:@"autotextnotesheader"] forKey:@"fieldtype"];
                                            //Hela infotext-objektet för sortering
                                            [thisAutoRow setObject:updatedRoom forKey:@"roomObj"];
                                            [thisAutoRow setObject:[updatedRoom valueForKey:@"notes"] forKey:@"noteObj"];
                                            //										[thisAutoRow setObject:[updatedRoom valueForKey:@"notes"] forKey:@"noteObj"];
                                            //                                      [thisAutoRow setObject:[room valueForKey:@"notes"] forKey:@"noteObj"];
                                            [tempArray2 addObject:thisAutoRow];
                                            /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till text-rad: %d",[tempArray2 count]);
                                            
                                            /*
                                             ###########################################
                                             ########## START NOTES ####################
                                             ###########################################
                                             */
                                            //NSLog(@"start notes");
                                            
                                            //NSLog(@"[updatedRoom valueForKey:@notes]: %@",[updatedRoom valueForKey:@"notes"]);
                                            
                                            int l = 0;
                                            for(NSObject *noteText in [updatedRoom valueForKey:@"notes"]){
                                                ///*DOLD*///NSLog(@"Rum: %@", room);
                                                
                                                //Skapa ett nytt dictionary-objekt för att lägga i _reportFields
                                                NSMutableDictionary *thisAutoNoteRow = [NSMutableDictionary dictionary];
                                                
                                                //Säg till tabell-renderaren vilken mall som ska användas, zon eller vanlig rad?
                                                [thisAutoNoteRow setObject:[NSString stringWithFormat:@"autotextnote"] forKey:@"fieldtype"];
                                                
                                                //ID-nummer
                                                [thisAutoNoteRow setObject:[noteText valueForKey:@"id"] forKey:@"id"];
                                                //Resten av objektet
                                                //NSLog(@"########################## noteText före: %@", noteText);
                                                /*
                                                 
                                                 
                                                 //NSLog(@"\nnoteText: %@ \n\neditedJsonOrig: %@ \n\neditedJson: %@", noteText, editedJsonOrig, editedJson);
                                                 if([noteText isEqual:editedJsonOrig]){
                                                 //NSLog(@"editedJsonOrig = notetext!");
                                                 //                                              [thisAutoNoteRow setObject:editedJson forKey:@"noteObj"];
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:editedJson forKey:@"noteObj"];
                                                 [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                 
                                                 //                                                [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"notes"] forKey:@"noteObj"];
                                                 //Om man uppdaterar en fält med flera texter
                                                 }else if([editedJsonOrig count] == 1 && [noteText isEqual:[editedJsonOrig objectAtIndex:0]]){
                                                 //NSLog(@"editedJsonOrig = notetext!");
                                                 //                                              [thisAutoNoteRow setObject:editedJson forKey:@"noteObj"];
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:[editedJson objectAtIndex:0] forKey:@"noteObj"];
                                                 [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                 
                                                 //                                                [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"notes"] forKey:@"noteObj"];
                                                 }else{
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:noteText forKey:@"noteObj"];
                                                 [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                 
                                                 //                                                [thisAutoNoteRow setObject:noteText forKey:@"noteObj"];
                                                 //NSLog(@"editedJsonOrig != notetext...");
                                                 }
                                                 
                                                 */
                                                
                                                //NSLog(@"########################## noteText efter: %@", noteText);
                                                
                                                
                                                
                                                
                                                
                                                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                
                                                //[tempDict setObject:[editedJson objectAtIndex:0] forKey:@"infoObj"];
                                                //[tempDict setObject:editedJson forKey:@"infoObj"];
                                                [tempDict setObject:[noteText copy] forKey:@"noteObj"];
                                                
                                                //NSLog(@"#1: %@\n\n#2: %@\n\n #3: %@\n\n", [editedJson objectAtIndex:0],editedJson,infoText);
                                                
                                                [thisAutoNoteRow setObject:updatedRoom forKey:@"roomObj"];
                                                [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                
                                                
                                                
                                                
                                                
                                                /*
                                                 //NSLog(@"\nnoteText: %@ \n\neditedJsonOrig: %@ \n\neditedJson: %@", noteText, editedJsonOrig, editedJson);
                                                 if([noteText isEqual:editedJsonOrig]){
                                                 //NSLog(@"editedJsonOrig = notetext!");
                                                 //                                              [thisAutoNoteRow setObject:editedJson forKey:@"noteObj"];
                                                 
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:editedJson forKey:@"noteObj"];
                                                 [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                 
                                                 //                                                [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"infos"] forKey:@"infoObj"];
                                                 
                                                 }else{
                                                 NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                                                 [tempDict setObject:noteText forKey:@"noteObj"];
                                                 [thisAutoNoteRow setObject:[tempDict valueForKey:@"noteObj"] forKey:@"noteObj"];
                                                 
                                                 //                                                [thisAutoNoteRow setObject:noteText forKey:@"noteObj"];
                                                 //NSLog(@"editedJsonOrig != notetext...");
                                                 }
                                                 //NSLog(@"########################## noteText efter: %@", noteText);
                                                 */
                                                
                                                
                                                //rum x av y (om man i tabellrenderingen vill veta vilken som är första och sista posten)
                                                [thisAutoNoteRow setObject:[NSNumber numberWithInt:l+1] forKey:@"noteNumber"];
                                                [thisAutoNoteRow setObject:[NSNumber numberWithInt:(int) [[updatedRoom valueForKey:@"notes"] count]] forKey:@"noteTotal"];
                                                
                                                //Lägg till i array
                                                //NSLog(@"thisAutoNoteRow: %@",thisAutoNoteRow);
                                                [tempArray2 addObject:thisAutoNoteRow];
                                                /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt notering: %d",[tempArray2 count]);
                                                
                                                l++;
                                            }
                                        }
                                        if([[updatedRoom valueForKey:@"infos"] count]<=0 && [[updatedRoom valueForKey:@"notes"] count]<=0){
                                            //Lägg till "Inget att notera"
                                            NSMutableDictionary *thisAutoRow = [NSMutableDictionary dictionary];
                                            [thisAutoRow setObject:[NSString stringWithFormat:@"autotextinganoteringar"] forKey:@"fieldtype"];
                                            [tempArray2 addObject:thisAutoRow];
                                            /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till text-rad: %d",[tempArray2 count]);
                                        }
                                        
                                        if([_editMode isEqualToNumber:[NSNumber numberWithInt:1]]){
                                            //Lägg till "Lägg till text"-rader
                                            NSMutableDictionary *thisAutoAddTextRow = [NSMutableDictionary dictionary];
                                            [thisAutoAddTextRow setObject:[NSString stringWithFormat:@"autotextaddinfo"] forKey:@"fieldtype"];
                                            
                                            if (updatedRoom){
                                                [thisAutoAddTextRow setObject:updatedRoom forKey:@"roomObj"];
                                            }
                                            
                                            [thisAutoAddTextRow setObject:[updatedRoom valueForKey:@"title"] forKey:@"roomName"];
                                            /*DOLD*///NSLog(@"[updatedRoom valueForKey:@title]: %@",[updatedRoom valueForKey:@"title"]);
                                            
                                            [tempArray2 addObject:thisAutoAddTextRow];
                                            
                                            NSMutableDictionary *thisAutoAddTextRow2 = [NSMutableDictionary dictionary];
                                            [thisAutoAddTextRow2 setObject:[NSString stringWithFormat:@"autotextaddnote"] forKey:@"fieldtype"];
                                            
                                            if (updatedRoom){
                                                [thisAutoAddTextRow2 setObject:updatedRoom forKey:@"roomObj"];
                                            }
                                            
                                            [thisAutoAddTextRow2 setObject:[updatedRoom valueForKey:@"title"] forKey:@"roomName"];
                                            /*DOLD*///NSLog(@"[updatedRoom valueForKey:@title]: %@",[updatedRoom valueForKey:@"title"]);
                                            
                                            [tempArray2 addObject:thisAutoAddTextRow2];
                                            /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till text-rad: %d",[tempArray2 count]);
                                        }
                                        j++;
                                        //[updatedRoom setObject:infos forKey:@"infos"];
                                        //[updatedRoom setObject:notes forKey:@"notes"];
                                        [updatedRoomArr addObject:updatedRoom];
                                    }
                                    
                                    [updatedZone setObject:updatedRoomArr forKey:@"room"];
                                    
                                    if([_editMode isEqualToNumber:[NSNumber numberWithInt:1]]){
                                        //Lägg till "Lägg till rum"-rad
                                        NSMutableDictionary *thisAutoAddRoomRow = [NSMutableDictionary dictionary];
                                        [thisAutoAddRoomRow setObject:[NSString stringWithFormat:@"autotextaddroom"] forKey:@"fieldtype"];
                                        [thisAutoAddRoomRow setObject:updatedZone forKey:@"zoneObj"];
                                        /*DOLD*///NSLog(@"############ ZONOBJ: ############ \n%@",updatedZone);
                                        [tempArray2 addObject:thisAutoAddRoomRow];
                                        /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till rum'-rad: %d",[tempArray2 count]);
                                    }
                                    i++;
                                    [updatedZoneArr addObject:updatedZone];
                                    
                                }
                                
                                if([_editMode isEqualToNumber:[NSNumber numberWithInt:1]]){
                                    //Lägg till "Lägg till zon"-rad
                                    NSMutableDictionary *thisAutoAddZoneRow = [NSMutableDictionary dictionary];
                                    [thisAutoAddZoneRow setObject:[NSString stringWithFormat:@"autotextaddzone"] forKey:@"fieldtype"];
                                    [thisAutoAddZoneRow setObject:updatedZoneArr forKey:@"zoneArrObj"];
                                    [tempArray2 addObject:thisAutoAddZoneRow];
                                    /*DOLD*///NSLog(@"TEMPARRAY2 COUNT tillagt 'Lägg till zon'-rad: %d",[tempArray2 count]);
                                }                            /*DOLD*///NSLog(@"TEMPARRAY2 COUNT vid slut: %d",[tempArray2 count]);
                                jsonArr = updatedZoneArr;
                                _appDelegate.completeJsonHolder = jsonArr;
                                /*DOLD3*///NSLog(@"uppdaterar appDelegate.completeJsonHolder");
                                NSLog(@"jsonArr vid slutet av tabellen: %@",jsonArr);
                                
                            }
                        }
                        //}  //if reportValues count > 0, slut
                    }else{
                        //Lägg inte till i tabellen om det är framside-material
                        //					NSLog(@"[dictionary valueForKey:@fieldlook]: %@",[dictionary valueForKey:@"fieldlook"]);
                        if([[dictionary valueForKey:@"fieldlook"] intValue] != 4 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 5 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 6 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 7 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 8 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 9 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 10 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 11 &&
                           [[dictionary valueForKey:@"fieldlook"] intValue] != 12 &&
                           ![[dictionary valueForKey:@"fieldtype"] isEqualToString:@"image"] ){
                            [tempArray2 addObject:dictionary];
                        }else{
                            NSLog(@"ignorerade fält");
                        }
                    }
                }

            }
			

			
            /*DOLD*///NSLog(@"#####################################################################");
            /*DOLD*///NSLog(@"####################### LOOPAT IGENOM ALLA FÄLT #####################");
            /*DOLD*///NSLog(@"#####################################################################");
            /*DOLD*///NSLog(@"");

            sqlite3_finalize(statement);
			begin_stmt = nil;
			beginSQL = nil;
            
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 4");
		}
        _reportFields = tempArray2;
        /*DOLD*///NSLog(@"reportfields count: %d", [_reportFields count]);
        
        
        
        
        
        
    }else{
		NSLog(@"Ingen _appDelegate.openDbConnection! 17");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
    
    //Uppdatera vyns titel (i navigation-bar) till Rapportmallsnamn #42 [ - Backup 1]
	
	NSString * navigationBkpTitleSuffix = @"";
	
	if(([_appDelegate.tableBackup isEqualToString:@"_bkp1"] ||
		[_appDelegate.tableBackup isEqualToString:@"_bkp2"] ||
		[_appDelegate.tableBackup isEqualToString:@"_bkp3"] ||
		[_appDelegate.tableBackup isEqualToString:@"_bkp4"] ||
		[_appDelegate.tableBackup isEqualToString:@"_bkp5"]) && _editMode != [NSNumber numberWithInt:1]){
		navigationBkpTitleSuffix = [NSString stringWithFormat:@" - IPAD-BACKUP %d", [[_appDelegate.tableBackup substringFromIndex:4] intValue]];
		self.navigationController.navigationBar.tintColor = [UIColor blueColor];
	}else{
		self.navigationController.navigationBar.tintColor = [UIColor grayColor];
	}
	
	
    self.navigationItem.title = [NSString stringWithFormat:@"%@ #%d%@",[[_templateInfo objectAtIndex:0] valueForKey:@"form_name"], [workOrder intValue], 	navigationBkpTitleSuffix];
    
    //Uppdatera tabell
    /*DOLD*///NSLog(@"##### RELOADDATA #####");
    [self.reportForm reloadData];

 
}

- (void) emptyVars{
	//töm json
	
	_appDelegate.completeJsonHolder = nil;
	_appDelegate.jsonOrigHolder = nil;
	_appDelegate.jsonHolder = nil;
	
	NSLog(@"Tömt json-delegates: appDelegate.completeJsonHolder = %@; appDelegate.jsonOrigHolder = %@; appDelegate.jsonHolder = %@", _appDelegate.completeJsonHolder,_appDelegate.jsonOrigHolder, _appDelegate.jsonHolder);
	
	//töm reportValuesDelegate
	_appDelegate.reportValuesDelegate = nil;
	NSLog(@"Tömt reportValuesDelegate i emptyVars: %@", _appDelegate.reportValuesDelegate);
}

//Denna funktion används inte just nu (130520)s
- (void) saveReportAndPop{
	NSLog(@"SaveReportAndPop");
	
	//resigna aktivt fält, så att fältets värde sparas i reportValuesDelegate
	[self.view endEditing:YES];
	
	[self saveReport];
	
	NSLog(@"Poppar navigationcontroller...");
    //[self.navigationController popViewControllerAnimated:YES];
	
	//Visa bekräftelse över att rapporten sparats
	NSString *alertString = [NSString stringWithFormat:@"Sparat"];
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"Rapporten har sparats (men inte laddats upp)." delegate:self cancelButtonTitle:@"Avfärda" otherButtonTitles:nil];
//	[alert show];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                message:@"Rapporten har sparats (men inte laddats upp)."
                                                                         preferredStyle:UIAlertControllerStyleAlert];
   //We add buttons to the alert controller by creating UIAlertActions:
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Avfärda"
                                                       style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                            [alertController dismissViewControllerAnimated:YES completion:nil];
                                                        }];
    
    [alertController addAction:actionCancel];
    
   [self presentViewController:alertController animated:YES completion:nil];
	
}


#pragma mark
- (void) saveReport{ //:(UIButton*)sender{
	
#pragma mark ##### Save Report
	/*
	 #####################################################################
	 #####################################################################
	 ############################ saveReport #############################
	 #####################################################################
	 #####################################################################
	 */

   /* VIKTIG! */NSLog(@"SAVEREPORT körs");
	
    /*DOLD*/NSLog(@"reportfields count: %lu",(unsigned long)[_reportFields count]);
    /*DOLD*///NSLog(@"reportfields: %@",_reportFields);
    
	
	
    NSMutableArray * insCols = [[NSMutableArray alloc] init];
    NSMutableArray * insVals = [[NSMutableArray alloc] init];
    NSString * sql = [[NSString alloc] init];
    
    [insCols addObject:@"workorder_id"];
    //[insCols addObject:[NSString stringWithFormat:@"%@",[[_reportFields objectAtIndex:i] valueForKey:@"fieldcolname"]]];
    
    [insVals addObject:[NSString stringWithFormat:@"\"%@\"",workOrder]];
    //[insVals addObject:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"\"%@\"",[[textFieldsInCell objectAtIndex:([textFieldsInCell count]-1)] valueForKey:@"text"]]]];

    /*
     #####################################################################
     ############################   START LOOP   #########################
     #####################################################################
     */

    /*DOLD3*///NSLog(@"Loopar igenom _desc-tabellens poster - arrayen _reportFields");
  
    if(/* DISABLES CODE */ (false)){
	for (NSMutableDictionary * thisFieldDict in _reportFields) {
        
//        NSLog(@"%@");
        
        //thisfielddict innehåller all information om en rad i desc-tabellen
        /*DOLD*///NSLog(@"Loopar igenom _desc-tabellens poster - arrayen _reportFields");
#pragma mark inputShortText
        /*
         #####################################################################
         ################ inputShortText/addedDate/updatedDate ###############
         #####################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"inputShortText"] || [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"addedDate"] || [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"updatedDate"]) {
            /*DOLD3*///NSLog(@"Hittade inputShortText/addedDate/updatedDate: %@", [thisFieldDict valueForKey:@"fieldtype"]);
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            //Kolla om inputfältet har renderats någon gång och därmed fått ett tag-nr.
            //Gör det genom att se om fieldcolname finns med i arrayen _inputFieldTagCounterArr
            if([_inputFieldTagCounterArr count]>0){
               /*DOLD*///NSLog(@"_inputFieldTagCounterArr count: %d",[_inputFieldTagCounterArr count]);
                if([_inputFieldTagCounterArr containsObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]])
                {
                   /*DOLD*///NSLog(@"_inputFieldTagCounterArr index för denna tag: %d",[_inputFieldTagCounterArr indexOfObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]]);
                    
                    //Hämta fältets tag-nummer från dictionaryt _inputFieldTagDict
                    /*DOLD*///NSLog(@"Hämtar tag-nr. _inputFieldTagDict: %@ \ncolname: %@\nTag-nr för denna borde bli %@.\n", _inputFieldTagDict,[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString], [_inputFieldTagDict valueForKey:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]]);
                    
                    
                    NSString * thisFieldTagNr = [_inputFieldTagDict valueForKey:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]];
                    
                    //Hämta fältets värde
                    /*DOLD*///NSLog(@"Hämtar fältets värde för input nr %@", thisFieldTagNr);
                    
                    
                    UITextField *thisTextField = (UITextField*)[self.view viewWithTag:[thisFieldTagNr intValue]];
                    NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[thisTextField.text stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];
                    /*DOLD3*///NSLog(@"Field %d has value: %@", [thisFieldTagNr intValue], fieldValue);
                    
					if (!fieldValue || [fieldValue isEqualToString:@"(null)"] || [fieldValue isEqualToString:@"\"(null)\""]) {
						//NSLog(@"Fieldvalue är (null), sätt till \"\" istället! 1");
						fieldValue = @"\"\"";
					}
                    //Lägg till databasens kolumn-namn för fältet
                    [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                    //Lägg till textfältets värde i insVals
                    [insVals addObject:fieldValue];
                }else{
                   /*DOLD*///NSLog(@"Denna input hittades inte i _inputFieldTagCounterArr");
                }
            }
        }
#pragma mark inputLongText
        /*
         #####################################################################
         ############################ inputLongText ##########################
         #####################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"inputLongText"]) {
            /*DOLD3*///NSLog(@"Hittade inputLongText");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            //Kolla om inputfältet har renderats någon gång och därmed fått ett tag-nr.
            //Gör det genom att se om fieldcolname finns med i arrayen _inputFieldTagCounterArr
            if([_inputFieldTagCounterArr count]>0){
                //NSLog(@"_inputFieldTagCounterArr count: %d",[_inputFieldTagCounterArr count]);
                if([_inputFieldTagCounterArr containsObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]])
                {
                    //NSLog(@"_inputFieldTagCounterArr index för denna tag: %d",[_inputFieldTagCounterArr indexOfObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]]);
                    
                    
                    //Hämta fältets tag-nummer från dictionaryt _inputFieldTagDict
                    /*DOLD*///NSLog(@"Hämtar tag-nr. _inputFieldTagDict: %@",_inputFieldTagDict);
                    NSString * thisFieldTagNr = [_inputFieldTagDict valueForKey:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]];
                    
                    //Hämta fältets värde
                    /*DOLD*///NSLog(@"Hämtar fältets värde för input nr %d", [thisFieldTagNr intValue]);
                    UITextField *thisTextField = (UITextField*)[self.view viewWithTag:[thisFieldTagNr intValue]];
                    NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[thisTextField.text stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];
                    /*DOLD3*///NSLog(@"Field %d has value: %@", [thisFieldTagNr intValue], fieldValue);
                    
                    //Lägg till databasens kolumn-namn för fältet
                    [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                    //Lägg till textfältets värde i insVals
                    [insVals addObject:fieldValue];
                }
            }
        }
#pragma mark signatureSimple
        /*
         #######################################################################
         ############################ signatureSimple ##########################
         #######################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"signatureSimple"]) {
            /*DOLD3*///NSLog(@"Hittade signatureSimple");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
			//NSLog(@"appdelegate-objekt blob count: %u", [_appDelegate.signatureImageData length]);
			
            if([_appDelegate.signatureImagesDict objectForKey:[thisFieldDict valueForKey:@"fieldcolname"]]){
				//Spara som bild och sätt in filnamnet som value
				
				
				CFUUIDRef uuidRef = CFUUIDCreate(NULL);
				CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
				CFRelease(uuidRef);
				//NSLog(@"UUID: %@",uuidStringRef);
				NSString * fileName = [NSString stringWithFormat:@"%@.jpg",(__bridge NSString *)(uuidStringRef)];
				
				NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
				
				//[_appDelegate.signatureImageData writeToFile:jpgPath atomically:YES];
                [[_appDelegate.signatureImagesDict objectForKey:[thisFieldDict valueForKey:@"fieldcolname"]] writeToFile:jpgPath atomically:YES];
                
				// Let's check to see if files were successfully written...
				
				// Create file manager
				NSError *error;
				NSFileManager *fileMgr = [NSFileManager defaultManager];
                NSLog(@"File Manager = %@", fileMgr);
                
				// Point to Document directory
				NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
                NSLog(@"Document Directory = %@", documentsDirectory);
				// Write out the contents of home directory to console
				//NSLog(@"Documents directory: %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
								
                NSLog(@"Error = %@", error.localizedDescription);
				//NSData * imageData = appDelegate.signatureImageData;

				//Lägg till databasens kolumn-namn för fältet
				[insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
				//Lägg till bildfilens namn för signaturen i insVals
				[insVals addObject:[NSString stringWithFormat:@"\"%@\"",fileName]];
//				[insVals addObject:[NSString stringWithFormat:@"?"]];

				/*VIKTIG*/NSLog(@"Sparade signaturen i documents som %@",fileName);
				//[insVals addObject:[NSString stringWithFormat:@"?",[thisFieldDict valueForKey:@"fieldcolname"]]];
			}
        }
        
#pragma mark attachImages
        /*
         #######################################################################
         ############################ attachImages ##########################
         #######################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"attachImages"]) {
            if ([_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]) {
                //Lägg till databasens kolumn-namn för fältet
                [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                
                //Lägg till bildfilens namn för signaturen i insVals
                [insVals addObject:[NSString stringWithFormat:@"\"%@\"", [_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]]];
                
                /*VIKTIG*/NSLog(@"Sparade attachImages-strängen: %@", [_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]);
            }
        }
        
        
#pragma mark simpleAutotext
        /*
         #####################################################################
         ############################ simpleAutotext ##########################
         #####################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"simpleAutotext"]) {
            /*DOLD*///NSLog(@"Hittade simpleAutotext");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            //Kolla om inputfältet har renderats någon gång och därmed fått ett tag-nr.
            //Gör det genom att se om fieldcolname finns med i arrayen _inputFieldTagCounterArr
            if([_inputFieldTagCounterArr count]>0){
                //NSLog(@"_inputFieldTagCounterArr count: %d",[_inputFieldTagCounterArr count]);
                if([_inputFieldTagCounterArr containsObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]])
                {
                    //NSLog(@"_inputFieldTagCounterArr index för denna tag: %d",[_inputFieldTagCounterArr indexOfObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]]);
                    
                    
                    //Hämta fältets tag-nummer från dictionaryt _inputFieldTagDict
                    /*DOLD*///NSLog(@"Hämtar tag-nr. _inputFieldTagDict: %@",_inputFieldTagDict);
                    NSString * thisFieldTagNr = [_inputFieldTagDict valueForKey:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]];
                    
                    //Hämta fältets värde
                    /*DOLD*///NSLog(@"Hämtar fältets värde för input nr %d", [thisFieldTagNr intValue]);
                    UITextField *thisTextField = (UITextField*)[self.view viewWithTag:[thisFieldTagNr intValue]];
                    NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[thisTextField.text stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];
                    /*DOLD3*///NSLog(@"Field %d has value: %@", [thisFieldTagNr intValue], fieldValue);
                    
                    //Lägg till databasens kolumn-namn för fältet
                    [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                    //Lägg till textfältets värde i insVals
                    [insVals addObject:fieldValue];
                }
            }
        }
#pragma mark 	Autotext
        /*
         #####################################################################
         ############################ Autotext ###############################
         #####################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"autotext"]) {
            /*DOLD3*///NSLog(@"Hittade Autotext");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            
			//Hämta fältets värde
			if(_appDelegate.completeJsonHolder){
				
				
				// appDelegate.completeJsonHolder
				
				NSString * jsonObject = [[NSString alloc] initWithData:
										 [NSJSONSerialization dataWithJSONObject:_appDelegate.completeJsonHolder options:kNilOptions error:nil]
															  encoding:NSUTF8StringEncoding];
				
				//stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]; //kniloptions, NSJSONWritingPrettyPrinted
				
				//NSLog(@"\n\n\n\n\n\njsonObject: \n\n\n%@\n\n\n\n\n\n",jsonObject);
				
				NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[[jsonObject stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]  stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]];
				
				//Lägg till databasens kolumn-namn för fältet
				[insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
				//Lägg till textfältets värde i insVals
				[insVals addObject:fieldValue];
			}
		}
        
#pragma mark  chkBox
        /*
         #####################################################################
         ############################### chkBox ##############################
         #####################################################################
         */
        
        /* DET GÅR INTE ATT SÄTTA TAG PÅ UISWITCHEN - VET EJ VARFÖR */
        /* http://stackoverflow.com/questions/9783541/save-and-load-uiswitch-state-in-tableview */
        
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"chkBox"]) {
            /*DOLD3*///NSLog(@"Hittade chkBox");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            //Kolla om inputfältet har renderats någon gång och därmed fått ett tag-nr.
            //Gör det genom att se om fieldcolname finns med i arrayen _inputFieldTagCounterArr
            if([_inputFieldTagCounterArr count]>0){
                //NSLog(@"_inputFieldTagCounterArr count: %d",[_inputFieldTagCounterArr count]);
                if([_inputFieldTagCounterArr containsObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]])
                {
                    //NSLog(@"_inputFieldTagCounterArr index för denna tag: %d",[_inputFieldTagCounterArr indexOfObject:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]]);
                    
                    
                    //Hämta fältets tag-nummer från dictionaryt _inputFieldTagDict
                    /*DOLD*///NSLog(@"Hämtar tag-nr. _inputFieldTagDict: %@",_inputFieldTagDict);
                    NSString * thisFieldTagNr = [_inputFieldTagDict valueForKey:[[thisFieldDict valueForKey:@"fieldcolname"] lowercaseString]];
                    
                    //Hämta fältets värde
                    /*DOLD3*///NSLog(@"Hämtar fältets värde för input nr %d", [thisFieldTagNr intValue]);
                    UISwitch *thisSwitch = (UISwitch*)[self.view viewWithTag:[thisFieldTagNr intValue]];
                    NSString *fieldValue;// = [[NSString alloc]init];
                    
                   /*DOLD*///NSLog(@"Switch obj: %@",thisSwitch);
                    
                    if(thisSwitch.on){
                       /*DOLD*///NSLog(@"ON");
                        fieldValue = @"1";
                    }else{
                       /*DOLD*///NSLog(@"OFF");
                        fieldValue = @"0";
                    }
                    /*DOLD3*///NSLog(@"Field %d has value: %@", [thisFieldTagNr intValue], fieldValue);
                    
                    //Lägg till databasens kolumn-namn för fältet
                    [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                    //Lägg till textfältets värde i insVals
                    [insVals addObject:fieldValue];
                }
            }
        }
    }
	}
	
	/*
     #####################################################################
     ############################   SLUT  LOOP   #########################
     #####################################################################
     */
	
	
	
	
	
	for (NSMutableDictionary * thisFieldDict in _reportFields) {
#pragma mark Start loop
        /*
         #####################################################################
         ############################ Autotext ###############################
         #####################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"autotext"]) {
            /*DOLD3*///NSLog(@"Hittade Autotext");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            
			//Hämta fältets värde
			if(_appDelegate.completeJsonHolder){
				
				
				// appDelegate.completeJsonHolder
				
				NSString * jsonObject = [[NSString alloc] initWithData:
										 [NSJSONSerialization dataWithJSONObject:_appDelegate.completeJsonHolder options:kNilOptions error:nil]
															  encoding:NSUTF8StringEncoding];
				
				//stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]; //kniloptions, NSJSONWritingPrettyPrinted
				
				//NSLog(@"\n\n\n\n\n\njsonObject: \n\n\n%@\n\n\n\n\n\n",jsonObject);
				
				NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[[jsonObject stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]  stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]];
				
				//Lägg till databasens kolumn-namn för fältet
				[insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
				//Lägg till textfältets värde i insVals
				[insVals addObject:fieldValue];
			}
		}
#pragma mark signatureSimple
        /*
         #######################################################################
         ############################ signatureSimple ##########################
         #######################################################################
         */
        else if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"signatureSimple"]) {
            /*DOLD3*///NSLog(@"Hittade signatureSimple");
            /*DOLD*///NSLog(@"Dictionary: %@", thisFieldDict);
            
            NSLog(@"appdelegate-objekt blob count: %lu", (unsigned long)[_appDelegate.signatureImageData length]);
			
            if([_appDelegate.signatureImagesDict objectForKey:[thisFieldDict valueForKey:@"fieldcolname"]]){
				//Spara som bild och sätt in filnamnet som value
				
				
				CFUUIDRef uuidRef = CFUUIDCreate(NULL);
				CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
				CFRelease(uuidRef);
				//NSLog(@"UUID: %@",uuidStringRef);
				NSString * fileName = [NSString stringWithFormat:@"%@.jpg",(__bridge NSString *)(uuidStringRef)];
				
				NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
				
                //[_appDelegate.signatureImageData writeToFile:jpgPath atomically:YES];
                [[_appDelegate.signatureImagesDict objectForKey:[thisFieldDict valueForKey:@"fieldcolname"]] writeToFile:jpgPath atomically:YES];
				
				// Let's check to see if files were successfully written...
				
				// Create file manager
				NSError *error;
				NSFileManager *fileMgr = [NSFileManager defaultManager];
				
				// Point to Document directory
				NSString *documentsDirectory = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
				
				// Write out the contents of home directory to console
				NSLog(@"Documents directory (sparade signatur): %@", [fileMgr contentsOfDirectoryAtPath:documentsDirectory error:&error]);
				
				
				
				
				//NSData * imageData = appDelegate.signatureImageData;
				
				//Lägg till databasens kolumn-namn för fältet
				[insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
				//Lägg till bildfilens namn för signaturen i insVals
				[insVals addObject:[NSString stringWithFormat:@"\"%@\"",fileName]];
				//				[insVals addObject:[NSString stringWithFormat:@"?"]];
				
				/*VIKTIG*/NSLog(@"Sparade signaturen i documents som %@",fileName);
				//[insVals addObject:[NSString stringWithFormat:@"?",[thisFieldDict valueForKey:@"fieldcolname"]]];
			}
        }
		else if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"inputShortText"] ||
				 [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"addedDate"] ||
				 [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"updatedDate"] ||
				 [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"inputLongText"] ||
				 [[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"simpleAutotext"]) {
			/*DOLD3*///NSLog(@"Hittade inputShortText/addedDate/updatedDate: %@", [thisFieldDict valueForKey:@"fieldtype"]);

			
			NSLog(@"thisFieldDict: %@",thisFieldDict);
			
			NSLog(@"ReportValuesDelegate: %@",_appDelegate.reportValuesDelegate);
			
			if ([_appDelegate.reportValuesDelegate count]>=1){
			
				NSString *fieldRawValue = [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[thisFieldDict valueForKey:@"fieldcolname"]];
				NSString *fieldValue = [NSString stringWithFormat:@"\"%@\"",[fieldRawValue stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""]];
				
				if (!fieldValue || [fieldValue isEqualToString:@"(null)"] || [fieldValue isEqualToString:@"\"(null)\""]) {
					//NSLog(@"Fieldvalue är (null), sätt till \"\" istället! 2");
					fieldValue = @"\"\"";
				}
				
				//NSLog(@"sätter col");
				[insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
				//NSLog(@"sätter val");
				[insVals addObject:fieldValue];
			}else{
				NSLog(@"Fältet %@ var tomt, _appDelegate.reportValuesDelegate count var 0. Hoppar över värdet.", [thisFieldDict valueForKey:@"fieldcolname"]);
			}
		}else{
			//NSLog(@"Sparade inte fältet eftersom hanterare saknas i spara-funktionen. Objekt för fältet (thisFieldDict): %@",thisFieldDict);
		}
#pragma mark attachImages
        /*
         #######################################################################
         ############################ attachImages ##########################
         #######################################################################
         */
        if ([[thisFieldDict valueForKey:@"fieldtype"] isEqualToString:@"attachImages"]) {
            
            if ([_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]) {
                //Lägg till databasens kolumn-namn för fältet
                [insCols addObject:[thisFieldDict valueForKey:@"fieldcolname"]];
                //Lägg till bildfilens namn för signaturen i insVals
                [insVals addObject:[NSString stringWithFormat:@"\"%@\"", [_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]]];
                
                /*VIKTIG*/NSLog(@"Sparade attachImages-strängen: %@", [_appDelegate.attachImagesDict valueForKey: [thisFieldDict valueForKey:@"fieldcolname"]]);
            }
        }
        
	}
	
	//NSLog(@"insCols: %@",insCols);
	//NSLog(@"insVals: %@",insVals);
	
	
//	insCols = [[[appDelegate.reportValuesDelegate objectAtIndex:0] allKeys] mutableCopy];
//	insVals = [[[appDelegate.reportValuesDelegate objectAtIndex:0] allValues] mutableCopy];

    
    
#pragma mark Generera SQL
	
	if([_reportInitialValues count]<=0){
		//rapport finns inte, gör insert
			
			//Ta bort alla poster som är "" ur insert-arrayen (både col och val)
			
			for (int i = 0; i<[insVals count]; i++) {
				NSString * thisValue = [insVals objectAtIndex:i];
				if ([[thisValue lowercaseString] isEqualToString:@"\"\""] ||
					[[thisValue lowercaseString] isEqualToString:@""] ||
					[[thisValue lowercaseString] isEqualToString:@"nil"] ||
					[[thisValue lowercaseString] isEqualToString:@"null"] ||
					[[thisValue lowercaseString] isEqualToString:@"(null)"]){
					//NSLog(@"Sträng var null. Plockar bort värdet för %@ i insVals och insCols.",[insCols objectAtIndex:i]);
					
					[insCols removeObjectAtIndex:i];
					[insVals removeObjectAtIndex:i];
				}
			}

		//NSLog(@"insCols efter rensning av nil: %@",insCols);
		//NSLog(@"insVals efter rensning av nil: %@",insVals);
	
		
            sql = [NSString stringWithFormat:@"INSERT INTO %@ (%@) VALUES (%@)",
                   [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"],
                   [insCols componentsJoinedByString:@","],
                   [insVals componentsJoinedByString:@","]];
            /*DOLD3*/NSLog(@"%@",sql);
        }else{
            //Rapport finns, gör update
            NSMutableString * sqlSetString = [[NSMutableString alloc] init];
            

            for (int i=0; i< [insCols count]; i++) {
                if([insVals objectAtIndex:i] != nil){
					
					NSString * thisEscInsval = [[NSString alloc] init];
					if ([[insVals objectAtIndex:i] isEqualToString:@"(null)"] || [[insVals objectAtIndex:i] isEqualToString:@"\"(null)\""]|| [[insVals objectAtIndex:i] isEqualToString:@"nil"]) {
						NSLog(@"Fieldvalue är (null), sätt till \"\" istället! nere i spara");
						thisEscInsval = @"\"\"";
					}else{
						thisEscInsval = [insVals objectAtIndex:i];
					}
			
					
                    [sqlSetString appendFormat:@"%@=%@",
                     [insCols objectAtIndex:i],
                     thisEscInsval];
                    if (i<([insCols count]-1)) {
                        if([insVals objectAtIndex:i+1] != nil){
                            [sqlSetString appendFormat:@", "];
                        }
                    }
                }else{
					
				}
            }
			
           
            sql = [NSString stringWithFormat:@"UPDATE %@ SET %@ WHERE workorder_id = %@ OR (workorder_id IS NULL AND id = %@)",
                   [[_templateInfo objectAtIndex:0] valueForKey:@"table_name"],
                   sqlSetString,
                   workOrder,
                   workOrder];
            /*DOLD3*/NSLog(@"SQL: %@",sql);
            
        }
#pragma mark Skriv till DB
    
        NSString *docsDir;
        NSArray *dirPaths;
        
        // Get the documents directory
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
		if([dirPaths count]>0){
			docsDir = [dirPaths objectAtIndex:0];
		}
    
        sqlite3_stmt *statement;
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Kör kommando: %@", sql);
            NSString *beginSQL = sql;
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
			
			/* BLOB - UTKOMMENTERAT
			NSLog(@"appdelegate-objekt blob count: %u", [appDelegate.signatureImageData length]);
			if(appDelegate.signatureImageData){
				
				const unsigned char *zBlob = [appDelegate.signatureImageData bytes];    // Pointer to blob of data 
				int nBlob = [appDelegate.signatureImageData length];					// Length of data pointed to by zBlob
				
				NSLog(@"binder blob...");
				NSLog(@"zBlob: %s", zBlob);
				NSLog(@"nBlob: %d",nBlob);
				
				sqlite3_bind_blob(statement, 1, zBlob, nBlob, SQLITE_STATIC);
            }*/
			
			if (sqlite3_step(statement) == SQLITE_DONE)
            {
               /*DOLD3*/NSLog(@"##### Sparat rapport. Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
            } else {
                /*DOLD3*/NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
            }
            
            /*DOLD*/////NSLog(@"%d", sqlite3_finalize(statement));
            
            beginSQL = nil;
            begin_stmt = nil;
		    sqlite3_finalize(statement);
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 5");
		}
    
        
        
        //Lägg till en post i en referenstabell (skapa den om den inte finns) som pekar på vilken post som just uppdaterats
        sql = [NSString stringWithFormat: //@"drop table if exists sys_poststosend"];
               @"CREATE TABLE IF NOT EXISTS sys_poststosend (id integer primary key autoincrement,reporttemplate text,workorderid text)"];
		
		//MED TIMESTAMP:
		//@"CREATE TABLE IF NOT EXISTS sys_poststosend (id integer primary key autoincrement,reporttemplate text,workorderid text,updated datetime default (datetime('now', 'localtime')))"];

	
		/*sql = [NSString stringWithFormat:
               @"DROP TABLE IF EXISTS sys_poststosend"];
        */
         /*,
               templateId,
               reportid];
                */

        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Kör kommando: %@", sql);
            NSString *beginSQL = sql;
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
               /*DOLD*///NSLog(@"Sparat rapport i referenstabell (sys_poststosend).");
                /*DOLD*///NSLog(@"##### Sparat rapport i referenstabell (sys_poststosend). Kod: %s; Kommando utfört: %@",sqlite3_errmsg(nil), beginSQL);
            } else {
                /*DOLD*///NSLog(@"#####%s;   Kommando: %@; Felkod: %d", sqlite3_errmsg(nil), beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
            }
            
            /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement););
            
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 6");
		}
    
        
    NSString *correctedReportTemplateNumber = [NSString stringWithFormat:@"%d", [_templateid intValue]];
    if ([correctedReportTemplateNumber isEqualToString:@"0"]) {
        correctedReportTemplateNumber = @"135";
    }
    
        sql = [NSString stringWithFormat:
               @"INSERT INTO sys_poststosend (reporttemplate, workorderid) VALUES (%@, %d);", correctedReportTemplateNumber, [workOrder intValue]];
        
        if (_appDelegate.openDbConnection)
        {
            /*DOLD*/NSLog(@"Kör kommando: %@", sql);
            NSString *beginSQL = sql;
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                /*DOLD3*///NSLog(@"##### Sparat rapport i referenstabell (sys_poststosend). Kommando utfört: %@",/*sqlite3_errmsg(nil),*/ beginSQL);
            } else {
                /*DOLD3*///NSLog(@"##### Misslyckades att spara rapport i referenstabell  Kommando: %@; Felkod: %d", /*sqlite3_errmsg(nil),*/ beginSQL, sqlite3_errcode(_appDelegate.openDbConnection));
            }
            
            /*DOLD*///NSLog(@"%d", sqlite3_finalize(statement));
            
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 1");
		}
    
        
        
        
        
	
#pragma mark Uppdatera PostsToSend
        
        
        NSMutableArray * poststosendArr = [[NSMutableArray alloc] init];

        if (_appDelegate.openDbConnection)
        {
            /*DOLD*///NSLog(@"Databasen öppnad för getCustomer, för att hämta customer-data baserat på kund-id - edit-vyn");
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
            /*DOLD*///NSLog(@"SQL: %@", beginSQL);
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            /*DOLD*///NSLog(@"Listar prot_tabell");
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                int i = 0;
                for (i=0; i<colCount;i++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, i);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, i);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[dictionary setObject:[myTempMutStr stringByDecodingHTMLEntities] forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                }
                [poststosendArr addObject:dictionary];
            }
            beginSQL = nil;
            begin_stmt = nil;
                sqlite3_finalize(statement);

                
        }else{
			NSLog(@"Ingen _appDelegate.openDbConnection! 7");
		}
    
        /*DOLD*///NSLog(@"%@",poststosendArr);
        
        
        
        
        
        
    //UTKOMMENTERAT eftersom spara-funktionen körs precis innan segue - i så fall töms alla json-holders och inga ändringar görs nånstans
    
        //töm json
    
	
        //AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        //appDelegate.completeJsonHolder = nil;
        //appDelegate.jsonOrigHolder = nil;
        //appDelegate.jsonHolder = nil;

    
        
        
    //}// Counter Loop
	
	/*if([sender isEqual:self.navigationItem.rightBarButtonItem]){
		[self.navigationController popViewControllerAnimated:YES];
	}*/
}

#pragma mark


#pragma mark Segues
/*      
 #####################################################################
 #####################################################################
 ######################### PREPARE FOR SEGUE #########################
 #####################################################################
 #####################################################################
 */

- (void)setValuesForSegue{
    
}

- (void) buttonPressed: (id) sender withEvent: (UIEvent *) event
{

    /*DOLD*///NSLog(@"######################################################");
    /*DOLD*///NSLog(@"################    BUTTONPRESSED    #################");
    /*DOLD*///NSLog(@"######################################################");

    UITouch * touch = [[event allTouches] anyObject];
    CGPoint location = [touch locationInView: _reportForm];
    CGPoint location2 = CGPointFromString([NSString stringWithFormat:@"{100, %f}",location.y]);
    
    /*DOLD*///NSLog(@"touchpoint: %@",NSStringFromCGPoint(location2));
    NSIndexPath * indexPath = [_reportForm indexPathForRowAtPoint: location2];
    
    /*DOLD*///NSLog(@"indexrad: %d", indexPath.row);
    
    _indexForClickedButton = indexPath;
    /*DOLD*///NSLog(@"sparad indexrad: %d", _indexForClickedButton.row);
    /* indexPath contains the index of the row containing the button */
    /* do whatever it is you need to do with the row data now */
}

//Endast tillgängligt fr.o.m. IOS6
/*
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
	NSLog(@"shouldPerformSegueWithIdentifier");
	
	if ([identifier isEqualToString:@"ATsortZone"]) {
		NSLog(@"shouldPerformSegueWithIdentifier för ATsortZone");
		if([[appDelegate.completeJsonHolder valueForKey:@"zoneObj"] count]>0){
			NSLog(@"zoneObj count > 1, kör");
			return YES;
		}else{
			NSLog(@"zoneObj count < 1, avbryt");
			return NO;
		}
    }else
	if ([identifier isEqualToString:@"ATsortRoom"]) {
		NSLog(@"shouldPerformSegueWithIdentifier för ATsortRoom");
		if([[appDelegate.completeJsonHolder valueForKey:@"roomObj"] count]>0){
			NSLog(@"roomObj count > 1, kör");
			return YES;
		}else{
			NSLog(@"roomObj count < 1, avbryt");
			return NO;
		}
    }else{
		NSLog(@"identifier är inte någon sorterings-sida, kör segue");
		return YES;
	}
}
 */

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.reportForm indexPathForSelectedRow];
    
	if(_editMode == [NSNumber numberWithInt:1]){
		//Spara innan ny vy laddas
		//NSLog(@"EDITMODE AKTIVT - FÖRSÖKER SPARA INNAN SEGUE");
		//[self performSelector:@selector(saveReport)];
		
		
		NSLog(@"Markera och avmarkera det fält som är på raden som valts för att behålla värdet en enkel autotext");
		
		if([_reportFields count]>=indexPath.row){
			//få ut colname för raden
			NSString * colname = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"];
			if(colname){
				NSLog(@"Colname: %@", colname);
				//hämta tag för colname
				NSString * tag = [_inputFieldTagDict valueForKey:colname];
				NSLog(@"tag: %@", tag);
				if(tag){
					//sätt fältet som firstresponder
					NSLog(@"Markerar fält %@",tag);
					[[self.view viewWithTag:[tag intValue]] becomeFirstResponder];
					[[self.view viewWithTag:[tag intValue]] resignFirstResponder];
				}
			}
		}
		//resigna aktivt fält, så att fältets värde sparas i reportValuesDelegate
		NSLog(@"Avmarkerar alla fält");
		[self.view endEditing:YES];
	}

    /*DOLD*///NSLog(@"######################################################");
    /*DOLD*///NSLog(@"######################################################");
    /*DOLD*///NSLog(@"################ PREPFORSEGUE: RAD %d #################", _indexForClickedButton.row);
    /*DOLD*///NSLog(@"######################################################");
    /*DOLD*///NSLog(@"######################################################");
    
    if ([segue.identifier isEqualToString:@"editCurrentReport"]) {
        //Känn av om knappen är spara eller redigera
        if([_editMode isEqualToNumber:[NSNumber numberWithInt:1]]){
            ReportDetailsViewController *destViewController = segue.destinationViewController;
            destViewController.workOrder = workOrder;
            destViewController.templateid = [NSNumber numberWithInt:135];
            destViewController.editMode = [NSNumber numberWithInt:0];
            /*DOLD*///NSLog(@"PrepareForSegue. Workorder: %d, template: %@, editmode: %@",[workOrder intValue], [NSNumber numberWithInt:135], [NSNumber numberWithInt:0]);
        }else{
			
			//Rensa signatur- och json-data innan man går in i edit-mode
            
            if(_appDelegate.signatureImageData ){
                _appDelegate.signatureImageData = nil;  //[[NSData alloc] init];
            }
            if(_appDelegate.signatureImagesDict != nil ){
                _appDelegate.signatureImagesDict = nil;  //[[NSData alloc] init];
            }
            
			//töm json
			_appDelegate.completeJsonHolder = nil;
			_appDelegate.jsonOrigHolder = nil;
			_appDelegate.jsonHolder = nil;

			
            ReportDetailsViewController *destViewController = segue.destinationViewController;
            destViewController.workOrder = workOrder;
            destViewController.templateid = _templateid;
            destViewController.editMode = [NSNumber numberWithInt:1];
            /*DOLD*///NSLog(@"PrepareForSegue. Workorder: %d, template: %d, editmode: %@",[workOrder intValue], [_templateid intValue], [NSNumber numberWithInt:1]);
			
			//töm reportValuesDelegate
			_appDelegate.reportValuesDelegate = nil;
			NSLog(@"Tömt reportValuesDelegate i prepareForSegue 1: %@", _appDelegate.reportValuesDelegate);
        }
    }
    if ([segue.identifier isEqualToString:@"viewRelatedReport"]) {
        ReportDetailsViewController *destViewController = segue.destinationViewController;
        destViewController.workOrder = workOrder;
        destViewController.templateid = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"incRepId"];
        destViewController.editMode = [NSNumber numberWithInt:0];
        /*DOLD*///NSLog(@"PrepareForSegue. Workorder: %d, template: %@, editmode: %@",[workOrder intValue], [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"incRepId"], [NSNumber numberWithInt:0]);
		
		//töm reportValuesDelegate
		
		_appDelegate.reportValuesDelegate = nil;
		NSLog(@"Tömt reportValuesDelegate i prepareForSegue 2: %@", _appDelegate.reportValuesDelegate);
		
    }
    
    if ([segue.identifier isEqualToString:@"ATsortZone"]) {
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.workOrder = workOrder;
        destViewController.editMode = @"sortZones";
		
		
		
		

		
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"ATsortZone at-objekt som skickas: %@",_appDelegate.completeJsonHolder);
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		
		if(_appDelegate.completeJsonHolder){
			destViewController.autotextObject = _appDelegate.completeJsonHolder;
		}else{

		}

//        destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"atObj"];
        /*DOLD*///NSLog(@"Skickat objekt till destviewer: %@",[_reportFields objectAtIndex:indexPath.row]);
        destViewController.autotextParentID = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.autotextParentType = @"Table";
    }
    if ([segue.identifier isEqualToString:@"ATsortRoom"]) {
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.workOrder = workOrder;
        destViewController.editMode = @"sortRooms";
		destViewController.autotextParentObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"];
        destViewController.autotextObject = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"] valueForKey:@"room"];
        /*DOLD*///NSLog(@"Skickat objekt till destviewer: %@",[_reportFields objectAtIndex:indexPath.row]);
        destViewController.autotextParentID = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.autotextParentType = @"Zone";
    }
    if ([segue.identifier isEqualToString:@"ATsortInfos"]) {
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.workOrder = workOrder;
        destViewController.editMode = @"sortInfos";
		destViewController.autotextParentObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"];
		destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"];
		
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"atsortinfos at-objekt som skickas (roomobj): %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"]);
		NSLog(@"atsortinfos at-objekt som skickas: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"]);
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		
        /*DOLD*///NSLog(@"Skickat objekt till destviewer: %@",[_reportFields objectAtIndex:indexPath.row]);
        destViewController.autotextParentID = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"id"] valueForKey:@"infos"];
        //destViewController.autotextParentID = [[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"id"] valueForKey:@"infos"];
        destViewController.autotextParentType = @"Room";
    }
    if ([segue.identifier isEqualToString:@"ATsortNotes"]) {
        AutotextSortViewController *destViewController = segue.destinationViewController;
        destViewController.workOrder = workOrder;
        destViewController.editMode = @"sortNotes";
		destViewController.autotextParentObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"];
		destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"];
		
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"atsortnotes at-objekt som skickas (roomobj): %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"]);
		NSLog(@"atsortnotes at-objekt som skickas: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"]);
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		NSLog(@"#####################################");
		
        /*DOLD*///NSLog(@"Skickat objekt till destviewer: %@",[_reportFields objectAtIndex:indexPath.row]);
        destViewController.autotextParentID = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"id"];
        destViewController.autotextParentType = @"Room";

        /*
        NSMutableDictionary * partToEdit = [[NSMutableDictionary alloc] init];
        
        [partToEdit setValue: forKey:@""];
        
        [_reportFields indexOfObject:]
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.jsonPartToEdit = partToEdit;
         */
    }


    if ([segue.identifier isEqualToString:@"ATaddZone"]) {
        AutotextAddTextSelectorViewController *destViewController = segue.destinationViewController;
		
		NSLog(@"autotextobjekt från RepDetVC: %@",[_reportFields objectAtIndex:indexPath.row]);
		
        destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneArrObj"];
        destViewController.editMode = @"addZone";

    }
    if ([segue.identifier isEqualToString:@"ATaddRoom"]) {
        AutotextAddTextSelectorViewController *destViewController = segue.destinationViewController;
//        destViewController.autotextParentObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"];
        destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"];
        destViewController.editMode = @"addRoom";
        
       /*DOLD*///NSLog(@"ATobj0: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"zoneObj"]);
    }
    if ([segue.identifier isEqualToString:@"ATaddInfo"]) {
        AutotextAddTextViewController *destViewController = segue.destinationViewController;
        
		NSLog(@"HELA autotextobjektet från RepDetVC: %@",[_reportFields objectAtIndex:indexPath.row]);
		NSLog(@"Det som skickas: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"]);
		
		destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"];
        destViewController.autotextRoomName = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"];
        destViewController.editMode = @"addInfo";
		
		if(_tagForRiskInputField){
			destViewController.tagForRiskInputField = _tagForRiskInputField;
			
			NSLog(@"RISKTEXT TAG: %@", _tagForRiskInputField);
			
			UITextField *thisTextField = (UITextField*)[self.view viewWithTag:[_tagForRiskInputField intValue]];
			NSString *riskTextValue = thisTextField.text;
			
			destViewController.currentRiskText = riskTextValue; // @"Det här är text som redan står i riskfältet.";//
			//[(UITextField*)[self.view viewWithTag:[_tagForRiskInputField intValue]] text];
			
			
			NSLog(@"Risktext som skickas med: %@",destViewController.currentRiskText);
		}else{
			NSLog(@"Ingen information om risktext-objekt skickas med, eftersom _tagForRiskInputField inte är satt.");
		}

		
       /*DOLD*///NSLog(@"ATobj0: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"]);
       /*DOLD*///NSLog(@"roomName0: %@", [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"]);
    }
    if ([segue.identifier isEqualToString:@"ATaddNote"]) {
        AutotextAddTextViewController *destViewController = segue.destinationViewController;

		NSLog(@"HELA autotextobjektet från RepDetVC: %@",[_reportFields objectAtIndex:indexPath.row]);
		NSLog(@"Det som skickas: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"]);

		destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"];
        destViewController.autotextRoomName = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"];
        destViewController.editMode = @"addNote";
		if(_tagForRiskInputField){
			destViewController.tagForRiskInputField = _tagForRiskInputField;
			
			NSLog(@"RISKTEXT TAG: %@", _tagForRiskInputField);
			
			UITextField *thisTextField = (UITextField*)[self.view viewWithTag:[_tagForRiskInputField intValue]];
			NSString *riskTextValue = thisTextField.text;
			
			destViewController.currentRiskText = riskTextValue; // @"Det här är text som redan står i riskfältet.";//
			//[(UITextField*)[self.view viewWithTag:[_tagForRiskInputField intValue]] text];
			
			
			NSLog(@"Risktext som skickas med: %@",destViewController.currentRiskText);
		}else{
			NSLog(@"Ingen information om risktext-objekt skickas med, eftersom _tagForRiskInputField inte är satt.");
		}

        
       /*DOLD*///NSLog(@"ATobj0: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"]);
       /*DOLD*///NSLog(@"roomName0: %@", [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"]);
    }
    
    
    if ([segue.identifier isEqualToString:@"ATeditInfo"] || [segue.identifier isEqualToString:@"ATeditNote"]) {
        AutotextSortViewController *destViewController = segue.destinationViewController;
        
		NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
		[tempDict setValue:[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"] forKey:@"roomObj"];

		if ([segue.identifier isEqualToString:@"ATeditInfo"]){
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"id"] forKey:@"id"];
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"rubrik"] forKey:@"title"];
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"] valueForKey:@"text"] forKey:@"text"];
			
			destViewController.editMode = @"editInfo";
		}else{
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"id"] forKey:@"id"];
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"rubrik"] forKey:@"title"];
			[tempDict setValue:[[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"noteObj"] valueForKey:@"text"] forKey:@"text"];
			
			destViewController.editMode = @"editNote";
		}
		
		destViewController.addTextDictionary = tempDict;
		destViewController.autotextObject = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomObj"];

    
	
	
	}
    if ([segue.identifier isEqualToString:@"simpleAT"]) {
        AutotextAddTextViewController *destViewController = segue.destinationViewController;
        
        NSLog(@"HELA simpleAutotext från RepDetVC: %@",[_reportFields objectAtIndex:indexPath.row]);
        NSLog(@"Det som skickas: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldtype"]);
        
        destViewController.simpleAutotextColname = [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"];
        
        NSString * fieldInputTag = [[_inputFieldTagDict valueForKey:[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"]] lowercaseString];
        
        destViewController.simpleAutotextText = [(UITextField*)[self.view viewWithTag:[fieldInputTag intValue]] text];
        destViewController.editMode = @"simpleAutotext";
        
        
        /*DOLD*///NSLog(@"ATobj0: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"]);
        /*DOLD*///NSLog(@"roomName0: %@", [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"]);
    }
    if ([segue.identifier isEqualToString:@"AttachImages"] || [segue.identifier isEqualToString:@"AttachImages-view"]) {
        AttachImagesCollectionViewController *destViewController = segue.destinationViewController;
        
        NSLog(@"AttachImages radobjekt: %@",[_reportFields objectAtIndex:indexPath.row]);
        
        
        NSString *thisFieldColName =  [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"];
        NSString *thisFieldValue = [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey: thisFieldColName];
        
        if ([_appDelegate.attachImagesDict valueForKey: thisFieldColName]) {
            //Uppdatera fältet med tillagda bilder
            thisFieldValue = [_appDelegate.attachImagesDict valueForKey: thisFieldColName];
            
        }
        
        destViewController.appDelegate = _appDelegate;
        destViewController.fieldName = thisFieldColName;
        destViewController.fieldValue = thisFieldValue;
        destViewController.editMode = _editMode;
        
        
        /*DOLD*///NSLog(@"ATobj0: %@",[[_reportFields objectAtIndex:indexPath.row] valueForKey:@"infoObj"]);
        /*DOLD*///NSLog(@"roomName0: %@", [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"roomName"]);
    }
    if ([segue.identifier isEqualToString:@"Signature"]) {
        signBezierPath *destViewController = segue.destinationViewController;
        
        NSString *thisFieldColName =  [[_reportFields objectAtIndex:indexPath.row] valueForKey:@"fieldcolname"];
        NSLog(@"Signature colname: %@",thisFieldColName);
        
        destViewController.FieldColName = thisFieldColName;
    }
}





-(void) keyboardDidShow: (NSNotification *)notif
{
	// If keyboard is visible, return
	if (keyboardVisible)
	{
		NSLog(@"Keyboard is already visible. Ignoring notification.");
		return;
	}
	
	// Get the size of the keyboard.
	NSDictionary* info = [notif userInfo];
    //Change
//	NSValue* aValue = [info objectForKey: UIKeyboardBoundsUserInfoKey];
    NSValue* aValue = [info objectForKey: UIKeyboardFrameBeginUserInfoKey];
	CGSize keyboardSize = [aValue CGRectValue].size;
	
	// Save the current location so we can restore
	// when keyboard is dismissed
	offset = self.reportForm.contentOffset;
	
	// Resize the scroll view to make room for the keyboard
	CGRect viewFrame = self.reportForm.frame;
	viewFrame.size.height -= keyboardSize.height;
	self.reportForm.frame = viewFrame;
	
	
	
	//För att scrolla till valt fält, kalla detta på lämpligt ställe (behöver indexPath):
	//[myTableView selectRowAtIndexPath:self.indexPath animated:YES scrollPosition:UITableViewScrollPositionBottom];

	
	
	// Keyboard is now visible
	keyboardVisible = YES;
}


-(void) keyboardDidHide: (NSNotification *)notif
{
	// Is the keyboard already shown
	if (!keyboardVisible)
	{
		NSLog(@"Keyboard is already hidden. Ignoring notification.");
		return;
	}
	
	// Reset the height of the scroll view to its original value
	self.reportForm.frame = CGRectMake(0, 0, 768, 960);//SCROLLVIEW_WIDTH, SCROLLVIEW_HEIGHT);
	
	// Reset the scrollview to previous location
	self.reportForm.contentOffset = offset;
	
	// Keyboard is no longer visible
	keyboardVisible = NO;
	
}


- (void)textViewDidEndEditing:(UITextView *)textView
{
	
	/*
	*//*DOLD3*//*//NSLog(@"Textfält inaktiverades. Sparar rapport. (tag: %d colname: %@ value:%@)",textField.tag, [_inputFieldTagCounterArr objectAtIndex:textField.tag-1], textField.text);
	 [self performSelector:@selector(saveReport)];
	 */
    NSLog(@"textViewDidEndEditing: tag: %ld value:%@ colname: %@",(long)textView.tag, textView.text, [_inputFieldTagCounterArr objectAtIndex:textView.tag-1]);
	
	//Uppdatera reportValuesDelegate med fältets innehåll
	if([_appDelegate.reportValuesDelegate count]>0){
		[[_appDelegate.reportValuesDelegate objectAtIndex:0] setValue:textView.text forKey:[_inputFieldTagCounterArr objectAtIndex:textView.tag-1]];
		NSLog(@"Uppdaterade reportValuesDelegate: %@ = \"%@\" \nVärde i delegate: %@",[_inputFieldTagCounterArr objectAtIndex:textView.tag-1], textView.text, [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[_inputFieldTagCounterArr objectAtIndex:textView.tag-1]]);
		
		//NSLog(@"Hela reportValuesDelegate: %@", appDelegate.reportValuesDelegate);
	}
	
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
	
	/*
	NSLog(@"Textfält inaktiverades. Sparar rapport. (tag: %d colname: %@ value:%@)",textField.tag, [_inputFieldTagCounterArr objectAtIndex:textField.tag-1], textField.text);
	[self performSelector:@selector(saveReport)];
	*/
    NSLog(@"textFieldDidEndEditing: tag: %ld value:%@ colname: %@",(long)textField.tag, textField.text, [_inputFieldTagCounterArr objectAtIndex:textField.tag-1]);
	
	//Uppdatera reportValuesDelegate med fältets innehåll
	if([_appDelegate.reportValuesDelegate count]>0){
		[[_appDelegate.reportValuesDelegate objectAtIndex:0] setValue:textField.text forKey:[_inputFieldTagCounterArr objectAtIndex:textField.tag-1]];
		NSLog(@"Uppdaterade reportValuesDelegate: %@ = \"%@\" \nVärde i delegate: %@\nDelegate: %@",[_inputFieldTagCounterArr objectAtIndex:textField.tag-1], textField.text, [[_appDelegate.reportValuesDelegate objectAtIndex:0] valueForKey:[_inputFieldTagCounterArr objectAtIndex:textField.tag-1]],_appDelegate.reportValuesDelegate);
		
		//NSLog(@"Hela reportValuesDelegate: %@", appDelegate.reportValuesDelegate);
	}
	
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
	NSLog(@"textFieldShouldReturn: %@",textField);
	
	NSInteger nextTag = textField.tag + 1;
	// Try to find next responder
	UIResponder* nextResponder = [self.view viewWithTag:nextTag];
	if (nextResponder) {
		// Found next responder, so set it.
		[nextResponder becomeFirstResponder];
	} else {
		// Not found, so remove keyboard.
		[textField resignFirstResponder];
	}
	return NO; // We do not want UITextField to insert line-breaks.
}

-(BOOL)textViewShouldReturn:(UITextField*)textView;
{
	NSLog(@"textFieldShouldReturn: %@",textView);
	
	NSInteger nextTag = textView.tag + 1;
	// Try to find next responder
	UIResponder* nextResponder = [self.view viewWithTag:nextTag];
	if (nextResponder) {
		// Found next responder, so set it.
		[nextResponder becomeFirstResponder];
	} else {
		// Not found, so remove keyboard.
		[textView resignFirstResponder];
	}
	return NO; // We do not want UITextField to insert line-breaks.
}


- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	
	if (self.isMovingFromParentViewController) {
		//Töm reportValuesDelegate när man går tillbaka till arbetsordrarna
		_appDelegate.reportValuesDelegate = nil;
        
        //Töm variabler i appdelegate
        _appDelegate.simpleAutotextDict = nil;
        _appDelegate.completeJsonHolder = nil;
        _appDelegate.jsonOrigHolder = nil;
        _appDelegate.jsonHolder = nil;
        _appDelegate.simpleAutotextDict = nil;
        _appDelegate.reportValuesDelegate = nil;
        
        _appDelegate.imageToDrawOn = nil;
        _appDelegate.imageToDrawOnFieldName = nil;
        _appDelegate.imageToDrawOnFieldValue = nil;
        _appDelegate.imageToDrawOnFieldSelectedIndex = nil;
        
        _appDelegate.attachImagesDict = nil;
		NSLog(@"Tömt reportValuesDelegate i viewWillDisappear: %@", _appDelegate.reportValuesDelegate);
		
	}
}

//Change
//- (void)viewDidUnload
- (void)didReceiveMemoryWarning

{
    [self setEditOrSaveButton:nil];
    [super didReceiveMemoryWarning];
    // Release any retained subviews of the main view.
	
    _window = nil;
    _editOrSaveButton = nil;
    _reportForm = nil;
	
	
    _databasePath = nil;
    workOrder = nil;
    _templateid = nil;
    _templateInfo = nil;
    _editMode = nil;
    _autotextMode = nil;
	
    _reportFields = nil;
    reportFieldsStr = nil;
    _reportInitialValues = nil;
    _defaultReportValues = nil;
	
    _workOrderForClickedButton = nil;
	
    _indexForClickedButton = nil;
	
    _inputFieldTagCounterArr = nil;
    _inputFieldTagDict = nil;
    _blobdictionary = nil;
	
	_tagForRiskInputField = nil;
    
	
	//Töm variabler i appdelegate
	_appDelegate.simpleAutotextDict = nil;
	_appDelegate.completeJsonHolder = nil;
	_appDelegate.jsonOrigHolder = nil;
	_appDelegate.jsonHolder = nil;
	_appDelegate.simpleAutotextDict = nil;
	_appDelegate.reportValuesDelegate = nil;
    
    _appDelegate.imageToDrawOn = nil;
    _appDelegate.imageToDrawOnFieldName = nil;
    _appDelegate.imageToDrawOnFieldValue = nil;
    _appDelegate.imageToDrawOnFieldSelectedIndex = nil;
    
    _appDelegate.attachImagesDict = nil;

}

- (void)dealloc
{
    [self setEditOrSaveButton:nil];
    // Release any retained subviews of the main view.
    
    _window = nil;
    _editOrSaveButton = nil;
    _reportForm = nil;
    
    
    _databasePath = nil;
    workOrder = nil;
    _templateid = nil;
    _templateInfo = nil;
    _editMode = nil;
    _autotextMode = nil;
    
    _reportFields = nil;
    reportFieldsStr = nil;
    _reportInitialValues = nil;
    _defaultReportValues = nil;
    
    _workOrderForClickedButton = nil;
    
    _indexForClickedButton = nil;
    
    _inputFieldTagCounterArr = nil;
    _inputFieldTagDict = nil;
    _blobdictionary = nil;
    
    _tagForRiskInputField = nil;
    
    
    //Töm variabler i appdelegate
    _appDelegate.simpleAutotextDict = nil;
    _appDelegate.completeJsonHolder = nil;
    _appDelegate.jsonOrigHolder = nil;
    _appDelegate.jsonHolder = nil;
    _appDelegate.simpleAutotextDict = nil;
    _appDelegate.reportValuesDelegate = nil;
    
    _appDelegate.imageToDrawOn = nil;
    _appDelegate.imageToDrawOnFieldName = nil;
    _appDelegate.imageToDrawOnFieldValue = nil;
    _appDelegate.imageToDrawOnFieldSelectedIndex = nil;
    
    _appDelegate.attachImagesDict = nil;

}


/*
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

//Change
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//    return NO;
//}

//- (BOOL)shouldAutorotate {
//
//    return YES;
//}
//
//-(UIInterfaceOrientationMask)supportedInterfaceOrientations
//{
//    return UIInterfaceOrientationMaskPortrait;
//}



@end

//
//  AppDelegate.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
NSMutableData *receivedData;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) NSMutableData *receivedData;

//Håller hela json-objektet. Om det finns data i den, ladda inte JSON från DB.


//Töms när användaren sparar rapporten.
@property (nonatomic, retain) NSMutableArray * completeJsonHolder; //Kom ihåg att tömma när en rapport stängs.

//Håller del av json-objekt som det var före förändring/omsortering.
@property (nonatomic, retain) NSMutableArray * jsonOrigHolder; //Kom ihåg att tömma när en rapport stängs.
//Håller del av json-objektet som uppdaterats, ersätter ovanstående.
@property (nonatomic, retain) NSMutableArray * jsonHolder; //Kom ihåg att tömma när en rapport stängs.

//Används ej
@property (nonatomic, retain) NSString * jsonSortZone;
@property (nonatomic, retain) NSString * riskText;
@property (nonatomic, retain) NSMutableDictionary * jsonPartToEdit;

//Håller text som ska läggas in i ett enkel-autotext-fält
@property (nonatomic, retain) NSMutableDictionary * simpleAutotextDict; //Kom ihåg att tömma när en rapport stängs.

//Håller datan som är bilden av en signatur i jpg-format
@property (nonatomic, retain) NSData * signatureImageData;
@property (nonatomic, retain) NSMutableDictionary * signatureImagesDict;

//Håller listan på bilder i attachImages-fält
@property (nonatomic, retain) NSMutableDictionary * attachImagesDict;
@property (nonatomic, retain) UIImage * imageToDrawOn;
@property (nonatomic, retain) NSString * imageToDrawOnFieldName;
@property (nonatomic, retain) NSString * imageToDrawOnFieldValue;
@property (nonatomic, assign) NSInteger * imageToDrawOnFieldSelectedIndex;

//Inloggad användare (username)
@property (nonatomic, retain) NSString * user;
//Inloggad användare (userpassword)
@property (nonatomic, retain) NSString * userpassword;
//Inloggad användare (sys_users.id)
@property (nonatomic, retain) NSNumber * userId;
//Inloggad användares roll (sys_users.role)
@property (nonatomic, retain) NSNumber * userRole;
//Inloggad användare dictionary
@property (nonatomic, retain) NSMutableDictionary * userDictionary;

//Håller alla värden i en rapport medan den redigeras
@property (nonatomic, retain) NSMutableArray * reportValuesDelegate; //Kom ihåg att tömma när en rapport stängs.

//Håller eventuellt suffix (tillägg) på en protokoll-tabell för att visa en backup (_bkp1, _bkp2 eller "")
@property (nonatomic, retain) NSString * tableBackup;

//Håller connectionen till den öppna databasfilen
@property (nonatomic) sqlite3 * openDbConnection;

//Håller en lista på poster som är märkta för uppladdning
@property (nonatomic, retain) NSMutableArray * postsToSend;

//Flagga från login-sidan som är true om synkronisering ska köras direkt när användaren kommer till secondviewcontroller
@property (nonatomic, assign) BOOL runFetchDataAutomatically;

//Lista över bilder som håller på att laddas upp
@property (nonatomic,assign) NSMutableArray * imagesToSend;
@property (strong,nonatomic) NSMutableArray * imagesToSendFailed;
@property (nonatomic,strong) NSMutableArray * imagesToSendSuccess;
@property (strong,nonatomic) UIActivityIndicatorView *activity;

- (void)showActivity;
- (void)hideActivity;

@end

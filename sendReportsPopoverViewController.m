//
//  sendReportsPopoverViewController.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2013-07-04.
//  Copyright (c) 2013 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FirstViewController.h"
#import <sqlite3.h>
#import "AppDelegate.h"
#import <MessageUI/MFMailComposeViewController.h>

#import "sendReportsPopoverViewController.h"
#import "NSString+HTML.h"
#import "ASIFormDataRequest.h"
#import "webservice.h"

@interface sendReportsPopoverViewController ()

@end

@implementation sendReportsPopoverViewController{
//Change
//    UIAlertView *sendReportInProgress;
UIAlertController *sendReportInProgress;
}
@synthesize tableView = _tableView;
@synthesize appDelegate = _appDelegate;
@synthesize sendReportsTableContent = _sendReportsTableContent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
if (self) {
    // Custom initialization
}
return self;
}



//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


-(void)viewDidLoad
{
[super viewDidLoad];
// Do any additional setup after loading the view.

_sendReportsTableContent = [[NSMutableArray alloc] init];
httpRequestIsRunning = false;
}



//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


-(void)didReceiveMemoryWarning
{
[super didReceiveMemoryWarning];
// Dispose of any resources that can be recreated.
}



//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


-(void) viewWillAppear:(BOOL)animated{
NSLog(@"Visar sendReportsPopoverViewController");
_appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
[[self tableView] reloadData];
}


//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


- (NSMutableArray *) getPostsByQuery:(NSString *)sql removeDuplicates:(BOOL *)removeDuplicates{
NSLog(@"getPostsByQuery, sql: %@", sql);
sqlite3_stmt *statement;


NSMutableArray * poststosend = [[NSMutableArray alloc] init];
if (_appDelegate.openDbConnection)
{
    //NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
    const char *begin_stmt = [sql UTF8String];
    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
    
    //Ger antalet kolumner för select-statement "pStmt"
    int colCount = sqlite3_column_count(statement);
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        NSString * myTempMutStr = [[NSMutableString alloc] init];
        
        int j = 0;
        for (j=0; j<colCount;j++) {
            //Ger namnet på kolumn N
            const char *colName = (const char*)sqlite3_column_name(statement, j);
            
            char *colValue = (char *)sqlite3_column_text(statement, j);
            if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
            (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
            myTempMutStr = nil;
            /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
        }
        [poststosend addObject:dictionary];
    }
    sql = nil;
    begin_stmt = nil;
    sqlite3_finalize(statement);
    
    NSLog(@"Alla poster i queryn:");
    NSLog(@"%@", poststosend);
    
    if (removeDuplicates) {
        //Ta bort dubletter från poststosendArr
        NSArray* originalArray = poststosend;
        NSMutableSet* existingNames = [NSMutableSet set];
        NSMutableArray* filteredArray = [NSMutableArray array];
        for (id object in originalArray) {
            if (![existingNames containsObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]]) {
                [existingNames addObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]];
                [filteredArray addObject:object];
            }
        }
        poststosend = filteredArray;
    }
    
    if ([poststosend count]>0) {
        NSLog(@"Returnerar resultatet...");
        //NSLog(poststosend);
        return poststosend;
    }else{
        return false;
    }
}else{
    return false;
}
}


- (NSString *) getOneStringValueByQuery:(NSString *)sql :(NSString *)columnToRetrieve{
NSLog(@"getOneStringValueByQuery, sql: %@; kolumn: %@", sql, columnToRetrieve);
sqlite3_stmt *statement;


NSMutableArray * poststosend = [[NSMutableArray alloc] init];
if (_appDelegate.openDbConnection)
{
    //NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
    const char *begin_stmt = [sql UTF8String];
    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
    
    //Ger antalet kolumner för select-statement "pStmt"
    int colCount = sqlite3_column_count(statement);
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        NSString * myTempMutStr = [[NSMutableString alloc] init];
        
        int j = 0;
        for (j=0; j<colCount;j++) {
            //Ger namnet på kolumn N
            const char *colName = (const char*)sqlite3_column_name(statement, j);
            
            char *colValue = (char *)sqlite3_column_text(statement, j);
            if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
            (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
            myTempMutStr = nil;
            /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
        }
        [poststosend addObject:dictionary];
    }
    sql = nil;
    begin_stmt = nil;
    sqlite3_finalize(statement);
    
    NSLog(@"Alla poster i queryn:");
    NSLog(@"%@", poststosend);
    
    if ([poststosend count]>0) {
        NSLog(@"Returnerar resultatet...");
        //NSLog(poststosend);
        return [[poststosend objectAtIndex:0] valueForKey:columnToRetrieve];
    }else{
        return false;
    }
}else{
    return false;
}
}



- (bool) runQuery:(NSString *)sql{
NSLog(@"getOneStringValueByQuery, sql: %@", sql);
sqlite3_stmt *statement;

if (_appDelegate.openDbConnection)
{
    //NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
    const char *begin_stmt = [sql UTF8String];
    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
    
    if(sqlite3_step(statement) == SQLITE_DONE) {
        NSLog(@"Följande SQL lyckades: %@", sql);
        return true;
    }else{
        NSLog(@"Följande SQL misslyckades (returnerade inte SQLITE_DONE): %@", sql);
        return false;
    }
    sql = nil;
    begin_stmt = nil;
    sqlite3_finalize(statement);

}else{
    return false;
}
}



- (NSMutableArray *) getListOfPostsToSend{

sqlite3_stmt *statement;


NSMutableArray * poststosend = [[NSMutableArray alloc] init];
if (_appDelegate.openDbConnection)
{
    NSString *sql = [NSString stringWithFormat: @"SELECT * FROM sys_poststosend"];
    const char *begin_stmt = [sql UTF8String];
    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
    
    //Ger antalet kolumner för select-statement "pStmt"
    int colCount = sqlite3_column_count(statement);
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        NSString * myTempMutStr = [[NSMutableString alloc] init];
        
        int j = 0;
        for (j=0; j<colCount;j++) {
            //Ger namnet på kolumn N
            const char *colName = (const char*)sqlite3_column_name(statement, j);
            
            char *colValue = (char *)sqlite3_column_text(statement, j);
            if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
            (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
            myTempMutStr = nil;
            /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
        }
        [poststosend addObject:dictionary];
    }
    sql = nil;
    begin_stmt = nil;
    sqlite3_finalize(statement);
    
    NSLog(@"Alla poster i PostsToSend-tabellen:");
    NSLog(@"%@", poststosend);
}


//Ta bort dubletter från poststosendArr
NSArray* originalArray = poststosend;
NSMutableSet* existingNames = [NSMutableSet set];
NSMutableArray* filteredArray = [NSMutableArray array];
for (id object in originalArray) {
    if (![existingNames containsObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]]) {
        [existingNames addObject:[NSString stringWithFormat:@"template%@report%@",[object valueForKey:@"reporttemplate"],[object valueForKey:@"workorderid"]]];
        [filteredArray addObject:object];
    }
}

poststosend = filteredArray;

return poststosend;
}




//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
return 2;
}

//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
if (section == 0) {
    return [NSString stringWithFormat:@"Rapporter redo att laddas upp"];
}else{
    return [NSString stringWithFormat:@"Bilder redo att laddas upp"];
}
}


//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
if (section == 0) {
    NSLog(@"numberOfRowsInSection 0: %lu", (unsigned long)[[self getListOfPostsToSend] count]);
    return [[self getListOfPostsToSend] count];
}else{
    NSLog(@"numberOfRowsInSection %ld: %lu", (long)section, [self numberOfImagesToUpload]);
    if ([self numberOfImagesToUpload] > 0) {
        return 1;
    }else{
        return 0;
    }
}
}


//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
NSLog(@"cellForRowAtIndexPath %li",(long)indexPath.row);
static NSString *simpleTableIdentifier = @"sendReportsPopoverCell";

UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
if (cell == nil){
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
}


if (indexPath.section == 0) {
    
    NSLog(@"Hämtar workorderid från index %li",(long)indexPath.row);
    NSString * reporttemplate = [[[self getListOfPostsToSend] objectAtIndex:indexPath.row] valueForKey:@"reporttemplate"];
    NSString * workorderid = [[[self getListOfPostsToSend] objectAtIndex:indexPath.row] valueForKey:@"workorderid"];
    NSString * ThisSQL = [NSString stringWithFormat:@"SELECT prot_arbetsorder.id, gatuadress, postnummer, stad, ordermottagare, avtaladtid_1 FROM prot_arbetsorder WHERE id = %@ ORDER BY prot_arbetsorder.avtaladtid_1 DESC;",workorderid];
    NSLog(@"SQL: %@", ThisSQL);
    NSMutableArray * rowPost = [self getPostsByQuery:ThisSQL removeDuplicates:false];
    
    
    if([rowPost count] > 0){
        //Spara undan resultatet i global array så vi inte behöver hämta infon flera gånger
        [_sendReportsTableContent addObject:[rowPost objectAtIndex:0]];
        NSLog(@"SendreportsTableContent: %@", _sendReportsTableContent);
        
        /*DOLD*/NSLog(@"Sektion: %ld Rad: %ld",(long)indexPath.section, (long)indexPath.row);
        
        NSString * thisId = [[rowPost objectAtIndex:0] valueForKey:@"id"];
        NSString * thisAdr = [[[rowPost objectAtIndex:0] valueForKey:@"gatuadress"] stringByDecodingHTMLEntities];
        NSString * thisStad = [[[rowPost objectAtIndex:0] valueForKey:@"stad"] stringByDecodingHTMLEntities];
        NSString * thisPostnr = [[[rowPost objectAtIndex:0] valueForKey:@"postnummer"] stringByDecodingHTMLEntities];
        NSString * thisName = [[[rowPost objectAtIndex:0] valueForKey:@"ordermottagare"] stringByDecodingHTMLEntities];
        
        NSRange r = NSMakeRange(0, 16); //substringa "2013-05-01 10:30"
        NSString * thisAvtaladTid = [[[[rowPost objectAtIndex:0] valueForKey:@"avtaladtid_1"] stringByDecodingHTMLEntities] substringWithRange: r];
        
        NSString * thisLabel = [NSString stringWithFormat:@"%@. %@, %@ %@, %@ (%@)",thisId,thisAdr,thisStad,thisPostnr,thisAvtaladTid,thisName];
        
        cell.userInteractionEnabled = YES;
        cell.textLabel.text = thisLabel;
        
        NSString * detailTextSql = [NSString stringWithFormat:@"SELECT id, form_name FROM sys_formtables WHERE id = %@",reporttemplate];
        cell.detailTextLabel.text = [self getOneStringValueByQuery:detailTextSql:@"form_name"];
        
        
    }else{
        NSLog(@"Hittade inte db-post för woid %@", workorderid);
        NSString * thisLabel = [NSString stringWithFormat:@"Arbetsorder %@ hittades inte.", workorderid];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        cell.accessoryView = nil;
        cell.textLabel.text = thisLabel;
    }
}else{
    NSInteger imgCount = [self numberOfImagesToUpload];
    if (imgCount > 0) {
        NSLog(@"numberOfImagesToUpload = %ld", (long)imgCount);
        
        NSString * thisLabel = [NSString stringWithFormat:@"Ladda upp %ld bild", (long)imgCount];
        if (imgCount > 1) {
            thisLabel = [NSString stringWithFormat:@"Ladda upp %ld bilder", (long)imgCount];
        }
        cell.userInteractionEnabled = YES;
        cell.textLabel.text = thisLabel;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Tryck här för att börja ladda upp"];
    }else{
        NSLog(@"numberOfImagesToUpload = 0");
        NSString * thisLabel = [NSString stringWithFormat:@"Inga bilder att ladda upp."];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.userInteractionEnabled = NO;
        cell.accessoryView = nil;
        cell.textLabel.text = thisLabel;
    }
}

return cell;
}





//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//Avmarkera raden användaren trycker på, så den inte är blå när man pop:ar subview:n
[tableView deselectRowAtIndexPath:indexPath animated:YES];
NSLog(@"Användaren tryckte på rad %li", (long)indexPath.row);

selectedRowNumber = (int) indexPath.row;

/*DOLD*/NSLog(@"Sektion: %ld Rad: %ld",(long)indexPath.section, (long)indexPath.row);

if (indexPath.section == 0) {

NSString * thisId = [[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"id"];
NSString * thisAdr = [[[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"gatuadress"] stringByDecodingHTMLEntities];
NSString * thisStad = [[[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"stad"] stringByDecodingHTMLEntities];
NSString * thisPostnr = [[[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"postnummer"] stringByDecodingHTMLEntities];
NSString * thisName = [[[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"ordermottagare"] stringByDecodingHTMLEntities];
NSRange r = NSMakeRange(0, 16); //substringa "2013-05-01 10:30"
NSString * thisAvtaladTid = [[[[_sendReportsTableContent objectAtIndex:indexPath.row] valueForKey:@"avtaladtid_1"] stringByDecodingHTMLEntities] substringWithRange: r];

NSString * reporttemplate = [[[self getListOfPostsToSend] objectAtIndex:indexPath.row] valueForKey:@"reporttemplate"];
NSString * detailTextSql = [NSString stringWithFormat:@"SELECT id, form_name FROM sys_formtables WHERE id = %@",reporttemplate];
NSString * thisFormTypeName = [self getOneStringValueByQuery:detailTextSql:@"form_name"];

NSString * thisLabel = [NSString stringWithFormat:@"%@?\nTyp: %@\n\n%@,\n%@ %@\n\nTid: %@\n(%@)",thisId,thisFormTypeName,thisAdr,thisStad,thisPostnr,thisAvtaladTid,thisName];

    
NSString *alertString = [NSString stringWithFormat:@"Skicka rapporten?"];
NSString *alertMsg = [NSString stringWithFormat:@"Vill du ladda upp rapport %@", thisLabel];
    
UIAlertController* alert = [UIAlertController alertControllerWithTitle:alertString message:alertMsg preferredStyle:UIAlertControllerStyleAlert];
UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Ja" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Nej" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {}];
[alert addAction:defaultAction];
[alert addAction:cancelAction];
[self presentViewController:alert animated:YES completion:nil];
    
    
    
//Sätt template och id för "Visa rapport i webbläsaren"-funktionen
selectedRowNumberTemplate = [reporttemplate intValue];
NSLog(@"Sätter selectedRowNumberTemplate till %d", [reporttemplate intValue]);
selectedRowNumberWorkorderId = [thisId intValue];
NSLog(@"Sätter selectedRowNumberWorkorderId till %d", [thisId intValue]); // Här ska det inte vara thisId, utan värdet ska hämtas från [self getListOfPostsToSend] så att det är uppdaterat!

//Nollställ alla vars
thisId = nil;
thisAdr = nil;
thisStad = nil;
thisPostnr = nil;
thisName = nil;
thisAvtaladTid = nil;
reporttemplate = nil;
detailTextSql = nil;
thisFormTypeName = nil;
thisLabel = nil;
alertString = nil;
alertMsg = nil;
alert = nil;

/*
 Fråga "Vill du ladda upp rapporten XXX?"
 Om ja, starta sändning av rapporten
 Visa alert "Skickar rapport..." utan ok/avbryt-knapp (tills det faktiskt implementerats)
 När sändning färdig, dölj alertview
 Ta bort posten ur tabellen PostsToSend mha ID-numret på raden
 Uppdatera tableview
 Visa ny alert "Rapport XXX laddades upp till servern!" [Avfärda]
 
 
 ATT UTVECKLA FRAMÖVER:
 - Verifiera att datan gick fram (jämför datan i ipad med datan på servern) och erbjud att skicka på nytt
 - Möjlighet att avbryta requesten, och säkerställ att inget sparas i serverns DB om man avbryter
 - Kolla upp hur enkelt det är att skriva direkt till databasen, och om man kan få loggar den vägen
 
 */
}

if ((int)indexPath.section == 1) {
    [self uploadAllImages];
}
}


//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////

//Change
//-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    if(buttonIndex == 1){
//        //Delegate för "Vill du skicka rapport xxx"-bekräftelse
//        NSString *btntitle = [alertView buttonTitleAtIndex:buttonIndex];
//        if([btntitle isEqualToString:@"Ja"]){
//            [self sendTablerow:selectedRowNumber];
//        }else if ([btntitle isEqualToString:@"Visa rapporten"]){
//            [self openReportInBrowser];
//        }
//    }
//}
-(void) alertButtonClicked:(NSString *)btntitle{
if([btntitle isEqualToString:@"Ja"]){
    [self sendTablerow:selectedRowNumber];
}else if ([btntitle isEqualToString:@"Visa rapporten"]){
    [self openReportInBrowser];
}
}

-(void)uploadAllImages{

if(![self hasInternet]){
    NSString *alertHeader = [NSString stringWithFormat:@"Ingen uppkoppling"];
    NSString *alertString = [NSString stringWithFormat:@"Kunde inte kontakta servern. Inga ändringar har gjorts, varken på servern eller i iPaden."];
    //Change
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertString delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertHeader
                                                                                        message:alertString
                                                                                 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                }];
            
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    
}else{
//        sendReportInProgress = [[UIAlertView alloc] initWithTitle:@"Laddar upp bilder..." message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    sendReportInProgress = [UIAlertController alertControllerWithTitle:@"Laddar upp bilder..."
                                                                                    message:@""
                                                                             preferredStyle:UIAlertControllerStyleAlert];
    
    [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];

    NSLog(@"Visar sendImagesInProgress-alert");
    
    UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]
                                        initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
    loading.frame=CGRectMake(140, 60, 16, 16);
    [loading startAnimating];
//        [sendReportInProgress addSubview:loading];
//        [sendReportInProgress show];
    [sendReportInProgress.view addSubview:loading];
    [self presentViewController:sendReportInProgress animated:YES completion:nil];
    
    NSLog(@"sendTablerow! httpRequestIsRunning: %d", httpRequestIsRunning);
    //Fråga om användaren vill ladda upp ouppladdade bilder
    //Kolla om ej uppladdade bilder finns
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    NSArray *filePathsArray = [filemgr subpathsOfDirectoryAtPath:documentsDirectory error:nil];
    NSMutableArray * imgFiles = [[NSMutableArray alloc] init];
    NSLog(@"Filer i Documents: %@", filePathsArray);
    for (NSString* filename in filePathsArray) {
        if ([filename containsString:@".jpg"]){
            if ([filename containsString:@"cached_"]){
                // Ignore cached files
            }else{
                NSLog(@"Bild hittad: %@", filename);
                [imgFiles addObject:[NSString stringWithFormat:@"%@", filename]];
            }
        }
    }
    
    if([imgFiles count] > 0){
        // Spara undan fil-arrayen i appDelegate
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.imagesToSend = imgFiles;
        appDelegate.imagesToSendFailed = [[NSMutableArray alloc] init];
        appDelegate.imagesToSendSuccess = [[NSMutableArray alloc] init];
        
        for (NSString *fileName in imgFiles) {
            NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
            
            // Create file manager
            NSFileManager *fileMgr = [NSFileManager defaultManager];
            
            //Finns filen?
            if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                
                //TEST: Ladda upp bilden på servern
                //Sätt URL för requesten
                
                //API For Image upload
                NSURL *signatureUploadUrl = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_inc/imageUpload.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                
                //Skapa requesten
                ASIFormDataRequest *_signatureUploadRequest = [ASIFormDataRequest requestWithURL:signatureUploadUrl];
                __weak ASIFormDataRequest *signatureUploadRequest = _signatureUploadRequest;
                
                
                [signatureUploadRequest addPostValue:nil forKey:@"template"];
                
                NSData * imageData = [[NSData alloc] initWithContentsOfFile:jpgPath];
                
                [signatureUploadRequest setData:imageData withFileName:fileName andContentType:@"image/jpeg" forKey:@"file"];
                
                [signatureUploadRequest setCompletionBlock:^{
                    NSString *responseString = [[NSString alloc] initWithData:[signatureUploadRequest responseData] encoding:NSUTF8StringEncoding];
                    /*VIKTIG*/NSLog(@"Fil skickad! Response: %@", responseString);
                    
                    //Filen skickades. Nu kollar vi om bilden finns på servern, och i så fall raderar vi den lokalt.
                    NSURL *signatureCheckUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/_inc/imageExists.asp?format=json&filename=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],fileName]];
                    
                    NSURLSession *sessionCheck = [NSURLSession sharedSession];
                    NSURLSessionDataTask *task = [sessionCheck dataTaskWithURL:signatureCheckUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

                        if (data != nil){
                            NSError *parseError = nil;
                            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                            if (parseError) {
                                NSLog(@"Admin data: %@",parseError);
                                //Kunde inte kolla om filen laddades upp.
                                NSLog(@"//Kunde inte kolla om filen laddades upp.");
    
                                AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                [appDelegate.imagesToSendFailed addObject:jpgPath];
    
                                [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];
                            }
                            else{
                                bool _isExists = [responseDictionary valueForKey:@"exists"];

                                NSLog(@"Response Signature check = %@",data);
//                                                    if([responseString isEqualToString:@"TRUE"]){
                                if(_isExists == true){
                                    NSLog(@"Bilden existerar på servern. Uppladdningen är komplett och färdig. Raderar bilden på iPaden.");
    
                                    [fileMgr removeItemAtPath: (jpgPath) error:NULL];
                                    NSLog(@"Raderade filen.");
    
    
                                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                    [appDelegate.imagesToSendSuccess addObject:jpgPath];
    
    
                                    //NSString *alertString = [NSString stringWithFormat:@"Fil skickad!"];
                                    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Filen laddades upp och servern bekräftade att den kom fram.") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    //[alert show];
    
    
                                    [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];
                                }
                            }
                        }else{
                            //Kunde inte kolla om filen laddades upp.
                            NSLog(@"//Kunde inte kolla om filen laddades upp.");

                            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                            [appDelegate.imagesToSendFailed addObject:jpgPath];

                            [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];
                        }
                    }];

                    // Begin task.
                    [task resume];
                    
                    [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];
                }];
                [signatureUploadRequest setFailedBlock:^{
                    NSError *error = [signatureUploadRequest error];
                    /*VIKTIG*/NSLog(@"Error när bilden skulle skickas: %@", error.localizedDescription);
                    
                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                    [appDelegate.imagesToSendFailed addObject:jpgPath];
                    NSLog(@"Success: %lu", (unsigned long)[appDelegate.imagesToSendSuccess count]);
                    NSLog(@"Failed: %lu", (unsigned long)[appDelegate.imagesToSendFailed count]);
                    NSLog(@"Totalt: %lu", (unsigned long)[appDelegate.imagesToSend count]);
                    
                    
                    [self performSelectorOnMainThread:@selector(updateProgressAlert) withObject:nil waitUntilDone:NO];
                }];
                NSLog(@"Skickar fil... Filnamn: %@",fileName);
                NSLog(@"Request: %@",signatureUploadRequest);
                
                //NSString *alertString = [NSString stringWithFormat:@"Skickar fil... Filnamn: %@",fileName];
                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //[alert show];
                
                [signatureUploadRequest startAsynchronous];
            }
        }
    }
}
}

-(void)updateProgressAlert{ //:(UIAlertView*)sendReportInProgress{

AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
[sendReportInProgress setTitle:@"Laddar upp bilder..."];
[sendReportInProgress setMessage:[NSString stringWithFormat:
                                  @"Bilder att ladda upp: %lu\nLyckades: %lu\nMisslyckades: %lu",
                                  (unsigned long)[appDelegate.imagesToSend count],
                                  (unsigned long)[appDelegate.imagesToSendSuccess count],
                                  (unsigned long)[appDelegate.imagesToSendFailed count]
                                  ]
 ];

if([appDelegate.imagesToSendSuccess count] + [appDelegate.imagesToSendFailed count] >= [appDelegate.imagesToSend count]){
    // Sista bilden är slutförd, visa statusrapport
    NSString *alertString = [NSString stringWithFormat:@"Bilduppladdning avslutad"];
    
    NSString *alertString2 = [NSString stringWithFormat:@"%lu bilder har laddats upp och %lu bilder misslyckades. ", (unsigned long)[appDelegate.imagesToSendSuccess count], (unsigned long)[appDelegate.imagesToSendFailed count]];
    //Change
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertString2 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//
//        // Dölj "Laddar upp..."-alerten
//        NSLog(@"Döljer sendprogressAlert");
//        [sendReportInProgress dismissWithClickedButtonIndex:0 animated:true];
//        [_tableView reloadData];
//        [alert show];
    
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                 message:alertString2
                                                                                 preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                               style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                    [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                    [self->_tableView reloadData];
                                                                }];
            
        [alertController addAction:actionCancel];
            
        [self presentViewController:alertController animated:YES completion:nil];
}
}

//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////


-(void)openReportInBrowser{
//Skicka med inloggningsuppgifter för att logga in: ?admin=[username]&pass=[password]
//Logga in som ett gästkonto med endast visningsrättigheter för att de inte ska "råka" redigera i desktopversionen på ipad. Konto: guest/guest123

//Lägg till parametrar för att visa rapporten
//mainid=10&template=135&reportid=6378

NSLog(@"Öppnar rapport i webläsare");

NSString * template = [NSString stringWithFormat:@"%d", selectedRowNumberTemplate]; // [[[self getListOfPostsToSend] objectAtIndex:selectedRowNumber] valueForKey:@"reporttemplate"];
NSString * workorder = [NSString stringWithFormat:@"%d", selectedRowNumberWorkorderId]; // [[[self getListOfPostsToSend] objectAtIndex:selectedRowNumber] valueForKey:@"workorderid"];

AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
NSString * loginparams = [NSString stringWithFormat:@"admin=%@&pass=%@", appDelegate.user, appDelegate.userpassword];

NSLog(@"Template: %@ Workorder: %@", template, workorder);

NSString * params = [NSString stringWithFormat:@"mainid=10&ipad=1&template=%@&workorder=%@&%@", template, workorder, loginparams];

NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/index.asp?%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"], params]];
//Change
//    [[UIApplication sharedApplication] openURL:url];
UIApplication *application = [UIApplication sharedApplication];
[application openURL:url options:@{} completionHandler:^(BOOL success) {
    if (success) {
         NSLog(@"Opened url");
    }
}];

selectedRowNumberTemplate = 0;
selectedRowNumberWorkorderId = 0;

}


//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////

-(NSInteger)numberOfImagesToUpload{
//Kolla om ej uppladdade bilder finns

NSFileManager *filemgr = [NSFileManager defaultManager];
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
NSString *documentsDirectory = [paths objectAtIndex:0];

NSArray *filePathsArray = [filemgr subpathsOfDirectoryAtPath:documentsDirectory error:nil];
NSMutableArray * imgFiles = [[NSMutableArray alloc] init];
NSLog(@"Filer i Documents: %@", filePathsArray);
for (NSString* filename in filePathsArray) {
    if ([filename containsString:@".jpg"]){
        if ([filename containsString:@"cached_"]){
            //Ignore cached files
        }else{
            NSLog(@"Bild hittad: %@", filename);
            [imgFiles addObject:[NSString stringWithFormat:@"%@", filename]];
        }
    }
}
NSLog(@"Bilder i Documents: %@", imgFiles);
if([imgFiles count] > 0){
    return [imgFiles count];
}
return 0;
}

-(void)checkForImagesToUpload{

//Kolla om ej uppladdade bilder finns

NSFileManager *filemgr = [NSFileManager defaultManager];
NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
NSString *documentsDirectory = [paths objectAtIndex:0];

NSArray *filePathsArray = [filemgr subpathsOfDirectoryAtPath:documentsDirectory error:nil];
NSMutableArray * imgFiles = [[NSMutableArray alloc] init];
NSLog(@"Filer i Documents: %@", filePathsArray);
for (NSString* filename in filePathsArray) {
    if ([filename containsString:@".jpg"]){
        if ([filename containsString:@"cached_"]){
            //Ignore cached files
        }else{
            NSLog(@"Bild hittad: %@", filename);
            [imgFiles addObject:[NSString stringWithFormat:@"%@", filename]];
        }
    }
}
NSLog(@"Bilder i Documents: %@", imgFiles);
if([imgFiles count] > 0){
    // Fråga om användaren vill ladda upp x bilder
    NSString *alertString = [NSString stringWithFormat:@"Det finns bilder som inte laddats upp"];
    NSString *bildAntal = [NSString stringWithFormat:@"%lu",(unsigned long)imgFiles.count];
    if([imgFiles count] > 1){
        bildAntal = [bildAntal stringByAppendingString:@" bilder"];
    }else{
        bildAntal = [bildAntal stringByAppendingString:@" bild"];
    }
    NSString *alertStringMsg = [NSString stringWithFormat:@"Hittade %@ på iPaden som inte har laddats upp till servern. Det kan t ex bero på dålig uppkoppling när rapporter laddades upp. Bilderna laddas upp i bakgrunden. \n\nVill du försöka ladda upp bilderna nu?\n\n(Frågan kommer igen \nnästa gång du loggar in)", bildAntal];
    
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertStringMsg delegate:self cancelButtonTitle:@"Ladda upp bilder nu" otherButtonTitles:@"Nej, senare",nil];
//        [alert show];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                             message:alertStringMsg
                                                                             preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Ladda upp bilder nu"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alertController dismissViewControllerAnimated:YES completion:nil];
                                                            }];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Nej"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {

                                                            }];
        
    [alertController addAction:actionCancel];
    [alertController addAction:actionOk];
        
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
}

- (bool)hasInternet {
dispatch_async(dispatch_get_main_queue(), ^{
    [self->sendReportInProgress setTitle:@"Kontrollerar anslutningen..."];
});

NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/ping.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];

NSLog(@"URL: %@", url);

NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:3.0];
//Change
//    BOOL connectedToInternet = NO;
__block BOOL connectedToInternet = NO;

//Change
if ([NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil]) {
    connectedToInternet = YES;
}
//    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData * data, NSURLResponse * response, NSError * error) {
//        connectedToInternet = YES;
//
//    }] resume];
//if (connectedToInternet)
///*DOLD*///NSLog(@"We Have Internet!");

return connectedToInternet;
}

//// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // //
/// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ///
// // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // // ////



-(void)sendTablerow:(int)rowNumberToSend{
//    sendReportInProgress = [[UIAlertView alloc] initWithTitle:@"Laddar upp rapport..." message:@"" delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
sendReportInProgress = [UIAlertController alertControllerWithTitle:@"Laddar upp rapport..."
                                                                         message:@""
                                                                         preferredStyle:UIAlertControllerStyleAlert];


NSLog(@"Visar sendprogressAlert");


UIActivityIndicatorView *loading = [[UIActivityIndicatorView alloc]
                                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleLarge];
loading.frame=CGRectMake(140, 60, 16, 16);
[loading startAnimating];
//    [sendReportInProgress addSubview:loading];
//
//
//
//    [sendReportInProgress show];
[sendReportInProgress.view addSubview:loading];
[self presentViewController:sendReportInProgress animated:YES completion:nil];

NSLog(@"sendTablerow! httpRequestIsRunning: %d", httpRequestIsRunning);

if(!httpRequestIsRunning){
    httpRequestIsRunning = false;
    /*DOLD*///NSLog(@"#### sendpoststoserver #####");
    /*VIKTIG*/NSLog(@"Skickar uppdateringsposter till servern...");
    
    
    //1. Hämta en array över vilka poster som ska skickas
    //(I det här fallet hämtas bara en post - den posten som användaren tryckte på)
    
    sqlite3_stmt *statement;
    NSMutableArray * poststosendArr = [[NSMutableArray alloc] init];
    
    [poststosendArr addObject: [[self getListOfPostsToSend] objectAtIndex:rowNumberToSend]];
    
    /*DOLD*/NSLog(@"posts: %@",poststosendArr);
    
    
    
    
    
    if([poststosendArr count]>0){
        
        //2. Hämta en array med alla poster i sys_formtables (för att få tabellnamnet som lagrar rapporten)
        
        NSMutableArray * formTablesArr = [[NSMutableArray alloc] init];
        if (_appDelegate.openDbConnection)
        {
            NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM sys_formtables"];
            
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
            
            //Ger antalet columner för select-statement "pStmt"
            int colCount = sqlite3_column_count(statement);
            /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
            
            while(sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                NSString * myTempMutStr = [[NSMutableString alloc] init];
                
                
                
                int j = 0;
                for (j=0; j<colCount;j++) {
                    //Ger namnet på kolumn N
                    const char *colName = (const char*)sqlite3_column_name(statement, j);
                    
                    char *colValue = (char *)sqlite3_column_text(statement, j);
                    if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                    (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                    myTempMutStr = nil;
                    /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                    
                    
                }
                [formTablesArr addObject:dictionary];
            }
            beginSQL = nil;
            begin_stmt = nil;
            sqlite3_finalize(statement);
        }
        
        //NSLog(@"formtables: %@", formTablesArr);
        
        
        
        
        
        
        //3. Hämta de poster som ska skickas från sina faktiska lagringsplatser
        
        NSMutableArray * colnamesArr = [[NSMutableArray alloc] init];
        NSMutableArray * postsContentArr = [[NSMutableArray alloc] init];
        
        NSMutableDictionary * reportDictionary = [[NSMutableDictionary alloc] init];
        
        for (int j=0; j< [poststosendArr count]; j++){
            reportDictionary = [poststosendArr objectAtIndex:j];
            //if([[reportDictionary objectForKey:@"reporttemplate"] isEqual: @"0"]){
            //    [reportDictionary setValue:@"135" forKey:@"reporttemplate"];
            //}
            
            NSLog(@"reporttemplate: %d",[[reportDictionary valueForKey:@"reporttemplate"] intValue]);
            
            //Hoppa över om reporttemplate = 0 i tabellen sys_poststosend
            if([[reportDictionary valueForKey:@"reporttemplate"] intValue] != 0){
                //NSLog(@"TEST");
                if (_appDelegate.openDbConnection)
                {
                    //Hitta tabellen som har id = template-id i formTablesArr
                    NSString * reportTable = [[NSString alloc] init];
                    for (NSMutableDictionary * k in formTablesArr) {
                        if([[k valueForKey:@"id"] isEqualToString:[reportDictionary valueForKey:@"reporttemplate"]])
                        {
                            reportTable = [NSString stringWithFormat:@"%@",[k valueForKey:@"table_name"]];
                        }
                    }
                    
                    //SELECT * från prot_tabell där id = workorder-id
                    NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM %@ WHERE workorder_id = %d OR (workorder_id IS NULL AND id = %d)", reportTable, [[reportDictionary valueForKey:@"workorderid"] intValue], [[reportDictionary valueForKey:@"workorderid"] intValue]];
                    
                    /*DOLD*///NSLog(@"SQL: %@", beginSQL);
                    
                    const char *begin_stmt = [beginSQL UTF8String];
                    sqlite3_prepare_v2(_appDelegate.openDbConnection, begin_stmt, -1, &statement, NULL);
                    
                    /*DOLD*///NSLog(@"Listar prot_tabell");
                    //Ger antalet columner för select-statement "pStmt"
                    int colCount = sqlite3_column_count(statement);
                    /*DOLD*///NSLog(@"Antal kolumner: %d", colCount);
                    
                    while(sqlite3_step(statement) == SQLITE_ROW) {
                        /*DOLD*///NSLog(@"sqlite_row...");
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                        [dictionary setObject:[[NSMutableDictionary alloc] init] forKey:@"blobdictionary"];
                        
                        NSString * myTempMutStr = [[NSMutableString alloc] init];
                        
                        //töm arrayen - vi vill bara ha en uppsättning kolumn-namn, så vi tar den från sista raden
                        NSMutableArray * tempColnamesArr = [[NSMutableArray alloc] init];
                        
                        
                        int j = 0;
                        for (j=0; j<colCount;j++) {
                            //Ger namnet på kolumn N
                            const char *colName = (const char*)sqlite3_column_name(statement, j);
                            //Lägg till namnet i colnamesArr
                            [tempColnamesArr addObject:[NSString stringWithUTF8String:colName]];
                            
                            char *colValue = (char *)sqlite3_column_text(statement, j);
                            if (colValue !=NULL){myTempMutStr = [NSString stringWithUTF8String: colValue];}else{myTempMutStr = @"";}
                            
                            //Undvik att fylla dictionary med nil
                            if (myTempMutStr) {
                                (void)[dictionary setObject:myTempMutStr forKey:[NSString stringWithUTF8String:colName]];
                            }else{
                                NSLog(@"TEMPMUTSTR var tom för %s", colName);
                                //Hittat en blob? Om det är en blob, så är colvalue inte NULL men blir ändå NIL.
                                
                                /* The pointer returned by sqlite3_column_blob() points to memory
                                 ** that is owned by the statement handle (pStmt). It is only good
                                 ** until the next call to an sqlite3_XXX() function (e.g. the
                                 ** sqlite3_finalize() below) that involves the statement handle.
                                 ** So we need to make a copy of the blob into memory obtained from
                                 ** malloc() to return to the caller.
                                 */
                                
                                NSData *imgData = [[NSData alloc] initWithBytes:sqlite3_column_blob(statement, j) length:sqlite3_column_bytes(statement, j)];
                                
                                NSLog(@"BLOBDATA HITTAT i rapporten: %lu", (unsigned long)[imgData length]);
                                NSLog(@"Lagrar... Resultat på nästa rad:");
                                
                                (void)[[dictionary valueForKey:@"blobdictionary"] setObject:imgData forKey:[NSString stringWithUTF8String:colName]];
                                
                                NSLog(@"blobdictionary forkey %@: %lu",[NSString stringWithUTF8String:colName], (unsigned long)[[[dictionary valueForKey:@"blobdictionary"] valueForKey:[NSString stringWithUTF8String:colName]] length]);
                                
                            }
                            myTempMutStr = nil;
                            /*#dolt#*////*DOLD*///NSLog(@"colname: %s, colvalue: %s, dictionary count: %d", colName, colValue, [dictionary count]);
                        }
                        [colnamesArr addObject:[tempColnamesArr copy]];
                        [postsContentArr addObject:dictionary];
                    }
                    
                    beginSQL = nil;
                    begin_stmt = nil;
                    sqlite3_finalize(statement);
                }
            }
        }
        
        /*DOLD*/NSLog(@"colnames: %@",colnamesArr);
        /*DOLD*/NSLog(@"postsContentArr: %@", postsContentArr);
        
    #pragma mark Skapa POST-datasträng
        //4. Skapa en datasträng till POST-requesten
        //Loopar igenom alla rapporter som ska postas, och i varje iteration loopas varje kolumn igenom
        //Skapa en request för varje stort varv och fyll requesten i den lilla loopen
        
        for (int j=0; j<[poststosendArr count]; j++)
        {
            if([[[poststosendArr objectAtIndex:j] valueForKey:@"reporttemplate"] intValue] != 0)
            {
                //Sätt URL för requesten
//                    NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_scripts/report_ajax-save-report.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                NSString *url = [NSString stringWithFormat:@"%@/_scripts/report_ajax-save-report.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]];
                
                NSString * dbstr = [NSString stringWithFormat:@"template=%@",[[poststosendArr objectAtIndex:j] valueForKey:@"reporttemplate"]];
                NSString *postParam = @"";
                
                
                if([colnamesArr count]>j){
                    
                    //Fyll requesten
                    for (int k=0; k<[[colnamesArr objectAtIndex:j] count]; k++)
                    {
                        NSString * colstr = [[NSString alloc] init];
                        NSString * valstr = [[NSString alloc] init];
                        colstr = [[colnamesArr objectAtIndex:j] objectAtIndex:k];
                        if([colstr isEqualToString:@"workorder_id"]){
                            colstr = @"workorderID";
                        }
                        if([colstr isEqualToString:@"id"]){
                            colstr = @"reportID";
                        }
                        
                        valstr = [[postsContentArr objectAtIndex:j] valueForKey:[[colnamesArr objectAtIndex:j] objectAtIndex:k]];
                        
                        if(valstr){
//                                /*DOLD*///NSLog(@"Valstr är inte nil");
//                                /*DOLD*///NSLog(@"Lägger till värde: %@ = %@", colstr, valstr);
//                                [request addPostValue:valstr forKey:colstr];
                            NSString *stringNew = [NSString stringWithFormat:@"&%@=%@",colstr,valstr];
                            NSString *myString = [NSString stringWithFormat:@"%@%@", postParam, stringNew];
                            postParam = myString;
                            
                            //BILDUPPLADDNING AV SIGNATUR:
                            //Om valstr är filnamnet på en bild, kolla om den finns i filsystemet, ladda upp den till servern, verifiera att den    existerar på servern, och radera filen lokalt på ipaden.
                            
                            if([valstr rangeOfString:@".jpg"].location != NSNotFound){
                                //Fieldvaluen innehöll .jpg, försök ladda upp bild
                                NSString * fileName = valstr;
                                
                                //Kolla om det är flera bilder eller bilder med bildtexter (som ska laddas upp till /_images/)
                                if([valstr rangeOfString:@"###&###"].location != NSNotFound){
                                    NSArray *imageAndTextList = nil;
                                    if([valstr rangeOfString:@"###;###"].location != NSNotFound){
                                        imageAndTextList = [valstr componentsSeparatedByString:@"###;###"];
                                    }else{
                                        imageAndTextList = [NSArray arrayWithObjects: valstr, nil];
                                    }
                                    
                                    if([imageAndTextList count] > 0){
                                        
                                        for (NSString *imageVal in imageAndTextList) {
                                            // Get file name
                                            NSArray *imageObject = [imageVal componentsSeparatedByString:@"###&###"];
                                            fileName = [imageObject objectAtIndex:0];
                                            
                                            
                                            NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
                                            
                                            // Create file manager
                                            NSFileManager *fileMgr = [NSFileManager defaultManager];
                                            
                                            //Finns filen?
                                            if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                                                
                                                //TEST: Ladda upp bilden på servern
                                                //Sätt URL för requesten
                                                NSURL *signatureUploadUrl = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_inc/imageUpload.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                                                
                                                //Skapa requesten
                                                ASIFormDataRequest *_signatureUploadRequest = [ASIFormDataRequest requestWithURL:signatureUploadUrl];
                                                __weak ASIFormDataRequest *signatureUploadRequest = _signatureUploadRequest;
                                                
                                                
                                                [signatureUploadRequest addPostValue:nil forKey:@"template"];
                                                
                                                NSData * imageData = [[NSData alloc] initWithContentsOfFile:jpgPath];
                                                
                                                [signatureUploadRequest setData:imageData withFileName:fileName andContentType:@"image/jpeg" forKey:@"file"];
                                                
                                                [signatureUploadRequest setCompletionBlock:^{
                                                    NSString *responseString = [[NSString alloc] initWithData:[signatureUploadRequest responseData] encoding:NSUTF8StringEncoding];
                                                    /*VIKTIG*/NSLog(@"Fil skickad! Response: %@", responseString);
                                                    
                                                    //Filen skickades. Nu kollar vi om bilden finns på servern, och i så fall raderar vi den lokalt.
                                                    NSURL *signatureCheckUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/_inc/imageExists.asp?format=json&filename=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],fileName]];
                                                    
                                                    NSURLSession *sessionCheck = [NSURLSession sharedSession];
                                                    NSURLSessionDataTask *task = [sessionCheck dataTaskWithURL:signatureCheckUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
                                                        if (data != nil){
                                                            NSError *parseError = nil;
                                                            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                            NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                                            if (parseError) {
                                                                NSLog(@"Admin data: %@",parseError);
                                                                //Kunde inte kolla om filen laddades upp.
                                                                NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                                            }
                                                            else{
                                                                bool _isExists = [responseDictionary valueForKey:@"exists"];
            
                                                                NSLog(@"Response Signature check = %@",data);
            //                                                    if([responseString isEqualToString:@"TRUE"]){
                                                                if(_isExists == true){
                                                                    NSLog(@"Bilden existerar på servern. Uppladdningen är komplett och färdig. Raderar bilden på iPaden.");
    
                                                                    [fileMgr removeItemAtPath: (jpgPath) error:NULL];
                                                                    NSLog(@"Raderade filen.");
    
                                                                    //NSString *alertString = [NSString stringWithFormat:@"Fil skickad!"];
                                                                    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Filen laddades upp och servern bekräftade att den kom fram.") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                                    //[alert show];
    
                                                                }
                                                            }
                                                        }else{
                                                            //Kunde inte kolla om filen laddades upp.
                                                            NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                                        }
                                                    }];
            
                                                    // Begin task.
                                                    [task resume];

                                                }];
                                                [signatureUploadRequest setFailedBlock:^{
                                                    NSError *error = [signatureUploadRequest error];
                                                    /*VIKTIG*/NSLog(@"Error när bilden skulle skickas: %@", error.localizedDescription);
                                                    
                                                    NSString *alertString = [NSString stringWithFormat:@"Ett fel uppstod, försök igen senare."];
                                                    NSString *alertString2 = [NSString stringWithFormat:@"Ett fel uppstod när bilden skulle skickas. Det kan till exempel bero på dålig uppkoppling/täckning. Försök igen senare. Felmeddelande: %@", error.localizedDescription];
//                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertString2 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                                        [alert show];
                                                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                                                             message:alertString2
                                                                                                                             preferredStyle:UIAlertControllerStyleAlert];

                                                    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                                                                           style:UIAlertActionStyleDefault
                                                                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                                                                [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                                                            }];
                                                        
                                                    [alertController addAction:actionCancel];
                                                        
                                                    [self presentViewController:alertController animated:YES completion:nil];
                                                }];
                                                NSLog(@"Skickar fil... Filnamn: %@",fileName);
                                                NSLog(@"Request: %@",signatureUploadRequest);
                                                
                                                //NSString *alertString = [NSString stringWithFormat:@"Skickar fil... Filnamn: %@",fileName];
                                                //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                //[alert show];
                                                
                                                [signatureUploadRequest startAsynchronous];
                                            }
                        
                                        }
                                        
                                    }
                                }else{
                                    
                                    NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents//%@",fileName]];
                                    
                                    // Create file manager
                                    NSFileManager *fileMgr = [NSFileManager defaultManager];
                                    
                                    //Finns filen?
                                    if ([fileMgr fileExistsAtPath: jpgPath ] == YES){
                                        
                                        //TEST: Ladda upp bilden på servern
                                        //Sätt URL för requesten
                                        NSURL *signatureUploadUrl = [NSURL URLWithString: [NSString stringWithFormat:@"%@/_inc/signatureUpload.asp", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"]]];
                                        
                                        //Skapa requesten
                                        ASIFormDataRequest *_signatureUploadRequest = [ASIFormDataRequest requestWithURL:signatureUploadUrl];
                                        __weak ASIFormDataRequest *signatureUploadRequest = _signatureUploadRequest;
                                        
                                        
                                        [signatureUploadRequest addPostValue:nil forKey:@"template"];
                                        
                                        NSData * imageData = [[NSData alloc] initWithContentsOfFile:jpgPath];
                                        
                                        [signatureUploadRequest setData:imageData withFileName:fileName andContentType:@"image/jpeg" forKey:@"file"];
                                        
                                        [signatureUploadRequest setCompletionBlock:^{
                                            NSString *responseString = [[NSString alloc] initWithData:[signatureUploadRequest responseData] encoding:NSUTF8StringEncoding];
                                            /*VIKTIG*/NSLog(@"Fil skickad! Response: %@", responseString);
                                            
                                            //Filen skickades. Nu kollar vi om bilden finns på servern, och i så fall raderar vi den lokalt.
                                            NSURL *signatureCheckUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/_inc/signatureImageExists.asp?format=json&filename=%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"siteurl"],fileName]];
                                            
                                            NSURLSession *sessionCheck = [NSURLSession sharedSession];
                                            NSURLSessionDataTask *task = [sessionCheck dataTaskWithURL:signatureCheckUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

                                                if (data != nil){
                                                    NSError *parseError = nil;
                                                    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                                                    NSLog(@"%@",[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]);
                                                    if (parseError) {
                                                        NSLog(@"Admin data: %@",parseError);
                                                        //Kunde inte kolla om filen laddades upp.
                                                        NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                                    }
                                                    else{
                                                        bool _isExists = [responseDictionary valueForKey:@"exists"];

                                                        NSLog(@"Response Signature check = %@",data);
    //                                                    if([responseString isEqualToString:@"TRUE"]){
                                                        if(_isExists == true){
                                                            NSLog(@"Bilden existerar på servern. Uppladdningen är komplett och färdig. Raderar bilden på iPaden.");
    
                                                            [fileMgr removeItemAtPath: (jpgPath) error:NULL];
                                                            NSLog(@"Raderade filen.");
    
                                                            //NSString *alertString = [NSString stringWithFormat:@"Fil skickad!"];
                                                            //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:(@"Filen laddades upp och servern bekräftade att den kom fram.") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                            //[alert show];
    
                                                        }
                                                    }
                                                }else{
                                                    //Kunde inte kolla om filen laddades upp.
                                                    NSLog(@"//Kunde inte kolla om filen laddades upp.");
                                                }
                                            }];

                                            // Begin task.
                                            [task resume];
                                            
                                        }];
                                        [signatureUploadRequest setFailedBlock:^{
                                            NSError *error = [signatureUploadRequest error];
                                            /*VIKTIG*/NSLog(@"Error när filen som innehåller signaturen skulle skickas: %@", error.localizedDescription);
                                            
                                            NSString *alertString = [NSString stringWithFormat:@"Ett fel uppstod, försök igen senare."];
                                            NSString *alertString2 = [NSString stringWithFormat:@"Ett fel uppstod när signaturen skulle skickas. Det kan till exempel bero på dålig uppkoppling/täckning. Försök igen senare. Felmeddelande: %@", error.localizedDescription];
//                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:alertString2 delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                                [alert show];
                                            
                                            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertString
                                                                                                                     message:alertString2
                                                                                                                     preferredStyle:UIAlertControllerStyleAlert];

                                            UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                                                                   style:UIAlertActionStyleDefault
                                                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                                                        [alertController dismissViewControllerAnimated:YES completion:nil];
                                                                                                    }];
                                                
                                            [alertController addAction:actionCancel];
                                            [self presentViewController:alertController animated:YES completion:nil];
                                            
                                        }];
                                        NSLog(@"Skickar fil... Filnamn: %@",fileName);
                                        
                                        //NSString *alertString = [NSString stringWithFormat:@"Skickar fil... Filnamn: %@",fileName];
                                        //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                        //[alert show];
                                        
                                        [signatureUploadRequest startAsynchronous];
                                    }
                                }
                            }

                            
                        }else{
                            /*DOLD*/NSLog(@"Valstr är nil, går vidare till nästa fält/kolumn");
                        }
                        
                    }
                    
                }else{
                    NSLog(@"[colnamesArr objectAtIndex:j] var nil");
                }
                
                NSLog(@"%@", postParam);
                NSString *newParameters = [NSString stringWithFormat:@"%@%@", dbstr, postParam];
                NSLog(@"Appended string = %@", newParameters);

                [webservice executequery:url strpremeter:newParameters withblock:^(NSData * dbdata, NSError *error) {
                    NSLog(@"Data: %@", dbdata);
                    if(error){
                        /*VIKTIG*/NSLog(@"Error när post %d av %lu skulle skickas: %@",  j+1, (unsigned long)[poststosendArr count], error.localizedDescription);
                        self->httpRequestIsRunning = false;
                        [self postWasCompleted:false];
                    }
                    if (dbdata!=nil)
                    {
                        NSDictionary *maindic = [NSJSONSerialization JSONObjectWithData:dbdata options:NSJSONReadingAllowFragments error:nil];
                        NSLog(@"Response Data: %@", maindic);
//                            NSString *responseString = [[NSString alloc] initWithData:[dbdata responseData] encoding:NSUTF8StringEncoding];
                        /*VIKTIG*/NSLog(@"Post %d av %lu skickad! Response: %@", j+1, (unsigned long)[poststosendArr count], dbdata);
                        if(j+1 == [poststosendArr count]){
                            //Alla poster har skickats
                            /*VIKTIG*/NSLog(@"Alla poster har skickats!");
                            self->httpRequestIsRunning = false;
                            [self removePostFromSendList:rowNumberToSend];
                            [self postWasCompleted:true];
                        }
                    }else{
                        /*VIKTIG*/NSLog(@"Error när post %d av %lu skulle skickas: %@",  j+1, (unsigned long)[poststosendArr count], error.localizedDescription);
                        self->httpRequestIsRunning = false;
                        [self postWasCompleted:false];
                    }
                }];
                                    
            }
        }
        
    }else{
        //Om uppladdnings-kön är tom, kör fetchData och uppdatera databasen.
        /*VIKTIG*/NSLog(@"Inga rapporter att skicka.");
        httpRequestIsRunning = false;
        [self removePostFromSendList:rowNumberToSend];
    }
}
/*
else{
    [self removePostFromSendList:rowNumberToSend];
    [self postWasCompleted];
}
*/
}

-(void)removePostFromSendList:(int)rowNumberToSend{
//Här kan man lägga in nån typ av validerings-check, men det blir ett senare projekt

NSLog(@"Skickad rad ska bort:");
NSLog(@"%@",[[[self getListOfPostsToSend] objectAtIndex:rowNumberToSend] valueForKey:@"id"]);


NSString * reporttemplate = [[[self getListOfPostsToSend] objectAtIndex:rowNumberToSend] valueForKey:@"reporttemplate"];
NSString * workorderid = [[[self getListOfPostsToSend] objectAtIndex:rowNumberToSend] valueForKey:@"workorderid"];

//Radera alla poster med samma template och workorder - det kan finnas dubletter
NSString * sql = [NSString stringWithFormat:@"DELETE FROM sys_poststosend WHERE reporttemplate = %@ AND workorderid = %@", reporttemplate, workorderid];
NSLog(@"%@",sql);
[self runQuery:sql];

NSLog(@"_sendReportsTableContent: \n%@", _sendReportsTableContent);
NSLog(@"Tömmer _sendReportsTableContent...");
[_sendReportsTableContent removeAllObjects];
NSLog(@"_sendReportsTableContent: \n%@", _sendReportsTableContent);
[_tableView reloadData];

}
-(void)postWasCompleted:(bool)success{
//Dölj alertview och meddela användaren att rapporten skickats
NSLog(@"Döljer sendprogressAlert");
//    [sendReportInProgress dismissWithClickedButtonIndex:0 animated:true];
[sendReportInProgress dismissViewControllerAnimated:true completion:nil];
NSString * alertHeader;
NSString * alertText;
//    UIAlertView * alert;
UIAlertController * alert;

if(success){
    alertHeader = @"Uppladdningen slutfördes";
    alertText = @"Rapporten har laddats upp till servern. Vill du se den? (Öppnas i en webbläsare)";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Nej" otherButtonTitles:@"Visa rapporten", nil];
    alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Nej"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                            }];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Visa rapporten"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
        [self alertButtonClicked:@"Visa rapporten"];
                                                            }];
        
    [alert addAction:actionCancel];
    [alert addAction:actionOk];
    
}else{
    alertHeader = @"Uppladdningen misslyckades";
    alertText = @"Rapporten kunde inte laddas upp till servern. Det kan bero på en tillfällig störning - försök igen om en liten stund.";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Stäng" otherButtonTitles:nil];
    alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Stäng"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                            }];
    [alert addAction:actionCancel];
}
NSLog(@"Visar alert från postWasCompleted");
//    [alert show];
[self presentViewController:alert animated:YES completion:nil];
}

-(void)imageUploadWasCompleted:(bool)success{
//Dölj alertview och meddela användaren att rapporten skickats
NSLog(@"Döljer sendprogressAlert");
//    [sendReportInProgress dismissWithClickedButtonIndex:0 animated:true];
[sendReportInProgress dismissViewControllerAnimated:true completion:nil];
NSString * alertHeader;
NSString * alertText;
//    UIAlertView * alert;
UIAlertController *alert;

if(success){
    alertHeader = @"Uppladdningen slutfördes";
    alertText = @"Bilderna har laddats upp till servern.";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Stäng" otherButtonTitles:nil];
    alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"OK"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                            }];
        
    [alert addAction:actionCancel];
}else{
    alertHeader = @"Uppladdningen misslyckades";
    alertText = @"En eller flera av bilderna kunde inte laddas upp till servern. Det kan bero på en tillfällig störning - försök igen om en liten stund.";
//        alert = [[UIAlertView alloc] initWithTitle:alertHeader message:alertText delegate:self cancelButtonTitle:@"Stäng" otherButtonTitles:nil];
    alert = [UIAlertController alertControllerWithTitle:alertHeader message:alertText preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Stäng"
                                                           style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                            }];
        
    [alert addAction:actionCancel];
}
NSLog(@"Visar alert från imageUploadWasCompleted");
//    [alert show];
[self presentViewController:alert animated:YES completion:nil];
}



@end

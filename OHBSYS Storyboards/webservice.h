//
//  webservice.h
//  jsondemo post
//
//  Created by Yogesh Patel on 08/11/17.
//  Copyright © 2017 Yogesh Patel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface webservice : NSObject
//+ instance Globally -"ViewController me used "
//+ second Web View
//argumentname type objectname
+(void)executequery:(NSString *)strurl strpremeter:(NSString *)premeter withblock:(void(^)(NSData *, NSError*))block;
+(void)executeUploadquery:(NSString *)strurl strpremeter:(NSString *)premeter formData:(NSData *)formdata withblock:(void (^)(NSData *, NSError *))block;

@end

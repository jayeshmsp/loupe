//
//  syncdb.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-04.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "syncdb.h"

@implementation syncdb

@synthesize myFetchedData, resultat, tablesAndChecks, tablesArr, checksArr, tablesToRequest,receivedData, receivedDataString, SQL, sqlStatementsArr;

#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Connection-metoderna startar");
    
    receivedData = [[NSMutableData alloc] init];
    receivedDataString = [[NSMutableString alloc] init];
    
    if ([response expectedContentLength] < 0)
    {
        NSLog(@"Connection error pga nåt med att längd < 0");
        //here cancel your connection.
        //[connection cancel];
        //return;
    }
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    //[receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"Connectionen tog emot data");
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:(data)];
    NSLog(@"connection tog emot data!");
    
    NSString * thisReceivedData = [[NSString alloc] init];
    thisReceivedData = [NSString stringWithUTF8String:[data bytes]];
    [receivedDataString appendString:(thisReceivedData)];
    
    NSLog(@".");
    NSLog(@"receivedData innehåller %d tecken",[receivedData length]);
    NSLog(@"thisReceivedData innehåller %d tecken",[thisReceivedData length]);
    NSLog(@"receivedDataString innehåller %d tecken",[receivedDataString length]);
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    NSLog(@"Connection failade med error");
    // release the connection, and the data object
    connection = nil;
    // receivedData is declared as a method instance elsewhere
    receivedData = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@",
          [error localizedDescription]
          //    ,[[error userInfo] objectForKey:NSErrorFailingURLStringKey]
          );
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"connection did finish loading");
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    
    //Uppdatera databasen
    if([receivedDataString length]>0){
        sqlite3_stmt *statement;
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            NSLog(@"Databasen öppnad");
            NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Kommando utfört: %d", [beginSQL length]);
            } else {
                NSLog(@"Kommando misslyckades: %@; Felkod: %d", beginSQL, sqlite3_errcode(contactDB));
            }
            
            
            NSString * thisSQLstring;
            NSString *insertSQL;
            const char *insert_stmt;
            
            NSString * tempSqlTest = @"INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn1\", \"adr1\", \"tel1\");INSERT INTO CONTACTS (name, address, phone) VALUES (\"namn2\", \"adr2\", \"tel2\")";
            NSLog(@"tempSqlTest: %@", tempSqlTest);
            
            sqlStatementsArr = [receivedDataString componentsSeparatedByString: @";"];
            NSLog(@".");
            NSLog(@"Antal statements att köra: %d", [sqlStatementsArr count]);
            NSLog(@".");
            NSLog(@"Mottagen data: %d tecken", [receivedDataString length]);
            NSLog(@". [sqlStatementsArr count]");
            
            for(i=0;i<[sqlStatementsArr count];i++){
                //NSLog(@".");
                //NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([sqlStatementsArr count]), [sqlStatementsArr objectAtIndex:i]);
                
                thisSQLstring = nil;
                insertSQL = [NSString stringWithFormat: @"%@", [sqlStatementsArr objectAtIndex:i]];
                insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(contactDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    NSLog(@"Kommando utfört: %d", [insertSQL length]);
                } else {
                    NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d", insertSQL, sqlite3_errmsg(statement), sqlite3_errcode(contactDB));
                }
            }
            
            sqlStatementsArr = nil;
            
            
            
            NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
            const char *commit_stmt = [commitSQL UTF8String];
            sqlite3_prepare_v2(contactDB, commit_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Kommando utfört: %@", commitSQL);
            } else {
                NSLog(@"Kommando misslyckades: %@; Felkod: %d", commitSQL, sqlite3_errcode(contactDB));
            }
            
            NSLog(@"Finalize: %d", sqlite3_finalize(statement));
            sqlite3_close(contactDB);
        }
    }
    
    //loopa igenom databasen i loggen
    
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        NSLog(@"Databasen öppnad");
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM prot_arbetsorder_desc"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *col1 = (char *)sqlite3_column_text(statement, 0);
            NSString * col1str = nil;
            if (col1 !=NULL)
                col1str = [NSString stringWithUTF8String: col1];
            
            char *col2 = (char *)sqlite3_column_text(statement, 1);
            NSString * col2str = nil;
            if (col2 !=NULL)
                col2str = [NSString stringWithUTF8String: col2];
            
            char *col3 = (char *)sqlite3_column_text(statement, 2);
            NSString * col3str = nil;
            if (col3 !=NULL)
                col3str = [NSString stringWithUTF8String: col3];
            
            NSLog(@"ID: %@ sortID: %@ Title: %@",col1str, col2str, col3str);
            
			
        }
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Kommando utfört: %d", [beginSQL length]);
        } else {
            NSLog(@"Kommando: %@; Felkod: %d", beginSQL, sqlite3_errcode(contactDB));
        }
        
        NSLog(@"%d", sqlite3_finalize(statement));
        sqlite3_close(contactDB);
    }
    
    
    NSLog(@"Succeeded! Received %d bytes of data",[receivedData length]);
    
    
    // release the connection, and the data object
    connection = nil;
    receivedData = nil;
    NSLog(@"Nollställer receivedData");
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    
    NSLog(@"Nollställer variabler...");
    
    myFetchedData = nil;
    resultat = nil;
    tablesAndChecks = nil;
    tablesArr = nil;
    checksArr = nil;
    tablesToRequest = nil;
    receivedData = nil;
    receivedDataString = nil;
    
    SQL = nil;
    sqlStatementsArr = nil;
}






- (void)fetchData
{
    
    NSLog(@"Start!");
    
    
    NSLog(@"Hämtar data...");
    NSString *url = [NSString stringWithFormat:@"http://ohbsys.se/_remote/get_table_checksums.asp"];
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:url ]];
    NSString *serverOutput= [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
    myFetchedData = serverOutput;
    NSLog(@"Data: %d", [myFetchedData length]);
    NSLog(@"Slut på data.");
    
    
    tablesAndChecks = [myFetchedData componentsSeparatedByString: @"&"];
    tablesArr = [[tablesAndChecks objectAtIndex:0] componentsSeparatedByString: @","];
    checksArr = [[tablesAndChecks objectAtIndex:1] componentsSeparatedByString: @","];
    
    resultat = [[NSMutableString alloc] initWithCapacity:0];
    
    for(i=0;i<[tablesArr count];i++){
        [resultat appendString:([tablesArr objectAtIndex:i])];
        [resultat appendString:(@" = ")];
        [resultat appendString:([checksArr objectAtIndex:i])];
        [resultat appendString:(@"; \n")];
    }
    NSLog(@"RESULTATET ÄR: %d, tablesArr är %d, checksArr är %d", [resultat length], [tablesArr count], [checksArr count]);
    
    NSMutableArray *dbTablesArr = [[NSMutableArray alloc] init];
    //[NSArray arrayWithObjects: @"prot_arbetsorder_desc", @"prot_arbetsorder", @"falskafyran", nil];
    NSMutableArray *dbChecksArr = [[NSMutableArray alloc] init];
    //[NSArray arrayWithObjects: @"100", @"200", @"400", nil];
    
    //Loopa igenom databasens tabell meta_tablechecksums och fyll i arrayerna dbTablesArr och dbChecksArr
    sqlite3_stmt *statement;
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
    {
        NSLog(@"Databasen öppnad");
        NSString *beginSQL = [NSString stringWithFormat: @"SELECT * FROM meta_tablechecksums"];
        const char *begin_stmt = [beginSQL UTF8String];
        sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *col1 = (char *)sqlite3_column_text(statement, 1);
            NSString * col1str = nil;
            if (col1 !=NULL)
                col1str = [NSString stringWithUTF8String: col1];
            
            char *col2 = (char *)sqlite3_column_text(statement, 2);
            NSString * col2str = nil;
            if (col2 !=NULL)
                col2str = [NSString stringWithUTF8String: col2];
            
            [dbTablesArr addObject:(@"%@", col1str)];
            [dbChecksArr addObject:(@"%@", col2str)];
            
            NSLog(@"Tabellnamn: %@ Checksum: %@",col1str, col2str);
            
			
        }
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Kommando utfört: %d", [beginSQL length]);
        } else {
            NSLog(@"Kommando: %@; Felkod: %d", beginSQL, sqlite3_errcode(contactDB));
        }
        
        NSLog(@"%d", sqlite3_finalize(statement));
        sqlite3_close(contactDB);
    }
    
    
    
    
    NSMutableArray *tablesToDropArr = [[NSMutableArray alloc] init];
    NSMutableArray *tablesToAddArr = [[NSMutableArray alloc] init];
    
    //checksum-tabellen droppas alltid för att sedan skapas på nytt
    [tablesToDropArr addObject:@"meta_tablechecksums"];
    
    for(i=0;i<[tablesArr count];i++){
        NSUInteger indexOfListAinListB = [dbTablesArr indexOfObject: ([tablesArr objectAtIndex: i])]; //finns tabellen lokalt?
        NSLog(@"indexOfListAinListB: %d", indexOfListAinListB);
        NSString *checksumA = [checksArr objectAtIndex: i]; //Vad är senaste checksum?
        NSString *checksumB = nil;
        if(indexOfListAinListB>[dbTablesArr count]){
            NSLog(@"index för stort");
            NSLog(@"finns inte i lista B. Lägg till %@ i tablesToAddArr",[tablesArr objectAtIndex:i]);
            //finns inte i lista B. Lägg till i tablesToAddArr
            [tablesToAddArr addObject:[tablesArr objectAtIndex:i]];
        }else{
            checksumB = [dbChecksArr objectAtIndex: indexOfListAinListB];
            NSLog(@"checksumA: %@ - checksumB: %@", checksumA, checksumB);
            if([checksumA isEqualToString:checksumB]){
                //checksummorna är lika, tabellen behöver inte uppdateras
                NSLog(@"Checksummorna är lika");
            }else{
                //checksummorna är OLIKA, lägg till tabellnamnet i både tablesToDropArr och tablesToAddArr
                NSLog(@"checksummorna är OLIKA, lägg till tabellnamnet '%@' i både tablesToDropArr och tablesToAddArr", [tablesArr objectAtIndex:i]);
                
                [tablesToDropArr addObject:[tablesArr objectAtIndex:i]];
                [tablesToAddArr addObject:[tablesArr objectAtIndex:i]];
                
            }
        }
    }
    //Kolla om det finns någon tabell i lista B som inte finns i lista A (ta bort tabeller som finns lokalt med ej på servern)
    for(i=0;i<[dbTablesArr count];i++){
        NSUInteger indexOfListBinListA = [tablesArr indexOfObject: ([dbTablesArr objectAtIndex: i])]; //finns tabellen lokalt?
        if(indexOfListBinListA>[tablesArr count]){
            NSLog(@"%@ finns i lista B men inte i lista A. Ta bort den lokalt.",[dbTablesArr objectAtIndex:i]);
            
            //finns inte i lista B. Lägg till i tablesToAddArr
            [tablesToDropArr addObject:[dbTablesArr objectAtIndex:i]];
        }
    }
    
    //JÄMFÖRELSE KLAR. UPPDATERA DATABASEN:
    
    NSLog(@"Drop tables: %@", tablesToDropArr);
    
    
    
    //DROP TABLES
    //Om den enda tabellen som ska droppas är meta_checksums (dvs om [tablestodrop count]==1), droppa inget! Detta händer om lokala databasen redan är helt uppdaterad.
    if([tablesToDropArr count]>1){
        sqlite3_stmt *statement;
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            NSLog(@"Databasen öppnad för att DROPPA TABELLER");
            NSString *beginSQL = [NSString stringWithFormat: @"BEGIN"];
            const char *begin_stmt = [beginSQL UTF8String];
            sqlite3_prepare_v2(contactDB, begin_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Kommando utfört: %d", [beginSQL length]);
            } else {
                NSLog(@"Kommando misslyckades: %@; Felkod: %d", beginSQL, sqlite3_errcode(contactDB));
            }
            
            NSString * thisSQLstring;
            NSString *insertSQL;
            const char *insert_stmt;
            
            sqlStatementsArr = tablesToDropArr;
            NSLog(@".");
            NSLog(@"Antal statements att köra: %d", [sqlStatementsArr count]);
            NSLog(@".");
            NSLog(@"Mottagen data: %d tecken", [receivedDataString length]);
            NSLog(@". [sqlStatementsArr count]");
            
            for(i=0;i<[sqlStatementsArr count];i++){
                //NSLog(@".");
                //NSLog(@"Loop %d av %d: \n\n\n %@ \n\n", i, ([sqlStatementsArr count]), [sqlStatementsArr objectAtIndex:i]);
                
                thisSQLstring = nil;
                insertSQL = [NSString stringWithFormat: @"DROP TABLE IF EXISTS %@", [sqlStatementsArr objectAtIndex:i]];
                insert_stmt = [insertSQL UTF8String];
                sqlite3_prepare_v2(contactDB, insert_stmt, -1, &statement, NULL);
                if (sqlite3_step(statement) == SQLITE_DONE)
                {
                    NSLog(@"Kommando utfört: %d", [insertSQL length]);
                } else {
                    NSLog(@"\n\nKommando: \n%@ \n\nSQL-svar: %s - Felkod: %d\n\n", insertSQL, sqlite3_errmsg(statement), sqlite3_errcode(contactDB));
                }
            }
            
            sqlStatementsArr = nil;
            
            NSString *commitSQL = [NSString stringWithFormat: @"COMMIT"];
            const char *commit_stmt = [commitSQL UTF8String];
            sqlite3_prepare_v2(contactDB, commit_stmt, -1, &statement, NULL);
            if (sqlite3_step(statement) == SQLITE_DONE)
            {
                NSLog(@"Kommando utfört: %@", commitSQL);
            } else {
                NSLog(@"Kommando misslyckades: %@; Felkod: %d", commitSQL, sqlite3_errcode(contactDB));
            }
            NSLog(@"Finalize: %d", sqlite3_finalize(statement));
            sqlite3_close(contactDB);
        }
    }
    
    
    NSLog(@"Add tables: %d", [tablesToAddArr count]);
    
    //Gör en array för att fråga servern om sql-script som skapar och fyller de tabeller som saknas på iPaden
    tablesToRequest = [[NSMutableString alloc] initWithCapacity:0];
    for(i=0;i<[tablesToAddArr count];i++){
        [tablesToRequest appendString:([tablesToAddArr objectAtIndex:i])];
        [tablesToRequest appendString:(@",")];
    }
    NSLog(@"tablesToRequest: %d", [tablesToRequest length]);
    
    //Hämta data från servern via http POST (om det finns något att hämta, dvs om tablesToRequest > 0
    if([tablesToRequest length]>0){
        NSString * myURL = @"http://ohbsys.se/_remote/get_table_insertstring_sqlite.asp";
        
        NSLog(@"content-type satt");
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]
                                        initWithURL:[NSURL
                                                     URLWithString: myURL]];
        
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        NSLog(@"requestedTables=%@",tablesToRequest);
        NSString * tableString = [NSString stringWithFormat:@"requestedTables=%@",tablesToRequest];
        
        [request setValue:[NSString stringWithFormat:@"%d",
                           [tableString length]]
       forHTTPHeaderField:@"Content-length"];
        
        [request setHTTPBody:[tableString
                              dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
        
        NSLog(@"Request-string: %d", [tableString length]);
        NSURLConnection *theConnection=[[NSURLConnection alloc] initWithRequest:request delegate:self];
        if (theConnection) {
            //receivedData = [NSMutableData data];
            NSLog(@"ReceivedData är: %d", [receivedData length]);
        } else {
            NSLog(@"Connection Failed!");
        }
        //Allt klart här! När requesten är färdig uppdateras tabellen.
    }else{
        NSLog(@"############################### \n############################### \nDatabasen är redan up-to-date!\n###############################\n###############################");
        
        NSString *alertString = [NSString stringWithFormat:@"Databasen är redan up-to-date!"];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
        [alert show];
        
    }
}

- (void)viewDidLoad
{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"contacts.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    /*    if ([filemgr fileExistsAtPath: databasePath ] == YES)
     [filemgr removeItemAtPath: (databasePath) error:NULL];
     */
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
		const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS CONTACTS (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, ADDRESS TEXT, PHONE TEXT)";
            NSLog(@"Skapar databas (alltså contact-tabellen).");
            if (sqlite3_exec(contactDB, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                NSLog(@"Failed to create table");
            }
            
            sqlite3_close(contactDB);
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
}


@end

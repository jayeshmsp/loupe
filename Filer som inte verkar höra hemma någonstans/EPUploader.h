//
//  EPUploader.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-28.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EPUploader : NSObject {
    NSURL *serverURL;
    NSString *filePath;
    id delegate;
    SEL doneSelector;
    SEL errorSelector;
    
    BOOL uploadDidSucceed;
}

-   (id)initWithURL: (NSURL *)serverURL
           filePath: (NSString *)filePath
           delegate: (id)delegate
       doneSelector: (SEL)doneSelector
      errorSelector: (SEL)errorSelector;

-   (NSString *)filePath;

@end
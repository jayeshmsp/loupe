//
//  syncdb.h
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-04.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface syncdb : NSObject{
    
    NSString *databasePath;
    sqlite3 *contactDB;
    
    int i;
    int myCounter;
    int tablesArrCountInt;
    
    NSString *myFetchedData;
    NSMutableString *resultat;
    NSArray *tablesAndChecks;
    NSArray *tablesArr;
    NSArray *checksArr;
    NSMutableString *tablesToRequest;
    NSMutableData *receivedData;
    NSMutableString * receivedDataString;
    
    NSMutableString * SQL;
    NSArray * sqlStatementsArr;
}

- (IBAction) fetchData;
@property (nonatomic, retain) NSString *myFetchedData;
@property (nonatomic, retain) NSMutableString *resultat;
@property (nonatomic, retain) NSArray *tablesAndChecks;
@property (nonatomic, retain) NSArray *tablesArr;
@property (nonatomic, retain) NSArray *checksArr;
@property (nonatomic, retain) NSMutableString *tablesToRequest;
@property (nonatomic, retain) NSMutableData *receivedData;
@property (nonatomic, retain) NSMutableString * receivedDataString;

@property (nonatomic, retain) NSMutableString * SQL;
@property (nonatomic, retain) NSArray * sqlStatementsArr;

@end

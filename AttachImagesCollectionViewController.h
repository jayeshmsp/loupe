//
//  AttachImagesCollectionViewController.h
//  OHBSYS Storyboards iOS8
//
//  Created by David Forsberg on 2015-09-27.
//  Copyright (c) 2015 David Forsberg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AttachImagesCollectionViewController : UICollectionViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@property (retain, nonatomic) AppDelegate * appDelegate;
@property (retain, nonatomic) NSString * fieldName;
@property (retain, nonatomic) NSString * fieldValue;
@property (retain, nonatomic) NSNumber * editMode;
@property (retain, nonatomic) NSIndexPath * lastIndex;

- (IBAction) addImage;
- (IBAction) addImageByCamera;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *imgPickerBtn;
@property (weak, nonatomic) IBOutlet UIButton *cameraBtn;

@end


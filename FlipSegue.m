
//
//  FlipSegue.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-10-23.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "FlipSegue.h"

@implementation FlipSegue

- (void) perform {
    NSLog(@"kör segue");
    UIViewController *src = (UIViewController *) self.sourceViewController;
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    
    [UIView transitionWithView:src.navigationController.view duration:1.2
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        [src.navigationController pushViewController:dst animated:NO];
                    }
                    completion:NULL];
    
}

@end


//
//  AppDelegate.m
//  OHBSYS Storyboards
//
//  Created by David Forsberg on 2012-09-25.
//  Copyright (c) 2012 David Forsberg. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize receivedData, jsonHolder;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    //Skriv loggen (alla NSLog-kommandon) till en fil.
    //Om appen kraschar, byts filnamnet ut till crash.log. Om en fil med det namnet finns när appen startar, så frågas användaren om den ska mailas för felsökning. Efter det raderas den, så att inte samma rapport skickas flera gånger.
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    
    NSString *aLogName = [NSString stringWithFormat:@"log_%f-.log",[[NSDate date] timeIntervalSince1970]];
    NSString *archivedLogPath = [documentsDirectory stringByAppendingPathComponent:aLogName];
    
    //Om en crash.log existerar, maila den vid nästa db-synk.
    
    //Om en gammal loggfil existerar, radera den
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: logPath ] == YES)
    {
        [filemgr moveItemAtPath:logPath toPath:archivedLogPath error:NULL];
        NSLog(@"Döpte om loggfilen till %@",archivedLogPath);
        
        /*
         [filemgr removeItemAtPath: (logPath) error:NULL];
        NSLog(@"Raderade den gamla loggfilen.");
         */
    }
    
    _activity=[[UIActivityIndicatorView alloc]init];
    
    //###########################################
    //###########################################
    //###########################################
    //###########################################
    // AVKOMMENTERA RADEN "freopen" FÖR ATT SKRIVA LOGGEN TILL FIL
    //###########################################
    //###########################################
    //###########################################
    
    //Öppna en ny loggfil för append (a+)
    //freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);

    // Override point for customization after application launch.
    return YES;
}

//MARK: - Methods to show and hide activiy indicator
- (void)showActivity
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->_window addSubview:self->_activity];
        [self->_activity startAnimating];
    });

}

- (void)hideActivity
{
    dispatch_async(dispatch_get_main_queue(), ^{
        //also remove activity from window
        [self->_activity stopAnimating];
    });
}
//--

// Internal error reporting
void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);

    //Om appen kraschar, döp om loggfilen till crash.log
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *logPath = [documentsDirectory stringByAppendingPathComponent:@"console.log"];
    NSString *crashLogPath = [documentsDirectory stringByAppendingPathComponent:@"crash.log"];
    NSFileManager *filemgr = [NSFileManager defaultManager];
    /*
     if ([filemgr fileExistsAtPath: crashLogPath ] == YES)
    {
        [filemgr removeItemAtPath: (crashLogPath) error:NULL];
        NSLog(@"Raderade den gamla kraschloggfilen.");
    }*/
    
    if ([filemgr fileExistsAtPath: logPath ] == YES)
    {
        [filemgr moveItemAtPath:logPath toPath:crashLogPath error:NULL];
        NSLog(@"Bytte namn på loggfilen till crash.log, för att upptäckas nästa gång appen körs.");
    }
}
                            
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
//    dispatch_async(dispatch_get_main_queue(), ^{
//        self->_user = nil;
//        self->_userId = nil;
//        self->_userRole = nil;
//        self->_userDictionary = nil;
//    });
//    sqlite3_close(self->_openDbConnection);
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark NSURLConnection Delegate Methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Connection-metoderna startar");
    if ([response expectedContentLength] < 0)
    {
        NSLog(@"Connection error");
        //here cancel your connection.
        [connection cancel];
        return;
    }
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    // receivedData is an instance variable declared elsewhere.
    //[receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSLog(@"Connectionen tog emot data");
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [receivedData appendData:data];
    NSLog(@"connection tog emot data!");
    NSLog(@"%@",data);
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    NSLog(@"Connection failade med error");
    // release the connection, and the data object
    connection = nil;
    // receivedData is declared as a method instance elsewhere
    receivedData = nil;
    
    // inform the user
    NSLog(@"Connection failed! Error - %@",
          [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"connection did finish loading");
    // do something with the data
    // receivedData is declared as a method instance elsewhere
    NSLog(@"Succeeded! Received %lu bytes of data",(unsigned long)[receivedData length]);
    
    // release the connection, and the data object
    connection = nil;
    receivedData = nil;
    NSLog(@"Nollställer receivedData");
}


@end
